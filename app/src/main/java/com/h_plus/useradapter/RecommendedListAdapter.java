package com.h_plus.useradapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.h_plus.R;

import org.json.JSONObject;

import java.util.LinkedList;

/**
 * Created by Muzammil on 7/29/2016.
 */
public class RecommendedListAdapter extends BaseAdapter {
    Context mContext;
    LinkedList<JSONObject> list;

    public RecommendedListAdapter(Context appContext, LinkedList<JSONObject> list) {
        this.mContext = appContext;
        this.list = list;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater li = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView == null) {
            convertView = li.inflate(R.layout.recommended_list_item_tab_layout, null);
        }
        try {
            ((TextView) convertView.findViewById(R.id.project_name_on_recommended)).setText(list.get(position).getString(""));
            ((TextView) convertView.findViewById(R.id.discription_review_item)).setText(list.get(position).getString(""));
            ((TextView) convertView.findViewById(R.id.textView15)).setText(list.get(position).getString(""));
            ((TextView) convertView.findViewById(R.id.mint_count)).setText(list.get(position).getString(""));
            ((TextView) convertView.findViewById(R.id.bids_count_recommended)).setText(list.get(position).getString(""));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return convertView;
    }
}
