package com.h_plus.useradapter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import com.h_plus.HP_Service_Provider.SendMilestone_Activity;
import com.h_plus.R;
import com.h_plus.customwidget.CustomButton;
import com.h_plus.customwidget.CustomTextView;
import com.h_plus.useractivity.ChattingScreen_Activity;
import com.h_plus.usertabs.utility.Constants;
import com.h_plus.usertabs.utility.Utility;

import org.json.JSONObject;

import java.util.LinkedList;

/**
 * Created by Infograins on 12/29/2016.
 */

public class Adapter_MyWorkingProjectList extends BaseAdapter {
    Context mContext;
    int res;
    LinkedList<JSONObject> mlist;
    int[] img = {
            R.drawable.purple_patti,
            R.drawable.red_patti,
            R.drawable.blue_patti};

    public Adapter_MyWorkingProjectList(Context context, int res, LinkedList<JSONObject> list) {
        this.mContext = context;
        this.res = res;
        this.mlist = list;

    }

    @Override
    public int getCount() {
        return mlist.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        final ViewHolder viewHolder;

        if (convertView == null) {
            LayoutInflater li = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = li.inflate(res, null);
            viewHolder = new ViewHolder(convertView);

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        JSONObject j = mlist.get(position);
        try {
            if (Utility.getSharedPreferences(mContext, Constants.USER_TYPE).equals("1")) {
                viewHolder.sp_uname.setText(j.getString("name"));
            } else {
                viewHolder.sp_uname.setText(j.getString("client_name"));
            }
            viewHolder.sp_proname.setText(j.getString("project_name"));
            viewHolder.sp_desc.setText(j.getString("description"));
            viewHolder.sp_budget.setText("$" + (j.getString("total_bidding_amount")));
            viewHolder.bidder_completion_days.setText("In " + (j.getString("time_duration") + " Days"));
            if (j.getString("status").equals("awared")) {
                viewHolder.sp_acceptreject_layout.setVisibility(View.GONE);
                viewHolder.sp_wp_project_detail_btn.setVisibility(View.VISIBLE);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        //viewHolder.hire_worker.setOnClickListener(AcceptBidRequest(position));
        viewHolder.sp_wp_project_detail_btn.setOnClickListener(new SP_ProjectDetail(position));
        viewHolder.do_chat_btn.setOnClickListener(new ChatScreen(position));
        ((RelativeLayout) convertView.findViewById(R.id.relatv_layout)).setBackgroundResource(img[position % 3]);
        return convertView;
    }

    public class SP_ProjectDetail implements View.OnClickListener {
        int pos;

        public SP_ProjectDetail(int pos) {
            this.pos = pos;
        }

        @Override
        public void onClick(View v) {
            try {

                Utility.setSharedPreference(mContext, Constants.PROJECT_ID, mlist.get(pos).getString("project_id"));
                mContext.startActivity(new Intent(mContext, SendMilestone_Activity.class));
            } catch (Exception e) {
                e.printStackTrace();
            }
            // Utility.setSharedPreference(mContext,list.get(pos));
        }
    }

    public class ChatScreen implements View.OnClickListener {
        int pos;

        public ChatScreen(int pos) {
            this.pos = pos;
        }

        @Override
        public void onClick(View v) {
            Bundle b = new Bundle();
            try {
                Utility.setSharedPreference(mContext, Constants.CHECKCHATBOX_REDIRECT, "1");
                Intent i = new Intent(mContext, ChattingScreen_Activity.class);
                b.putString("PROJECTID", mlist.get(pos).getString("project_id"));
                if (Utility.getSharedPreferences(mContext, Constants.USER_TYPE).equals("1")) {
                    b.putString("CLIENTID", mlist.get(pos).getString("to_user"));
                    b.putString("CLIENTNAME", mlist.get(pos).getString("name"));
                } else if (Utility.getSharedPreferences(mContext, Constants.USER_TYPE).equals("2")) {
                    b.putString("CLIENTID", mlist.get(pos).getString("client_id"));
                    b.putString("CLIENTNAME", mlist.get(pos).getString("client_name"));
                }

                i.putExtras(b);
                mContext.startActivity(i);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public class ViewHolder {

        CustomTextView sp_uname, sp_proname, sp_desc, bidder_project_completion_rate, sp_budget, bidder_completion_days;
        com.h_plus.customwidget.CircleImageView hire_image;
        CustomButton sp_wp_project_detail_btn, do_chat_btn;
        LinearLayout sp_acceptreject_layout;
        ImageView flag;
        ProgressBar adapter_pb;

        public ViewHolder(View base) {

            sp_uname = (CustomTextView) base.findViewById(R.id.sp_uname);
            sp_proname = (CustomTextView) base.findViewById(R.id.sp_proname);
            sp_desc = (CustomTextView) base.findViewById(R.id.sp_desc);
            bidder_project_completion_rate = (CustomTextView) base.findViewById(R.id.bidder_project_completion_rate);
            sp_budget = (CustomTextView) base.findViewById(R.id.sp_budget);
            bidder_completion_days = (CustomTextView) base.findViewById(R.id.bidder_completion_days);
            sp_wp_project_detail_btn = (CustomButton) base.findViewById(R.id.sp_wp_project_detail_btn);
            do_chat_btn = (CustomButton) base.findViewById(R.id.do_chat_btn);
            sp_acceptreject_layout = (LinearLayout) base.findViewById(R.id.sp_acceptreject_layout);

            hire_image = (com.h_plus.customwidget.CircleImageView) base.findViewById(R.id.hire_image);
            flag = (ImageView) base.findViewById(R.id.flag);
            ProgressBar adapter_pb = (ProgressBar) base.findViewById(R.id.adapter_pb);

        }
    }
}
