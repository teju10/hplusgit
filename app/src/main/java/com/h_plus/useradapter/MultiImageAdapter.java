package com.h_plus.useradapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.h_plus.R;
import com.h_plus.useractivity.MyHomeFeature;

import java.util.ArrayList;

/**
 * Created by Infograins on 10-Dec-16.
 */
public class MultiImageAdapter extends RecyclerView.Adapter{
    LayoutInflater layoutInflater;
    Context mContext;
    ArrayList<String> arrayList;
    int res;
    public MultiImageAdapter(ArrayList<String> listmultiimg, int multiimage_item, Context mContext) {
       this.arrayList=listmultiimg;
        this.res = multiimage_item;
        this.mContext = mContext;
        layoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = layoutInflater.inflate(res, parent, false);
        return new MyViewHolder(v);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
   try{
       Glide.with(mContext).load(arrayList.get(position)).into(((MyViewHolder)holder).home_img_pic);
            ((MyViewHolder) holder).cross.setOnClickListener(new Myclick(position));
   }catch (Exception e){
       e.printStackTrace();
   }
    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView home_img_pic,cross;
        public MyViewHolder(View itemView) {
            super(itemView);
            home_img_pic = (ImageView) itemView.findViewById(R.id.home_img_pic);
            cross = (ImageView) itemView.findViewById(R.id.cross);

        }
    }

    private class Myclick implements View.OnClickListener {
        int pos;
        public Myclick(int position) {
            this.pos=position;
        }

        @Override
        public void onClick(View v) {
            ((MyHomeFeature)mContext).RemoveSelectedImages(pos);
        }
    }
}
