package com.h_plus.useradapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.h_plus.R;
import com.h_plus.customwidget.CustomButton;
import com.h_plus.customwidget.CustomTextView;
import com.h_plus.HP_Service_Provider.Bidder_Project_Detail_Activity;
import com.h_plus.usertabs.utility.Constants;
import com.h_plus.usertabs.utility.Utility;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.LinkedList;

/**
 * Created by Infograins on 12/6/2016.
 */

public class Search_Project_Adapter extends BaseAdapter {

    Context mContext;
    LinkedList<JSONObject> list;
    int res;
    int[] img = {R.drawable.purple_patti,
            R.drawable.red_patti,
            R.drawable.blue_patti};

    public Search_Project_Adapter(Context appContext, int res, LinkedList<JSONObject> list) {
        this.mContext = appContext;
        this.list = list;
        this.res = res;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        if (convertView == null) {
            LayoutInflater li = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = li.inflate(res, null);
            viewHolder = new ViewHolder(convertView);


            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        JSONObject j = list.get(position);
        try {

            viewHolder.sp_proname.setText(j.getString("project_name"));
            viewHolder.sp_desc.setText(j.getString("description"));

            viewHolder.sp_budget.setText((j.getString("currency") +
                    j.getString("currency_symbol") + j.getString("min_budget") + "-" + j.getString("currency_symbol") + j.getString("max_budget")));

            Glide.with(mContext).load(j.getString("user_image_url"))
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(((com.h_plus.customwidget.CircleImageView) viewHolder.image_v));

            if (j.getString("hire_to").equals("1")) {
                viewHolder.awarded_p_starimg.setVisibility(View.VISIBLE);
            }else if(j.getString("hire_to").equals("0")){
                viewHolder.awarded_p_starimg.setVisibility(View.GONE);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        viewHolder.sp_btn_detail.setOnClickListener(new MyClick(position));
        ((RelativeLayout) convertView.findViewById(R.id.relatv_layout)).setBackgroundResource(img[position % 3]);
        return convertView;
    }

    public class MyClick implements View.OnClickListener {
        int pos;

        public MyClick(int pos) {
            this.pos = pos;
        }

        @Override
        public void onClick(View v) {
            try {
                Utility.setSharedPreference(mContext, Constants.PROJECTID, list.get(pos).getString("id"));
                Utility.setSharedPreference(mContext, Constants.PROJECT_TITLE, list.get(pos).getString("project_name"));
                mContext.startActivity(new Intent(mContext, Bidder_Project_Detail_Activity.class));
            } catch (JSONException e) {
                e.printStackTrace();
            }
            // Utility.setSharedPreference(mContext,list.get(pos));

        }
    }

    class ViewHolder {

        CustomTextView sp_proname, sp_budget, sp_desc;
        com.h_plus.customwidget.CircleImageView image_v;
        CustomButton sp_btn_detail;
        ImageView awarded_p_starimg;

        public ViewHolder(View base) {

            sp_proname = (CustomTextView) base.findViewById(R.id.sp_proname);
            sp_budget = (CustomTextView) base.findViewById(R.id.sp_budget);
            sp_desc = (CustomTextView) base.findViewById(R.id.sp_desc);
            sp_btn_detail = (CustomButton) base.findViewById(R.id.sp_btn_detail);
            image_v = (com.h_plus.customwidget.CircleImageView) base.findViewById(R.id.image_v);
            awarded_p_starimg = (ImageView) base.findViewById(R.id.awarded_p_starimg);
        }

    }

}
