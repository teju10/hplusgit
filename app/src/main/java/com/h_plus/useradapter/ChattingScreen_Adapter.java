package com.h_plus.useradapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.h_plus.R;
import com.h_plus.customwidget.CustomTextView;
import com.h_plus.usertabs.utility.Constants;
import com.h_plus.usertabs.utility.Utility;

import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by Infograins on 12/22/2016.
 */

public class ChattingScreen_Adapter extends BaseAdapter {

    Context mContext;
    //ArrayList<GS_Message> jsonObjects;
    LayoutInflater inflater;
    ArrayList<JSONObject> jsonObjects;
    String current_date, current_time, time;

    public ChattingScreen_Adapter(Context context, ArrayList<JSONObject> object) {

        this.mContext = context;
        this.jsonObjects = object;
        inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

  /*  public ChattingScreen_Adapter(Context context, ArrayList<GS_Message> object) {

        this.mContext = context;
        this.jsonObjects = object;
        inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }*/

    @Override
    public int getCount() {
        return jsonObjects.size();
    }

    @Override
    public Object getItem(int position) {
        return jsonObjects.get(position);
        //return 0;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder viewHolder;
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.adapter_chatting_layout_right, null);
            viewHolder = new ViewHolder(convertView);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        try {
            //final GS_Message message = jsonObjects.get(position);
            JSONObject j = jsonObjects.get(position);
            if (j.getString("user_id").equals(Utility.getSharedPreferences(mContext, Constants.USERID))) {
                // System.out.println("CHATTING SCREEN " + message.getUserid());
           /* if (message.getUserid() == Integer.parseInt(Utility.getSharedPreferences(mContext, Constants.USERID))) {
                if (convertView == null) {
                    convertView = inflater.inflate(R.layout.adapter_chatting_layout_right, null);
                    viewHolder = new ViewHolder(convertView);
                    convertView.setTag(viewHolder);
                } else {
                    viewHolder = (ViewHolder) convertView.getTag();
                }*/
                viewHolder.layout_msg_left.setVisibility(View.GONE);
                viewHolder.layout_msg_right.setVisibility(View.VISIBLE);
                //viewHolder.text_msg_right.setText(j.getString("message"));
                viewHolder.text_msg_right.setText(j.getString("message"));
                time = j.getString("datetime");
                String[] date = time.split(" ");
                current_date = date[0];
                time = date[1];
                current_time = Utility.Time(time);
                viewHolder.text_msg_datetime.setText(current_time);
                // current_time = Utility.Time(message.getTime());
                // viewHolder.text_msg_datetime.setText(message.getTime());

               /* if (j.getString("read_status").equals("1")) {
                    viewHolder.read_unread_icon.setImageResource(R.drawable.ic_read_icon);
                } else {
                    viewHolder.read_unread_icon.setImageResource(R.drawable.ic_unread_icon);

                }*/
                // viewHolder.text_msg_right.setText(j.getString("cenas_name"));
                //  viewHolder.text_msg_datetime.setText(parseDateToyyyyMMdd(j.getString("datetime")));
                // } else if (message.getWorker_id() == Integer.parseInt(Utility.getSharedPreferences(mContext, Constants.TO_USERID))) {

            } else {
                viewHolder.layout_msg_right.setVisibility(View.GONE);
                viewHolder.layout_msg_left.setVisibility(View.VISIBLE);
                // viewHolder.img_left.setVisibility(View.VISIBLE);
                // Glide.with(mContext).load(j.getString("cenas_image_url")).into(viewHolder.img_left);
                //viewHolder.text_msg_left.setText(j.getString("cenas_name"));
                //  current_time = Utility.Time(message.getTime());
                viewHolder.text_msg_left.setText(j.getString("message"));
                time = j.getString("datetime");
                String[] date = time.split(" ");
                current_date = date[0];
                time = date[1];
                current_time = Utility.Time(time);
                viewHolder.chat_left_date_time.setText(current_time);

              /*  if (j.getString("read_status").equals("1")) {
                    viewHolder.read_unread_icon.setImageResource(R.drawable.ic_read_icon);
                } else {
                    viewHolder.read_unread_icon.setImageResource(R.drawable.ic_unread_icon);
                }*/
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return convertView;
    }

    public String parseDateToyyyyMMdd(String time) {
        String outputPattern = "dd/MM/yyyy";
        String inputPattern = "yyyy-MM-dd";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);
        Date date = null;
        String str = null;
        String dayOfTheWeek = null;
        try {
            date = inputFormat.parse(time);
            SimpleDateFormat sdf = new SimpleDateFormat("EEEE");
            dayOfTheWeek = sdf.format(date);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return dayOfTheWeek + " " + str;
    }

    public String Time(String time) {
        SimpleDateFormat parseFormat = new SimpleDateFormat("HH:mm");
        SimpleDateFormat displayFormat = new SimpleDateFormat("hh:mm a");
        String str = null;
        try {
            Date date = parseFormat.parse(time);
            str = displayFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }

    class ViewHolder {
        RelativeLayout layout_msg_right, layout_msg_left;
        CustomTextView text_msg_right, text_msg_left, text_msg_datetime, chat_left_date_time;

        ImageView read_unread_icon /*img_left, img_right*/;

        public ViewHolder(View convertView) {
            layout_msg_right = (RelativeLayout) convertView.findViewById(R.id.layout_msg_right);
            layout_msg_left = (RelativeLayout) convertView.findViewById(R.id.layout_msg_left);
            text_msg_right = (CustomTextView) convertView.findViewById(R.id.text_msg_right);
            text_msg_left = (CustomTextView) convertView.findViewById(R.id.text_msg_left);
            text_msg_datetime = (CustomTextView) convertView.findViewById(R.id.chat_date_time);
            chat_left_date_time = (CustomTextView) convertView.findViewById(R.id.chat_left_date_time);
            read_unread_icon = (ImageView) convertView.findViewById(R.id.read_unread_icon);
            /*img_left = (ImageView) convertView.findViewById(R.id.img_left);
            img_right = (ImageView) convertView.findViewById(R.id.img_right);*/
        }
    }

}
