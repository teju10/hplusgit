package com.h_plus.useradapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.h_plus.R;
import com.h_plus.customwidget.CircleImageView;
import com.h_plus.usertabs.utility.Constants;
import com.h_plus.usertabs.utility.Utility;

import org.json.JSONObject;

import java.util.LinkedList;


/**
 * Created by Muzammil on 7/15/2016.
 */
public class ListOfAvailableServiceProvidersAdapter extends BaseAdapter {
    Context mContext;
    LinkedList<JSONObject> list;

    public ListOfAvailableServiceProvidersAdapter(Context appContext, LinkedList<JSONObject> list) {
        this.mContext = appContext;
        this.list = list;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater li = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView == null) {
            convertView = li.inflate(R.layout.available_service_providers_list_view_item, null);
        }
        try {
            ((TextView) convertView.findViewById(R.id.user_name)).setText(list.get(position).getString(""));
            ((TextView) convertView.findViewById(R.id.discription)).setText(list.get(position).getString(""));
            ((TextView) convertView.findViewById(R.id.date)).setText(list.get(position).getString(""));
            ((TextView) convertView.findViewById(R.id.price)).setText(list.get(position).getString(""));
            ((TextView) convertView.findViewById(R.id.review_count)).setText(list.get(position).getString(""));
            Glide.with(mContext)
                    .load(Utility.getSharedPreferences(mContext, Constants.PRO_IMG))
                    .override(150, 150)
                    //.placeholder(R.drawable.blank_user)
                    .into((CircleImageView) convertView.findViewById(R.id.user_image_available_service_providers_list_item));
        } catch (Exception e) {

        }
        return convertView;
    }
}
