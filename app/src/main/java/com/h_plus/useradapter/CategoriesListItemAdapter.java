package com.h_plus.useradapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.h_plus.R;

import org.json.JSONObject;

import java.util.LinkedList;

/**
 * Created by Muzammil on 7/15/2016.
 */
public class CategoriesListItemAdapter extends BaseAdapter {
    Context mContext;
    LinkedList<JSONObject> list;
    int[] img = {R.drawable.banner1,
            R.drawable.banner2,
            R.drawable.banner3,
            R.drawable.banner4};

    public CategoriesListItemAdapter(Context appContext, LinkedList<JSONObject> list) {
        this.mContext = appContext;
        this.list = list;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position)     {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater li = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView == null) {
            convertView = li.inflate(R.layout.categories_list_item, null);
        }
        try {
            ((TextView) convertView.findViewById(R.id.category_name_on_category_tab)).setText(list.get(position).getString(""));
            ((TextView) convertView.findViewById(R.id.projects_count)).setText(list.get(position).getString(""));
            ((RelativeLayout) convertView.findViewById(R.id.today_RelativeLayout)).setBackgroundResource(img[position % 4]);
        } catch (Exception e) {

        }
        return convertView;
    }
}
