package com.h_plus.useradapter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.h_plus.HP_Client.Hire_ShowSP_PortfolioActivity;
import com.h_plus.R;
import com.h_plus.RegistrationSection.MainActivity;
import com.h_plus.ServerInteraction.ResponseListener;
import com.h_plus.ServerInteraction.ResponseTask;
import com.h_plus.customwidget.CustomButton;
import com.h_plus.customwidget.CustomTextView;
import com.h_plus.usertabs.utility.Constant_Urls;
import com.h_plus.usertabs.utility.Constants;
import com.h_plus.usertabs.utility.Utility;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.LinkedList;

/**
 * Created by Infograins on 12/10/2016.
 */

public class FreeLencerBidingAdapter extends BaseAdapter {

    Context mContext;
    LinkedList<JSONObject> obj;
    int res;

    int[] img = {
            R.drawable.purple_patti,
            R.drawable.red_patti,
            R.drawable.blue_patti};
    ResponseTask rt;

    public FreeLencerBidingAdapter(Context context, int res, LinkedList<JSONObject> list) {

        this.mContext = context;
        this.res = res;
        this.obj = list;

    }

    @Override
    public int getCount() {
        return obj.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder viewHolder;

        if (convertView == null) {
            LayoutInflater li = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = li.inflate(res, null);
            viewHolder = new ViewHolder(convertView);

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        JSONObject j = obj.get(position);
        try {
            if (Utility.getSharedPreferences(mContext, Constants.SP_LISYKEY).equals("sp_key")) {
                viewHolder.see_portfolio_ll.setVisibility(View.GONE);
                viewHolder.bider_name.setText(j.getString("name"));
                viewHolder.bidder_project_completion_rate.setText(j.getString("complete_project"));

                Glide.with(mContext).load(j.getString("image"))
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .into(((com.h_plus.customwidget.CircleImageView) viewHolder.b_image_v));
            } else if (Utility.getSharedPreferences(mContext, Constants.SP_LISYKEY).equals("my_key")) {
                viewHolder.bider_name.setText(j.getString("user_name"));
                viewHolder.bidder_price.setText("$" + (j.getString("total_bidding_amount")));
                viewHolder.bidder_time_duration_days.setText("In " + (j.getString("time_duration") + " Days"));
                if (j.getString("status").equals("2")) {
                    viewHolder.hire_worker.setVisibility(View.GONE);
                }
                Glide.with(mContext).load(j.getString("user_image_url"))
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .into(((com.h_plus.customwidget.CircleImageView) viewHolder.b_image_v));

                Glide.with(mContext).load(j.getString("user_flag_url"))
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .into((viewHolder.flag));
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        if(Utility.getSharedPreferences(mContext,Constants.USER_TYPE).equals("1")){
            convertView.setOnClickListener(new OnClick(position));
        }
        viewHolder.hire_worker.setOnClickListener(new HireBiderRequest(position));
        viewHolder.reject_worker.setOnClickListener(new ViewPortfolio_SP(position));
        ((RelativeLayout) convertView.findViewById(R.id.relatv_layout)).setBackgroundResource(img[position % 3]);
        return convertView;
    }

    public void AcceptBidrequest(String project_id, String Worker_id) {
        try {
            JSONObject jo = new JSONObject();
            jo.put(Constants.ACTION, Constants.HIRE_SP_BIDINGREQUEST);
            jo.put("from_user", Utility.getSharedPreferences(mContext, Constants.USERID));
            jo.put("project_id", project_id);
            jo.put("to_user", Worker_id);

            Utility.ShowLoading(mContext, mContext.getResources().getString(R.string.please_wait));
            rt = new ResponseTask(Constant_Urls.SERVER_URL, jo);
            rt.setListener(new ResponseListener() {
                @Override
                public void onGetPickSuccess(String result) {
                    Utility.HideDialog();
                    try {
                        JSONObject j = new JSONObject(result);
                        if (j.getString("msg").equals("1")) {
                            Utility.showToast(mContext, "request accepted");
                        } else {
                            Utility.showToast(mContext, j.getString("msg"));
                        }


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }

            });
            rt.execute();
        } catch (JSONException j) {
            j.printStackTrace();
        }
    }

    public class HireBiderRequest implements View.OnClickListener {
        int pos;

        public HireBiderRequest(int pos) {
            this.pos = pos;
        }

        @Override
        public void onClick(View v) {
            try {
                AcceptBidrequest(obj.get(pos).getString("project_id"), obj.get(pos).getString("worker_id"));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    public class ViewPortfolio_SP implements View.OnClickListener {
        int pos;

        public ViewPortfolio_SP(int pos) {
            this.pos = pos;
        }

        @Override
        public void onClick(View v) {
            try {
                Bundle b = new Bundle();
                Intent i = new Intent(mContext, Hire_ShowSP_PortfolioActivity.class);
                b.putString("PROJECTID", obj.get(pos).getString("project_id"));
                b.putString("FROM_USERID", obj.get(pos).getString("worker_id"));
                i.putExtras(b);
                mContext.startActivity(i);
                ((MainActivity)mContext).finish();
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }

    public class OnClick implements View.OnClickListener {
        int pos;

        public OnClick(int pos) {
            this.pos = pos;
        }

        @Override
        public void onClick(View v) {
            try {
                Bundle b = new Bundle();
                Intent i = new Intent(mContext, Hire_ShowSP_PortfolioActivity.class);
                Utility.setSharedPreference(mContext, Constants.HIREPROVIDER, "0");
                b.putString("FROM_USERID", obj.get(pos).getString("user_id"));
                i.putExtras(b);
                mContext.startActivity(i);
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }

    public class ViewHolder {

        CustomTextView bider_name, bidder_time_days_ago, bidder_project_completion_rate,
                bidder_ratting_review, bidder_price, bidder_time_duration_days;
        com.h_plus.customwidget.CircleImageView b_image_v;
        CustomButton hire_worker, reject_worker;
        ImageView flag, rating_1, rating_2, rating_3, rating_4, rating_5;
        LinearLayout see_portfolio_ll;

        public ViewHolder(View base) {

            bider_name = (CustomTextView) base.findViewById(R.id.bider_name);
            bidder_time_days_ago = (CustomTextView) base.findViewById(R.id.bidder_time_days_ago);
            bidder_project_completion_rate = (CustomTextView) base.findViewById(R.id.bidder_project_completion_rate);
            bidder_price = (CustomTextView) base.findViewById(R.id.bidder_price);
            bidder_time_duration_days = (CustomTextView) base.findViewById(R.id.bidder_completion_days);
            bidder_ratting_review = (CustomTextView) base.findViewById(R.id.bidder_ratting_review);
            hire_worker = (CustomButton) base.findViewById(R.id.hire_worker);
            reject_worker = (CustomButton) base.findViewById(R.id.reject_worker);
            see_portfolio_ll = (LinearLayout) base.findViewById(R.id.see_portfolio_ll);

            b_image_v = (com.h_plus.customwidget.CircleImageView) base.findViewById(R.id.b_image_v);

            flag = (ImageView) base.findViewById(R.id.flag);
            rating_1 = (ImageView) base.findViewById(R.id.rating_1);
            rating_2 = (ImageView) base.findViewById(R.id.rating_2);
            rating_3 = (ImageView) base.findViewById(R.id.rating_3);
            rating_4 = (ImageView) base.findViewById(R.id.rating_4);
            rating_5 = (ImageView) base.findViewById(R.id.rating_5);
        }
    }
}
