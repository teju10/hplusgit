package com.h_plus.useradapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;

import com.h_plus.R;

/**
 * Created by Infograins on 11/30/2016.
 */

public class UserList_Adapter extends BaseAdapter {
    int[] img = {R.drawable.purple_patti,
            R.drawable.red_patti,
            R.drawable.blue_patti};
    Context mContext;
//    LinkedList<JSONObject>list;

    public UserList_Adapter(Context appContext) {
        this.mContext = appContext;
//        this.list = list;
    }

    @Override
    public int getCount() {
        return 10;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater li = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView == null) {
            convertView = li.inflate(R.layout.adapter_home_userlist, null);
        }
        ((LinearLayout) convertView.findViewById(R.id.main)).setBackgroundResource(img[position % 3]);
        return convertView;
    }
}
