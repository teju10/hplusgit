package com.h_plus.useradapter;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.RelativeLayout;

import com.h_plus.R;
import com.h_plus.ServerInteraction.ResponseListener;
import com.h_plus.ServerInteraction.ResponseTask;
import com.h_plus.customwidget.CustomButton;
import com.h_plus.customwidget.CustomTextView;
import com.h_plus.RegistrationSection.MainActivity;
import com.h_plus.usertabs.utility.Constant_Urls;
import com.h_plus.usertabs.utility.Constants;
import com.h_plus.usertabs.utility.Utility;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Infograins on 12/6/2016.
 */

public class MyProjectAdapter extends BaseAdapter {

    ArrayList<JSONObject> list;
    Context mContext;
    int res;
    int[] img = {
            R.drawable.purple_patti,
            R.drawable.red_patti,
            R.drawable.blue_patti};

    ResponseTask rt;

    public MyProjectAdapter(Context appContext, int res, ArrayList<JSONObject> list) {
        this.mContext = appContext;
        this.list = list;
        this.res = res;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        if (convertView == null) {
            LayoutInflater li = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = li.inflate(res, null);
            viewHolder = new ViewHolder(convertView);


            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        JSONObject j = list.get(position);
        try {
            viewHolder.mp_proname.setText(j.getString("project_name"));
            viewHolder.mp_desc.setText(j.getString("description"));
            viewHolder.mp_budget.setText((j.getString("currency_symbol") + j.getString("min_budget") + "-" +
                    j.getString("currency_symbol") + j.getString("max_budget")));

           /* Glide.with(mContext).load(j.getString("user_image_url"))
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(((com.h_plus.customwidget.CircleImageView) viewHolder.mp_image_v));*/

        } catch (Exception e) {
            e.printStackTrace();
        }
        viewHolder.mp_edit_btn.setOnClickListener(new EditProject(position));
        viewHolder.mp_delete_btn.setOnClickListener(new DeleteProject(position));
        viewHolder.mp_view_btn.setOnClickListener(new ViewProject(position));
        ((RelativeLayout) convertView.findViewById(R.id.relatv_layout)).setBackgroundResource(img[position % 3]);

        return convertView;
    }

    //Todo API's and ol kachra
    public void DeleteProject(final int postition, String project_id) {
        try {
            JSONObject jo = new JSONObject();
            jo.put(Constants.ACTION, Constants.DELETEPROJECT);
            jo.put("id", project_id);

            Utility.ShowLoading(mContext, mContext.getResources().getString(R.string.please_wait));
            rt = new ResponseTask(Constant_Urls.SERVER_URL, jo);
            rt.setListener(new ResponseListener() {
                               @Override
                               public void onGetPickSuccess(String result) {
                                   Utility.HideDialog();
                                   if (result == null) {
                                       Utility.ShowToastMessage(mContext, "sever not responding");

                                   } else {
                                       try {
                                           JSONObject jsonObject = new JSONObject(result);
                                           if (jsonObject.getString("success").equals("1")) {
                                               list.remove(postition);
                                               notifyDataSetChanged();
                                               Utility.showToast(mContext, "Your project is deleted successfully..");
                                           }

                                       } catch (Exception e) {
                                           e.printStackTrace();
                                       }
                                   }
                               }
                           }
            );
        } catch (JSONException e) {
            e.printStackTrace();
        }
        rt.execute();
    }

    public void DeleteAlert(final int pos) {
        try {
            final int pos1 = pos;
            final AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
            builder.setTitle("Delete");
            builder.setIcon(R.drawable.ic_launcher);
            builder.setMessage("If you want to delete?")
                    .setCancelable(false).setPositiveButton("YES", new DialogInterface.OnClickListener() {
                public void onClick(final DialogInterface dialog, final int id) {
                    try {
                        DeleteProject(pos1, list.get(pos).getString("id"));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }).setNegativeButton("NO", new DialogInterface.OnClickListener() {
                public void onClick(final DialogInterface dialog, final int id) {
                    dialog.cancel();
                    dialog.dismiss();
                }
            });
            final AlertDialog alert = builder.create();
            alert.show();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public class EditProject implements View.OnClickListener {
        int pos;

        public EditProject(int pos) {
            this.pos = pos;
        }

        @Override
        public void onClick(View v) {
            try {
                Utility.setSharedPreference(mContext, Constants.UPDATE_PROJECTID, list.get(pos).getString("id"));
                Utility.setSharedPreference(mContext, Constants.EDITPROJECTBTNCLICK, "92");
            } catch (JSONException e) {
                e.printStackTrace();
            }

            ((MainActivity) mContext).callSelectedFragment(Constants.FRAGMENT_HOME_PROJECTSUMMARY, "Edit Project");

            // Utility.setSharedPreference(mContext,list.get(pos));

        }
    }

    public class DeleteProject implements View.OnClickListener {
        int pos;

        public DeleteProject(int pos) {
            this.pos = pos;
        }

        @Override
        public void onClick(View v) {
            DeleteAlert(pos);

        }
    }

    public class ViewProject implements View.OnClickListener {
        int posi;

        public ViewProject(int pos) {
            this.posi = pos;
        }


        @Override
        public void onClick(View v) {
            try {
                Utility.setSharedPreference(mContext, Constants.PROJECTID, list.get(posi).getString("id"));
                ((MainActivity)mContext).callSelectedFragment(Constants.Fragment_ProjectDetailandProposalListTab,"");

            } catch (JSONException e) {
                e.printStackTrace();
            }
           /* Intent i = new Intent();
            i.putExtra("CON", "1");
            Intent in = new Intent(mContext, Fragment_Client_ProjectDetail.class);
            in.putExtras(i);
            mContext.startActivity(in);*/

        }

    }
}

class ViewHolder {

    CustomTextView mp_proname, mp_budget, mp_desc;
    com.h_plus.customwidget.CircleImageView mp_image_v;
    CustomButton mp_edit_btn, mp_delete_btn, mp_view_btn;

    public ViewHolder(View base) {

        mp_proname = (CustomTextView) base.findViewById(R.id.sp_bidlist_proname);
        mp_budget = (CustomTextView) base.findViewById(R.id.sp_bidlist_budget);
        mp_desc = (CustomTextView) base.findViewById(R.id.sp_bidlist_desc);
        mp_edit_btn = (CustomButton) base.findViewById(R.id.mp_edit_btn);
        mp_delete_btn = (CustomButton) base.findViewById(R.id.mp_delete_btn);
        mp_view_btn = (CustomButton) base.findViewById(R.id.mp_view_btn);
        mp_image_v = (com.h_plus.customwidget.CircleImageView) base.findViewById(R.id.sp_bidlist_image_v);
    }

}


