package com.h_plus.useractivity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import com.h_plus.R;
import com.h_plus.ServerInteraction.ResponseTask;
import com.h_plus.customwidget.CustomEditText;
import com.h_plus.usertabs.utility.Constants;
import com.h_plus.usertabs.utility.Utility;

import org.json.JSONObject;

import static com.h_plus.R.array.avaibility_data;
import static com.h_plus.R.array.no_age;
import static com.h_plus.R.array.no_balcony;
import static com.h_plus.R.array.no_bathroom;
import static com.h_plus.R.array.no_bedroom;
import static com.h_plus.R.array.no_hall;
import static com.h_plus.R.array.no_kitchen;
import static com.h_plus.R.array.year_data;


/**
 * Created by Infograins on 08-Dec-16.
 */

public class MyHomeDetail extends Activity implements View.OnClickListener {
    private static String TAG = MyHomeDetail.class.getSimpleName();
    Context mContext;
    Spinner spin_hall,spin_kitchen,spin_totalfloor,spin_beds,spin_balconies,spin_bathrooms,spin_avaibility,spin_pAge,spin_year;
    CustomEditText BuildArea;
    ResponseTask responseTask;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.my_home_details_layout);
        mContext=this;
        bind();
        inti();
        editdata();
    }

    private void editdata() {
        Bundle extraa=getIntent().getExtras();
   if (extraa.getString(Constants.MODE).equals("edit_mode")){

       System.out.println("bundle of    "+extraa );
       try {
           String prop_hall,prop_kitchen
                   ,prop_floors,prop_bed,prop_balconies,prop_bathroom,prop_avaibility,prop_age_count,prop_age_year;
           JSONObject j=new JSONObject(extraa.getString(Constants.OBJECT));
           BuildArea.setText(j.getString("area"));
           //to set the desire value on the basis of json object index position and set it into spinner
           prop_hall=j.getString("hall");
           prop_kitchen=j.getString("kitchen");
           prop_floors=j.getString("total_floor");
           prop_bed=j.getString("beadroom");
           prop_balconies=j.getString("balconies");
           prop_bathroom=j.getString("bathrooms");
           prop_avaibility=j.getString("avalilability");
           prop_age_count=j.getString("property_age");
           prop_age_year=j.getString("property_age1");
           for (int i = 0; i< getResources().getStringArray(no_hall).length; i++){
               if (getResources().getStringArray(R.array.no_hall)[i].equals(prop_hall)){
                   spin_hall.setSelection(i);
                   break;
               }
           }

           for (int i = 0; i< getResources().getStringArray(no_kitchen).length; i++){
               if (getResources().getStringArray(no_kitchen)[i].equals(prop_kitchen)){
                   spin_kitchen.setSelection(i);
                   break;
               }
           }
           for (int i = 0; i< getResources().getStringArray(R.array.total_floor).length; i++){
               if (getResources().getStringArray(R.array.total_floor)[i].equals(prop_floors)){
                   spin_totalfloor.setSelection(i);
                   break;
               }
           }
           for (int i = 0; i< getResources().getStringArray(R.array.no_bedroom).length; i++){
               if (getResources().getStringArray(R.array.no_bedroom)[i].equals(prop_bed)){
                   spin_beds.setSelection(i);
                   break;
               }
           }
           for (int i = 0; i< getResources().getStringArray(no_balcony).length; i++){
               if (getResources().getStringArray(no_balcony)[i].equals(prop_balconies)){
                   spin_balconies.setSelection(i);
                   break;
               }
           }
           for (int i = 0; i< getResources().getStringArray(no_bathroom).length; i++){
               if (getResources().getStringArray(no_bathroom)[i].equals(prop_bathroom)){
                   spin_bathrooms.setSelection(i);
                   break;
               }
           } for (int i = 0; i< getResources().getStringArray(avaibility_data).length; i++){
               if (getResources().getStringArray(avaibility_data)[i].equals(prop_avaibility)){
                   spin_avaibility.setSelection(i);
                   break;
               }
           }
           for (int i = 0; i< getResources().getStringArray(no_age).length; i++){
               if (getResources().getStringArray(no_age)[i].equals(prop_age_count)){
                   spin_pAge.setSelection(i);
                   break;
               }
           }
           for (int i = 0; i< getResources().getStringArray(year_data).length; i++){
               if (getResources().getStringArray(year_data)[i].equals(prop_age_year)){
                   spin_year.setSelection(i);
                   break;
               }
           }
       }catch (Exception e){
           e.printStackTrace();
       }
   }

    }

    private void bind() {
        BuildArea=(CustomEditText)findViewById(R.id.ctv_built_area);
        spin_hall=(Spinner)findViewById(R.id.spin_hall);
        spin_kitchen=(Spinner)findViewById(R.id.spin_kitchen);
        spin_totalfloor=(Spinner)findViewById(R.id.spin_totalfloor);
        spin_beds=(Spinner)findViewById(R.id.spin_bedroom);
        spin_balconies=(Spinner)findViewById(R.id.spin_balconies);
        spin_bathrooms=(Spinner)findViewById(R.id.spin_bathrooms);
        spin_avaibility=(Spinner)findViewById(R.id.spin_availability);
        spin_pAge=(Spinner)findViewById(R.id.spin_number);
        spin_year=(Spinner)findViewById(R.id.spin_year);
        (findViewById(R.id.btn_next)).setOnClickListener(this);

    }
    private void inti() {

        ArrayAdapter<String> adapter= new ArrayAdapter<String>(mContext,android.R.layout.simple_spinner_dropdown_item,getResources().getStringArray(R.array.total_floor));
        spin_totalfloor.setAdapter(adapter);


        ArrayAdapter<String> adapter_bed= new ArrayAdapter<String>(mContext,android.R.layout.simple_spinner_dropdown_item,getResources().getStringArray(no_bedroom));
        spin_beds.setAdapter(adapter_bed);


        ArrayAdapter<String> adapter_hall= new ArrayAdapter<String>(mContext,android.R.layout.simple_spinner_dropdown_item,getResources().getStringArray(no_hall));
        spin_hall.setAdapter(adapter_hall);


        ArrayAdapter<String> adapter_kitchen= new ArrayAdapter<String>(mContext,android.R.layout.simple_spinner_dropdown_item,getResources().getStringArray(no_kitchen));
        spin_kitchen.setAdapter(adapter_kitchen);
        ArrayAdapter<String> adapter_balcony= new ArrayAdapter<String>(mContext,android.R.layout.simple_spinner_dropdown_item,getResources().getStringArray(no_balcony));
        spin_balconies.setAdapter(adapter_balcony);
        ArrayAdapter<String> adapter_bathroom= new ArrayAdapter<String>(mContext,android.R.layout.simple_spinner_dropdown_item,getResources().getStringArray(no_bathroom));
        spin_bathrooms.setAdapter(adapter_bathroom);
        ArrayAdapter<String> adapter_avability= new ArrayAdapter<String>(mContext,android.R.layout.simple_spinner_dropdown_item,getResources().getStringArray(avaibility_data));
        spin_avaibility.setAdapter(adapter_avability);
        ArrayAdapter<String> adapter_age= new ArrayAdapter<String>(mContext,android.R.layout.simple_spinner_dropdown_item,getResources().getStringArray(no_age));
        spin_pAge.setAdapter(adapter_age);
        ArrayAdapter<String> adapter_year= new ArrayAdapter<String>(mContext,android.R.layout.simple_spinner_dropdown_item,getResources().getStringArray(year_data));
        spin_year.setAdapter(adapter_year);

    }
    @Override
    public void onClick(View v) {
       switch (v.getId()){
           case R.id.btn_next:
               if (Utility.isConnectingToInternet(mContext)){
                 if (spin_totalfloor.getSelectedItemPosition()==0){
                     Utility.showToast(mContext, "please select floor");
                 }
                   else if(spin_beds.getSelectedItemPosition()==0){
                     Utility.showToast(mContext, "please select beds");
                 }
                 else if(spin_hall.getSelectedItemPosition()==0){
                     Utility.showToast(mContext, "please select hall");
                 }
                 else if(spin_kitchen.getSelectedItemPosition()==0){
                     Utility.showToast(mContext, "please select kitchen");
                 }
                 else if(spin_balconies.getSelectedItemPosition()==0){
                     Utility.showToast(mContext, "please select balconies");
                 }
                 else if(spin_bathrooms.getSelectedItemPosition()==0){
                     Utility.showToast(mContext, "please select bathrooms");
                 }
                 else if(spin_avaibility.getSelectedItemPosition()==0){
                     Utility.showToast(mContext, "choose Avaibility");
                 }
                 else if(spin_pAge.getSelectedItemPosition()==0){
                     Utility.showToast(mContext, "choose property age");
                 }
                 else if(spin_year.getSelectedItemPosition()==0){
                     Utility.showToast(mContext, "choose year");
                 }
                 else if(BuildArea.getText().toString().equals("")){
                     BuildArea.setError("please select Build Area");
                     BuildArea.requestFocus();
                 }
                   else {
                     Intent in= new Intent(mContext,MyHomePricing.class);
                     Bundle extraa=getIntent().getExtras();
                     Bundle databundle=getIntent().getExtras();
                     if (extraa.getString(Constants.MODE).equals("edit_mode")){
                         extraa.putString(Constants.PROPERTY_BUIL_AREA,BuildArea.getText().toString());
                         extraa.putString(Constants.PROPERTY_HALL,spin_hall.getSelectedItem().toString());
                         extraa.putString(Constants.PROPERTY_KITCHEN,spin_kitchen.getSelectedItem().toString());
                         extraa.putString(Constants.PROPERTY_FLOORS,spin_totalfloor.getSelectedItem().toString());
                         extraa.putString(Constants.PROPERTY_BED,spin_beds.getSelectedItem().toString());
                         extraa.putString(Constants.PROPERTY_BALCONIES,spin_balconies.getSelectedItem().toString());
                         extraa.putString(Constants.PROPERTY_BATHROOM,spin_bathrooms.getSelectedItem().toString());
                         extraa.putString(Constants.PROPERTY_AVAIBILITY,spin_avaibility.getSelectedItem().toString());
                         extraa.putString(Constants.PROPERTY_AGE_COUNT,spin_pAge.getSelectedItem().toString());
                         extraa.putString(Constants.PROPERTY_AGE_YEAR,spin_year.getSelectedItem().toString());
                         in.putExtras(extraa);
                         System.out.println("out value  "+extraa);
                     }
                     else {

                         databundle.putString(Constants.PROPERTY_BUIL_AREA,BuildArea.getText().toString());
                         databundle.putString(Constants.PROPERTY_HALL,spin_hall.getSelectedItem().toString());
                         databundle.putString(Constants.PROPERTY_KITCHEN,spin_kitchen.getSelectedItem().toString());
                         databundle.putString(Constants.PROPERTY_FLOORS,spin_totalfloor.getSelectedItem().toString());
                         databundle.putString(Constants.PROPERTY_BED,spin_beds.getSelectedItem().toString());
                         databundle.putString(Constants.PROPERTY_BALCONIES,spin_balconies.getSelectedItem().toString());
                         databundle.putString(Constants.PROPERTY_BATHROOM,spin_bathrooms.getSelectedItem().toString());
                         databundle.putString(Constants.PROPERTY_AVAIBILITY,spin_avaibility.getSelectedItem().toString());
                         databundle.putString(Constants.PROPERTY_AGE_COUNT,spin_pAge.getSelectedItem().toString());
                         databundle.putString(Constants.PROPERTY_AGE_YEAR,spin_year.getSelectedItem().toString());
                         in.putExtras(databundle);
                         System.out.println("out value  "+databundle);
                     }
                     startActivity(in);
                     finish();
                 }
               }
               else {
                   Utility.showToast(mContext, "Please Check Your Internet Connection");
               }
               break;
       }
    }

}
