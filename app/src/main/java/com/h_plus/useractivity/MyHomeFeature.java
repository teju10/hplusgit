package com.h_plus.useractivity;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.h_plus.R;
import com.h_plus.customwidget.CircleImageView;
import com.h_plus.customwidget.CustomButton;
import com.h_plus.customwidget.CustomEditText;
import com.h_plus.customwidget.CustomTextView;
import com.h_plus.fragment_digg.MyHomePosted;
import com.h_plus.useradapter.MultiImageAdapter;
import com.h_plus.usertabs.utility.Constant_Urls;
import com.h_plus.usertabs.utility.Constants;
import com.h_plus.usertabs.utility.MultipartUtility;
import com.h_plus.usertabs.utility.Utility;
import com.soundcloud.android.crop.Crop;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


/**
 * Created by Infograins on 09-Dec-16.
 */
public class MyHomeFeature extends AppCompatActivity implements View.OnClickListener {
    Context mContext;
    CircleImageView HomeImg;
    MultiImageAdapter adapter;
    ImageButton imgcam;
    CustomEditText Description;
    RecyclerView recycleView;
    CheckBox check_wardrobe, check_ac, check_bed, check_mkitchen, check_refrigrator, check_geyser, check_water, check_lift, check_fensui, check_park, check_wdisposal, check_harvesting, check_gas, check_purifier, check_maintainance;
    Spinner spin_facing, spin_type_flooring, spin_furnishing;
    ArrayList<String> listmultiimg = new ArrayList<>();
    String encodedImage = "", picturePath = "", mCropImageUri, path = null;
    String id, prop_use, prop_type, prop_title, prop_address, prop_city, prop_state, prop_country, prop_zip, prop_buildarea, prop_hall, prop_kitchen, prop_floors, prop_bed, prop_balconies, prop_bathroom, prop_avaibility, prop_age_count, prop_age_year, prop_totalprice, prop_discount, prop_after_discount, prop_fromdate, prop_todate, prop_ownership;
    String checkmode = "";
    String prop_facing, prop_type_of_flooring, prop_furnishing, prop_description, prop_features, prop_img;
    List<String> list = new ArrayList<>();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.my_home_features);
        mContext = this;
   /*     mToolbar = (Toolbar) findViewById(R.id.h_toolbar);
        setSupportActionBar(mToolbar);
        ((CustomTextView) findViewById(R.id.toolbar_title)).setText("Home Feature");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);*/
        bind();
        init();
        editdata();
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void editdata() {
        Bundle extraa = getIntent().getExtras();
        if (extraa.getString(Constants.MODE).equals("edit_mode")) {
            ((CustomButton)findViewById(R.id.btn_done)).setText("Update");
            try {
                JSONObject j = new JSONObject(extraa.getString(Constants.OBJECT));

                //prop_description = j.getString("description");
                Description.setText(j.getString("description"));
                String S_facing, S_typ_flooring, S_furnishing;
                S_facing = j.getString("facing");
                S_typ_flooring = j.getString("type_of_flooring");
                S_furnishing = j.getString("furnishing");
                for (int i = 0; i < getResources().getStringArray(R.array.facing).length; i++) {
                    if (getResources().getStringArray(R.array.facing)[i].equals(S_facing)) {
                        spin_facing.setSelection(i);
                        break;
                    }
                }
                for (int i = 0; i < getResources().getStringArray(R.array.type_of_flooring).length; i++) {
                    if (getResources().getStringArray(R.array.type_of_flooring)[i].equals(S_typ_flooring)) {
                        spin_type_flooring.setSelection(i);
                        break;
                    }
                }
                for (int i = 0; i < getResources().getStringArray(R.array.furnishing).length; i++) {
                    if (getResources().getStringArray(R.array.furnishing)[i].equals(S_furnishing)) {
                        spin_furnishing.setSelection(i);
                        break;
                    }
                }
                list.addAll(Arrays.asList(j.getString("facility").split(",")));
                System.out.println("data lisst   " + list);
                for (int i = 0; i < list.size(); i++) {
                    if (list.get(i).equals("Wardrobes")) {
                        check_wardrobe.isChecked();
                        check_wardrobe.setChecked(true);
                    }
                    if (list.get(i).equals("AC")) {

                        check_ac.setChecked(true);
                    }
                    if (list.get(i).equals("Bed")) {

                        check_bed.setChecked(true);
                    }
                    if (list.get(i).equals("Moduler Kitchen")) {

                        check_mkitchen.setChecked(true);
                    }
                    if (list.get(i).equals("Refrigerator")) {

                        check_refrigrator.setChecked(true);
                    }
                    if (list.get(i).equals("Geyser")) {

                        check_geyser.setChecked(true);
                    }
                    if (list.get(i).equals("Water Storage")) {

                        check_water.setChecked(true);
                    }
                    if (list.get(i).equals("Lift(s)")) {

                        check_lift.setChecked(true);
                    }
                    if (list.get(i).equals("Lift(s)")) {

                        check_lift.setChecked(true);
                    }
                    if (list.get(i).equals("Feng Shui / Vaastu Compliant")) {

                        check_fensui.setChecked(true);
                    }
                    if (list.get(i).equals("Waste Disposal")) {

                        check_wdisposal.setChecked(true);
                    }
                    if (list.get(i).equals("Rain Water Harvesting")) {

                        check_harvesting.setChecked(true);
                    }
                    if (list.get(i).equals("Piped-gas")) {

                        check_gas.setChecked(true);
                    }
                    if (list.get(i).equals("Water Purifier")) {

                        check_purifier.setChecked(true);
                    }
                    if (list.get(i).equals("Maintanance Staff")) {

                        check_maintainance.setChecked(true);
                    }

                }

                if (!j.getString("images").equals("")){
                    JSONArray jArry = j.getJSONArray("images");
                    for (int i = 0; i < jArry.length(); i++) {
                        listmultiimg.add(jArry.getString(i));
                    }
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }


    private void bind() {
        check_wardrobe = (CheckBox) findViewById(R.id.check_wardrobe);
        check_ac = (CheckBox) findViewById(R.id.check_ac);
        check_bed = (CheckBox) findViewById(R.id.check_bed);
        check_mkitchen = (CheckBox) findViewById(R.id.check_mkitchen);
        check_refrigrator = (CheckBox) findViewById(R.id.check_refrigrator);
        check_geyser = (CheckBox) findViewById(R.id.check_geyser);
        check_water = (CheckBox) findViewById(R.id.check_water);
        check_lift = (CheckBox) findViewById(R.id.check_lift);
        check_fensui = (CheckBox) findViewById(R.id.check_fensui);
        check_park = (CheckBox) findViewById(R.id.check_park);
        check_wdisposal = (CheckBox) findViewById(R.id.check_wdisposal);
        check_harvesting = (CheckBox) findViewById(R.id.check_harvesting);
        check_gas = (CheckBox) findViewById(R.id.check_gas);
        check_purifier = (CheckBox) findViewById(R.id.check_purifier);
        check_maintainance = (CheckBox) findViewById(R.id.check_maintainance);
        spin_facing = (Spinner) findViewById(R.id.spin_facing);
        spin_type_flooring = (Spinner) findViewById(R.id.spin_type_flooring);
        spin_furnishing = (Spinner) findViewById(R.id.spin_furnishing);
        Description = (CustomEditText) findViewById(R.id.ctv_discription);
        recycleView = (RecyclerView) findViewById(R.id.recycleView);
        HomeImg = (CircleImageView) findViewById(R.id.circleImageView2);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        recycleView.setLayoutManager(layoutManager);
        adapter = new MultiImageAdapter(listmultiimg, R.layout.multiimage_item, mContext);
        recycleView.setAdapter(adapter);
        recycleView.setItemAnimator(new DefaultItemAnimator());
        imgcam = (ImageButton) findViewById(R.id.click_img);
        imgcam.setOnClickListener(this);
        (findViewById(R.id.btn_done)).setOnClickListener(this);
    }

    private void init() {
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(mContext, android.R.layout.simple_spinner_dropdown_item, getResources().getStringArray(R.array.facing));
        spin_facing.setAdapter(adapter);
        spin_facing.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                prop_facing = spin_facing.getSelectedItem().toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        ArrayAdapter<String> adaptertype = new ArrayAdapter<String>(mContext, android.R.layout.simple_spinner_dropdown_item, getResources().getStringArray(R.array.type_of_flooring));
        spin_type_flooring.setAdapter(adaptertype);
        spin_type_flooring.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                prop_type_of_flooring = spin_type_flooring.getSelectedItem().toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        ArrayAdapter<String> adapterfurnish = new ArrayAdapter<String>(mContext, android.R.layout.simple_spinner_dropdown_item, getResources().getStringArray(R.array.furnishing));
        spin_furnishing.setAdapter(adapterfurnish);
        spin_furnishing.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                prop_furnishing = spin_furnishing.getSelectedItem().toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    public void onCheckboxClicked(View v) {

        switch (v.getId()) {
            case R.id.check_wardrobe:
                if (check_wardrobe.isChecked()) {
                    list.add(check_wardrobe.getText().toString());
                } else {
                    list.remove(check_wardrobe.getText().toString());
                }
                break;
            case R.id.check_ac:
                if (check_ac.isChecked()) {
                    list.add(check_ac.getText().toString());
                } else {
                    list.remove(check_ac.getText().toString());
                }
                break;
            case R.id.check_bed:
                if (check_bed.isChecked()) {
                    list.add(check_bed.getText().toString());
                } else {
                    list.remove(check_bed.getText().toString());
                }
                break;
            case R.id.check_mkitchen:
                if (check_mkitchen.isChecked()) {
                    list.add(check_mkitchen.getText().toString());
                } else {
                    list.remove(check_mkitchen.getText().toString());
                }
                break;
            case R.id.check_refrigrator:
                if (check_refrigrator.isChecked()) {
                    list.add(check_refrigrator.getText().toString());
                } else {
                    list.remove(check_refrigrator.getText().toString());
                }
                break;
            case R.id.check_geyser:
                if (check_geyser.isChecked()) {
                    list.add(check_geyser.getText().toString());
                } else {
                    list.remove(check_geyser.getText().toString());
                }
                break;
            case R.id.check_water:
                if (check_water.isChecked()) {
                    list.add(check_water.getText().toString());
                } else {
                    list.remove(check_water.getText().toString());
                }
                break;
            case R.id.check_lift:
                if (check_lift.isChecked()) {
                    list.add(check_lift.getText().toString());
                } else {
                    list.remove(check_lift.getText().toString());
                }
                break;
            case R.id.check_fensui:
                if (check_fensui.isChecked()) {
                    list.add(check_fensui.getText().toString());
                } else {
                    list.remove(check_fensui.getText().toString());
                }
                break;
            case R.id.check_park:
                if (check_park.isChecked()) {
                    list.add(check_park.getText().toString());
                } else {
                    list.remove(check_park.getText().toString());
                }
                break;
            case R.id.check_wdisposal:
                if (check_wdisposal.isChecked()) {
                    list.add(check_wdisposal.getText().toString());
                } else {
                    list.remove(check_wdisposal.getText().toString());
                }
                break;
            case R.id.check_harvesting:
                if (check_harvesting.isChecked()) {
                    list.add(check_harvesting.getText().toString());
                } else {
                    list.remove(check_harvesting.getText().toString());
                }
                break;
            case R.id.check_gas:
                if (check_gas.isChecked()) {
                    list.add(check_gas.getText().toString());
                } else {
                    list.remove(check_gas.getText().toString());
                }
                break;
            case R.id.check_purifier:
                if (check_purifier.isChecked()) {
                    list.add(check_purifier.getText().toString());
                } else {
                    list.remove(check_purifier.getText().toString());
                }
                break;
            case R.id.check_maintainance:
                if (check_maintainance.isChecked()) {
                    list.add(check_maintainance.getText().toString());
                } else {
                    list.remove(check_maintainance.getText().toString());
                }
                break;

        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_done:
                if (Utility.isConnectingToInternet(mContext)) {
                    if (spin_facing.getSelectedItemPosition() == 0) {
                        Utility.showToast(mContext, "Select Facing");
                    } else if (spin_type_flooring.getSelectedItemPosition() == 0) {
                        Utility.showToast(mContext, "Select type of Flooring");
                    } else if (spin_furnishing.getSelectedItemPosition() == 0) {
                        Utility.showToast(mContext, "Select Furnishing");
                    } else if (Description.getText().toString().equals("")) {
                        Description.setError("Please give some Description");
                    } else if (listmultiimg.size() < 0) {
                       Utility.showToast(mContext,"Please Upload Atleast one Picture");
                    } else {
                        Bundle extraa = getIntent().getExtras();
                        Bundle databundle = getIntent().getExtras();
                        if (extraa.getString(Constants.MODE).equals("edit_mode")) {
                            checkmode = "edit_mode";
                            id = extraa.getString(Constants.ID);
                            prop_use = extraa.getString(Constants.PROPERTY_USE);
                            prop_type = extraa.getString(Constants.PROPERTY_TYPE);
                            prop_title = extraa.getString(Constants.PROPERTY_TITLE);
                            prop_address = extraa.getString(Constants.PROPERTY_ADDRESS);
                            prop_city = extraa.getString(Constants.PROPERTY_CITY);
                            prop_state = extraa.getString(Constants.PROPERTY_STATE);
                            prop_country = extraa.getString(Constants.PROPERTY_COUNTYR);
                            prop_zip = extraa.getString(Constants.PROPERTY_ZIP);
                            prop_buildarea = extraa.getString(Constants.PROPERTY_BUIL_AREA);
                            prop_hall = extraa.getString(Constants.PROPERTY_HALL);
                            prop_kitchen = extraa.getString(Constants.PROPERTY_KITCHEN);
                            prop_floors = extraa.getString(Constants.PROPERTY_FLOORS);
                            prop_bed = extraa.getString(Constants.PROPERTY_BED);
                            prop_balconies = extraa.getString(Constants.PROPERTY_BALCONIES);
                            prop_bathroom = extraa.getString(Constants.PROPERTY_BATHROOM);
                            prop_avaibility = extraa.getString(Constants.PROPERTY_AVAIBILITY);
                            prop_age_count = extraa.getString(Constants.PROPERTY_AGE_COUNT);
                            prop_age_year = extraa.getString(Constants.PROPERTY_AGE_YEAR);
                            prop_totalprice = extraa.getString(Constants.PROPERTY_TOTAL_PRICE);
                            prop_discount = extraa.getString(Constants.PROPERTY_DISCOUNT);
                            prop_after_discount = extraa.getString(Constants.PROPERTY_AFTER_DISCOUNT);
                            prop_fromdate = extraa.getString(Constants.PROPERTY_FROMDATE);
                            prop_todate = extraa.getString(Constants.PROPERTY_TODATE);
                            prop_ownership = extraa.getString(Constants.PROPERTY_OWNERSHIP);
                        } else {
                            prop_use = databundle.getString(Constants.PROPERTY_USE);
                            prop_type = databundle.getString(Constants.PROPERTY_TYPE);
                            prop_title = databundle.getString(Constants.PROPERTY_TITLE);
                            prop_address = databundle.getString(Constants.PROPERTY_ADDRESS);
                            prop_city = databundle.getString(Constants.PROPERTY_CITY);
                            prop_state = databundle.getString(Constants.PROPERTY_STATE);
                            prop_country = databundle.getString(Constants.PROPERTY_COUNTYR);
                            prop_zip = databundle.getString(Constants.PROPERTY_ZIP);
                            prop_buildarea = databundle.getString(Constants.PROPERTY_BUIL_AREA);
                            prop_hall = databundle.getString(Constants.PROPERTY_HALL);
                            prop_kitchen = databundle.getString(Constants.PROPERTY_KITCHEN);
                            prop_floors = databundle.getString(Constants.PROPERTY_FLOORS);
                            prop_bed = databundle.getString(Constants.PROPERTY_BED);
                            prop_balconies = databundle.getString(Constants.PROPERTY_BALCONIES);
                            prop_bathroom = databundle.getString(Constants.PROPERTY_BATHROOM);
                            prop_avaibility = databundle.getString(Constants.PROPERTY_AVAIBILITY);
                            prop_age_count = databundle.getString(Constants.PROPERTY_AGE_COUNT);
                            prop_age_year = databundle.getString(Constants.PROPERTY_AGE_YEAR);
                            prop_totalprice = databundle.getString(Constants.PROPERTY_TOTAL_PRICE);
                            prop_discount = databundle.getString(Constants.PROPERTY_DISCOUNT);
                            prop_after_discount = databundle.getString(Constants.PROPERTY_AFTER_DISCOUNT);
                            prop_fromdate = databundle.getString(Constants.PROPERTY_FROMDATE);
                            prop_todate = databundle.getString(Constants.PROPERTY_TODATE);
                            prop_ownership = databundle.getString(Constants.PROPERTY_OWNERSHIP);

                        }
                        //Prop_Total_Age=prop_age_count+" "+prop_age_year;

                        if (list.size() < 0) {
                            prop_features = "";
                        } else {
                            prop_features = TextUtils.join(",", list);
                        }

                        if (listmultiimg.size() < 0) {
                            prop_img = "";
                        } else {
                            prop_img = TextUtils.join(",", listmultiimg);
                        }
                        prop_description = Description.getText().toString();
                        new SendDiggData().execute();
                    }
                }

                break;
            case R.id.click_img:
                selectmultipleImage();
                break;
        }

    }

    private void selectmultipleImage() {
        final CharSequence[] options = {mContext.getResources().getString(R.string.from_camera), mContext.getResources().getString(R.string.from_gallery),
                mContext.getResources().getString(R.string.Close)};
        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        builder.setTitle(mContext.getResources().getString(R.string.add_photo));
        builder.setItems(options, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (options[item].equals(mContext.getResources().getString(R.string.from_camera))) {
                    Uri outputFileUri = getCaptureImageOutputUri();
                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    // File f = new File(Environment.getExternalStorageDirectory(), "temp.jpg");
                    intent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri);
                    startActivityForResult(intent, 1);
                } else if (options[item].equals(mContext.getResources().getString(R.string.from_gallery))) {
                    Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    startActivityForResult(intent, 2);
                } else if (options[item].equals(mContext.getResources().getString(R.string.Close))) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == 1) {
                Uri imageUri = getPickImageResultUri(data);
                beginCrop(imageUri);
            } else if (requestCode == 2) {
                Uri selectedImage = data.getData();
                beginCrop(selectedImage);
            } else if (requestCode == Crop.REQUEST_CROP) {
                handleCrop(resultCode, data);
            }
        }
    }

    private Uri getCaptureImageOutputUri() {
        Uri outputFileUri = null;
        File getImage = getExternalCacheDir();
        if (getImage != null) {
            outputFileUri = Uri.fromFile(new File(getImage.getPath(), "pickImageResult.jpeg"));
        }
        return outputFileUri;
    }

    public Uri getPickImageResultUri(Intent data) {
        boolean isCamera = true;
        if (data != null && data.getData() != null) {
            String action = data.getAction();
            isCamera = action != null && action.equals(MediaStore.ACTION_IMAGE_CAPTURE);
        }
        return isCamera ? getCaptureImageOutputUri() : data.getData();
    }

    private void beginCrop(Uri source) {
        Uri destination = Uri.fromFile(new File(getCacheDir(), System.currentTimeMillis() + "cropped.jpeg"));
        Crop.of(source, destination).asSquare().start(this);
    }

    private void handleCrop(int resultCode, Intent result) {
        if (resultCode == RESULT_OK) {
            File f = new File(Crop.getOutput(result).getPath());
            String croped_file = "" + Crop.getOutput(result).getPath();
            BitmapFactory.Options bmOptions = new BitmapFactory.Options();
            Bitmap bitmap = BitmapFactory.decodeFile(f.getAbsolutePath(), bmOptions);
            bitmap = Bitmap.createScaledBitmap(bitmap, 300, 300, true);
            bitmap = Utility.rotateImage(bitmap, f);
            picturePath = croped_file;
            listmultiimg.add(picturePath);
            adapter.notifyDataSetChanged();
            // PicturePath = Utility.encodeTobase64(bitmap);
            Glide.with(mContext).load(croped_file).into(HomeImg);
            //circleImageView.setImageBitmap(bitmap);
        } else if (resultCode == Crop.RESULT_ERROR) {
            Toast.makeText(this, Crop.getError(result).getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        if (mCropImageUri != null && grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

        } else {
            Toast.makeText(this, "Required permissions are not granted", Toast.LENGTH_LONG).show();
        }
    }

    public void RemoveSelectedImages(int selected_img) {
        listmultiimg.remove(selected_img);
        int selected = listmultiimg.size() - 1;
        adapter.notifyDataSetChanged();
        if (selected == -1) {
            HomeImg.setImageResource(R.drawable.propertyimg);
        } else {
            Glide.with(mContext).load(listmultiimg.get(selected)).into(HomeImg);
        }
    }

    public class SendDiggData extends AsyncTask<String, String, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            Utility.ShowLoading(mContext, "Please Wait");
        }

        @Override
        protected String doInBackground(String... params) {
            try {
                MultipartUtility multipart = new MultipartUtility(Constant_Urls.SERVER_URL, "UTF-8");
                if (checkmode.equals("edit_mode")) {
                    multipart.addFormField(Constants.ACTION, Constants.UPDATEDIG);
                    multipart.addFormField("id", id);
                } else {
                    multipart.addFormField(Constants.ACTION, Constants.ADDDIGG);
                    multipart.addFormField("user_id", Utility.getSharedPreferences(mContext, Constants.USERID));
                    // multipart.addFormField("user_id","99");
                }
                multipart.addFormField("property_type", prop_type);
                multipart.addFormField("property_for", prop_use);
                multipart.addFormField("title", prop_title);
                multipart.addFormField("address", prop_address);
                multipart.addFormField("city", prop_city);
                multipart.addFormField("state", prop_state);
                multipart.addFormField("country", prop_country);
                multipart.addFormField("pincode", prop_zip);
                multipart.addFormField("area", prop_buildarea);
                multipart.addFormField("beadroom", prop_bed);
                multipart.addFormField("hall", prop_hall);
                multipart.addFormField("kitchen", prop_kitchen);
                multipart.addFormField("bathrooms", prop_bathroom);
                multipart.addFormField("balconies", prop_balconies);
                multipart.addFormField("total_floor", prop_floors);
                multipart.addFormField("avalilability", prop_avaibility);
                multipart.addFormField("property_age", prop_age_count);
                multipart.addFormField("property_age1", prop_age_year);
                multipart.addFormField("ownership", prop_ownership);
                multipart.addFormField("facing", prop_facing);
                multipart.addFormField("type_of_flooring", prop_type_of_flooring);
                multipart.addFormField("furnishing", prop_furnishing);
                multipart.addFormField("description", prop_description);
                multipart.addFormField("facility", prop_features);
                multipart.addFormField("price", prop_totalprice);
                multipart.addFormField("discount", prop_discount);
                multipart.addFormField("fromdate", prop_fromdate);
                multipart.addFormField("todate", prop_todate);
                if (listmultiimg.equals("")) {
                    multipart.addFormField("image[]", " ");
                } else {
                    for (int i = 0; i < listmultiimg.size(); i++) {
                        if (listmultiimg.get(i).contains("https://") || listmultiimg.get(i).contains("http://")) {
                            multipart.addFormField("image[]", listmultiimg.get(i));
                        } else {
                            multipart.addFilePart("image[]", String.valueOf(new File(listmultiimg.get(i))));
                        }
                    }

                }
                return multipart.finish();
            } catch (Exception e) {
                e.printStackTrace();
                return "Server Not Responding !";
            }

        }

        @Override
        protected void onPostExecute(String result) {
            Utility.HideDialog();
            System.out.println("Server result ====> " + result);
            if (result == null) {
                Utility.ShowToastMessage(mContext, "Please check your Internet connection");
            } else {
                try {
                    JSONObject jobj = new JSONObject(result);
                    if (jobj.get(Constants.RESULT_KEY).equals("1")) {
                        Utility.showToast(mContext,jobj.getString(Constants.SERVER_MSG));
                        startActivity(new Intent(mContext, MyHomePosted.class));
                        //finish();
                    } else {
                        Utility.showCroutonWarn(jobj.getString(Constants.SERVER_MSG), MyHomeFeature.this);
                    }
                } catch (JSONException e) {
                    Utility.showCroutonWarn(getResources().getString(R.string.server_fail), MyHomeFeature.this);
                }
            }
            super.onPostExecute(result);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
