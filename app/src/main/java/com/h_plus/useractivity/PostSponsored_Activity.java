package com.h_plus.useractivity;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.support.annotation.ColorInt;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;

import com.h_plus.R;
import com.h_plus.customwidget.CustomCheckBox;
import com.h_plus.customwidget.CustomTextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;

import static com.paypal.android.sdk.cv.i;

/**
 * Created by Infograins on 12/14/2016.
 */

public class PostSponsored_Activity extends AppCompatActivity {

    Context mContext;
    ListView sponsor_recycle_list;
    Bundle bundle;
    JSONObject jsonObject;
    ArrayList<JSONObject> sponsored_array_list;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post_sponsoredlist);

        mContext = this;
        bundle = getIntent().getExtras();
        BIND();
        INIT();

    }

    public void BIND() {
        sponsor_recycle_list = (ListView) findViewById(R.id.sponsored_list);

    }

    public void INIT() {
        try {
            jsonObject = new JSONObject(bundle.getString("SPONSOR_RESULT"));
            JSONArray jarray = jsonObject.getJSONArray("sponsor_result");
            sponsored_array_list = new ArrayList<>();

            for (int i = 0; i < jarray.length(); i++) {
                sponsored_array_list.add(jarray.getJSONObject(i).put("Status", "0"));
            }
            ((ListView) findViewById(R.id.sponsored_list)).setAdapter(new PostSponsoredAdapter(mContext, R.layout.hire_adapter_postproject_sponsorview, sponsored_array_list));

        } catch (JSONException j) {
            j.printStackTrace();
        }

    }

    public class PostSponsoredAdapter extends BaseAdapter {

        Context mContext;
        ArrayList<JSONObject> obj;
        int res;
        String Sponsor_totalprice, RecruiterProce, FeaturedPrice, UrgentPrice;


        public PostSponsoredAdapter(Context cx, int res, ArrayList<JSONObject> obj) {
            this.obj = obj;
            this.mContext = cx;
            this.res = res;
        }

        @Override
        public int getCount() {
            return obj.size();
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            SponsorViewHolder viewHolder;

            LinearLayoutManager horizontalLayoutManagaer = new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false);
            HorizontalListAdapter horizontalListAdapter;

            if (convertView == null) {
                LayoutInflater li = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = li.inflate(res, null);
                viewHolder = new SponsorViewHolder(convertView);

                convertView.setTag(viewHolder);
            } else {
                viewHolder = (SponsorViewHolder) convertView.getTag();
            }

            JSONObject j = obj.get(position);
            try {
                viewHolder.pp_sponsor_discount_title.setText(j.getString("title"));
                viewHolder.pp_sponsor_description.setText(j.getString("description"));
                viewHolder.price.setText("$ " + j.getString("price"));

                ArrayList<String> sponsor_type = new ArrayList<>();
                ArrayList<String> sponsor_type_colorcode = new ArrayList<>();
                sponsor_type.addAll(Arrays.asList(j.getString("type").split(",")));
                sponsor_type_colorcode.addAll(Arrays.asList(j.getString("color_code").split(",")));

                viewHolder.pp_sponsor_type.setLayoutManager(horizontalLayoutManagaer);
                horizontalListAdapter = new HorizontalListAdapter(mContext, R.layout.horizontal_skills_required_adapter, sponsor_type, sponsor_type_colorcode);
                viewHolder.pp_sponsor_type.setAdapter(horizontalListAdapter);

                //viewHolder.pp_sponsor_discount_all.setOnCheckedChangeListener(new CheckSponsorType(position));
                return convertView;
            } catch (JSONException e) {
                e.printStackTrace();
            }
            convertView.setOnClickListener(new OnClickCheck(position));
            return convertView;
        }

        public GradientDrawable getDrawable(@ColorInt int color) {
            GradientDrawable footerBackground = (GradientDrawable) getResources().getDrawable(R.drawable.orange_bg_btn);
            footerBackground.setColor(color);
            return footerBackground;
            //views.setBackgroundDrawable(footerBackground);
        }

        public class OnClickCheck implements View.OnClickListener {
            int position, check = 0;

            public OnClickCheck(int pos) {
                position = pos;
            }

            @Override
            public void onClick(View v) {
                JSONObject j = new JSONObject();
                try {
                    if (position == 0) {
                        if (sponsored_array_list.get(position).getString("Status").equals("0")) {
                            for (int i = 0; i < sponsored_array_list.size(); i++) {
                                if (i == 0) {
                                    check = 0;
                                    j.put("title", sponsored_array_list.get(i).getString("title"));
                                    j.put("description", sponsored_array_list.get(i).getString("description"));
                                    j.put("price", sponsored_array_list.get(i).getString("price"));
                                    j.put("type", sponsored_array_list.get(i).getString("type"));
                                    sponsored_array_list.add(j.put("Status", "1"));
                                }
                            }
                            notifyDataSetChanged();
                        } else if (sponsored_array_list.get(position).getString("Status").equals("1")) {

                            j.put("title", sponsored_array_list.get(i).getString("title"));
                            j.put("description", sponsored_array_list.get(i).getString("description"));
                            j.put("price", sponsored_array_list.get(i).getString("price"));
                            j.put("type", sponsored_array_list.get(i).getString("type"));
                        }
                    } else if (check == sponsored_array_list.size() - 2) {
                        sponsored_array_list.add(j.put("Status", "0"));
                    }

                } catch (JSONException e) {

                }
            }
        }

        class HorizontalListAdapter extends RecyclerView.Adapter<HorizontalListAdapter.ViewHolder> {

            Context mContext;
            int res;
            ArrayList<String> sponsors_type_ArrayList;
            ArrayList<String> sponsor_type_colorcode;

            public HorizontalListAdapter(Context context, int resourse, ArrayList<String> sponsors_type_ArrayList, ArrayList<String> sponsor_type_colorcode) {
                this.mContext = context;
                this.res = resourse;
                this.sponsors_type_ArrayList = sponsors_type_ArrayList;
                this.sponsor_type_colorcode = sponsor_type_colorcode;
            }

            @Override
            public HorizontalListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
                View convertView = LayoutInflater.from(parent.getContext()).inflate(res, parent, false);
                return new HorizontalListAdapter.ViewHolder(convertView);
            }

            @Override
            public void onBindViewHolder(final HorizontalListAdapter.ViewHolder holder, int position) {

                holder.adapter_skill_txtview.setText(sponsors_type_ArrayList.get(position));
                holder.adapter_skill_txtview.setBackground(getDrawable(Color.parseColor(sponsor_type_colorcode.get(position))));

            }


            @Override
            public int getItemCount() {
                return sponsors_type_ArrayList.size();
            }

            class ViewHolder extends RecyclerView.ViewHolder {

                CustomTextView adapter_skill_txtview;

                public ViewHolder(View itemView) {
                    super(itemView);
                    adapter_skill_txtview = (CustomTextView) itemView.findViewById(R.id.adapter_skill_txtview);
                }
            }
        }

        public class SponsorViewHolder {

            CustomTextView pp_sponsor_discount_title, pp_sponsor_description, price, sponsors_check_totalamount;
            CustomCheckBox pp_sponsor_discount_all;
            RecyclerView pp_sponsor_type;

            public SponsorViewHolder(View b) {

                pp_sponsor_discount_title = (CustomTextView) b.findViewById(R.id.pp_sponsor_discount_title);
                pp_sponsor_description = (CustomTextView) b.findViewById(R.id.pp_sponsor_description);
                price = (CustomTextView) b.findViewById(R.id.price);
                sponsors_check_totalamount = (CustomTextView) b.findViewById(R.id.sponsors_check_totalamount);
                pp_sponsor_discount_all = (CustomCheckBox) b.findViewById(R.id.pp_sponsor_discount_all);
                pp_sponsor_type = (RecyclerView) b.findViewById(R.id.pp_sponsor_type);
            }
        }

    }


}
