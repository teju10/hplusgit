package com.h_plus.useractivity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;

import com.bumptech.glide.Glide;
import com.h_plus.DBHelper;
import com.h_plus.R;
import com.h_plus.ServerInteraction.ResponseListener;
import com.h_plus.ServerInteraction.ResponseTask;
import com.h_plus.customwidget.CircleImageView;
import com.h_plus.customwidget.CustomEditText;
import com.h_plus.customwidget.CustomTextView;
import com.h_plus.customwidget.PullToRefreshListView;
import com.h_plus.useradapter.ChattingScreen_Adapter;
import com.h_plus.usertabs.MyService;
import com.h_plus.usertabs.utility.Constant_Urls;
import com.h_plus.usertabs.utility.Constants;
import com.h_plus.usertabs.utility.Utility;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;


/**
 * Created by Infograins on 12/22/2016.
 */

public class ChattingScreen_Activity extends AppCompatActivity {
    public static final String mBroadcastString = "mynotification";
    public static String isOPenActivity;
    public static String user_id;
    public static String worker_id;
    public static String project_id;
    DBHelper dbHelper;
    //ArrayList<GS_Message> chatlist;
    ChattingScreen_Adapter adapter;
    Context mContext;
    PullToRefreshListView chat_list;
    ArrayList<JSONObject> chatlist = new ArrayList<>();
    ResponseTask rt;
    Bundle b;
    String Project_id, ToUsername, FromUserimage, Clientid = "", data = "";
    private Toolbar mToolbar;
    private MyService myService;
    private IntentFilter mIntentFilter;

    private BroadcastReceiver mReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals(mBroadcastString)) {
                LoadConversationChatList();
            }
        }
    };

    @Override
    public void onResume() {
        super.onResume();
        try {
            registerReceiver(mReceiver, mIntentFilter);
        } catch (Exception e) {

        }
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chatting_screen);
        mContext = this;
        dbHelper = new DBHelper(mContext);
        isOPenActivity = "hjghgh";
        mIntentFilter = new IntentFilter();
        mIntentFilter.addAction(mBroadcastString);

        Find();
        GetData();
        Init();
    }

    @Override
    protected void onDestroy() {
        isOPenActivity = null;
        try {
            unregisterReceiver(mReceiver);
        } catch (Exception e) {
        }
        super.onDestroy();
    }

    public void Find() {
        mToolbar = (Toolbar) findViewById(R.id.chat_toolbar);
        setSupportActionBar(mToolbar);
       /* getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);*/
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        ((CircleImageView) findViewById(R.id.chat_user_image)).setVisibility(View.VISIBLE);

        chat_list = (PullToRefreshListView) findViewById(R.id.chat_list);


    }

    public void Init() {

        findViewById(R.id.send_msg_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (((CustomEditText) findViewById(R.id.send_msg_edittext)).getText().toString().equals("")) {
                    Utility.ShowToastMessage(mContext, "please enter your message");
                } else {
                    Sendmsgtask(((CustomEditText) findViewById(R.id.send_msg_edittext)).getText().toString());
                }
            }
        });
    }

    public void GetData() {
        b = new Bundle();
        try {
            if (Utility.getSharedPreferences(mContext, Constants.CHECKCHATBOX_REDIRECT).equals("1")) {
                b = getIntent().getExtras();
                Project_id = b.getString("project_id");
                ToUsername = b.getString("other_name");
                Clientid = b.getString("other_user_id");
                FromUserimage = b.getString("image");
                user_id = Utility.getSharedPreferences(mContext, Constants.USERID);
                worker_id = Clientid;
                project_id = Project_id;
                ((CustomTextView) mToolbar.findViewById(R.id.chat_toolbar_title)).setText(ToUsername);

                Glide.with(mContext).load(FromUserimage).
                        into(((CircleImageView) mToolbar.findViewById(R.id.chat_user_image)));

            } else {
                b = getIntent().getExtras();
                Project_id = b.getString("project_id");
                Clientid = b.getString("other_user_id");
                ToUsername = b.getString("other_name");
                ((CustomTextView) mToolbar.findViewById(R.id.chat_toolbar_title)).setText(ToUsername);
                Glide.with(mContext).load(b.getString("image")).
                        into(((CircleImageView) mToolbar.findViewById(R.id.chat_user_image)));

                user_id = Utility.getSharedPreferences(mContext, Constants.USERID);
                worker_id = Clientid;
                project_id = Project_id;
            }


            if (Utility.isConnectingToInternet(mContext)) {
                LoadConversationChatList();
            } else {
                loadAndSetChatData();
            }

        } catch (Exception j) {
            j.printStackTrace();
        }

    }

    private void loadAndSetChatData() {
     /*   ArrayList<GS_Message> mCursor = dbHelper.getChatBetweenTwoUsersData(Integer.parseInt(Utility.getSharedPreferences(mContext, Constants.USERID)),
                Integer.parseInt(Clientid), Integer.parseInt(Project_id));*/
      /*  if (mCursor != null) {*/
        adapter = new ChattingScreen_Adapter(mContext, chatlist);
        System.out.println("Size of array " + chatlist.size());
        chat_list.setAdapter(adapter);
        chat_list.setSelection(adapter.getCount() - 1);

    }

    public void LoadConversationChatList() {
        try {
            JSONObject jcl = new JSONObject();
            jcl.put(Constants.ACTION, Constants.CHATLIST);
            jcl.put("project_id", Project_id);
            jcl.put("user_id", Utility.getSharedPreferences(mContext, Constants.USERID));
            jcl.put("to_user", Clientid);
            Utility.ShowLoading(mContext, mContext.getResources().getString(R.string.please_wait));
            rt = new ResponseTask(Constant_Urls.SERVER_URL, jcl);
            rt.setListener(new ResponseListener() {
                @Override
                public void onGetPickSuccess(String result) {
                    Utility.HideDialog();
                    if (result == null) {
                        Utility.showCroutonWarn(getResources().getString(R.string.server_fail), ChattingScreen_Activity.this);
                    } else {
                        try {
                            JSONObject jobj = new JSONObject(result);
                            if (jobj.getString(Constants.RESULT_KEY).equals("1")) {
                                JSONArray jsonArray = jobj.getJSONArray("object");
                                JSONObject jo = new JSONObject();
                                chatlist.clear();
                                for (int i = 0; i < jsonArray.length(); i++) {
                                    chatlist.add(jobj.getJSONArray("object").getJSONObject(i));
                                    jo = jsonArray.getJSONObject(i);
                                    int msgid = jo.getInt("chat_id");
                                    Utility.setSharedPreference(mContext, Constants.TO_USERID, jo.getString("to_user"));
                                    String formattedDate = jo.getString("datetime");
                                    String[] date = formattedDate.split(" ");
                                    String current_date = date[0];
                                    String time = date[1];
                                    boolean insertSuccess = false;
                                    if (Utility.getSharedPreferences(mContext, Constants.USERID).equals(jo.getString("user_id"))) {
                                        insertSuccess = dbHelper.insertUpdateMsg(Integer.parseInt(Utility.getSharedPreferences(mContext, Constants.USERID)),
                                                Integer.parseInt(jo.getString("to_user")), msgid, (current_date), (time), Integer.parseInt(Project_id), 0, 1);
                                    } else {
                                        insertSuccess = dbHelper.insertUpdateMsgData(Integer.parseInt(Utility.getSharedPreferences(mContext, Constants.USERID)),
                                                Integer.parseInt(jo.getString("to_user")), msgid, jo.getString("message"), (current_date), (time), Integer.parseInt(Project_id), 0, 1);

                                    }
                                }
                                loadAndSetChatData();
                                Sendreadstatus();
                            } else {
                                loadAndSetChatData();
                                Utility.showCroutonWarn(jobj.getString(Constants.SERVER_MSG), ChattingScreen_Activity.this);
                            }
                        } catch (JSONException e2) {
                            loadAndSetChatData();
                            e2.printStackTrace();
                            Utility.showCroutonWarn(getResources().getString(R.string.server_fail), ChattingScreen_Activity.this);
                        }
                    }
                }
            });
            rt.execute();

        } catch (JSONException j) {
            j.printStackTrace();
        }
    }

    public void Sendmsgtask(String msg) {
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put(Constants.ACTION, Constants.SENDCHATMESSAGE);
            jsonObject.put("user_id", Utility.getSharedPreferences(mContext, Constants.USERID));
            jsonObject.put("to_user", Clientid);
            jsonObject.put("message", msg);
            jsonObject.put("project_id", Project_id);
            //  Utility.ShowLoading(mContext, mContext.getResources().getString(R.string.please_wait));
            rt = new ResponseTask(Constant_Urls.SERVER_URL, jsonObject);
            rt.setListener(new ResponseListener() {
                @Override
                public void onGetPickSuccess(String result) {
                    //        Utility.HideDialog();
                    if (result == null) {
                        Utility.showCroutonWarn(getResources().getString(R.string.server_fail), ChattingScreen_Activity.this);
                    } else {
                        try {
                            JSONObject jobj = new JSONObject(result);
                            if (jobj.getString(Constants.RESULT_KEY).equals("1")) {
                                JSONObject jchat = new JSONObject();
                                jchat = jobj.getJSONObject("object");
                                int msgid = jchat.getInt("chat_id");
                                String formattedDate = jchat.getString("datetime");
                                String[] date = formattedDate.split(" ");
                                String current_date = date[0];
                                String time = date[1];
                                boolean insertSuccess = dbHelper.insertMsgData(Integer.parseInt(Utility.getSharedPreferences(mContext, Constants.USERID)),
                                        Integer.parseInt(Clientid), msgid, ((CustomEditText) findViewById(R.id.send_msg_edittext)).getText().toString(),
                                        (current_date), (time), Integer.parseInt(Project_id), 0, 1);

                                Log.e("insertSuccess", "msg inserted ==== " + insertSuccess);
                                ((CustomEditText) findViewById(R.id.send_msg_edittext)).setText("");
                                LoadConversationChatList();
                            } else {
                                Utility.showCroutonWarn(jobj.getString(Constants.SERVER_MSG), ChattingScreen_Activity.this);
                            }
                        } catch (JSONException e2) {
                            e2.printStackTrace();
                            Utility.showCroutonWarn(getResources().getString(R.string.server_fail), ChattingScreen_Activity.this);
                        }
                    }
                }
            });
            rt.execute();
        } catch (JSONException e) {
            Utility.HideDialog();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    public void Sendreadstatus() {
        try {
            JSONObject j = new JSONObject();
            j.put("from_user", Utility.getSharedPreferences(mContext, Constants.USERID));
            j.put("to_user", Clientid);

            rt.setListener(new ResponseListener() {
                @Override
                public void onGetPickSuccess(String result) {
                    if (result == null) {
                        Utility.showCroutonWarn(getResources().getString(R.string.server_fail), ChattingScreen_Activity.this);
                    } else {
                        try {
                            JSONObject jobj = new JSONObject(result);
                            if (jobj.getString(Constants.RESULT_KEY).equals("1")) {
                            } else {
                                // Utility.showCroutonWarn(jobj.getString(Constants.SERVER_MSG), ChattingScreen_Activity.this);
                            }
                        } catch (JSONException e2) {
                            e2.printStackTrace();
                            //Utility.showCroutonWarn(getResources().getString(R.string.server_fail), ChattingScreen_Activity.this);
                        }
                    }
                }
            });
        } catch (JSONException j) {
            j.printStackTrace();
        }
    }


}
