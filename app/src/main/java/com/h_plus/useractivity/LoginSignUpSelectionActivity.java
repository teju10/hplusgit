package com.h_plus.useractivity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.h_plus.R;
import com.h_plus.RegistrationSection.LoginActivity;
import com.h_plus.RegistrationSection.SignupActivity;

/**
 * Created by Muzammil on 7/13/2016.
 */
public class LoginSignUpSelectionActivity extends Activity {
    Context mContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_signup_selection_activity);
        mContext = this;
        ButtonClicks();
    }

    public void ButtonClicks() {
        (findViewById(R.id.login_btn_on_selection)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent mm = new Intent(mContext, LoginActivity.class);
                startActivity(mm);
                finish();
                overridePendingTransition(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
            }
        });
        (findViewById(R.id.signup_btn_on_selection)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent mm = new Intent(mContext, SignupActivity.class);
                startActivity(mm);
                finish();
                overridePendingTransition(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
            }
        });
    }
}
