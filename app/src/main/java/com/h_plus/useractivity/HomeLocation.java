package com.h_plus.useractivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import com.h_plus.R;
import com.h_plus.ServerInteraction.ResponseTask;
import com.h_plus.customwidget.CustomEditText;
import com.h_plus.usertabs.utility.Constants;
import com.h_plus.usertabs.utility.Utility;

import org.json.JSONException;
import org.json.JSONObject;

import static com.h_plus.R.array.property_usee;


/**
 * Created by Infograins on 08-Dec-16.
 */

public class HomeLocation extends AppCompatActivity implements View.OnClickListener {
    private static String TAG = HomeLocation.class.getSimpleName();
    Context mContext;
    Spinner spin_prop_use,spin_prop_type;
    CustomEditText Propertytitle,Address,City,State,Country,Zip;
    //CustomButton BtnNext;
    String prop_use,prop_typ;
    ResponseTask responseTask;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.home_location_layout);
        mContext=this;
        bind();
        editdata();

    }

    private void editdata() {

        Bundle extraa=getIntent().getExtras();
        if (extraa.getString(Constants.MODE).equals("edit_mode")){
            try{
                JSONObject j=new JSONObject(extraa.getString(Constants.OBJECT));
                //to set the desire value on the basis of json object index position and set it into spinner
                prop_use=j.getString("property_for");
                prop_typ=j.getString("property_type");
                for (int i = 0; i< getResources().getStringArray(property_usee).length; i++){
                    if (getResources().getStringArray(property_usee)[i].equals(prop_use)){
                        spin_prop_use.setSelection(i);
                        System.out.println("sandy_errror  "+ i);
                        break;
                    }
                }

                for (int i = 0; i< getResources().getStringArray(R.array.property_type).length; i++){
                    if (getResources().getStringArray(R.array.property_type)[i].equals(prop_typ)){
                        spin_prop_type.setSelection(i);
                        System.out.println("sandy_errror  "+ i);
                        break;
                    }
                }
                Propertytitle.setText(j.getString("title"));
                Address.setText(j.getString("address"));
                City.setText(j.getString("city"));
                State.setText(j.getString("state"));
                Country.setText(j.getString("country"));
                Zip.setText(j.getString("pincode"));
            }catch (JSONException e){

            }
        }

    }


    private void bind() {
        spin_prop_use=(Spinner)findViewById(R.id.spin_property_use);
        spin_prop_type=(Spinner)findViewById(R.id.spin_property_type);
        Propertytitle=(CustomEditText)findViewById(R.id.ctv_prop_title);
        Address=(CustomEditText)findViewById(R.id.ctv_addresss);
        City=(CustomEditText)findViewById(R.id.ctv_city);
        State=(CustomEditText)findViewById(R.id.ctv_state);
        Country=(CustomEditText)findViewById(R.id.ctv_country);
        Zip=(CustomEditText)findViewById(R.id.ctv_zip);
        (findViewById(R.id.btn_next)).setOnClickListener(this);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(mContext, android.R.layout.simple_spinner_dropdown_item, getResources().getStringArray(property_usee));
        spin_prop_use.setAdapter(adapter);
        spin_prop_use.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                prop_use=spin_prop_use.getSelectedItem().toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                Utility.showToast(mContext, "kindly select Property Use");
            }
        });
        ArrayAdapter<String> adapter_type = new ArrayAdapter<String>(mContext, android.R.layout.simple_spinner_dropdown_item, getResources().getStringArray(R.array.property_type));
        spin_prop_type.setAdapter(adapter_type);
        spin_prop_type.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                prop_typ=spin_prop_type.getSelectedItem().toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                Utility.showToast(mContext, "kindly select Property Type");
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case   R.id.btn_next:
                if (Utility.isConnectingToInternet(mContext)){
                   if(Propertytitle.getText().toString().equals("")){
                       (Propertytitle).setError("please enter title");
                       (Propertytitle).requestFocus();
                   }
                    else if (Address.getText().toString().equals("")){
                       (Address).setError("please enter address");
                       (Address).requestFocus();
                   }
                   else if (City.getText().toString().equals("")){
                       (City).setError("please enter city");
                       (City).requestFocus();
                   }
                   else if (State.getText().toString().equals("")){
                       (State).setError("please enter state");
                       (State).requestFocus();
                   }
                   else if (Country.getText().toString().equals("")){
                       (Country).setError("please enter country");
                       (Country).requestFocus();
                   }
                   else if (Zip.getText().toString().equals("")){
                       (Zip).setError("please enter zipcode");
                       (Zip).requestFocus();
                   }

                   else {
                       Intent in = new Intent(mContext, MyHomeDetail.class);
                       Bundle extraa=getIntent().getExtras();
                       Bundle databundle = new Bundle();
                       if (extraa.getString(Constants.MODE).equals("edit_mode")){
                           extraa.putString(Constants.PROPERTY_USE, prop_use);
                           extraa.putString(Constants.PROPERTY_TYPE, prop_typ);
                           extraa.putString(Constants.PROPERTY_TITLE, Propertytitle.getText().toString());
                           extraa.putString(Constants.PROPERTY_ADDRESS, Address.getText().toString());
                           extraa.putString(Constants.PROPERTY_CITY, City.getText().toString());
                           extraa.putString(Constants.PROPERTY_STATE, State.getText().toString());
                           extraa.putString(Constants.PROPERTY_COUNTYR, Country.getText().toString());
                           extraa.putString(Constants.PROPERTY_ZIP, Zip.getText().toString());
                           in.putExtras(extraa);
                           System.out.println("out value" + extraa);
                       }
                       else {

                           databundle.putString(Constants.MODE, "new");
                           databundle.putString(Constants.PROPERTY_USE, prop_use);
                           databundle.putString(Constants.PROPERTY_TYPE, prop_typ);
                           databundle.putString(Constants.PROPERTY_TITLE, Propertytitle.getText().toString());
                           databundle.putString(Constants.PROPERTY_ADDRESS, Address.getText().toString());
                           databundle.putString(Constants.PROPERTY_CITY, City.getText().toString());
                           databundle.putString(Constants.PROPERTY_STATE, State.getText().toString());
                           databundle.putString(Constants.PROPERTY_COUNTYR, Country.getText().toString());
                           databundle.putString(Constants.PROPERTY_ZIP, Zip.getText().toString());
                           in.putExtras(databundle);
                           System.out.println("out value" + databundle);
                       }
                       startActivity(in);
                       finish();
                   }
                }
                else {
                    Utility.showToast(mContext, "Please Check Your Internet Connection");
                }
                break;
        }
    }

}
