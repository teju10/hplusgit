package com.h_plus.useractivity;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.View;

import com.h_plus.R;

/**
 * Created by Muzammil on 7/26/2016.
 */
public class WaitingScreenActivity extends Activity implements View.OnClickListener {
    Context mContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.waiting_screeen);
        mContext = this;
        BINDING();
    }

    public void BINDING() {
        (findViewById(R.id.read_more)).setOnClickListener(this);
        (findViewById(R.id.cross)).setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.read_more:
                (findViewById(R.id.term_and_con2)).setVisibility(View.GONE);
                (findViewById(R.id.cross)).setVisibility(View.VISIBLE);
                (findViewById(R.id.sc)).setVisibility(View.VISIBLE);
                (findViewById(R.id.read_more)).setVisibility(View.GONE);
                break;
            case R.id.cross:
                (findViewById(R.id.term_and_con2)).setVisibility(View.VISIBLE);
                (findViewById(R.id.cross)).setVisibility(View.GONE);
                (findViewById(R.id.sc)).setVisibility(View.GONE);
                (findViewById(R.id.read_more)).setVisibility(View.VISIBLE);
                break;
            default:
                break;
        }
    }
}
