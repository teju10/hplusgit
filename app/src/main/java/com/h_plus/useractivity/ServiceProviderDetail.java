package com.h_plus.useractivity;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.h_plus.R;

/**
 * Created by Muzammil on 7/28/2016.
 */
public class ServiceProviderDetail extends AppCompatActivity implements View.OnClickListener {
    Context mContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.service_provider_detail_screen);
        mContext = this;
        BINDING();
    }

    public void BINDING() {
        (findViewById(R.id.appointment_btn_on_user_detail_page)).setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

        }
    }
}
