package com.h_plus.useractivity;


import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import com.borax12.materialdaterangepicker.date.DatePickerDialog;
import com.h_plus.R;
import com.h_plus.customwidget.CustomEditText;
import com.h_plus.customwidget.CustomTextView;
import com.h_plus.usertabs.utility.Constants;
import com.h_plus.usertabs.utility.Utility;

import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;


/**
 * Created by Infograins on 08-Dec-16.
 */

public class MyHomePricing extends AppCompatActivity implements View.OnClickListener, DatePickerDialog.OnDateSetListener {
    Context mContext;
    CustomEditText totalprice, discount, afterDiscount;
    CustomTextView fromdate, todate;
    Spinner Ownership;
    String opendpd_for = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.my_home_pricing_layout);
        mContext = this;

        bind();
        init();
        editdata();
        Getpercentage();

    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void editdata() {
        Bundle extraa=getIntent().getExtras();
        String prop_ownership;
        if (extraa.getString(Constants.MODE).equals("edit_mode")){
            try{
                JSONObject j=new JSONObject(extraa.getString(Constants.OBJECT));
                totalprice.setText(j.getString("price"));
                discount.setText(j.getString("discount"));
                fromdate.setText(j.getString("fromdate"));
                todate.setText(j.getString("todate"));
                prop_ownership=j.getString("ownership");
                if (!totalprice.getText().toString().equals("")&!discount.getText().toString().equals("")){
                    ChanngePercent();
                }
                for (int i = 0; i< getResources().getStringArray(R.array.oownership).length; i++){
                    if (getResources().getStringArray(R.array.oownership)[i].equals(prop_ownership)){
                        Ownership.setSelection(i);
                        break;
                    }
                }
            }
            catch (Exception e){
                e.printStackTrace();
            }
        }
    }

    private void bind() {
        totalprice = (CustomEditText) findViewById(R.id.ctv_total_price);
        discount = (CustomEditText) findViewById(R.id.ctv_discount);
        afterDiscount = (CustomEditText) findViewById(R.id.ctv_final_price);
        afterDiscount.setEnabled(false);
        fromdate = (CustomTextView) findViewById(R.id.ctv_fromdate);
        fromdate.setOnClickListener(this);
        todate = (CustomTextView) findViewById(R.id.ctv_todate);
        todate.setEnabled(false);
        todate.setClickable(false);
        todate.setOnClickListener(this);
        Ownership = (Spinner) findViewById(R.id.spin_ownertype);
        (findViewById(R.id.btn_next)).setOnClickListener(this);

    }

    private void init() {

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(mContext, android.R.layout.simple_spinner_dropdown_item, getResources().getStringArray(R.array.oownership));
        Ownership.setAdapter(adapter);


    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_next:
                if (Utility.isConnectingToInternet(mContext)){
                    if (totalprice.getText().toString().equals("")){
                        totalprice.setError("Please select Total price");
                        totalprice.requestFocus();
                    }
                   /* else if (discount.getText().toString().equals("")){
                        discount.setError("Please select Total price");
                        discount.requestFocus();
                    }*/
                    /*else if (afterDiscount.getText().toString().equals("")){
                        afterDiscount.setError("Please select Total price");
                        afterDiscount.requestFocus();
                    }*/
                    else if (fromdate.getText().toString().equals("")){
                      Utility.showToast(mContext,"please select Discount Fromdate");
                    }
                    else if (todate.getText().toString().equals("")){
                        Utility.showToast(mContext,"please select Discount Todate");
                    }
                    else if (Ownership.getSelectedItemPosition()==0){
                       Utility.showToast(mContext,"Please select ownership Type");
                    }
                    else{
                        Intent in= new Intent(mContext, MyHomeFeature.class);
                        Bundle extraa=getIntent().getExtras();
                        Bundle databundle=getIntent().getExtras();
                        if (extraa.getString(Constants.MODE).equals("edit_mode")){
                            extraa.putString(Constants.PROPERTY_TOTAL_PRICE,totalprice.getText().toString());
                            extraa.putString(Constants.PROPERTY_DISCOUNT,discount.getText().toString());
                            extraa.putString(Constants.PROPERTY_AFTER_DISCOUNT,afterDiscount.getText().toString());
                            extraa.putString(Constants.PROPERTY_FROMDATE,fromdate.getText().toString());
                            extraa.putString(Constants.PROPERTY_TODATE,todate.getText().toString());
                            extraa.putString(Constants.PROPERTY_OWNERSHIP,Ownership.getSelectedItem().toString());
                            in.putExtras(extraa);
                            System.out.println("out value"+extraa);
                        }
                        else {
                            databundle.putString(Constants.PROPERTY_TOTAL_PRICE,totalprice.getText().toString());
                            databundle.putString(Constants.PROPERTY_DISCOUNT,discount.getText().toString());
                            databundle.putString(Constants.PROPERTY_AFTER_DISCOUNT,afterDiscount.getText().toString());
                            databundle.putString(Constants.PROPERTY_FROMDATE,fromdate.getText().toString());
                            databundle.putString(Constants.PROPERTY_TODATE,todate.getText().toString());
                            databundle.putString(Constants.PROPERTY_OWNERSHIP,Ownership.getSelectedItem().toString());
                            in.putExtras(databundle);
                            System.out.println("out value"+databundle);
                        }

                        startActivity(in);
                       // finish();
                    }
                }
                break;
            case R.id.ctv_fromdate:
                opendpd_for = "start";
                choosefromdate();
                break;
            case R.id.ctv_todate:
                opendpd_for = "end";
                choosefromdate();
                break;

        }

    }

    private void Getpercentage() {
        totalprice.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                ChanngePercent();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        discount.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                ChanngePercent();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

    }

    private void choosefromdate() {
        // Launch Date Picker Dialog
        if (opendpd_for.equals("start")) {
            Calendar now = Calendar.getInstance();
            DatePickerDialog dpd = DatePickerDialog.newInstance(this,
                    now.get(Calendar.YEAR),
                    now.get(Calendar.MONTH),
                    now.get(Calendar.DAY_OF_MONTH)
            );
            dpd.setMinDate(now);
            dpd.setAccentColor(Color.parseColor("#e98d43"));
            dpd.show(this.getFragmentManager(), "Datepickerdialog");

        } else {
            Calendar now = Calendar.getInstance();
           DatePickerDialog dpd = DatePickerDialog.newInstance(this,
                    now.get(Calendar.YEAR),
                    now.get(Calendar.MONTH),
                    now.get(Calendar.DAY_OF_MONTH)
            );
            String dateString = fromdate.getText().toString();
            SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
            Date convertedDate = new Date();
            try {
                convertedDate = dateFormat.parse(dateString);
                now.setTime(convertedDate);
            } catch (ParseException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            System.out.println(convertedDate.getTime());
            dpd.setMinDate(now);
            dpd.setAccentColor(Color.parseColor("#e98d43"));
            dpd.show(this.getFragmentManager(), "Datepickerdialog");

        }
    }

    private void ChanngePercent() {
        if (!totalprice.getText().toString().equals("") && !discount.getText().toString().equals("")) {
            double percentage;
            double i = Double.parseDouble(totalprice.getText().toString());
            double ii = Double.parseDouble(discount.getText().toString());
            percentage = (i * ii /100);
            afterDiscount.setText("" + (i-percentage));
        }else {
            afterDiscount.setText("");
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
           finish();
    }

    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth, int yearEnd, int monthOfYearEnd, int dayOfMonthEnd) {
        if (opendpd_for.equals("start")) {
            fromdate.setText(dayOfMonth + "-"
                    + (monthOfYear + 1) + "-" + year );
            todate.setEnabled(true);
            todate.setClickable(true);
        }
        else {
            todate.setText(dayOfMonth + "-"
                    + (monthOfYear + 1) + "-" + year);
        }
    }
}
