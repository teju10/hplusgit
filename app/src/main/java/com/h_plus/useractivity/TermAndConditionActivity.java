package com.h_plus.useractivity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;

import com.h_plus.R;
import com.h_plus.usertabs.utility.Utility;

/**
 * Created by Muzammil on 7/22/2016.
 */
public class TermAndConditionActivity extends Activity implements CompoundButton.OnCheckedChangeListener, View.OnClickListener {
    Context mContext;
    SharedPreferences share;
    SharedPreferences.Editor edit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.terms_and_condition);
        mContext = this;
        share = mContext.getSharedPreferences("pref", MODE_PRIVATE);
        edit = share.edit();
        edit.putString("terms_condition_true", "true");
        edit.commit();
        edit.apply();
        BINDING();
    }

    public void BINDING() {
        ((CheckBox) findViewById(R.id.accept_checkbox)).setOnCheckedChangeListener(this);
        (findViewById(R.id.accept)).setOnClickListener(this);
        (findViewById(R.id.accept)).setEnabled(false);
        (findViewById(R.id.accept)).setBackgroundColor(getResources().getColor(R.color.set_btn_color));
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        if (isChecked) {
            Log.e("condition true", "condition true");
            (findViewById(R.id.accept)).setEnabled(true);
            (findViewById(R.id.accept)).setBackgroundColor(getResources().getColor(R.color.colorPrimary));

        } else {
            Log.e("condition false", "false");
            (findViewById(R.id.accept)).setEnabled(false);
            (findViewById(R.id.accept)).setBackgroundColor(getResources().getColor(R.color.set_btn_color));
            Utility.Alert(mContext, "please accept terms & conditions of Hplus");
        }
    }

    @Override
    public void onClick(View v) {
        Intent nn = new Intent(mContext, LoginSignUpSelectionActivity.class);
        startActivity(nn);
        finish();
        overridePendingTransition(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
    }
}
