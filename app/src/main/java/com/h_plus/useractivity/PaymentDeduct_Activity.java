package com.h_plus.useractivity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;

import com.h_plus.R;
import com.h_plus.RegistrationSection.LoginActivity;
import com.h_plus.ServerInteraction.ResponseListener;
import com.h_plus.ServerInteraction.ResponseTask;
import com.h_plus.customwidget.CustomTextView;
import com.h_plus.usertabs.utility.Constant_Urls;
import com.h_plus.usertabs.utility.Constants;
import com.h_plus.usertabs.utility.Utility;
import com.paypal.android.sdk.payments.PayPalConfiguration;
import com.paypal.android.sdk.payments.PayPalPayment;
import com.paypal.android.sdk.payments.PayPalService;
import com.paypal.android.sdk.payments.PaymentActivity;
import com.paypal.android.sdk.payments.PaymentConfirmation;

import org.json.JSONException;
import org.json.JSONObject;

import java.math.BigDecimal;

/**
 * Created by Infograins on 11/29/2016.
 */

public class PaymentDeduct_Activity extends AppCompatActivity {

    private static final String CONFIG_ENVIRONMENT = PayPalConfiguration.ENVIRONMENT_SANDBOX;
    // note that these credentials will differ between live & sandbox environments.
    // private static final String CONFIG_CLIENT_ID = "ARvamUIW4ZNkHCb0SqyhQzLStrAx2tP4piJpsniwjGAgduoPl6NbaH9Xl6jI8gPGRjzDCtgkRFCQ47YH";
    private static final String CONFIG_CLIENT_ID = "ARvamUIW4ZNkHCb0SqyhQzLStrAx2tP4piJpsniwjGAgduoPl6NbaH9Xl6jI8gPGRjzDCtgkRFCQ47YH";
    private static final int PAYPAL_REQUEST_CODE  = 1;
    private static PayPalConfiguration config = new PayPalConfiguration()
            .environment(CONFIG_ENVIRONMENT)
            .clientId(CONFIG_CLIENT_ID);

    public String TAG = PaymentDeduct_Activity.class.getSimpleName();
    Context mContext;
    Bundle bundle;
    ResponseTask responseTask;
    Toolbar toolbar;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ask_for_payment);
        mContext = this;
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        bundle = new Bundle();
        bundle = getIntent().getExtras();


        find();
        init();

        Intent intent = new Intent(this, PayPalService.class);
        intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, config);
        startService(intent);
//        payment_paypal = (RelativeLayout) findViewById(R.id.payment_paypal);
//        payment_credit_card = (RelativeLayout) findViewById(R.id.payment_credit_card);

    }

    public void find() {

        setSupportActionBar((Toolbar) findViewById(R.id.h_toolbar));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        ((CustomTextView) findViewById(R.id.toolbar_title)).setText("Payment");
    }

    public void init() {

        findViewById(R.id.paypal).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PayPayment();
            }
        });

        findViewById(R.id.credit_card).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Utility.showToast(mContext, "coming soon");
            }
        });

    }

    @Override
    public void onBackPressed() {
        finish();
        super.onBackPressed();
    }

    @Override
    public void onDestroy() {
        stopService(new Intent(this, PayPalService.class));
        super.onDestroy();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void PayPayment() {
        PayPalPayment thingToBuy = getThingToBuy(PayPalPayment.PAYMENT_INTENT_SALE);
        Intent intent = new Intent(mContext, PaymentActivity.class);

        // send the same configuration for restart resiliency
        intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, config);
        intent.putExtra(PaymentActivity.EXTRA_PAYMENT, thingToBuy);
        startActivityForResult(intent, PAYPAL_REQUEST_CODE  );
    }

    private PayPalPayment getThingToBuy(String paymentIntent) {
        return new PayPalPayment(new BigDecimal("0.01"), "USD", "Payment", paymentIntent);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode,resultCode,data);
        if (requestCode == PAYPAL_REQUEST_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                PaymentConfirmation confirm = data.getParcelableExtra(PaymentActivity.EXTRA_RESULT_CONFIRMATION);
                if (confirm != null) {
                    try {
                        Log.e(TAG, confirm.toJSONObject().toString(4));
                        Log.e(TAG, confirm.getPayment().toJSONObject().toString(4));
                       /* JSONObject j = new JSONObject(confirm.toJSONObject().toString(4));
                        System.out.println("JSON");

                        String paypalid = j.getString("id");
                        String status = j.getString("state");
                        System.out.println("PAYPAL_STATUS " + paypalid + status);
                        Log.e(TAG, paypalid + status);*/
                        stopService(new Intent(this, PayPalService.class));
                        PaymentConfirmation();
                    } catch (JSONException e) {
                        Log.e(TAG, "an extremely unlikely failure occurred: ", e);
                    }
                  //  PaymentConfirmation();
                }
            } else if (resultCode == Activity.RESULT_CANCELED) {
                Log.e(TAG, "The user canceled.");
            } else if (resultCode == PaymentActivity.RESULT_EXTRAS_INVALID) {
                Log.e(TAG, "An invalid Payment or PayPalConfiguration was submitted. Please see the docs.");
            }
        }
    }

    public void PaymentConfirmation() {
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put(Constants.ACTION, Constants.PAYPAL_PAYMENT_VERIFICATION);
            jsonObject.put("user_id", Utility.getSharedPreferences(mContext, Constants.USERID));
            jsonObject.put("amount", "0.01");
           Utility.ShowLoading(mContext,getResources().getString(R.string.please_wait));
            responseTask = new ResponseTask(Constant_Urls.SERVER_URL, jsonObject);
            responseTask.setListener(new ResponseListener() {
                @Override
                public void onGetPickSuccess(String result) {
                   Utility.HideDialog();
                    System.out.println("Server result ====> " + result);
                    if (result == null) {
                        Utility.showToast(mContext, "Process incomplete..please try again!");
                    } else {
                        try {
                            JSONObject jobj = new JSONObject(result);
                            if (jobj.getString("success").equals("1")) {

                                Utility.showToast(mContext, "payment successfully done");
                                Utility.setSharedPreference(mContext, Constants.SPLASH_CHECK, "1");
                                startActivity(new Intent(mContext, LoginActivity.class));
                                finish();
                                overridePendingTransition(android.R.anim.slide_in_left, android.R.anim.slide_out_right);

                            } else {
                                Utility.showToast(mContext, jobj.getString("msg"));
                                Utility.showCroutonWarn(jobj.getString(Constants.SERVER_MSG), PaymentDeduct_Activity.this);
                            }
                        } catch (JSONException e2) {
                            Utility.showCroutonWarn(getResources().getString(R.string.payment_server_fail), PaymentDeduct_Activity.this);
                        }
                    }
                }

            });
            responseTask.execute();

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }


}


