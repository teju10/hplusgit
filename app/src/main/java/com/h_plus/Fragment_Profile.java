package com.h_plus;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.h_plus.HP_Client.ActivityProfile_CircleProgress;
import com.h_plus.HP_Service_Provider.Portfolio_HomeActivity;
import com.h_plus.RegistrationSection.Change_PasswordActivity;
import com.h_plus.RegistrationSection.EditProfile_Activity;
import com.h_plus.RegistrationSection.MainActivity;
import com.h_plus.ServerInteraction.ResponseListener;
import com.h_plus.ServerInteraction.ResponseTask;
import com.h_plus.customwidget.CircleImageView;
import com.h_plus.customwidget.CustomTextView;
import com.h_plus.usertabs.utility.Constant_Urls;
import com.h_plus.usertabs.utility.Constants;
import com.h_plus.usertabs.utility.Utility;

import org.json.JSONObject;

import static com.h_plus.R.id.profile_addportfolio;

/**
 * Created by Infograins on 12/29/2016.
 */

public class Fragment_Profile extends Fragment implements View.OnClickListener {

    View rootview;
    Context mContext;
    ResponseTask responseTask;
    Bundle b;
    String TrustScore = "";

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootview = inflater.inflate(R.layout.fragment_profile, container, false);
        mContext = getActivity();
        b = new Bundle();
       /* if (Utility.isConnectingToInternet(mContext)) {
            getprofile();
        } else {
            Utility.ShowToastMessage(mContext, "please check your network connection");
        }*/

        if (Utility.getSharedPreferences(mContext, Constants.USER_TYPE).equals("1")) {
            rootview.findViewById(R.id.profile_addportfolio).setVisibility(View.GONE);
        }
        rootview.findViewById(R.id.profile_editprofile).setOnClickListener(this);
        rootview.findViewById(R.id.profile_cpw).setOnClickListener(this);
        rootview.findViewById(profile_addportfolio).setOnClickListener(this);

        return rootview;
    }

    @Override
    public void onResume() {
        super.onResume();
        getprofile();
    }

    private void getprofile() {
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put(Constants.ACTION, Constants.GETUSERDETAILBYID);
            jsonObject.put("user_id", Utility.getSharedPreferences(mContext, Constants.USERID));
            Utility.ShowLoading(mContext, mContext.getResources().getString(R.string.please_wait));
            responseTask = new ResponseTask(Constant_Urls.SERVER_URL, jsonObject);
            responseTask.setListener(new ResponseListener() {
                @Override
                public void onGetPickSuccess(String result) {
                    Utility.HideDialog();
                    if (result == null) {
                        Utility.showCroutonWarn(getResources().getString(R.string.server_fail), getActivity());
                    } else {
                        try {
                            JSONObject jobj = new JSONObject(result);
                            if (jobj.getString(Constants.RESULT_KEY).equals("1")) {
                                JSONObject jarray = jobj.getJSONObject("object");

                                TrustScore = jarray.getString("trust_percentage");
                                System.out.println("JSON TRUST SCORE " + TrustScore);
                                ((CustomTextView) rootview.findViewById(R.id.profile_username)).setText(jarray.getString("user_name"));
                                ((CustomTextView) rootview.findViewById(R.id.u_profile_name)).setText((jarray.getString("first_name")) + "\t"
                                        + (jarray.getString("last_name")));
                                ((CustomTextView) rootview.findViewById(R.id.u_profile_dob)).setText((jarray.getString("dob")));
                                ((CustomTextView) rootview.findViewById(R.id.u_profile_email)).setText(jarray.getString("email"));
                                ((CustomTextView) rootview.findViewById(R.id.u_profile_address)).setText(jarray.getString("address"));
                                ((CustomTextView) rootview.findViewById(R.id.u_profile_pnumber)).setText(jarray.getString("contact_number"));
                                if (jarray.getString("city").equals("")) {
                                    ((CustomTextView) rootview.findViewById(R.id.u_profile_city)).setText("City");
                                } else {
                                    ((CustomTextView) rootview.findViewById(R.id.u_profile_city)).setText(jarray.getString("city"));
                                }
                                if (jarray.getString("city").equals("")) {
                                    ((CustomTextView) rootview.findViewById(R.id.u_profile_gender)).setText("Gender");
                                } else {
                                    ((CustomTextView) rootview.findViewById(R.id.u_profile_gender)).setText(jarray.getString("gender"));
                                }

                                if (jarray.getString("image_url").equals("")) {
                                    Glide.with(mContext).load(R.drawable.blank_user).
                                            diskCacheStrategy(DiskCacheStrategy.ALL).
                                            into((CircleImageView) rootview.findViewById(R.id.profile_user_image));
                                } else {
                                    Glide.with(mContext).load(jarray.getString("image_url")).
                                            diskCacheStrategy(DiskCacheStrategy.ALL).
                                            listener(new RequestListener<String, GlideDrawable>() {
                                                @Override
                                                public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                                                    return false;
                                                }

                                                @Override
                                                public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                                                    ((ProgressBar) rootview.findViewById(R.id.profile_progress)).setVisibility(View.GONE);
                                                    return false;
                                                }
                                            }).into((CircleImageView) rootview.findViewById(R.id.profile_user_image));
                                }
                                /*if (jarray.getString("image_url").equals("")) {
                                    Glide.with(mContext).load(getResources().getDrawable(R.drawable.user)).into((CircleImageView)rootview.findViewById(R.id.profile_user_image));
                                } else {
                                    Glide.with(mContext).load(jarray.getString("image_url")).into((circleImageView));
                                }*/
                                ((MainActivity) mContext).setdata();
                            } else {
                                Utility.showCroutonWarn(jobj.getString(Constants.SERVER_MSG), getActivity());
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            });
            responseTask.execute();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.main, menu);
        menu.getItem(1).setVisible(false);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menu) {

        switch (menu.getItemId()) {

            case R.id.menu_checkstatus:
                System.out.println("TRUST SCORE " + TrustScore);
                Intent i = new Intent(mContext, ActivityProfile_CircleProgress.class);
                b.putString("TrustScore", TrustScore);
                i.putExtras(b);
                startActivity(i);
                return true;

            default:
                return super.onOptionsItemSelected(menu);

        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case profile_addportfolio:
                startActivity(new Intent(mContext, Portfolio_HomeActivity.class));
                break;

            case R.id.profile_editprofile:
                startActivity(new Intent(mContext, EditProfile_Activity.class));
                break;

            case R.id.profile_cpw:
                startActivity(new Intent(mContext, Change_PasswordActivity.class));
                break;

            default:
                break;
        }
    }
}
