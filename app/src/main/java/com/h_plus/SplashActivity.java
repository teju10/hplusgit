package com.h_plus;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

import com.google.firebase.iid.FirebaseInstanceId;
import com.h_plus.RegistrationSection.LoginActivity;
import com.h_plus.RegistrationSection.MainActivity;
import com.h_plus.usertabs.utility.Constants;
import com.h_plus.usertabs.utility.Utility;

public class SplashActivity extends Activity {
    SharedPreferences share;
    SharedPreferences.Editor edit;
    String  login_session;
    // Animation
    Animation animFadein;
    private Context mContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        /*requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);*/
        setContentView(R.layout.splash_screen);
        mContext = this;

        share = mContext.getSharedPreferences("pref", MODE_PRIVATE);
        String token = FirebaseInstanceId.getInstance().getToken();
        System.out.println("token is" + token);
        Init();
    }

    private void Init() {
        if (Utility.getSharedPreferences(mContext, Constants.GCMID).equals("")) {
            String token = FirebaseInstanceId.getInstance().getToken();
            System.out.println("token is" + token);
            Utility.setSharedPreference(getApplicationContext(), Constants.GCMID, token);
        }

        // load the animation
        animFadein = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fade_in);
        // start the animation
        ((ImageView) findViewById(R.id.imageView)).startAnimation(animFadein);
        //  getSupportActionBar().hide();
        RedirectionHandler();
        login_session = share.getString("login_true", "");

    }

    private void RedirectionHandler() {

        if (Utility.getSharedPreferences(mContext, Constants.SPLASH_CHECK).equals("1")) {
            new Handler().postDelayed(new Runnable() {

                @Override
                public void run() {
                    Intent i = new Intent(mContext, MainActivity.class);
                    startActivity(i);
                    finish();
                    overridePendingTransition(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
                }

            }, 2000);
            /*if (Utility.getSharedPreferences(mContext, "SplashCheck").equals("2")) {
                new Handler().postDelayed(new Runnable() {

                    @Override
                    public void run() {
                        Intent i = new Intent(mContext, MortgageDetail_Activity.class);
                        startActivity(i);
                        finish();
                        overridePendingTransition(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
                    }

                }, 2000);


            }*/} else {
            new Handler().postDelayed(new Runnable() {

                @Override
                public void run() {
                    Intent i = new Intent(mContext, LoginActivity.class);
                    startActivity(i);
                    finish();
                    overridePendingTransition(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
                }
            }, 2000);
        }

}}

