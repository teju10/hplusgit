/**
 * Copyright 2016 Google Inc. All Rights Reserved.
 * <p/>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p/>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p/>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.h_plus;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.h_plus.HP_Service_Provider.fragment.ShowMilestone_History_Activity;
import com.h_plus.RegistrationSection.MainActivity;
import com.h_plus.useractivity.ChattingScreen_Activity;

import java.util.Map;


public class MyFirebaseMessagingService extends FirebaseMessagingService {
    private static final String TAG = "MyFirebaseMsgService";
    Context mContext;

    /**
     * Called when message is received.
     *
     * @param remoteMessage Object representing the message received from Firebase Cloud Messaging.
     */
    // [START receive_message]
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        // [START_EXCLUDE]
        // There are two types of messages data messages and notification messages. Data messages are handled
        // here in onMessageReceived whether the app is in the foreground or background. Data messages are the type
        // traditionally used with GCM. Notification messages are only received here in onMessageReceived when the app
        // is in the foreground. When the app is in the background an automatically generated notification is displayed.
        // When the user taps on the notification they are returned to the app. Messages containing both notification
        // and data payloads are treated as notification messages. The Firebase console always sends notification
        // messages. For more see: https://firebase.google.com/docs/cloud-messaging/concept-options
        // [END_EXCLUDE]

        // TODO(developer): Handle FCM messages here.
        // Not getting messages here? See why this may be: https://goo.gl/39bRNJ
        Log.w(TAG, "From: " + remoteMessage.getFrom());

        // Check if message contains a data payload.
        if (remoteMessage.getData().size() > 0) {
            mContext = this;
            Log.w(TAG, "Message data payload: " + remoteMessage.getData());
            Map<String, String> notification = remoteMessage.getData();
            Bundle bundle = new Bundle();
            for (Map.Entry<String, String> entry : notification.entrySet()) {
                bundle.putString(entry.getKey(), entry.getValue());
            }

            try {
                if (notification.get("type").equals("chat")) {

                 /*   DBHelper dbHelper = new DBHelper(getApplicationContext());
                    System.out.println("userid"+notification.get("from_user"));
                    System.out.println("workerid"+notification.get("to_user"));
                    boolean insertSuccess = dbHelper.insertMsgData(Integer.parseInt(notification.get("from_user")),
                            Integer.parseInt(notification.get("to_user")), Integer.parseInt(notification.get("chat_id"))
                            , (notification.get("message")), "hi", "hi", Integer.parseInt(notification.get("project_id")), 0, 1);
                    System.out.println("insert msg " + insertSuccess);*/
                    if (ChattingScreen_Activity.isOPenActivity != null) {
                        if (notification.get("to_user").equals(ChattingScreen_Activity.user_id) && notification.get("from_user").equals(ChattingScreen_Activity.worker_id)
                                && notification.get("project_id").equals(ChattingScreen_Activity.project_id)) {
                            Intent broadcastIntent = new Intent();
                            broadcastIntent.setAction(ChattingScreen_Activity.mBroadcastString);
                            sendBroadcast(broadcastIntent);
                        } else {
                            sendNotification(notification.get("notification"), bundle);
                        }
                    } else if (notification.get("type").equals("milestone")) {
                        sendNotification(notification.get("notification"), bundle);
                    } else if (notification.get("type").equals("bid")) {
                        sendNotification(notification.get("notification"), bundle);
                    }else if (notification.get("type").equals("hire")) {
                        sendNotification(notification.get("notification"), bundle);
                    }else if (notification.get("type").equals("awarded")) {
                        sendNotification(notification.get("notification"), bundle);
                    }

                } else {
                    sendNotification(notification.get("notification"), bundle);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        // Check if message contains a notification payload.
        if (remoteMessage.getNotification() != null) {
            Log.w(TAG, "Message Notification Body: " + remoteMessage.getNotification().getBody());
        }

        // Also if you intend on generating your own notifications as a result of a received FCM
        // message, here is where that should be initiated. See sendNotification method below.
    }
    // [END receive_message]

    /**
     * Create and show a simple notification containing the received FCM message.
     *
     * @param messageBody FCM message body received.
     */

    private void sendNotification(String messageBody, Bundle b) {
        Intent intent;
        if (b.getString("type").equals("milestone")) {
            intent = new Intent(this, ShowMilestone_History_Activity.class);
        } else if (b.getString("type").equals("chat")) {
            intent = new Intent(this, ChattingScreen_Activity.class);
        } else if (b.getString("type").equals("bid")) {
            intent = new Intent(this, MainActivity.class);
        }  else if (b.getString("type").equals("hire")) {
            intent = new Intent(this, MainActivity.class);
        } else {
            intent = new Intent(this, MainActivity.class);
        }
        intent.putExtras(b);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
                PendingIntent.FLAG_ONE_SHOT);
        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.ic_launcher)
                .setContentTitle(getResources().getString(R.string.app_name))
                .setContentText(messageBody)
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent);
        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(0 /* ID of notification */, notificationBuilder.build());
    }

}
