package com.h_plus.HP_Service_Provider;

/**
 * Created by Infograins on 1/7/2017.
 */

public class CategoryItems {

    public String id, name;
    public boolean selected;

    public CategoryItems() {

    }

    public CategoryItems(String id, String name, boolean selected) {
        this.id = id;
        this.name = name;
        this.selected = selected;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;

    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
