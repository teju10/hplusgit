package com.h_plus.HP_Service_Provider;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.dpizarro.autolabel.library.AutoLabelUI;
import com.dpizarro.autolabel.library.AutoLabelUISettings;
import com.dpizarro.autolabel.library.Label;
import com.h_plus.R;
import com.h_plus.ServerInteraction.ResponseListener;
import com.h_plus.ServerInteraction.ResponseTask;
import com.h_plus.customwidget.CircleImageView;
import com.h_plus.customwidget.CustomEditText;
import com.h_plus.customwidget.CustomTextView;
import com.h_plus.usertabs.utility.Constant_Urls;
import com.h_plus.usertabs.utility.Constants;
import com.h_plus.usertabs.utility.MultipartUtility;
import com.h_plus.usertabs.utility.Utility;
import com.soundcloud.android.crop.Crop;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Infograins on 1/16/2017.
 */

public class AddPortfolioActivity extends AppCompatActivity implements View.OnClickListener {

    Context mContext;
    Dialog skills_dialog;
    ResponseTask rt;
    ArrayList<CategoryItems> skills_array = new ArrayList<>();
    AdapDialog_SkillsList dialod_skillsadap;
    ArrayList<String> ids = new ArrayList<>();
    CustomTextView Skills_tv;
    CategoryItems categoryItems;
    String PicturePath = "";
    CustomEditText ap_title, ap_title_desc;
    Bundle b;
    String portfoliostr = "";
    String TAG = getClass().getSimpleName();
    CircleImageView portfolio_img;
    String skillstxt="";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.sp_addportfolio_activity);
        mContext = this;
        setSupportActionBar((Toolbar) findViewById(R.id.h_toolbar));
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        ((CustomTextView) findViewById(R.id.toolbar_title)).setText("Add Portfolio");

        Skills_tv = (CustomTextView) findViewById(R.id.Skills_tv);

        FIND();
        BUTTONCLICK();
        INIT();

    }

    public void FIND() {
        ap_title = (CustomEditText) findViewById(R.id.ap_title);
        ap_title_desc = (CustomEditText) findViewById(R.id.ap_title_desc);
        portfolio_img = (CircleImageView) findViewById(R.id.portfolio_img);
    }

    public void BUTTONCLICK() {
        findViewById(R.id.skills_btn).setOnClickListener(this);
        findViewById(R.id.ap_upload_file).setOnClickListener(this);
        findViewById(R.id.add_port_btn).setOnClickListener(this);
    }

    public void INIT() {

        try {
            b = new Bundle();
            b = getIntent().getExtras();
            if (b!=null) {
                portfoliostr = b.getString("JSONOBJECT");
                System.out.println(TAG + " JSONOBJECT " + portfoliostr);
                try {
                    JSONObject j = new JSONObject(portfoliostr);
                    ap_title.setText(j.getString("title"));
                    ap_title_desc.setText(j.getString("desc"));
                    Glide.with(mContext).load(j.getString("file")).into(portfolio_img);
                    JSONArray jsonArray = j.getJSONArray("skill");
                    for (int i = 0; i < jsonArray.length(); i++) {
                        ids.add(jsonArray.getJSONObject(i).getString("id"));
                    }

                } catch (JSONException je) {
                    je.printStackTrace();

                }
            }
        } catch (Exception e) {
            e.printStackTrace();

        }
        GetSkillsTask();
    }

    private void Skills_Dialog() {
        skills_dialog = new Dialog(mContext);
        skills_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        skills_dialog.setCancelable(false);
        skills_dialog.setCanceledOnTouchOutside(false);
        skills_dialog.setContentView(R.layout.dialog_select_profile_skills);
        skills_dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        ImageView cancel_skills_dialog, add_skills;

        add_skills = (ImageView) skills_dialog.findViewById(R.id.add_skills);
        cancel_skills_dialog = (ImageView) skills_dialog.findViewById(R.id.cancel_skills_dialog);

        dialod_skillsadap = new AdapDialog_SkillsList(mContext, R.layout.dialog_select_skills_list, skills_array);
        ((ListView) skills_dialog.findViewById(R.id.skills_list)).setAdapter(dialod_skillsadap);

        add_skills.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                skillstxt="";
                ids = new ArrayList<>();
                for (int i = 0; i < skills_array.size(); i++) {
                    if (skills_array.get(i).isSelected()) {
                        ids.add(skills_array.get(i).getId());
                        skillstxt=skillstxt+", "+skills_array.get(i).getName();
                        Skills_tv.setText(skillstxt.substring(1));
                    }
                }

                skills_dialog.dismiss();
            }
        });

        cancel_skills_dialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                skills_dialog.cancel();
            }
        });

        skills_dialog.show();
    }

    public void GetSkillsTask() {
        try {
            JSONObject j = new JSONObject();
            j.put(Constants.ACTION, Constants.CATEGORYLIST);
            Utility.ShowLoading(mContext, getResources().getString(R.string.please_wait));

            rt = new ResponseTask(Constant_Urls.SERVER_URL, j);
            rt.setListener(new ResponseListener() {
                @Override
                public void onGetPickSuccess(String result) {
                    Utility.HideDialog();
                    try {
                        JSONObject skills = new JSONObject(result);
                        if (result == null) {
                            Utility.showToast(mContext, "server not responding");
                        } else if (skills.getString("success").equals("1")) {

                            JSONArray jsonArray = skills.getJSONArray("object");
                            skillstxt="";
                            for (int i = 0; i < jsonArray.length(); i++) {
                                categoryItems = new CategoryItems();
                                categoryItems.setName(jsonArray.getJSONObject(i).getString("name"));
                                categoryItems.setId(jsonArray.getJSONObject(i).getString("id"));
                                boolean status = false;
                                for (int j = 0; j < ids.size(); j++) {
                                    if (jsonArray.getJSONObject(i).getString("id").equals(ids.get(j))) {
                                        status = true;
                                        skillstxt=skillstxt+", "+jsonArray.getJSONObject(i).getString("name");
                                        break;
                                    }
                                }

                                categoryItems.setSelected(status);
                                skills_array.add(categoryItems);
                            }
                            Skills_tv.setText(skillstxt.substring(1));
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            });
            rt.execute();
        } catch (JSONException j) {
            j.printStackTrace();
        }

    }

    public class SendSelectedSkillsTask extends AsyncTask<String, String, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            Utility.ShowLoading(mContext, mContext.getResources().getString(R.string.please_wait));
        }

        @Override
        protected String doInBackground(String... params) {
            String single_skills = ids.toString();
            single_skills = single_skills.substring(1, single_skills.length() - 1);
            single_skills = single_skills.replaceAll(", ", ",");
            try {
                MultipartUtility multipart = new MultipartUtility(Constant_Urls.SERVER_URL, "UTF-8");
                multipart.addFormField(Constants.ACTION, Constants.ADDPORTFOLIO);
                multipart.addFormField("user_id", Utility.getSharedPreferences(mContext,Constants.USERID));
                multipart.addFormField("type", "image");
                multipart.addFormField("title", params[0]);
                multipart.addFormField("desc", params[1]);
                multipart.addFilePart("file", params[2]);
                multipart.addFormField("skill",single_skills);
                return multipart.finish();
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        }

        @Override
        protected void onPostExecute(String result) {
            Utility.HideDialog();
            if (result == null) {
                Utility.ShowToastMessage(mContext, getResources().getString(R.string.server_fail));
            } else {
                try {
                    JSONObject json = new JSONObject(result);
                    Log.e("result result", "result result ============= " + json);
                    if (json.getString("success").equals("1")) {
                        Utility.showToast(mContext, "Portfolio added successfully");
                        ap_title.setText("");
                        ap_title_desc.setText("");
                        finish();
                    } else {
                        Utility.ShowToastMessage(mContext, json.getString("msg"));
                    }
                } catch (JSONException e) {

                }
            }
            super.onPostExecute(result);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.skills_btn:
                System.out.println("size is list========="+skills_array.size());
                Skills_Dialog();
            break;

            case R.id.ap_upload_file:
                selectImage();
            break;

            case R.id.add_port_btn:
                if (ap_title.getText().toString().equals("")) {
                    Utility.showToast(mContext, "please fill out the title of portfolio");
                } else if (ap_title_desc.getText().toString().equals("")) {
                    Utility.showToast(mContext, "please fill out the description of portfolio");
                } else {
                    if (Utility.isConnectingToInternet(mContext)) {
                        new SendSelectedSkillsTask().execute(ap_title.getText().toString(), ap_title_desc.getText().toString(), PicturePath);
                    } else {
                        Utility.showPromptDlgForSuccess(mContext, "", "Please check your network connection!");
                    }
                }
            break;
            default:
                break;
        }
    }

    public void selectImage() {
        final CharSequence[] options = {"From Camera", "From Gallery", "Close"};
        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(mContext);
        builder.setTitle("Add your image");
        builder.setItems(options, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (options[item].equals("From Camera")) {
                    Uri outputFileUri = getCaptureImageOutputUri();
                    Intent intent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                    // File f = new File(Environment.getExternalStorageDirectory(), "temp.jpg");
                    intent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri);
                    startActivityForResult(intent, 1);
                } else if (options[item].equals("From Gallery")) {
                    Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    startActivityForResult(intent, 2);
                } else if (options[item].equals("Close")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == 1) {
                Uri imageUri = getPickImageResultUri(data);
                beginCrop(imageUri);
            } else if (requestCode == 2) {
                Uri selectedImage = data.getData();
                beginCrop(selectedImage);
            } else if (requestCode == Crop.REQUEST_CROP) {
                handleCrop(resultCode, data);
            }
        }
    }

    public Uri getPickImageResultUri(Intent data) {
        boolean isCamera = true;
        if (data != null && data.getData() != null) {
            String action = data.getAction();
            isCamera = action != null && action.equals(MediaStore.ACTION_IMAGE_CAPTURE);
        }
        return isCamera ? getCaptureImageOutputUri() : data.getData();
    }

    private Uri getCaptureImageOutputUri() {
        Uri outputFileUri = null;
        File getImage = mContext.getExternalCacheDir();
        if (getImage != null) {
            outputFileUri = Uri.fromFile(new File(getImage.getPath(), "pickImageResult.jpeg"));
        }
        return outputFileUri;
    }

    private void beginCrop(Uri source) {
        Uri destination = Uri.fromFile(new File(mContext.getCacheDir(), System.currentTimeMillis() + "cropped.jpeg"));
        Crop.of(source, destination).asSquare().start(this);
    }

    private void handleCrop(int resultCode, Intent result) {
        if (resultCode == RESULT_OK) {

            File f = new File(Crop.getOutput(result).getPath());
            Bitmap roteted = Utility.loadBitmap(Crop.getOutput(result).toString());
            roteted = Utility.rotateImage(roteted, f);
            String croped_file = "" + Crop.getOutput(result).getPath();
            PicturePath = croped_file;
            //PicturePath = croped_file;
          /*  BitmapFactory.Options bmOptions = new BitmapFactory.Options();
            Bitmap bitmap = BitmapFactory.decodeFile(f.getAbsolutePath(), bmOptions);
            bitmap = Bitmap.createScaledBitmap(bitmap, 300, 300, true);
            bitmap = Utility.rotateImage(bitmap, f);*/
            Log.e("TEJAS ", "croped_file ===========> " + PicturePath);

            Glide.with(mContext).load(PicturePath).into(portfolio_img);
        } else if (resultCode == Crop.RESULT_ERROR) {
            Toast.makeText(mContext, Crop.getError(result).getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menu) {
        switch (menu.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(menu);

        }
    }

    class AdapDialog_SkillsList extends BaseAdapter {
        Context mContext;
        ArrayList<CategoryItems> list = new ArrayList<>();
        int res;

        public AdapDialog_SkillsList(Context mContext, int res, ArrayList<CategoryItems> list) {
            this.mContext = mContext;
            this.list = list;
            this.res = res;

        }

        @Override
        public int getCount() {
            return list.size();
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            final ViewHolder viewHolder;

            if (convertView == null) {
                LayoutInflater li = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = li.inflate(res, null);
                viewHolder = new ViewHolder(convertView);

                convertView.setTag(viewHolder);
            } else {
                viewHolder = (ViewHolder) convertView.getTag();
            }
            CategoryItems j = list.get(position);
            viewHolder.skills_txt.setText(j.getName());
            viewHolder.skills_chkbx.setChecked(j.isSelected());
            viewHolder.check_uncheck.setOnClickListener(new AdapDialog_SkillsList.MyClick(position));

            return convertView;
        }

        /*public void setItemSelected(int position, boolean isSelected) {
            if (position != -1) {
                skills_array.get(position).setSelected(isSelected);
                notifyDataSetChanged();
            }
        }*/

        class ViewHolder {
            CustomTextView skills_txt;
            CheckBox skills_chkbx;
            View check_uncheck;

            public ViewHolder(View v) {
                skills_chkbx = (CheckBox) v.findViewById(R.id.skills_chkbx);
                skills_txt = (CustomTextView) v.findViewById(R.id.skills_txt);
                check_uncheck = (View) v.findViewById(R.id.check_uncheck);
            }

        }


        public class MyClick implements View.OnClickListener {
            int position;

            public MyClick(int pos) {
                this.position = pos;
            }

            @Override
            public void onClick(View v) {
                if (skills_array.get(position).isSelected()) {
                    CategoryItems categoryItems = skills_array.get(position);
                    categoryItems.setSelected(false);
                    skills_array.remove(position);
                    skills_array.add(position, categoryItems);
                } else {
                    CategoryItems categoryItems = skills_array.get(position);
                    categoryItems.setSelected(true);
                    skills_array.remove(position);
                    skills_array.add(position, categoryItems);
                }
                notifyDataSetChanged();
            }
        }
    }
}
