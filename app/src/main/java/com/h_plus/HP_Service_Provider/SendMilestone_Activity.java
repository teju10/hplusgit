package com.h_plus.HP_Service_Provider;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.SimpleAdapter;
import android.widget.Spinner;
import android.widget.TableRow;

import com.h_plus.HP_Service_Provider.fragment.ShowMilestone_History_Activity;
import com.h_plus.R;
import com.h_plus.ServerInteraction.ResponseListener;
import com.h_plus.ServerInteraction.ResponseTask;
import com.h_plus.customwidget.CustomEditText;
import com.h_plus.customwidget.CustomTextView;
import com.h_plus.usertabs.utility.Constant_Urls;
import com.h_plus.usertabs.utility.Constants;
import com.h_plus.usertabs.utility.Utility;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Infograins on 1/2/2017.
 */

public class SendMilestone_Activity extends AppCompatActivity {
    Context mContext;
    Dialog sndmilestone_diolog;
    Spinner request_milestone_spinner;
    String[] requestmilestone_array;
    ArrayList<HashMap<String, String>> milestone_request;
    String str_requestmilestone = "", ProjectId;
    ResponseTask rt;
    Bundle b;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mstone_phistory);
        mContext = this;

        setSupportActionBar((Toolbar) findViewById(R.id.h_toolbar));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        ((CustomTextView) findViewById(R.id.toolbar_title)).setText("Project Detail");
        if (Utility.isConnectingToInternet(mContext)) {
            GetProjectDetail();
        } else {
            Utility.showToast(mContext, "please check your network connection");
        }
        request_milestone_spinner = (Spinner) findViewById(R.id.request_milestone_spinner);

        if (Utility.getSharedPreferences(mContext, Constants.USER_TYPE).equals("1")) {
            ((TableRow) findViewById(R.id.row_select_action)).setVisibility(View.GONE);
            // requestmilestone_array = mContext.getResources().getStringArray(R.array.Hire_types_of_milestone);

        }else if (Utility.getSharedPreferences(mContext, Constants.USER_TYPE).equals("2")) {
            requestmilestone_array = mContext.getResources().getStringArray(R.array.SP_types_of_milestone);
            getcurrency_spinner();
        }

        if (Utility.getSharedPreferences(mContext, Constants.USER_TYPE).equals("1")) {
            ((CustomTextView) findViewById(R.id.view_milestone_history)).setText("Requested Milestone");
            findViewById(R.id.view_milestone_history).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent(mContext, ShowMilestone_History_Activity.class);
                    b = new Bundle();
                    b.putString("project_id", ProjectId);
                    i.putExtras(b);
                    startActivity(i);
                }
            });

        } else if (Utility.getSharedPreferences(mContext, Constants.USER_TYPE).equals("2")) {
            ((CustomTextView) findViewById(R.id.view_milestone_history)).setText("Milestone History");
            findViewById(R.id.view_milestone_history).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent(mContext, ShowMilestone_History_Activity.class);
                    b = new Bundle();
                    b.putString("project_id", ProjectId);
                    i.putExtras(b);
                    startActivity(i);
                }
            });
        }

    }

    public void SendMilestonedialog() {

        sndmilestone_diolog = new Dialog(mContext);
        sndmilestone_diolog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        sndmilestone_diolog.setCancelable(false);
        sndmilestone_diolog.setCanceledOnTouchOutside(false);
        sndmilestone_diolog.setContentView(R.layout.dialog_select_milestone_request);
        sndmilestone_diolog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        sndmilestone_diolog.findViewById(R.id.request_milestone_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (((CustomEditText) sndmilestone_diolog.findViewById(R.id.milestone_amount)).getText().toString().equals("")) {
                    Utility.showToast(mContext, "please enter your amount");
                } else {
                    RequestMilestoneTask(((CustomEditText) sndmilestone_diolog.findViewById(R.id.milestone_amount)).getText().toString());
                }
            }
        });
        sndmilestone_diolog.findViewById(R.id.cancel_send_milestone_dialog).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sndmilestone_diolog.cancel();
            }
        });

        sndmilestone_diolog.show();
    }

    public void getcurrency_spinner() {

        milestone_request = new ArrayList<>();
        for (int i = 0; i < requestmilestone_array.length; i++) {
            HashMap<String, String> hm = new HashMap<>();
            hm.put("Type", requestmilestone_array[i]);
            milestone_request.add(hm);
        }
        int[] to = new int[]{R.id.s_item};
        String[] from = new String[]{"Type"};
        // Creating a SimpleAdapter for the Spinner
        SimpleAdapter adapter = new SimpleAdapter(mContext, milestone_request, R.layout.text_single_spinner, from, to);
        request_milestone_spinner.setAdapter(adapter);

        request_milestone_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position > 0) {
                    str_requestmilestone = milestone_request.get(position).get("Type").toString();

                } else {
                    //  Utility.showToast(mContext, "please select requested milestone");
                }
                if ((Utility.getSharedPreferences(mContext, Constants.USER_TYPE).equals("2") && position == 1)) {
                    SendMilestonedialog();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

    }

    public void RequestMilestoneTask(String amount) {

        /*
https://infograins.com/INFO01/homesplus/api/api.php?action=sendMilestone&project_id=&user_id&requested_amount=20*/
        try {
            JSONObject j = new JSONObject();
            j.put(Constants.ACTION, Constants.CREATEMILESTONE);
            j.put("project_id", Utility.getSharedPreferences(mContext, Constants.PROJECT_ID));
            j.put("user_id", Utility.getSharedPreferences(mContext, Constants.USERID));
            j.put("requested_amount", amount);

            Utility.ShowLoading(mContext, getResources().getString(R.string.please_wait));

            rt = new ResponseTask(Constant_Urls.SERVER_URL, j);
            rt.setListener(new ResponseListener() {
                @Override
                public void onGetPickSuccess(String result) {
                    Utility.HideDialog();
                    if (result == null) {
                        Utility.ShowToastMessage(mContext, "sever not responding");
                    } else {
                        try {
                            JSONObject jo = new JSONObject(result);
                            if (jo.getString("success").equals("1")) {
                                Utility.showToast(mContext, "Milestone request has been created successfully");
                                sndmilestone_diolog.dismiss();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
            });
            rt.execute();
        } catch (JSONException j) {
            j.printStackTrace();
        }
    }

    public void GetProjectDetail() {
        /*
https://infograins.com/INFO01/homesplus/api/api.php?action=milestoneDetailsForService&project_id=&user_id&requested_amount=20*/
        try {
            JSONObject j = new JSONObject();
            j.put(Constants.ACTION, Constants.GET_SP_MILESTONE_PROJECT_DETAIL);
            j.put("project_id", Utility.getSharedPreferences(mContext, Constants.PROJECT_ID));
            j.put("user_id", Utility.getSharedPreferences(mContext, Constants.USERID));

            Utility.ShowLoading(mContext, getResources().getString(R.string.please_wait));

            rt = new ResponseTask(Constant_Urls.SERVER_URL, j);
            rt.setListener(new ResponseListener() {
                @Override
                public void onGetPickSuccess(String result) {
                    Utility.HideDialog();
                    if (result == null) {
                        Utility.ShowToastMessage(mContext, "sever not responding");
                    } else {
                        try {
                            JSONObject jo = new JSONObject(result);
                            if (jo.getString("success").equals("1")) {

                                JSONObject j = jo.getJSONObject("object");
                                ProjectId = j.getString("project_id");
                                ((CustomTextView) findViewById(R.id.smilestone_pname)).setText(j.getString("project_name"));
                                if (Utility.getSharedPreferences(mContext, Constants.USER_TYPE).equals("1")) {
                                    ((CustomTextView) findViewById(R.id.hired_to)).setText("Hired to");
                                    ((CustomTextView) findViewById(R.id.smilestone_hiredname)).setText(j.getString("provider_name"));
                                } else {
                                    ((CustomTextView) findViewById(R.id.hired_to)).setText("Hired by");
                                    ((CustomTextView) findViewById(R.id.smilestone_hiredname)).setText(j.getString("client_name"));
                                }
                                ((CustomTextView) findViewById(R.id.smilestone_awarded_bid)).setText(j.getString("bid_amount"));
                                ((CustomTextView) findViewById(R.id.smilestone_releasedamount)).setText(j.getString("released_amount"));
                                if (j.getString("status").equals("1")) {
                                    ((CustomTextView) findViewById(R.id.smilestone_project_status)).setText("Hired");
                                } else if (j.getString("status").equals("2")) {
                                    ((CustomTextView) findViewById(R.id.smilestone_project_status)).setText("Awarded");
                                } else if (j.getString("status").equals("3")) {
                                    ((CustomTextView) findViewById(R.id.smilestone_project_status)).setText("Completed");
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
            });
            rt.execute();
        } catch (JSONException j) {
            j.printStackTrace();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menu) {

        switch (menu.getItemId()) {

            case android.R.id.home:
               finish();
                return true;
            default:
                return super.onOptionsItemSelected(menu);

        }
    }


}
