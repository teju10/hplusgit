package com.h_plus.HP_Service_Provider.fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.h_plus.HP_Service_Provider.AddPortfolioActivity;
import com.h_plus.R;
import com.h_plus.ServerInteraction.ResponseListener;
import com.h_plus.ServerInteraction.ResponseTask;
import com.h_plus.usertabs.utility.Constant_Urls;
import com.h_plus.usertabs.utility.Constants;
import com.h_plus.usertabs.utility.Utility;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Infograins on 1/16/2017.
 */

public class AddPortfolioFragment extends Fragment {

    Context mContext;
    View v;
    ArrayList<JSONObject> img;
    HorizontalListAdapter horizontalListAdapter;
    ResponseTask rt;
    RecyclerView sp_horizontal_certificatelist;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.sp_showportfolioscreen_fragment, container, false);

        mContext = getActivity();
        LinearLayoutManager horizontalLayoutManagaer = new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false);
        sp_horizontal_certificatelist = (RecyclerView) v.findViewById(R.id.sp_horizontal_certificatelist);
        sp_horizontal_certificatelist.setLayoutManager(horizontalLayoutManagaer);

        v.findViewById(R.id.add_port_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(mContext, AddPortfolioActivity.class));
            }
        });

        return v;
    }

    @Override
    public void onResume() {
        super.onResume();
        GetPortfolioTask();
    }

    public void GetPortfolioTask() {

        JSONObject j = new JSONObject();
        try {
            j.put(Constants.ACTION, Constants.SHOWPORTFOLIO);
            j.put("user_id", Utility.getSharedPreferences(mContext, Constants.USERID));
            rt = new ResponseTask(Constant_Urls.SERVER_URL, j);
            rt.setListener(new ResponseListener() {
                @Override
                public void onGetPickSuccess(String result) {
                    try {

                        JSONObject jo = new JSONObject(result);
                        if (jo.getString("success").equals("1")) {
                            JSONArray jsonArray = jo.getJSONArray("object");
                            img = new ArrayList<JSONObject>();
                            for (int i = 0; i < jsonArray.length(); i++) {
                                img.add(jsonArray.getJSONObject(i));
                            }
                            horizontalListAdapter = new HorizontalListAdapter(mContext, R.layout.horizontalimagelist_adapter, img);
                            sp_horizontal_certificatelist.setAdapter(horizontalListAdapter);
                        } else {
                            Utility.ShowToastMessage(mContext, jo.getString("msg"));
                        }
                    } catch (JSONException j) {
                        j.printStackTrace();
                    }
                }
            });
            rt.execute();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    class HorizontalListAdapter extends RecyclerView.Adapter<HorizontalListAdapter.ViewHolder> {

        Context mContext;
        int res;
        ArrayList<JSONObject> imageArrayList;

        public HorizontalListAdapter(Context context, int resourse, ArrayList<JSONObject> imageList) {
            this.mContext = context;
            this.res = resourse;
            this.imageArrayList = imageList;
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View convertView = LayoutInflater.from(parent.getContext()).inflate(res, parent, false);
            return new ViewHolder(convertView);
        }

        @Override
        public void onBindViewHolder(final ViewHolder holder, int position) {
            JSONObject j = imageArrayList.get(position);
            try {
                Glide.with(mContext)
                        .load(j.getString("file"))
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .listener(new RequestListener<String, GlideDrawable>() {
                            @Override
                            public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                                return false;
                            }

                            @Override
                            public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                                (holder.hori_progress_bar).setVisibility(View.GONE);
                                return false;
                            }
                        })
                        .into(holder.hori_certificate_image);
            } catch (JSONException je) {
                je.printStackTrace();
            }
            holder.hori_certificate_image.setOnClickListener(new onclick(position));
            holder.cancel_img.setOnClickListener(new DeletePortfolioClick(position));

        }

        @Override
        public int getItemCount() {
            return imageArrayList.size();
        }

        public void RemovePortfolio(final String portfolio_id, final int pos) {
            try {

                JSONObject jsonObject = new JSONObject();
                jsonObject.put(Constants.ACTION, "deletePortfolioByid");
                jsonObject.put("user_id", Utility.getSharedPreferences(mContext, Constants.USERID));
                jsonObject.put("portfolio_id", portfolio_id);
                Utility.ShowLoading(mContext, mContext.getResources().getString(R.string.please_wait));
                rt = new ResponseTask(Constant_Urls.SERVER_URL, jsonObject);
                rt.execute();
                rt.setListener(new ResponseListener() {
                    @Override
                    public void onGetPickSuccess(String result) {
                        Utility.HideDialog();
                        if (result == null) {
                            Utility.ShowToastMessage(mContext, mContext.getResources().getString(R.string.server_fail));
                        } else {
                            try {
                                JSONObject json = new JSONObject(result);
                                if (json.getString("success").equals("1")) {
                                    imageArrayList.remove(pos);
                                    notifyDataSetChanged();
                                } else {
                                    Utility.ShowToastMessage(mContext, json.getString("msg"));
                                    //   Utility.ShowToastMessage(appContext, appContext.getResources().getString(R.string.packunavailable));
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }
                });
            } catch (JSONException j) {
                j.printStackTrace();
            }
        }

        class onclick implements View.OnClickListener {
            int position;

            public onclick(int pos) {
                position = pos;
            }

            @Override
            public void onClick(View view) {

                try {
                    Bundle b = new Bundle();
                    Intent i = new Intent(getActivity(), AddPortfolioActivity.class);
                    b.putString("JSONOBJECT", imageArrayList.get(position).toString());
                    i.putExtras(b);
                    startActivity(i);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }

        class DeletePortfolioClick implements View.OnClickListener {
            int position;

            public DeletePortfolioClick(int pos) {
                position = pos;
            }

            @Override
            public void onClick(View view) {

                try {
                    RemovePortfolio(imageArrayList.get(position).getString("portfolio_id"),position);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }

        class ViewHolder extends RecyclerView.ViewHolder {

            ImageView hori_certificate_image, cancel_img;
            ProgressBar hori_progress_bar;
            TextView certified_name;

            public ViewHolder(View itemView) {
                super(itemView);
                hori_certificate_image = (ImageView) itemView.findViewById(R.id.product_img);
                cancel_img = (ImageView) itemView.findViewById(R.id.cancel_img);
                certified_name = (TextView) itemView.findViewById(R.id.certified_name);
                hori_progress_bar = (ProgressBar) itemView.findViewById(R.id.hori_progress_bar);
            }
        }
    }
}
