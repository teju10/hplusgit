package com.h_plus.HP_Service_Provider.fragment;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.h_plus.R;
import com.h_plus.ServerInteraction.ResponseListener;
import com.h_plus.ServerInteraction.ResponseTask;
import com.h_plus.usertabs.utility.Constant_Urls;
import com.h_plus.usertabs.utility.Constants;
import com.h_plus.usertabs.utility.MultipartUtility;
import com.h_plus.usertabs.utility.Utility;
import com.soundcloud.android.crop.Crop;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


/**
 * Created by Infograins on 1/6/2017.
 */

public class Port_CertificateFragment extends Fragment {
    Context mContext;
    View v;
    ResponseTask rt;
    HorizontalListAdapter horizontalListAdapter;
    ArrayList<HashMap<String, String>> h_certified_array = new ArrayList<>();
    RecyclerView sp_horizontal_certificatelist;

    String PicturePath = "";
    String TAG = getClass().getSimpleName();
    ArrayList<HashMap<String, String>> imgs;


    @Override
    public View onCreateView(LayoutInflater inflater,  ViewGroup container, Bundle savedInstanceState) {
        mContext = getActivity();
        v = inflater.inflate(R.layout.fragment_showcertificate, container, false);

        LinearLayoutManager horizontalLayoutManagaer = new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false);

        sp_horizontal_certificatelist = (RecyclerView) v.findViewById(R.id.sp_horizontal_certificatelist);
        sp_horizontal_certificatelist.setLayoutManager(horizontalLayoutManagaer);
        if (Utility.isConnectingToInternet(mContext)) {
            GetSelectedSkillsTask();
        } else {
            Utility.showPromptDlgForSuccess(mContext, "", mContext.getResources().getString(R.string.please_wait));
        }

        v.findViewById(R.id.add_certificate).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectImage();
                //Utility.showToast(mContext,"coming soon");
            }
        });

        return v;
    }

    public void selectImage() {
        final CharSequence[] options = {"From Camera", "From Gallery", "Close"};
        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(mContext);
        builder.setTitle("Add your image");
        builder.setItems(options, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (options[item].equals("From Camera")) {
                    Uri outputFileUri = getCaptureImageOutputUri();
                    Intent intent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                    // File f = new File(Environment.getExternalStorageDirectory(), "temp.jpg");
                    intent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri);
                    startActivityForResult(intent, 1);
                } else if (options[item].equals("From Gallery")) {
                    Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    startActivityForResult(intent, 2);
                } else if (options[item].equals("Close")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == 1) {
                Uri imageUri = getPickImageResultUri(data);
                beginCrop(imageUri);
            } else if (requestCode == 2) {
                Uri selectedImage = data.getData();
                beginCrop(selectedImage);
            } else if (requestCode == Crop.REQUEST_CROP) {
                handleCrop(resultCode, data);
            }
        }
    }

    public Uri getPickImageResultUri(Intent data) {
        boolean isCamera = true;
        if (data != null && data.getData() != null) {
            String action = data.getAction();
            isCamera = action != null && action.equals(MediaStore.ACTION_IMAGE_CAPTURE);
        }
        return isCamera ? getCaptureImageOutputUri() : data.getData();
    }

    private Uri getCaptureImageOutputUri() {
        Uri outputFileUri = null;
        File getImage = mContext.getExternalCacheDir();
        if (getImage != null) {
            outputFileUri = Uri.fromFile(new File(getImage.getPath(), "pickImageResult.jpeg"));
        }
        return outputFileUri;
    }

    private void beginCrop(Uri source) {
        Uri destination = Uri.fromFile(new File(mContext.getCacheDir(), System.currentTimeMillis() + "cropped.jpeg"));
        Crop.of(source, destination).asSquare().start(mContext, this);
    }

    private void handleCrop(int resultCode, Intent result) {
        if (resultCode == Activity.RESULT_OK) {

            File f = new File(Crop.getOutput(result).getPath());
            Bitmap roteted = Utility.loadBitmap(Crop.getOutput(result).toString());
            roteted = Utility.rotateImage(roteted, f);
            String croped_file = "" + Crop.getOutput(result).getPath();
            PicturePath = croped_file;
            //PicturePath = croped_file;
          /*  BitmapFactory.Options bmOptions = new BitmapFactory.Options();
            Bitmap bitmap = BitmapFactory.decodeFile(f.getAbsolutePath(), bmOptions);
            bitmap = Bitmap.createScaledBitmap(bitmap, 300, 300, true);
            bitmap = Utility.rotateImage(bitmap, f);*/
            Log.e(TAG, "croped_file ===========> " + PicturePath);
            if (Utility.isConnectingToInternet(mContext)) {
                new UploadProPic().execute(PicturePath);
            } else {
                Utility.showCroutonWarn("Please check your internet connection", getActivity());
            }

        } else if (resultCode == Crop.RESULT_ERROR) {
            Toast.makeText(getActivity(), Crop.getError(result).getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    private void SetBusPics() {
        imgs = new ArrayList<>();
        for (int i = 0; i < h_certified_array.size(); i++) {
            HashMap<String, String> img = new HashMap<>();
            img.put("file", h_certified_array.get(i).get("file"));
            imgs.add(img);
        }
        horizontalListAdapter = new HorizontalListAdapter(mContext, R.layout.horizontalimagelist_adapter, imgs);
        sp_horizontal_certificatelist.setAdapter(horizontalListAdapter);
    }

    public void GetSelectedSkillsTask() {

        try {
            JSONObject jk = new JSONObject();
            jk.put(Constants.ACTION, Constants.CERITIFICATELIST);
            jk.put("user_id", Utility.getSharedPreferences(mContext, Constants.USERID));

            Utility.ShowLoading(mContext, getResources().getString(R.string.please_wait));

            rt = new ResponseTask(Constant_Urls.SERVER_URL, jk);
            rt.setListener(new ResponseListener() {
                @Override
                public void onGetPickSuccess(String result) {
                    Utility.HideDialog();
                    try {
                        JSONObject skills = new JSONObject(result);
                        if (result == null) {
                            Utility.showToast(mContext, "server not responding");
                        } else if (skills.getString("success").equals("1")) {
                            JSONArray jsonArray = skills.getJSONArray("object");
                            for (int i = 0; i < jsonArray.length(); i++) {
                                HashMap<String, String> img = new HashMap<String, String>();
                                img.put("file", jsonArray.getJSONObject(i).getString("file"));
                                img.put("certificate_id", jsonArray.getJSONObject(i).getString("certificate_id"));
                                h_certified_array.add(img);
                                SetBusPics();
                                // h_certified_array.add(jsonArray.getJSONObject(i).getString("file"));
//                                imgs.add(jsonArray.getJSONObject(i).getString("file"));
                            }
                            horizontalListAdapter = new HorizontalListAdapter(mContext, R.layout.horizontalimagelist_adapter, h_certified_array);
                            sp_horizontal_certificatelist.setAdapter(horizontalListAdapter);
                        } else {

                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            });
            rt.execute();
        } catch (JSONException j) {
            j.printStackTrace();
        }

    }

    public class UploadProPic extends AsyncTask<String, String, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            Utility.ShowLoading(mContext, mContext.getResources().getString(R.string.please_wait));
        }

        @Override
        protected String doInBackground(String... params) {
            try {
                MultipartUtility multipart = new MultipartUtility(Constant_Urls.SERVER_URL, "UTF-8");
                multipart.addFormField(Constants.ACTION, "addCertificate");
                multipart.addFilePart("file", params[0]);
                multipart.addFormField("title", "HomePlus");
                multipart.addFormField("user_id", Utility.getSharedPreferences(mContext, Constants.USERID));
                return multipart.finish();
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        }

        @Override
        protected void onPostExecute(String result) {
            Utility.HideDialog();
            if (result == null) {
                Utility.ShowToastMessage(mContext, getResources().getString(R.string.server_fail));
            } else {
                try {
                    JSONObject json = new JSONObject(result);
                    Log.e("result result", "result result ============= " + json);
                    if (json.getString("success").equals("1")) {
                        HashMap<String, String> img = new HashMap<String, String>();
                        img.put("file", json.getString("file"));
                        h_certified_array.add(img);
                        SetBusPics();
                    } else {
                        Utility.ShowToastMessage(mContext, json.getString("msg"));
                    }
                } catch (JSONException e) {

                }
            }
            super.onPostExecute(result);
        }
    }

    class HorizontalListAdapter extends RecyclerView.Adapter<HorizontalListAdapter.ViewHolder> {

        Context mContext;
        int res;
        ArrayList<HashMap<String, String>> imageArrayList;

        public HorizontalListAdapter(Context context, int resourse, ArrayList<HashMap<String, String>> imageList) {
            this.mContext = context;
            this.res = resourse;
            this.imageArrayList = imageList;
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View convertView = LayoutInflater.from(parent.getContext()).inflate(res, parent, false);
            return new ViewHolder(convertView);
        }

        @Override
        public void onBindViewHolder(final ViewHolder holder, int position) {
            try {
                Glide.with(mContext)
                        .load(imageArrayList.get(position).get("file"))
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .listener(new RequestListener<String, GlideDrawable>() {
                            @Override
                            public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                                return false;
                            }

                            @Override
                            public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                                (holder.hori_progress_bar).setVisibility(View.GONE);
                                return false;
                            }
                        })
                        .into(holder.hori_certificate_image);
            } catch (Exception j) {
                j.printStackTrace();
            }
            holder.cancel_img.setOnClickListener(new onclick(position));

        }


        @Override
        public int getItemCount() {
            return imageArrayList.size();
        }

        public void RemoveImage(final String certificate, final int pos) {
            try {

                JSONObject jsonObject = new JSONObject();
                jsonObject.put(Constants.ACTION, "deleteCertificateByid");
                jsonObject.put("user_id", Utility.getSharedPreferences(mContext, Constants.USERID));
                jsonObject.put("certificate_id", certificate);
                Utility.ShowLoading(mContext, mContext.getResources().getString(R.string.please_wait));
                rt = new ResponseTask(Constant_Urls.SERVER_URL, jsonObject);
                rt.execute();
                rt.setListener(new ResponseListener() {
                    @Override
                    public void onGetPickSuccess(String result) {
                        Utility.HideDialog();
                        if (result == null) {
                            Utility.ShowToastMessage(mContext, mContext.getResources().getString(R.string.server_fail));
                        } else {
                            try {
                                JSONObject json = new JSONObject(result);
                                if (json.getString("success").equals("1")) {

                                    imageArrayList.remove(pos);
                                    notifyDataSetChanged();
                                    SetBusPics();
                                } else {
                                    Utility.ShowToastMessage(mContext, json.getString("msg"));
                                    //   Utility.ShowToastMessage(appContext, appContext.getResources().getString(R.string.packunavailable));
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }
                });
            } catch (JSONException j) {
                j.printStackTrace();
            }
        }

        class onclick implements View.OnClickListener {
            int position;

            public onclick(int pos) {
                position = pos;
            }

            @Override
            public void onClick(View view) {

                try {
                    RemoveImage(imageArrayList.get(position).get("certificate_id"), position);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }

        class ViewHolder extends RecyclerView.ViewHolder {

            ImageView hori_certificate_image, cancel_img;
            ProgressBar hori_progress_bar;
            TextView certified_name;

            public ViewHolder(View itemView) {
                super(itemView);
                hori_certificate_image = (ImageView) itemView.findViewById(R.id.product_img);
                cancel_img = (ImageView) itemView.findViewById(R.id.cancel_img);
                certified_name = (TextView) itemView.findViewById(R.id.certified_name);
                hori_progress_bar = (ProgressBar) itemView.findViewById(R.id.hori_progress_bar);
            }
        }
    }

}
