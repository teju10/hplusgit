package com.h_plus.HP_Service_Provider.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.h_plus.R;
import com.h_plus.ServerInteraction.ResponseListener;
import com.h_plus.ServerInteraction.ResponseTask;
import com.h_plus.customwidget.LoadMoreListView;
import com.h_plus.useradapter.Search_Project_Adapter;
import com.h_plus.usertabs.utility.Constant_Urls;
import com.h_plus.usertabs.utility.Constants;
import com.h_plus.usertabs.utility.Utility;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.LinkedList;

/**
 * Created by Infograins on 12/6/2016.
 */

public class Fragment_Search_Project extends Fragment {


    public static String TAG = "Fragment_search";
    Context mContext;
    View rootView;
    ResponseTask responseTask;
    LinkedList<JSONObject> JsonArrayList = new LinkedList<>();
    int index = 0;
    private LoadMoreListView list_v;
    private Search_Project_Adapter search_project_adapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.sp_fragment_searchproject_list_, container, false);
        mContext = getActivity();
        bind();
        init();
        return rootView;
    }

    private void bind() {
        list_v = (LoadMoreListView) rootView.findViewById(R.id.project_list);

    }

    private void init() {
        search_project_adapter = new Search_Project_Adapter(mContext, R.layout.sp_adapter_search_project_item, JsonArrayList);
        list_v.setAdapter(search_project_adapter);
        Get_ListData(index);

        list_v.setOnLoadMoreListener(new LoadMoreListView.OnLoadMoreListener() {
            @Override
            public void onLoadMore() {
                index = index + 1;
                Get_ListData(index);
            }
        });
        Utility.setSharedPreference(mContext, Constants.BACKPRESSEDNAME, Constants.FRAGMENT_HOME);


    }

    private void Get_ListData(int page_index) {
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put(Constants.ACTION, Constants.LISTALLPROJECT);
            jsonObject.put("user_id", Utility.getSharedPreferences(mContext,Constants.USERID));
            jsonObject.put("page", page_index);
            if (index == 0) {
                Utility.ShowLoading(mContext, getResources().getString(R.string.please_wait));
            }
            responseTask = new ResponseTask(Constant_Urls.SERVER_URL, jsonObject);
            responseTask.setListener(new ResponseListener() {

                @Override
                public void onGetPickSuccess(String result) {
                    if (index == 0) {
                        Utility.HideDialog();
                    }
                    if (result == null) {
                        //  Utility.ShowToastMessage(mContext, "sever not responding");
//                        Get_ListData(index);
                    } else {
                        try {
                            if (result == null) {
                                Utility.showToast(mContext, "server not responding");
                            } else {
                                JSONObject jsonObject = new JSONObject(result);
                                if (jsonObject.getString("success").equals("1")) {
                                    JSONArray jsonArray = jsonObject.getJSONArray("object");
                                    Log.e(TAG, "result ===============> " + jsonArray.toString());
                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        JsonArrayList.add(jsonArray.getJSONObject(i));
                                    }
                                    search_project_adapter.notifyDataSetChanged();
                                    if (index == 0) {
                                    } else {
                                        list_v.onLoadMoreComplete();
                                    }
                                } else {
                                    if (index == 0) {
                                        Utility.ShowToastMessage(mContext, "Services is  not Available At This Time");
                                    } else {
                                        Utility.ShowToastMessage(mContext, "No More project Available At This Time");
                                        list_v.onLoadMoreComplete();
                                    }

                                }
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
        responseTask.execute();
    }
}
