package com.h_plus.HP_Service_Provider.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.h_plus.HP_Service_Provider.adapter.AdapterSP_Bidlist;
import com.h_plus.R;
import com.h_plus.ServerInteraction.ResponseListener;
import com.h_plus.ServerInteraction.ResponseTask;
import com.h_plus.customwidget.LoadMoreListView;
import com.h_plus.usertabs.utility.Constant_Urls;
import com.h_plus.usertabs.utility.Constants;
import com.h_plus.usertabs.utility.Utility;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.LinkedList;


/**
 * Created by Tejs on 11/30/2016.
 */
public class MyBidedProject extends Fragment {
    View rootView;
    AdapterSP_Bidlist adapter_spbidlist;
    ResponseTask rt;
    Context mContext;
    LinkedList<JSONObject> mybid_arraylist = new LinkedList<>();
    int index = 0;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.myproject_list_layout, container, false);
        mContext = getActivity();

        Init();
        return rootView;
    }


    private void Init() {
        adapter_spbidlist = new AdapterSP_Bidlist(mContext, R.layout.sp_adapter_awardedproject_fargment, mybid_arraylist );
        ((LoadMoreListView)rootView.findViewById(R.id.myproject_list)).setAdapter(adapter_spbidlist);
        GetMyWorkingProjectList(index);

        ((LoadMoreListView)rootView.findViewById(R.id.myproject_list)).setOnLoadMoreListener(new LoadMoreListView.OnLoadMoreListener() {
            @Override
            public void onLoadMore() {
                index = index + 1;
                GetMyWorkingProjectList(index);
            }
        });
    }

    public void GetMyWorkingProjectList(int page_index) {
        /*https://infograins.com/INFO01/homesplus/api/api.php?action=CurrentProject&user_id=261*/
        try {
            JSONObject jo = new JSONObject();
            jo.put(Constants.ACTION, Constants.SERVICEPROVIDER_BIDLIST);
            jo.put("user_id", Utility.getSharedPreferences(mContext, Constants.USERID));
            jo.put("page", page_index);

            Utility.ShowLoading(mContext, mContext.getResources().getString(R.string.please_wait));
            rt = new ResponseTask(Constant_Urls.SERVER_URL, jo);
            rt.setListener(new ResponseListener() {
                @Override
                public void onGetPickSuccess(String result) {
                    Utility.HideDialog();
                    if (result == null) {
                        Utility.ShowToastMessage(mContext, "sever not responding");
                    } else {
                        try {
                            JSONObject jsonObject = new JSONObject(result);
                            if (jsonObject.getString("success").equals("1")) {
                                JSONArray jsonArray = jsonObject.getJSONArray("object");
                                Log.e("MYPROJECT", "result ===============> " + jsonArray.toString());
                                for (int i = 0; i < jsonArray.length(); i++) {
                                    mybid_arraylist .add(jsonArray.getJSONObject(i));
                                }
                                adapter_spbidlist.notifyDataSetChanged();
                                if (index == 0) {

                                } else {
                                    ((LoadMoreListView)rootView.findViewById(R.id.myproject_list)).onLoadMoreComplete();
                                }
                            } else {
                                if (index == 0) {
                                    Utility.ShowToastMessage(mContext, "You have no more project available");
                                } else {
                                    Utility.ShowToastMessage(mContext, "No More project Available At This Time");
                                    ((LoadMoreListView)rootView.findViewById(R.id.myproject_list)).onLoadMoreComplete();
                                }
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            });
            rt.execute();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

}
