package com.h_plus.HP_Service_Provider.fragment;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.ListView;

import com.dpizarro.autolabel.library.AutoLabelUI;
import com.dpizarro.autolabel.library.AutoLabelUISettings;
import com.dpizarro.autolabel.library.Label;
import com.h_plus.HP_Service_Provider.CategoryItems;
import com.h_plus.R;
import com.h_plus.ServerInteraction.ResponseListener;
import com.h_plus.ServerInteraction.ResponseTask;
import com.h_plus.customwidget.CustomTextView;
import com.h_plus.usertabs.utility.Constant_Urls;
import com.h_plus.usertabs.utility.Constants;
import com.h_plus.usertabs.utility.Utility;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Infograins on 1/6/2017.
 */

public class Port_SkillsFragment extends Fragment {

    View v;
    Context mContext;
    Dialog skills_dialog;
    ResponseTask rt;
    ArrayList<CategoryItems> skills_array = new ArrayList<>();
    AdapDialog_SkillsList dialod_skillsadap;
    ArrayList<String> ids = new ArrayList<>();
    AutoLabelUI SkillsAutoLabelUI;
    CategoryItems categoryItems;
    ArrayList<HashMap<String, String>> s_list;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.sp_port_skillsfragment, null);

        mContext = getActivity();

        SkillsAutoLabelUI = (AutoLabelUI) v.findViewById(R.id.SkillsAutoLabelUI);
        AutoLabelUISettings autoLabelUISettings = new AutoLabelUISettings.Builder()
                .withIconCross(R.drawable.cross)
                .withBackgroundResource(R.drawable.curve_orange_bg)
                .withLabelsClickables(true)
                .withShowCross(true)
                .withTextColor(R.color.white)
                .withTextSize(R.dimen.label_title_size)
                .withLabelPadding(R.dimen._5sdp)
                .build();
        SkillsAutoLabelUI.setSettings(autoLabelUISettings);

        v.findViewById(R.id.add_skills).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Skills_Dialog();
            }
        });

        SkillsAutoLabelUI.setOnRemoveLabelListener(new AutoLabelUI.OnRemoveLabelListener() {
            @Override
            public void onRemoveLabel(Label removedLabel, int position) {
                try {
                    SkillsAutoLabelUI.removeLabel(position);
                    CategoryItems categoryItems = skills_array.get(position);
                    ids.remove(categoryItems.getId());
                    categoryItems.setSelected(false);
                    skills_array.remove(position);
                    skills_array.add(position, categoryItems);
                } catch (ArrayIndexOutOfBoundsException e) {

                }
            }
        });

        GetSelectedSkillsTask();

        v.findViewById(R.id.send_skills).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SendSelectedSkillsTask();
            }
        });

        return v;

    }

    /*  public void ItemCheckedSelected(int position) {
          edit_profile_AutoLabelUI.addLabel(h_list.get(position).get("name"));
      }
  */

    private void Skills_Dialog() {

        skills_dialog = new Dialog(mContext);
        skills_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        skills_dialog.setCancelable(false);
        skills_dialog.setCanceledOnTouchOutside(false);
        skills_dialog.setContentView(R.layout.dialog_select_profile_skills);
        skills_dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        ImageView cancel_skills_dialog, add_skills;

        add_skills = (ImageView) skills_dialog.findViewById(R.id.add_skills);
        cancel_skills_dialog = (ImageView) skills_dialog.findViewById(R.id.cancel_skills_dialog);

        dialod_skillsadap = new AdapDialog_SkillsList(mContext, R.layout.dialog_select_skills_list, skills_array);
        ((ListView) skills_dialog.findViewById(R.id.skills_list)).setAdapter(dialod_skillsadap);

        add_skills.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SkillsAutoLabelUI.clear();
                ids = new ArrayList<>();
                for (int i = 0; i < skills_array.size(); i++) {
                    if (skills_array.get(i).isSelected()) {
                        ids.add(skills_array.get(i).getId());
                        SkillsAutoLabelUI.addLabel(skills_array.get(i).getName(), i);
                    }
                }
                skills_dialog.dismiss();
            }
        });

        cancel_skills_dialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                skills_dialog.cancel();
            }
        });

        skills_dialog.show();
    }

    public void GetSkillsTask() {

        try {
            JSONObject j = new JSONObject();
            j.put(Constants.ACTION, Constants.CATEGORYLIST);
            Utility.ShowLoading(mContext, getResources().getString(R.string.please_wait));

            rt = new ResponseTask(Constant_Urls.SERVER_URL, j);
            rt.setListener(new ResponseListener() {
                @Override
                public void onGetPickSuccess(String result) {
                    Utility.HideDialog();
                    try {
                        JSONObject skills = new JSONObject(result);
                        if (result == null) {
                            Utility.showToast(mContext, "server not responding");
                        } else if (skills.getString("success").equals("1")) {

                            JSONArray jsonArray = skills.getJSONArray("object");
                            SkillsAutoLabelUI.clear();
                            for (int i = 0; i < jsonArray.length(); i++) {
                                categoryItems = new CategoryItems();
                                categoryItems.setName(jsonArray.getJSONObject(i).getString("name"));
                                categoryItems.setId(jsonArray.getJSONObject(i).getString("id"));
                                boolean status = false;
                                for (int j = 0; j < ids.size(); j++) {
                                    if (jsonArray.getJSONObject(i).getString("id").equals(ids.get(j))) {
                                        status = true;
                                        SkillsAutoLabelUI.addLabel(jsonArray.getJSONObject(i).getString("name"), i);
                                        break;
                                    }
                                }
                                categoryItems.setSelected(status);
                                skills_array.add(categoryItems);
                            }

                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            });
            rt.execute();
        } catch (JSONException j) {
            j.printStackTrace();
        }

    }

    public void GetSelectedSkillsTask() {

        try {
            JSONObject jk = new JSONObject();
            jk.put(Constants.ACTION, Constants.GETSELECTEDLIST);
            jk.put("user_id", Utility.getSharedPreferences(mContext, Constants.USERID));

            //Utility.ShowLoading(mContext, getResources().getString(R.string.please_wait));

            rt = new ResponseTask(Constant_Urls.SERVER_URL, jk);
            rt.setListener(new ResponseListener() {
                @Override
                public void onGetPickSuccess(String result) {
                    //  Utility.HideDialog();
                    try {
                        JSONObject skills = new JSONObject(result);
                        if (result == null) {
                            Utility.showToast(mContext, "server not responding");
                        } else if (skills.getString("success").equals("1")) {

                            JSONArray jsonArray = skills.getJSONArray("object");

                            for (int i = 0; i < jsonArray.length(); i++) {
                                // SkillsAutoLabelUI.clear();
                                ids.add(jsonArray.getJSONObject(i).getString("id"));
                            }
                            GetSkillsTask();
                        } else {
                            GetSkillsTask();
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            });
            rt.execute();
        } catch (JSONException j) {
            j.printStackTrace();
        }

    }

    public void SendSelectedSkillsTask() {
        String single_skills = ids.toString();
        single_skills = single_skills.substring(1, single_skills.length() - 1);
        single_skills = single_skills.replaceAll(", ", ",");
        try {
            JSONObject jk = new JSONObject();
            jk.put(Constants.ACTION, Constants.ADDSKILLS);
            jk.put("user_id", Utility.getSharedPreferences(mContext, Constants.USERID));
            jk.put("skill", single_skills);
            System.out.println("Skills ADD " + single_skills);
            Utility.ShowLoading(mContext, getResources().getString(R.string.please_wait));

            rt = new ResponseTask(Constant_Urls.SERVER_URL, jk);
            rt.setListener(new ResponseListener() {
                @Override
                public void onGetPickSuccess(String result) {
                    Utility.HideDialog();
                    try {
                        JSONObject skills = new JSONObject(result);
                        if (result == null) {
                            Utility.showToast(mContext, "server not responding");
                        } else if (skills.getString("success").equals("1")) {
                            Utility.showToast(mContext, "Skills added successfully");
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            });
            rt.execute();
        } catch (JSONException j) {
            j.printStackTrace();
        }

    }

    class AdapDialog_SkillsList extends BaseAdapter {
        Context mContext;
        ArrayList<CategoryItems> list = new ArrayList<>();
        int res;

        public AdapDialog_SkillsList(Context mContext, int res, ArrayList<CategoryItems> list) {
            this.mContext = mContext;
            this.list = list;
            this.res = res;

        }

        @Override
        public int getCount() {
            return list.size();
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            final ViewHolder viewHolder;

            if (convertView == null) {
                LayoutInflater li = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = li.inflate(res, null);
                viewHolder = new ViewHolder(convertView);

                convertView.setTag(viewHolder);
            } else {
                viewHolder = (ViewHolder) convertView.getTag();
            }
            CategoryItems j = list.get(position);
            viewHolder.skills_txt.setText(j.getName());
            viewHolder.skills_chkbx.setChecked(j.isSelected());
            viewHolder.check_uncheck.setOnClickListener(new MyClick(position));

            return convertView;
        }

        /*public void setItemSelected(int position, boolean isSelected) {
            if (position != -1) {
                skills_array.get(position).setSelected(isSelected);
                notifyDataSetChanged();
            }
        }*/

        class ViewHolder {
            CustomTextView skills_txt;
            CheckBox skills_chkbx;
            View check_uncheck;

            public ViewHolder(View v) {
                skills_chkbx = (CheckBox) v.findViewById(R.id.skills_chkbx);
                skills_txt = (CustomTextView) v.findViewById(R.id.skills_txt);
                check_uncheck = (View) v.findViewById(R.id.check_uncheck);
            }

        }

        /*public class GetCheckedSkills implements CompoundButton.OnCheckedChangeListener {
            int pos;

            public GetCheckedSkills(int pos) {
                this.pos = pos;
            }

            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                try {
                    if(isChecked==true){

                       HashMap hm = new HashMap();
                        hm.put("name",list.get(pos).getString("name"));
                        hm.put("id",list.get(pos).getString("id"));
                    }

                } catch (JSONException ex) {
                    ex.printStackTrace();
                }
            }
        }*/
        public class MyClick implements View.OnClickListener {
            int position;

            public MyClick(int pos) {
                this.position = pos;
            }

            @Override
            public void onClick(View v) {
                if (skills_array.get(position).isSelected()) {
                    CategoryItems categoryItems = skills_array.get(position);
                    categoryItems.setSelected(false);
                    skills_array.remove(position);
                    skills_array.add(position, categoryItems);
                } else {
                    CategoryItems categoryItems = skills_array.get(position);
                    categoryItems.setSelected(true);
                    skills_array.remove(position);
                    skills_array.add(position, categoryItems);
                }
                notifyDataSetChanged();
            }
        }
    }

}
