package com.h_plus.HP_Service_Provider.fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.h_plus.HP_Service_Provider.Add_Exeprience_Activity;
import com.h_plus.HP_Service_Provider.adapter.Adapter_ExperienceDetailView;
import com.h_plus.R;
import com.h_plus.ServerInteraction.ResponseListener;
import com.h_plus.ServerInteraction.ResponseTask;
import com.h_plus.usertabs.utility.Constant_Urls;
import com.h_plus.usertabs.utility.Constants;
import com.h_plus.usertabs.utility.Utility;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Infograins on 1/6/2017.
 */

public class Port_ExperienceFragment extends Fragment {
    ResponseTask rt;
    View rooView;
    Adapter_ExperienceDetailView adapter_viewexp;
    Context mContext;
    ArrayList<JSONObject> exp_arraylist = new ArrayList<>();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        mContext = getActivity();

        rooView = inflater.inflate(R.layout.sp_port_experiancefargment, container, false);

        rooView.findViewById(R.id.add_exp_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Utility.setSharedPreference(mContext,Constants.ADDEXPERIENCEKEY,"AE");
                startActivity(new Intent(mContext, Add_Exeprience_Activity.class));
            }
        });

        return rooView;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (Utility.isConnectingToInternet(mContext)) {
            ViewExperienceDetailTask();

        } else {
            Utility.showPromptDlgForSuccess(mContext, "", "please check your network connection");
        }
    }

    public void ViewExperienceDetailTask() {

        try {

            JSONObject jo = new JSONObject();
            jo.put(Constants.ACTION, Constants.SP_EXPERIENCELIST);
            jo.put("user_id", Utility.getSharedPreferences(mContext, Constants.USERID));

            Utility.ShowLoading(mContext, getResources().getString(R.string.please_wait));

            rt = new ResponseTask(Constant_Urls.SERVER_URL, jo);
            rt.setListener(new ResponseListener() {
                @Override
                public void onGetPickSuccess(String result) {
                    Utility.HideDialog();
                    try {
                        JSONObject je = new JSONObject(result);
                        if (result == null) {
                            Utility.showToast(mContext, getResources().getString(R.string.server_fail));
                        } else if (je.getString("success").equals("1")) {
                            exp_arraylist = new ArrayList<JSONObject>();
                            exp_arraylist.clear();
                            JSONArray j = je.getJSONArray("object");
                            for (int i = 0; i < j.length(); i++) {
                                exp_arraylist.add(j.getJSONObject(i));
                            }
                            adapter_viewexp = new Adapter_ExperienceDetailView(mContext, R.layout.sp_adapter_show_experience_list, exp_arraylist);
                            ((ListView) rooView.findViewById(R.id.experience_list)).setAdapter(adapter_viewexp);
                        } else {
                            Utility.showToast(mContext, je.getString("msg"));
                        }
                    } catch (JSONException j) {
                        j.printStackTrace();
                    }

                }
            });
            rt.execute();
        } catch (JSONException e) {
            e.printStackTrace();
        }


    }
}
