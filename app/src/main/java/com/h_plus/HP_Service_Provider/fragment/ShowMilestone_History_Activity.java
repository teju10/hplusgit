package com.h_plus.HP_Service_Provider.fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.h_plus.HP_Service_Provider.SendMilestone_Activity;
import com.h_plus.HP_Service_Provider.adapter.Adapter_MilestoneHistory;
import com.h_plus.R;
import com.h_plus.ServerInteraction.ResponseListener;
import com.h_plus.ServerInteraction.ResponseTask;
import com.h_plus.customwidget.CustomTextView;
import com.h_plus.customwidget.LoadMoreListView;
import com.h_plus.usertabs.utility.Constant_Urls;
import com.h_plus.usertabs.utility.Constants;
import com.h_plus.usertabs.utility.Utility;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.LinkedList;


/**
 * Created by Infograins on 1/3/2017.
 */

public class ShowMilestone_History_Activity extends AppCompatActivity {

    Context mContext;
    LinkedList<JSONObject> milestone_linkedlist = new LinkedList<>();
    int index = 0;
    ResponseTask rt;
    Adapter_MilestoneHistory ada;
    Bundle b;
    String ProjectId = "";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.sp_fragment_milestone_historylist);

        mContext = this;
        b = new Bundle();
        ((CustomTextView)findViewById(R.id.toolbar_title)).setText("Milestone History");
        setSupportActionBar((Toolbar) findViewById(R.id.h_toolbar));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        Find();
        Init();

    }

    public void Find() {
        b = getIntent().getExtras();
        System.out.println("BUNDLE " + b);
        ProjectId = b.getString("project_id");
    }

    public void Init() {

        ada = new Adapter_MilestoneHistory(mContext, R.layout.sp_adapter_milestones_history, milestone_linkedlist);
        ((LoadMoreListView) findViewById(R.id.milstone_history_list)).setAdapter(ada);
        GetMyMilestoneHistoryListList(index);
        ((LoadMoreListView) findViewById(R.id.milstone_history_list)).setOnLoadMoreListener(new LoadMoreListView.OnLoadMoreListener() {
            @Override
            public void onLoadMore() {
                index = index + 1;
                GetMyMilestoneHistoryListList(index);
            }
        });
    }

    public void removeaccptitem(int pos) {
        try {
            System.out.println("MAIN ");
            JSONObject j = milestone_linkedlist.get(pos);
            JSONObject j2 = new JSONObject();
            j2.put("status", "accepted");
            j2.put("requested_amount", j.getString("requested_amount"));
            j2.put("milestone_id", j.getString("milestone_id"));
            milestone_linkedlist.remove(pos);
            milestone_linkedlist.add(pos, j2);
            ada.notifyDataSetChanged();
            System.out.println("MAIN AFTER ");
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void removerejectitem(int pos) {
        try {
            System.out.println("MAIN ");
            JSONObject j = milestone_linkedlist.get(pos);
            JSONObject j2 = new JSONObject();
            j2.put("status", "rejected");
            j2.put("requested_amount", j.getString("requested_amount"));
            j2.put("milestone_id", j.getString("milestone_id"));
            milestone_linkedlist.remove(pos);
            milestone_linkedlist.add(pos, j2);
            ada.notifyDataSetChanged();
            System.out.println("MAIN AFTER ");
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void GetMyMilestoneHistoryListList(int page) {
    /*https://infograins.com/INFO01/homesplus/api/api.php?action=milestoneDetailServiceProvider&project_id=40&user_id=97*/

        try {
            JSONObject j = new JSONObject();
            if (Utility.getSharedPreferences(mContext, Constants.USER_TYPE).equals("1")) {
                j.put(Constants.ACTION, Constants.HIRE_MILESTONEHISTORY);
            } else if (Utility.getSharedPreferences(mContext, Constants.USER_TYPE).equals("2")) {
                j.put(Constants.ACTION, Constants.SERVICEPROVIDER_MILESTONEHISTORY);
            }
            j.put("project_id", ProjectId);
            j.put("user_id", Utility.getSharedPreferences(mContext, Constants.USERID));
            j.put("page", page);

            Utility.ShowLoading(mContext, getResources().getString(R.string.please_wait));

            rt = new ResponseTask(Constant_Urls.SERVER_URL, j);
            rt.setListener(new ResponseListener() {
                @Override
                public void onGetPickSuccess(String result) {
                    Utility.HideDialog();
                    try {
                        if (result == null) {
                            Utility.showToast(mContext, "server not responding");
                        } else {
                            JSONObject m = new JSONObject(result);
                            if (m.getString("success").equals("1")) {
                                JSONArray jsonArray = m.getJSONArray("object");
                                for (int i = 0; i < jsonArray.length(); i++) {
                                    milestone_linkedlist.add(jsonArray.getJSONObject(i));
                                }
                                ada.notifyDataSetChanged();
                                if (index == 0) {
                                } else {
                                    ((LoadMoreListView) findViewById(R.id.milstone_history_list)).onLoadMoreComplete();/*record not found !*/
                                }
                            } else {
                                if (index == 0) {
                                   // Utility.ShowToastMessage(mContext, "You have no more project available");
                                } else {
                                    //Utility.ShowToastMessage(mContext, "No More project Available At This Time");
                                    ((LoadMoreListView) findViewById(R.id.milstone_history_list)).onLoadMoreComplete();
                                }
                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });
            rt.execute();
        } catch (JSONException j) {
            j.printStackTrace();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menu) {

        switch (menu.getItemId()) {

            case android.R.id.home:
                onBackPressed();
                return true;

            default:
                return super.onOptionsItemSelected(menu);

        }
    }


    @Override
    public void onBackPressed() {
        startActivity(new Intent(mContext, SendMilestone_Activity.class));
        finish();
    }
}