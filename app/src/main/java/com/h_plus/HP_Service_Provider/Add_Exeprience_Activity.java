package com.h_plus.HP_Service_Provider;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;

import com.h_plus.R;
import com.h_plus.ServerInteraction.ResponseListener;
import com.h_plus.ServerInteraction.ResponseTask;
import com.h_plus.customwidget.CustomButton;
import com.h_plus.customwidget.CustomEditText;
import com.h_plus.customwidget.CustomTextView;
import com.h_plus.customwidget.MonthYearPicker;
import com.h_plus.usertabs.utility.Constant_Urls;
import com.h_plus.usertabs.utility.Constants;
import com.h_plus.usertabs.utility.Utility;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


/**
 * Created by Infograins on 1/7/2017.
 */

public class Add_Exeprience_Activity extends AppCompatActivity implements View.OnClickListener {

    ResponseTask rt;
    CustomEditText addexp_jobtitle, addexp_company_name, addexp_year_of_exp, addexp_summary;
    CustomTextView addexp_joining_year, addexp_leave_year;
    Context mContext;
    String joiningmonth = "", leavingmonth = "";
    int joiningyear = 0, leavingyear = 0;
    String CID = "";
    Bundle b;
    private MonthYearPicker myp;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.sp_add_certificate_activity);

        mContext = this;

        setSupportActionBar((Toolbar) findViewById(R.id.h_toolbar));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        b = new Bundle();
        b = getIntent().getExtras();

        ((CustomTextView) findViewById(R.id.toolbar_title)).setText("Add Experience");

        addexp_jobtitle = (CustomEditText) findViewById(R.id.addexp_jobtitle);
        addexp_company_name = (CustomEditText) findViewById(R.id.addexp_company_name);
        addexp_year_of_exp = (CustomEditText) findViewById(R.id.addexp_year_of_exp);
        addexp_summary = (CustomEditText) findViewById(R.id.addexp_summary);

        addexp_joining_year = (CustomTextView) findViewById(R.id.addexp_joining_year);
        addexp_leave_year = (CustomTextView) findViewById(R.id.addexp_leave_year);
        // Find();


        addexp_joining_year.setOnClickListener(this);
        addexp_leave_year.setOnClickListener(this);
        ((CustomButton) findViewById(R.id.add_exp_btn)).setOnClickListener(this);

        if (Utility.getSharedPreferences(mContext, Constants.ADDEXPERIENCEKEY).equals("EE")) {
            CID = b.getString("experience_id");
            ((CustomButton) findViewById(R.id.add_exp_btn)).setText("Update Experience");
            if (Utility.isConnectingToInternet(mContext)) {
                GetExperienceDetail(CID);
            } else {
                Utility.showPromptDlgForSuccess(mContext, "", "please check your network connection");
            }

        }
    }

    /* public void Find() {

     }
 */
    public void GetJoiningYearPicker() {
        myp = new MonthYearPicker(this);
        myp.build(new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                addexp_joining_year.setText(myp.getSelectedYear() + "/" + myp.getSelectedMonthShortName());
                joiningyear = myp.getSelectedYear();
                joiningmonth = myp.getSelectedMonthShortName();
            }
        }, null);

        myp.show();
    }

    public void GetLeavingYearPicker() {
        myp = new MonthYearPicker(this);
        myp.build(new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                addexp_leave_year.setText(myp.getSelectedYear() + "/" + myp.getSelectedMonthShortName());
                leavingyear = myp.getSelectedYear();
                leavingmonth = myp.getSelectedMonthShortName();
            }
        }, null);

        myp.show();
    }

    public void AddExperience(String action, String job_title, String company_name, String exp, String j_year, String l_year, String exp_id, String desc, String j_month, String l_month) {
        /*https://infograins.com/INFO01/homesplus/api/api.php?action=addExperience&
        jobtitle=PHP&company=Info&totalexperience=2%20year&joiningyear=2016&leavingyear=2018&user_id=2&
        desc=hii&joiningmonth=07&leavingmonth=10*/
        try {
            JSONObject j = new JSONObject();
            j.put(Constants.ACTION, action);
            j.put("jobtitle", job_title);
            j.put("company", company_name);
            j.put("totalexperience", exp);
            j.put("joiningyear", j_year);
            j.put("leavingyear", l_year);
            j.put("experience_id", exp_id);
            j.put("user_id", Utility.getSharedPreferences(mContext, Constants.USERID));
            j.put("desc", desc);
            j.put("joiningmonth", j_month);
            j.put("leavingmonth", l_month);

            Utility.ShowLoading(mContext, getResources().getString(R.string.please_wait));

            rt = new ResponseTask(Constant_Urls.SERVER_URL, j);
            rt.setListener(new ResponseListener() {
                @Override
                public void onGetPickSuccess(String result) {
                    Utility.HideDialog();
                    try {
                        JSONObject skills = new JSONObject(result);
                        if (result == null) {
                            Utility.showToast(mContext, "server not responding");
                        } else if (skills.getString("success").equals("1")) {
                            Utility.showToast(mContext, "Experience added successfully");
                            finish();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            });
            rt.execute();
        } catch (JSONException j) {
            j.printStackTrace();
        }

    }


    public void GetExperienceDetail(String cid) {
        /*https://infograins.com/INFO01/homesplus/api/api.php?action=addExperience&
        jobtitle=PHP&company=Info&totalexperience=2%20year&joiningyear=2016&leavingyear=2018&user_id=2&
        desc=hii&joiningmonth=07&leavingmonth=10*/
        try {
            JSONObject j = new JSONObject();
            j.put(Constants.ACTION, "editExperienceByEid");
            j.put("experience_id", cid);

            Utility.ShowLoading(mContext, getResources().getString(R.string.please_wait));

            rt = new ResponseTask(Constant_Urls.SERVER_URL, j);
            rt.setListener(new ResponseListener() {
                @Override
                public void onGetPickSuccess(String result) {
                    Utility.HideDialog();
                    try {
                        JSONObject exp_detail = new JSONObject(result);
                        if (result == null) {
                            Utility.showToast(mContext, "server not responding");
                        } else if (exp_detail.getString("success").equals("1")) {
                            JSONArray ja = exp_detail.getJSONArray("object");

                            JSONObject j = new JSONObject();
                            for (int i = 0; i < ja.length(); i++) {
                                j = ja.getJSONObject(i);
                                addexp_jobtitle.setText(j.getString("jobtitle"));
                                addexp_company_name.setText(j.getString("company"));
                                addexp_joining_year.setText(j.getString("joiningyear") + "/" + j.getString("joiningmonth"));
                                addexp_year_of_exp.setText(j.getString("totalexperience"));
                                addexp_summary.setText(j.getString("desc"));
                                addexp_leave_year.setText(j.getString("leavingyear") + "/" + j.getString("leavingmonth"));
                            }

                        }else{
                            Utility.showToast(mContext,exp_detail.getString("msg"));
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            });
            rt.execute();
        } catch (JSONException j) {
            j.printStackTrace();
        }

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.add_exp_btn:
                if (addexp_jobtitle.getText().toString().equals("")) {
                    Utility.showToast(mContext, "please enter your jobtitle");
                } else if (addexp_company_name.getText().toString().equals("")) {
                    Utility.showToast(mContext, "please enter your company detail");
                } else if (addexp_company_name.getText().toString().equals("")) {
                    Utility.showToast(mContext, "please enter your company detail");
                } else if (addexp_year_of_exp.getText().toString().equals("")) {
                    Utility.showToast(mContext, "please enter year of experience");
                } else if (addexp_summary.getText().toString().equals("")) {
                    Utility.showToast(mContext, "fill summary detail");
                } else {

                    if (Utility.getSharedPreferences(mContext, Constants.ADDEXPERIENCEKEY).equals("AE")) {
                        ((CustomButton) findViewById(R.id.add_exp_btn)).setText("Add Experience");
                        AddExperience(Constants.ADDEXPERIENCE, addexp_jobtitle.getText().toString(),
                                addexp_company_name.getText().toString(),
                                addexp_year_of_exp.getText().toString(),
                                String.valueOf(joiningyear), String.valueOf(leavingyear), "",
                                addexp_summary.getText().toString(),
                                joiningmonth, leavingmonth);
                    } else if (Utility.getSharedPreferences(mContext, Constants.ADDEXPERIENCEKEY).equals("EE")) {
                        CID = b.getString("experience_id");

                        AddExperience(Constants.EDIT_EXPERIENCE, addexp_jobtitle.getText().toString(),
                                addexp_company_name.getText().toString(),
                                addexp_year_of_exp.getText().toString(),
                                String.valueOf(joiningyear), String.valueOf(leavingyear), CID,
                                addexp_summary.getText().toString(),
                                joiningmonth, leavingmonth);

                    }

                }
                break;
            case R.id.addexp_joining_year:
                GetJoiningYearPicker();
                break;

            case R.id.addexp_leave_year:
                GetLeavingYearPicker();
            default:
                break;
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menu) {

        switch (menu.getItemId()) {

            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(menu);

        }
    }
}
