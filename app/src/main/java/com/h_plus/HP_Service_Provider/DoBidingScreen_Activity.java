package com.h_plus.HP_Service_Provider;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.MenuItem;
import android.view.View;
import android.widget.CompoundButton;

import com.h_plus.R;
import com.h_plus.ServerInteraction.ResponseListener;
import com.h_plus.ServerInteraction.ResponseTask;
import com.h_plus.customwidget.CustomAutoCompleteTextView;
import com.h_plus.customwidget.CustomButton;
import com.h_plus.customwidget.CustomCheckBox;
import com.h_plus.customwidget.CustomEditText;
import com.h_plus.customwidget.CustomTextView;
import com.h_plus.usertabs.utility.Constant_Urls;
import com.h_plus.usertabs.utility.Constants;
import com.h_plus.usertabs.utility.Utility;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;

/**
 * Created by Infograins on 12/7/2016.
 */

public class DoBidingScreen_Activity extends AppCompatActivity implements View.OnClickListener {

    Context mContext;
    ResponseTask rt;
    String free_ka_chkboxstr = "0";

    double Sponsor_bid_price = 0.0, HightLight_Price = 0.0, Hp_Bid_Amount = 0.0, Admin_bid_amount = 0.0, TotalBidAmount = 0;
    CustomEditText bid_edt_txt;
    CustomTextView total_hp_bid_fees;
    Toolbar mToolbar;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.sp_activity_do_biding_screen);
        mContext = this;

        Find();
        Init();

    }

    public void Find() {
        mToolbar = (Toolbar) findViewById(R.id.h_toolbar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        ((CustomTextView) mToolbar.findViewById(R.id.toolbar_title)).setText("Place a bid");

        bid_edt_txt = (CustomEditText) findViewById(R.id.bid_edt_txt);
        total_hp_bid_fees = (CustomTextView) findViewById(R.id.total_hp_bid_fees);

        ((CustomTextView) findViewById(R.id.bid_hp_fees_txt)).setText(("Homes Plus Project Fee: Rs 0.00 INR"));
        ((CustomTextView) findViewById(R.id.total_hp_bid_fees)).setText(("Your bid: Rs 0.00 INR"));

        ((CustomTextView) findViewById(R.id.project_title)).setText(Utility.getSharedPreferences(mContext, Constants.PROJECT_TITLE));

        findViewById(R.id.place_bid).setOnClickListener(this);
        findViewById(R.id.bid_on_project_btn).setOnClickListener(this);

    }

    public void Init() {

        ((CustomTextView) findViewById(R.id.sponsor_total_bid)).setText("$0.00 USD");

        if (Utility.isConnectingToInternet(mContext)) {
            GetProjectDetail(Utility.getSharedPreferences(mContext, Constants.PROJECTID));
        } else {
            Utility.showPromptDlgForSuccess(mContext, "please check your network connection", "Hello");
        }

        bid_edt_txt.addTextChangedListener(new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                try {

                    DecimalFormat format = new DecimalFormat("##.##");
                    String formatted = format.format(22.123);
                    ((CustomTextView) findViewById(R.id.bid_hp_fees_txt)).setText(formatted);
                    total_hp_bid_fees.setText(formatted);

                    if (s.length() > 0) {
                        Hp_Bid_Amount = ((Double.parseDouble(s.toString()) * Admin_bid_amount)) / 100;
                        //  Hp_Bid_Amount = ((Integer.parseInt(s.toString()) * Admin_bid_amount)) / 100;
                        ((CustomTextView) findViewById(R.id.bid_hp_fees_txt)).setText(("Homes Plus Project Fee: $ " + (Hp_Bid_Amount)) + " USD");
                        TotalBidAmount = Double.parseDouble(s.toString()) + Hp_Bid_Amount;
                        total_hp_bid_fees.setText("Your bid: " + TotalBidAmount + " USD");
                    }
                    if (s.length() <= 0) {

                        ((CustomTextView) findViewById(R.id.bid_hp_fees_txt)).setText(("Homes Plus Project Fee: Rs 0.00 INR"));
                        ((CustomTextView) findViewById(R.id.total_hp_bid_fees)).setText(("Your bid: Rs 0.00 INR"));
                    }


                } catch (Exception e) {
                    e.printStackTrace();
                }

                if (s.length() < 0) {
                    ((CustomTextView) findViewById(R.id.bid_hp_fees_txt)).setText("");
                    total_hp_bid_fees.setText("");
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });


        ((CustomCheckBox) findViewById(R.id.sponsor_chkbox)).setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    if (((CustomCheckBox) findViewById(R.id.sponsor_hightlight_chkbox)).isChecked()) {
                        ((CustomTextView) findViewById(R.id.sponsor_total_bid)).setText("$" + (HightLight_Price + Sponsor_bid_price) + " USD");
                    } else {
                        ((CustomTextView) findViewById(R.id.sponsor_total_bid)).setText("$" + Sponsor_bid_price + " USD");
                    }
                } else {
                    if (((CustomCheckBox) findViewById(R.id.sponsor_hightlight_chkbox)).isChecked()) {
                        ((CustomTextView) findViewById(R.id.sponsor_total_bid)).setText("$" + HightLight_Price + " USD");
                    } else {
                        ((CustomTextView) findViewById(R.id.sponsor_total_bid)).setText("$0.00 USD");
                    }
                }

            }
        });

        ((CustomCheckBox) findViewById(R.id.sponsor_hightlight_chkbox)).setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    if (((CustomCheckBox) findViewById(R.id.sponsor_chkbox)).isChecked()) {
                        ((CustomTextView) findViewById(R.id.sponsor_total_bid)).setText("$" + (HightLight_Price + Sponsor_bid_price) + " USD");
                    } else {
                        ((CustomTextView) findViewById(R.id.sponsor_total_bid)).setText("$" + HightLight_Price + " USD");
                    }
                } else {
                    if (((CustomCheckBox) findViewById(R.id.sponsor_chkbox)).isChecked()) {
                        ((CustomTextView) findViewById(R.id.sponsor_total_bid)).setText("$" + Sponsor_bid_price + " USD");
                    } else {
                        ((CustomTextView) findViewById(R.id.sponsor_total_bid)).setText("$0.00 USD");
                    }
                }

            }
        });

        ((CustomCheckBox) findViewById(R.id.free_ka_chkbox)).setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    free_ka_chkboxstr = "1";

                } else {
                    free_ka_chkboxstr = "0";
                }
            }

        });

    }


    public void GetProjectDetail(String project_id) {
        try {
            JSONObject joo = new JSONObject();
            joo.put(Constants.ACTION, Constants.Project_DetailBY_ID);
            joo.put("project_id", project_id);

            rt = new ResponseTask(Constant_Urls.SERVER_URL, joo);

            Utility.ShowLoading(mContext, mContext.getResources().getString(R.string.please_wait));

            rt.setListener(new ResponseListener() {
                @Override
                public void onGetPickSuccess(String result) {
                    Utility.HideDialog();
                    try {

                        JSONObject jo = new JSONObject(result);
                        if (jo.getString("success").equals("1")) {

                            ((CustomTextView) findViewById(R.id.bid_time_left)).setText((jo.getJSONObject("object").getString("days_left") +
                                    " days" + "," + (jo.getJSONObject("object").getString("hours_left") + " hours" + " left")));

                            ((CustomTextView) findViewById(R.id.bid_bid_count)).setText(jo.getJSONObject("object").getString("bid_count"));
                            ((CustomTextView) findViewById(R.id.currency_txt)).setText("Avg Bid" + (jo.getJSONObject("object").
                                    getString("currency_symbol")));
                            ((CustomTextView) findViewById(R.id.projct_budget_type)).setText
                                    ("Project budget" + (jo.getJSONObject("object").getString("currency_symbol")));

                            ((CustomTextView) findViewById(R.id.bid_avgbid)).
                                    setText((jo.getJSONObject("object").getString("currency_symbol")) + (jo.getJSONObject("object").getString("average_bid")));

                            ((CustomTextView) findViewById(R.id.bid_project_budget)).setText((jo.getJSONObject("object").getString("currency_symbol") +
                                    jo.getJSONObject("object").getString("min_budget") + "-" +
                                    jo.getJSONObject("object").getString("currency_symbol") +
                                    jo.getJSONObject("object").getString("max_budget")));

                            ((CustomTextView) findViewById(R.id.bid_sponsor_amount)).setText(jo.getJSONObject("object").
                                    getString("currency_symbol") +
                                    (jo.getJSONObject("object").
                                            getString("sponsor_bid_amount") +
                                            jo.getJSONObject("object").
                                                    getString("currency")));

                            ((CustomTextView) findViewById(R.id.bid_hightlight_amount)).setText(jo.getJSONObject("object").
                                    getString("currency_symbol") +
                                    (jo.getJSONObject("object").
                                            getString("highlight_bid_amount") +
                                            jo.getJSONObject("object").
                                                    getString("currency")));
                            try {
                                Admin_bid_amount = Double.parseDouble(jo.getJSONObject("object").getString("admin_bid_percentage"));
                                Sponsor_bid_price = Double.parseDouble(jo.getJSONObject("object").getString("sponsor_bid_amount"));
                                HightLight_Price = Double.parseDouble(jo.getJSONObject("object").getString("highlight_bid_amount"));
                                System.out.println("ADMIN_AMOUNT " + Admin_bid_amount);
                            } catch (NumberFormatException e) {
                                e.printStackTrace();
                            }
                        }


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });
        } catch (JSONException j) {
            j.printStackTrace();
        }
        rt.execute();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.bid_on_project_btn:

                if (bid_edt_txt.getText().toString().equals("")) {
                    Utility.ShowToastMessage(mContext, "please enter your budget");
                } else if (((CustomEditText) findViewById(R.id.bid_delivery_days_txt)).getText().toString().equals("")) {
                    Utility.ShowToastMessage(mContext, "please enter your deliveries days");
                } else if (((CustomEditText) findViewById(R.id.bider_commnt)).getText().toString().equals("")) {
                    Utility.ShowToastMessage(mContext, "please give you comments");

                } else if (Utility.isConnectingToInternet(mContext)) {
                    PostBid(Utility.getSharedPreferences(mContext, Constants.USERID), Utility.getSharedPreferences(mContext, Constants.PROJECTID),
                            bid_edt_txt.getText().toString(), ((CustomEditText) findViewById(R.id.bid_delivery_days_txt)).getText().toString(),
                            ((CustomTextView) findViewById(R.id.sponsor_total_bid)).getText().toString(),
                            ((CustomTextView) findViewById(R.id.bid_hightlight_amount)).getText().toString(), free_ka_chkboxstr,
                            ((CustomTextView) findViewById(R.id.sponsor_total_bid)).getText().toString(),
                            ((CustomEditText) findViewById(R.id.bider_commnt)).getText().toString());
                } else {
                    Utility.showPromptDlgForSuccess(mContext, "Please check your network connection", "Oops");
                }

                break;


            case R.id.place_bid:
                ((CustomEditText) findViewById(R.id.bider_commnt)).setVisibility(View.VISIBLE);
                ((CustomButton) findViewById(R.id.place_bid)).setVisibility(View.GONE);
        }

    }

    public void PostBid(String u_id, String project_id, String bid_amount, String timeduration, String sponsor_bid,
                        String highlight_bid, String initiate_chat, String total_amount, String comment) {
        try {
            JSONObject joo = new JSONObject();
            joo.put(Constants.ACTION, Constants.BIDTHEPROJECT);
            joo.put("user_id", u_id);
            joo.put("project_id", project_id);
            joo.put("bidding_amount", bid_amount);
            joo.put("time_duration", timeduration);
            joo.put("sponser_bid", sponsor_bid);
            joo.put("highlight_bid", highlight_bid);
            joo.put("initiat_chat", initiate_chat);
            joo.put("supercharge_amount", total_amount);
            joo.put("comment", comment);

            rt = new ResponseTask(Constant_Urls.SERVER_URL, joo);
            Utility.ShowLoading(mContext, mContext.getResources().getString(R.string.please_wait));

            rt.setListener(new ResponseListener() {
                @Override
                public void onGetPickSuccess(String result) {
                    Utility.HideDialog();
                    try {

                        JSONObject jo = new JSONObject(result);
                        if (jo.getString("success").equals("1")) {
                            Utility.ShowToastMessage(mContext, "you have bid a project");
                            finish();
                        } else {
                            Utility.showPromptDlgForSuccess(mContext, jo.getString("msg"), result);
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });
        } catch (JSONException j) {
            j.printStackTrace();
        }
        rt.execute();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(mContext, Bidder_Project_Detail_Activity.class));
        finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menu) {
        switch (menu.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(menu);

        }
    }
}
