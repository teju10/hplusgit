package com.h_plus.HP_Service_Provider;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import com.h_plus.R;
import com.h_plus.ServerInteraction.ResponseListener;
import com.h_plus.ServerInteraction.ResponseTask;
import com.h_plus.customwidget.CustomButton;
import com.h_plus.customwidget.CustomTextView;
import com.h_plus.useractivity.ChattingScreen_Activity;
import com.h_plus.usertabs.utility.Constant_Urls;
import com.h_plus.usertabs.utility.Constants;
import com.h_plus.usertabs.utility.Utility;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by Infograins on 12/12/2016.
 */

public class Bidder_Project_Detail_Activity extends AppCompatActivity implements View.OnClickListener {

    Context mContext;
    ResponseTask rt;
    RecyclerView hp_skillsrequired;
    ArrayList<String> skills = new ArrayList<>();
    HorizontalListAdapter horizontalListAdapter;
    Bundle b;
    CustomButton bid_on_project_btn, free_lancer_bidlist;
    String ClientName, Clientid, Clientimg, Projectid, ProjectDesc, project_status;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_project_descrption_layout);

        mContext = this;
        b = new Bundle();
        setSupportActionBar((Toolbar) findViewById(R.id.h_toolbar));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        ((CustomTextView) findViewById(R.id.toolbar_title)).setText("Project Detail");

        bid_on_project_btn = (CustomButton) findViewById(R.id.bid_on_project_btn);
        free_lancer_bidlist = (CustomButton) findViewById(R.id.free_lancer_bidlist);

        bid_on_project_btn.setOnClickListener(this);
        free_lancer_bidlist.setOnClickListener(this);
        Init();

    }
/*
    @Override
    public void onCreate(Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        super.onCreate(savedInstanceState);
    }*/

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        menu.getItem(0).setVisible(false);
        return super.onCreateOptionsMenu(menu);
    }

    public void Init() {

        findViewById(R.id.chat_wd_client).setOnClickListener(this);

       /* b = new Bundle();
        b = getIntent().getExtras();

        try {
            if (b.getString("CON").equals("1")) {
                ((CustomButton) findViewById(R.id.chat_wd_client)).setVisibility(View.GONE);
                ((CustomButton) findViewById(R.id.bid_on_project_btn)).setVisibility(View.GONE);

            }
        } catch (Exception e) {
            e.printStackTrace();
        }*/

        LinearLayoutManager horizontalLayoutManagaer = new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false);
        hp_skillsrequired = (RecyclerView) findViewById(R.id.hp_skillsrequired);
        hp_skillsrequired.setLayoutManager(horizontalLayoutManagaer);
        horizontalListAdapter = new HorizontalListAdapter(mContext, R.layout.horizontal_skills_required_adapter, skills);
        hp_skillsrequired.setAdapter(horizontalListAdapter);
        if (Utility.isConnectingToInternet(mContext)) {
            GetProjectDesription(Utility.getSharedPreferences(mContext, Constants.PROJECTID));
        } else {
            Utility.Alert(mContext, "please check your network connection");
        }
    }

    public void GetProjectDesription(String project_id) {
        try {
            JSONObject jobj = new JSONObject();
            jobj.put(Constants.ACTION, Constants.Project_DetailBY_ID);
            jobj.put("project_id", project_id);
//            jobj.put("user_id", Utility.getSharedPreferences(mContext,Constants.USERID));

            Utility.ShowLoading(mContext, mContext.getResources().getString(R.string.please_wait));

            rt = new ResponseTask(Constant_Urls.SERVER_URL, jobj);
            rt.setListener(new ResponseListener() {
                @Override
                public void onGetPickSuccess(String result) {
                    Utility.HideDialog();

                    System.out.println("Server result ====> " + result);
                    if (result == null) {

                    } else {
                        try {
                            JSONObject jobj = new JSONObject(result);
                            if (jobj.getString(Constants.RESULT_KEY).equals("1")) {
                                Projectid = jobj.getJSONObject("object").getString("id");
                                Clientid = jobj.getJSONObject("object").getString("client_id");
                                ClientName = jobj.getJSONObject("object").getString("client_name");
                                ProjectDesc = jobj.getJSONObject("object").getString("description");
                                Clientimg = jobj.getJSONObject("object").getString("image");
                                //  Utility.setSharedPreference(mContext,Constants.PROFILE_IMAGE,jobj.getString("image"));
                                ClientName = jobj.getJSONObject("object").getString("client_name");
                                //ClientName = jobj.getJSONObject("object").getString("id");
                                Utility.setSharedPreference(mContext, Constants.PROJECT_TITLE, jobj.getJSONObject("object").getString("project_name"));
                                ((CustomTextView) findViewById(R.id.hp_desc)).setText(jobj.getJSONObject("object").getString("description"));
                                ((CustomTextView) findViewById(R.id.pd_bid_count)).setText(jobj.getJSONObject("object").getString("bid_count"));
                                ((CustomTextView) findViewById(R.id.pd_avg_bid)).setText(jobj.getJSONObject("object").getString("average_bid"));

                                if (jobj.getJSONObject("object").getString("number_of_hours").equals("0")) {
                                    ((LinearLayout) findViewById(R.id.hours_of_work_layout)).setVisibility(View.GONE);
                                } else {
                                    ((CustomTextView) findViewById(R.id.pd_hours_ofwork)).setText(jobj.getJSONObject("object").getString("number_of_hours"));
                                }
                                ((CustomTextView) findViewById(R.id.pd_completion_time)).setText(jobj.getJSONObject("object").getString("project_duration") + " days");
                                ((CustomTextView) findViewById(R.id.currency_txt)).
                                        setText("Avg Bid" + "(" + jobj.getJSONObject("object").getString("currency") + ")");

                                ((CustomTextView) findViewById(R.id.projct_budget_type)).
                                        setText("Project Budget" + "(" + jobj.getJSONObject("object").getString("currency") + ")");

                                ((CustomTextView) findViewById(R.id.pd_project_budget)).setText((jobj.getJSONObject("object").getString("currency_symbol") +
                                        jobj.getJSONObject("object").getString("min_budget") + "-" +
                                        jobj.getJSONObject("object").getString("currency_symbol") +
                                        jobj.getJSONObject("object").getString("max_budget")));

                                ((CustomTextView) findViewById(R.id.pd_time_left)).setText((jobj.getJSONObject("object").getString("days_left")) + " days" + "," +
                                        (jobj.getJSONObject("object").getString("hours_left")) + " hours left");

                                ((CustomTextView) findViewById(R.id.pd_bid_count)).setText(jobj.getJSONObject("object").getString("bid_count"));
                                ((CustomTextView) findViewById(R.id.pd_client_name)).setText(jobj.getJSONObject("object").getString("client_name"));

                                Utility.setSharedPreference(mContext, Constants.PROJECTID, jobj.getJSONObject("object").getString("id"));
                                ((CustomTextView) findViewById(R.id.pd_project_id)).setText(jobj.getJSONObject("object").getString("id"));

                                skills.addAll(Arrays.asList(jobj.getJSONObject("object").getString("skill").split(",")));
                                horizontalListAdapter.notifyDataSetChanged();

                                if (jobj.getJSONObject("object").getString("payment_verification").equals("1")) {
                                    ((ImageView) findViewById(R.id.dollar_veri_img)).setVisibility(View.VISIBLE);
                                } else if (jobj.getJSONObject("object").getString("payment_verification").equals("0")) {
                                    ((ImageView) findViewById(R.id.dollar_veri_img)).
                                            setBackgroundDrawable(getResources().getDrawable(R.drawable.dollar_veri_black));
                                }

                                if (jobj.getJSONObject("object").getString("email_verification").equals("1")) {
                                    ((ImageView) findViewById(R.id.email_veri_img)).setVisibility(View.VISIBLE);
                                } else if (jobj.getJSONObject("object").getString("email_verification").equals("0")) {
                                    ((ImageView) findViewById(R.id.email_veri_img)).
                                            setBackgroundDrawable(getResources().getDrawable(R.drawable.email_veri_black));
                                }

                                if (jobj.getJSONObject("object").getString("user_verification").equals("1")) {
                                    ((ImageView) findViewById(R.id.user_veri_img)).setVisibility(View.VISIBLE);
                                } else if (jobj.getJSONObject("object").getString("user_verification").equals("0")) {
                                    ((ImageView) findViewById(R.id.user_veri_img)).
                                            setBackgroundDrawable(getResources().getDrawable(R.drawable.user_veri_black));
                                }

                                if (jobj.getJSONObject("object").getString("phone_verification").equals("1")) {
                                    ((ImageView) findViewById(R.id.call_veri_img)).setVisibility(View.VISIBLE);
                                } else if (jobj.getJSONObject("object").getString("phone_verification").equals("0")) {
                                    ((ImageView) findViewById(R.id.call_veri_img)).
                                            setBackgroundDrawable(getResources().getDrawable(R.drawable.phone_veri_black));
                                }

                            } else {
                                Utility.showCroutonWarn(jobj.getString(Constants.SERVER_MSG), Bidder_Project_Detail_Activity.this);
                            }
                        } catch (JSONException e2) {
                            Utility.showCroutonWarn(getResources().getString(R.string.server_fail), Bidder_Project_Detail_Activity.this);
                        }
                    }
                }
            });

        } catch (JSONException e) {
            e.printStackTrace();
        }
        rt.execute();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.bid_on_project_btn:
                startActivity(new Intent(mContext, DoBidingScreen_Activity.class));
                finish();
                break;

            case R.id.free_lancer_bidlist:
                Utility.showToast(mContext, "coming soon");
                //  startActivity(new Intent(mContext, DoBidingScreen_Activity.class));
                break;

            case R.id.chat_wd_client:
                Intent i = new Intent(mContext, ChattingScreen_Activity.class);
                Utility.setSharedPreference(mContext, Constants.CHECKCHATBOX_REDIRECT, "1");
                b.putString("project_id", Projectid);
                b.putString("other_name", ClientName);
                b.putString("other_user_id", Clientid);
                b.putString("image", Clientimg);
                i.putExtras(b);
                startActivity(i);
                break;

        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menu) {

        switch (menu.getItemId()) {
          /*  case R.id.menu_checkstatus:
                Intent i = new Intent(mContext, ActivityProfile_CircleProgress.class);
                b.putString("PROJECTDESC", ProjectDesc);
                b.putString("PROJECTDESC", ProjectDesc);
                i.putExtras(b);
                startActivity(i);
                finish();
                return true;*/
            case R.id.menu_project_whistory:

                Utility.showToast(mContext, "coming soon");
                /*Intent in = new Intent(mContext, ActivityProfile_CircleProgress.class);
                b.putString("PROJECTDESC", ProjectDesc);
                b.putString("PROJECTDESC", ProjectDesc);
                in.putExtras(b);
                 startActivity(in);
                finish();*/
                return true;

            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(menu);

        }
    }

    class HorizontalListAdapter extends RecyclerView.Adapter<HorizontalListAdapter.ViewHolder> {

        Context mContext;
        int res;
        ArrayList<String> skillsArrayList;

        public HorizontalListAdapter(Context context, int resourse, ArrayList<String> imageList) {
            this.mContext = context;
            this.res = resourse;
            this.skillsArrayList = imageList;
        }

        @Override
        public HorizontalListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View convertView = LayoutInflater.from(parent.getContext()).inflate(res, parent, false);
            return new HorizontalListAdapter.ViewHolder(convertView);
        }

        @Override
        public void onBindViewHolder(final HorizontalListAdapter.ViewHolder holder, int position) {

            holder.adapter_skill_txtview.setText(skillsArrayList.get(position));
        }


        @Override
        public int getItemCount() {
            return skillsArrayList.size();
        }

        class ViewHolder extends RecyclerView.ViewHolder {

            CustomTextView adapter_skill_txtview;
            ProgressBar progress_bar;

            public ViewHolder(View itemView) {
                super(itemView);
                adapter_skill_txtview = (CustomTextView) itemView.findViewById(R.id.adapter_skill_txtview);
            }
        }
    }

}
