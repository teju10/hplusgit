package com.h_plus.HP_Service_Provider;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;

import com.h_plus.HP_Service_Provider.fragment.AddPortfolioFragment;
import com.h_plus.HP_Service_Provider.fragment.Port_CertificateFragment;
import com.h_plus.HP_Service_Provider.fragment.Port_ExperienceFragment;
import com.h_plus.HP_Service_Provider.fragment.Port_SkillsFragment;
import com.h_plus.R;
import com.h_plus.customwidget.CustomButton;
import com.h_plus.customwidget.CustomTextView;


/**
 * Created by Infograins on 1/6/2017.
 */

public class Portfolio_HomeActivity extends AppCompatActivity implements View.OnClickListener {
    CustomButton tab_exp, tab_certifi, tab_skills, tab_add_port;
    Context mContext;
    int globle_index = 0;
    Fragment fragment;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.sp_activity_portfolio);

        mContext = this;

        setSupportActionBar((Toolbar) findViewById(R.id.h_toolbar));
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ((CustomTextView) findViewById(R.id.toolbar_title)).setText("Portfolio");
        Find();

    }

    public void Find() {
        tab_exp = (CustomButton) findViewById(R.id.tab_exp);
        tab_skills = (CustomButton) findViewById(R.id.tab_skills);
        tab_certifi = (CustomButton) findViewById(R.id.tab_certifi);
        tab_add_port = (CustomButton) findViewById(R.id.tab_add_port);

        tab_exp.setOnClickListener(this);
        tab_skills.setOnClickListener(this);
        tab_certifi.setOnClickListener(this);
        tab_add_port.setOnClickListener(this);

        ChangeTabColor(0);
    }

    private void ChangeTabColor(int pos) {
        globle_index = pos;
        switch (pos) {
            case 0:
                tab_exp.setTextColor(getResources().getColor(R.color.colorPrimary));
                tab_certifi.setTextColor(getResources().getColor(R.color.black));
                tab_skills.setTextColor(getResources().getColor(R.color.black));
                tab_add_port.setTextColor(getResources().getColor(R.color.black));
                fragment = new Port_ExperienceFragment();
                break;

            case 1:
                tab_exp.setTextColor(getResources().getColor(R.color.black));
                tab_certifi.setTextColor(getResources().getColor(R.color.colorPrimary));
                tab_skills.setTextColor(getResources().getColor(R.color.black));
                tab_add_port.setTextColor(getResources().getColor(R.color.black));
                fragment = new Port_CertificateFragment();
                // fragment.setArguments(getArguments());
                break;

            case 2:
                tab_exp.setTextColor(getResources().getColor(R.color.black));
                tab_certifi.setTextColor(getResources().getColor(R.color.black));
                tab_skills.setTextColor(getResources().getColor(R.color.colorPrimary));
                tab_add_port.setTextColor(getResources().getColor(R.color.black));
                fragment = new Port_SkillsFragment();
                break;

            case 3:
                tab_exp.setTextColor(getResources().getColor(R.color.black));
                tab_certifi.setTextColor(getResources().getColor(R.color.black));
                tab_skills.setTextColor(getResources().getColor(R.color.black));
                tab_add_port.setTextColor(getResources().getColor(R.color.colorPrimary));
                fragment = new AddPortfolioFragment();
                break;
        }

        if (fragment != null) {
            FragmentManager fragmentManager = getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.container_body, fragment);
            fragmentTransaction.commit();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tab_exp:
                ChangeTabColor(0);
                break;

            case R.id.tab_certifi:
                ChangeTabColor(1);
                break;

            case R.id.tab_skills:
                ChangeTabColor(2);
                break;

            case R.id.tab_add_port:
                ChangeTabColor(3);
                break;
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }


    }
}
