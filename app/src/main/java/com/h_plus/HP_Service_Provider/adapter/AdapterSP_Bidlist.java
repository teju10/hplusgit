package com.h_plus.HP_Service_Provider.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.h_plus.R;
import com.h_plus.ServerInteraction.ResponseListener;
import com.h_plus.ServerInteraction.ResponseTask;
import com.h_plus.customwidget.CustomButton;
import com.h_plus.customwidget.CustomTextView;
import com.h_plus.usertabs.utility.Constant_Urls;
import com.h_plus.usertabs.utility.Constants;
import com.h_plus.usertabs.utility.Utility;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.LinkedList;

/**
 * Created by Infograins on 12/31/2016.
 */

public class AdapterSP_Bidlist extends BaseAdapter {
    Context mContext;
    int res;
    LinkedList<JSONObject> mlist;
    ResponseTask rt;
    int[] img = {
            R.drawable.purple_patti,
            R.drawable.red_patti,
            R.drawable.blue_patti};

    public AdapterSP_Bidlist(Context context, int res, LinkedList<JSONObject> list) {
        this.mContext = context;
        this.res = res;
        this.mlist = list;

    }

    @Override
    public int getCount() {
        return mlist.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        final ViewHolder viewHolder;

        if (convertView == null) {
            LayoutInflater li = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = li.inflate(res, null);
            viewHolder = new ViewHolder(convertView);

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        JSONObject j = mlist.get(position);
        try {
            viewHolder.sp_uname.setText(j.getString("name"));
            viewHolder.sp_proname.setText(j.getString("project_name"));
            viewHolder.sp_desc.setText(j.getString("description"));
            viewHolder.sp_budget.setText("$" + (j.getString("total_bidding_amount")));
            viewHolder.bidder_completion_days.setText("In " + (j.getString("time_duration") + " Days"));
            if (j.getString("status").equals("1")) {
                viewHolder.sp_acceptreject_layout.setVisibility(View.VISIBLE);
                // viewHolder.sp_btn_detail.setVisibility(View.GONE);
            } else if (j.getString("status").equals("2")) {
                viewHolder.sp_acceptreject_layout.setVisibility(View.GONE);
            }

            Glide.with(mContext).load(j.getString("image"))
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .listener(new RequestListener<String, GlideDrawable>() {
                        @Override
                        public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                            (viewHolder.adapter_pb).setVisibility(View.GONE);

                            return false;
                        }
                    })
                    .into(((com.h_plus.customwidget.CircleImageView) viewHolder.b_image_v));

          /*  Glide.with(mContext).load(j.getString("user_flag_url"))
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into((viewHolder.flag));*/

        } catch (Exception e) {
            e.printStackTrace();
        }
        viewHolder.sp_btn_accept_project.setOnClickListener(new AcceptBidRequestClick(position));
        viewHolder.sp_btn_reject_project.setOnClickListener(new RejectBidrequestClick(position));
        ((RelativeLayout) convertView.findViewById(R.id.relatv_layout)).setBackgroundResource(img[position % 3]);
        return convertView;
    }

    public void AcceptBidrequest(String project_id) {
        try {
            JSONObject jo = new JSONObject();
            jo.put(Constants.ACTION, Constants.ACCEPTREJECTAWARD_REQUEST);
            jo.put("user_id", Utility.getSharedPreferences(mContext, Constants.USERID));
            jo.put("status", "1");
            jo.put("project_id", project_id);

            Utility.ShowLoading(mContext, mContext.getResources().getString(R.string.please_wait));
            rt = new ResponseTask(Constant_Urls.SERVER_URL, jo);
            rt.setListener(new ResponseListener() {
                @Override
                public void onGetPickSuccess(String result) {
                    Utility.HideDialog();
                    try {
                        JSONObject j = new JSONObject(result);
                        if (j.getString("msg").equals("1")) {
                            Utility.showToast(mContext, "request accepted");
                        } else {
                            Utility.showToast(mContext, j.getString("msg"));
                        }


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                }

            });
            rt.execute();
        } catch (JSONException j) {
            j.printStackTrace();
        }
    }

    public void RejectBidrequest(String project_id) {
        try {
            JSONObject jo = new JSONObject();
            jo.put(Constants.ACTION, Constants.ACCEPTREJECTAWARD_REQUEST);
            jo.put("user_id", Utility.getSharedPreferences(mContext, Constants.USERID));
            jo.put("status", "2");
            jo.put("project_id", project_id);


            Utility.ShowLoading(mContext, mContext.getResources().getString(R.string.please_wait));
            rt = new ResponseTask(Constant_Urls.SERVER_URL, jo);
            rt.setListener(new ResponseListener() {
                @Override
                public void onGetPickSuccess(String result) {
                    Utility.HideDialog();
                    try {
                        JSONObject j = new JSONObject(result);
                        if (j.getString("msg").equals("1")) {
                            Utility.showToast(mContext, "request rejected");
                        } else {
                            Utility.showToast(mContext, j.getString("msg"));
                        }


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                }

            });
            rt.execute();
        } catch (JSONException j) {
            j.printStackTrace();
        }
    }

    public class SP_ProjectDetail implements View.OnClickListener {
        int pos;

        public SP_ProjectDetail(int pos) {
            this.pos = pos;
        }

        @Override
        public void onClick(View v) {
            try {
                Utility.showToast(mContext, "coming soon");
               /* Utility.setSharedPreference(mContext, Constants.PROJECTID,list.get(pos).getString("id"));
                Utility.setSharedPreference(mContext, Constants.PROJECT_TITLE,list.get(pos).getString("project_name"));*/
                // mContext.startActivity(new Intent(mContext, Bidder_Project_Detail_Activity.class));
            } catch (Exception e) {
                e.printStackTrace();
            }
            // Utility.setSharedPreference(mContext,list.get(pos));

        }
    }

    public class ViewHolder {

        CustomTextView sp_uname, sp_proname, sp_desc, bidder_completion_days, sp_budget;
        com.h_plus.customwidget.CircleImageView b_image_v;
        CustomButton sp_btn_accept_project, sp_btn_reject_project;
        LinearLayout sp_acceptreject_layout;
        ImageView flag;
        ProgressBar adapter_pb;

        public ViewHolder(View base) {

            sp_uname = (CustomTextView) base.findViewById(R.id.sp_uname);
            sp_proname = (CustomTextView) base.findViewById(R.id.sp_proname);
            sp_desc = (CustomTextView) base.findViewById(R.id.sp_desc);
            bidder_completion_days = (CustomTextView) base.findViewById(R.id.bidder_completion_days);
            sp_budget = (CustomTextView) base.findViewById(R.id.sp_budget);
            sp_btn_accept_project = (CustomButton) base.findViewById(R.id.sp_btn_accept_project);
            // sp_btn_detail = (CustomButton) base.findViewById(sp_btn_detail);
            sp_btn_reject_project = (CustomButton) base.findViewById(R.id.sp_btn_reject_project);
            sp_acceptreject_layout = (LinearLayout) base.findViewById(R.id.sp_acceptreject_layout);

            b_image_v = (com.h_plus.customwidget.CircleImageView) base.findViewById(R.id.hire_image);
            flag = (ImageView) base.findViewById(R.id.flag);
            adapter_pb = (ProgressBar) base.findViewById(R.id.adapter_pb);

        }
    }

    public class AcceptBidRequestClick implements View.OnClickListener {
        int pos;

        public AcceptBidRequestClick(int pos) {
            this.pos = pos;
        }

        @Override
        public void onClick(View v) {
            try {
                AcceptBidrequest(mlist.get(pos).getString("project_id"));
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }

    public class RejectBidrequestClick implements View.OnClickListener {
        int pos;

        public RejectBidrequestClick(int pos) {
            this.pos = pos;
        }

        @Override
        public void onClick(View v) {
            try {
                RejectBidrequest(mlist.get(pos).getString("project_id"));
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }
}
