package com.h_plus.HP_Service_Provider.adapter;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.h_plus.HP_Service_Provider.Add_Exeprience_Activity;
import com.h_plus.R;
import com.h_plus.ServerInteraction.ResponseListener;
import com.h_plus.ServerInteraction.ResponseTask;
import com.h_plus.customwidget.CustomTextView;
import com.h_plus.usertabs.utility.Constant_Urls;
import com.h_plus.usertabs.utility.Constants;
import com.h_plus.usertabs.utility.Utility;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Infograins on 1/9/2017.
 */

public class Adapter_ExperienceDetailView extends BaseAdapter {
    ResponseTask rt;
    Context mContext;
    int res;
    ArrayList<JSONObject> mlist;

    public Adapter_ExperienceDetailView(Context context, int res, ArrayList<JSONObject> list) {
        this.mContext = context;
        this.res = res;
        this.mlist = list;

    }

    @Override
    public int getCount() {
        return mlist.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        final ViewHolder viewHolder;

        if (convertView == null) {
            LayoutInflater li = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = li.inflate(res, null);
            viewHolder = new ViewHolder(convertView);

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        JSONObject j = mlist.get(position);
        try {
            viewHolder.portfolio_exp_spname.setText(j.getString("name"));
            viewHolder.portfolio_exp_cmpny_name.setText(j.getString("company"));
            viewHolder.portfolio_exp_since.setText(j.getString("joiningmonth")+" , "+j.getString("joiningyear")
                    +" - "+j.getString("leavingmonth")+" , "+j.getString("leavingyear"));
            viewHolder.portfolio_exp_year_of_exp.setText(j.getString("totalexperience"));
            viewHolder.portfolio_exp_exp_detail.setText(j.getString("desc"));


           /* if(Utility.getSharedPreferences(mContext,Constants.HIDE_EDITEXP_KEY).equals("0")){
                viewHolder.main.setVisibility(View.GONE);
            }*/
            if(Utility.getSharedPreferences(mContext,Constants.USER_TYPE).equals("1")){
                viewHolder.main.setVisibility(View.GONE);
            }
        } catch (JSONException je) {
            je.printStackTrace();
        }

        viewHolder.edit_exp.setOnClickListener(new EditExperience(position));
        viewHolder.delete_exp.setOnClickListener(new DeleteExperince(position));
        return convertView;
    }

    public void DeleteAlert(final int pos) {
        try {
            final int pos1 = pos;
            final AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
            builder.setTitle("Delete");
            builder.setIcon(R.drawable.ic_launcher);
            builder.setMessage("If you want to delete?")
                    .setCancelable(false).setPositiveButton("YES", new DialogInterface.OnClickListener() {
                public void onClick(final DialogInterface dialog, final int id) {
                    try {
                        DeleteExperienceTask(mlist.get(pos1).getString("experience_id"),pos);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            }).setNegativeButton("NO", new DialogInterface.OnClickListener() {
                public void onClick(final DialogInterface dialog, final int id) {
                    dialog.cancel();
                    dialog.dismiss();
                }
            });
            final AlertDialog alert = builder.create();
            alert.show();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void DeleteExperienceTask(String mPosition, final int pos) {

        try {
            JSONObject jo = new JSONObject();
            jo.put(Constants.ACTION, Constants.DELETE_EXPERIENCEBYSERVICEPROVIDER);
            jo.put("user_id", Utility.getSharedPreferences(mContext, Constants.USERID));
            jo.put("experience_id", mPosition);
            Utility.ShowLoading(mContext, mContext.getResources().getString(R.string.please_wait));

            rt = new ResponseTask(Constant_Urls.SERVER_URL, jo);
            rt.setListener(new ResponseListener() {
                @Override
                public void onGetPickSuccess(String result) {
                    Utility.HideDialog();
                    try {
                        JSONObject j = new JSONObject(result);
                        if (result == null) {
                            Utility.showToast(mContext, mContext.getResources().getString(R.string.server_fail));
                        } else {
                            mlist.remove(pos);
                            notifyDataSetChanged();
                            Utility.showToast(mContext, "your experience detail deleted successfully");
                        }
                    } catch (JSONException je) {
                        je.printStackTrace();
                    }
                }
            });
            rt.execute();
        } catch (JSONException j) {
            j.printStackTrace();
        }

    }

    public class ViewHolder {

        CustomTextView portfolio_exp_spname, portfolio_exp_cmpny_name, portfolio_exp_year_of_exp, portfolio_exp_since, portfolio_exp_exp_detail;
        ImageView edit_exp, delete_exp;
        LinearLayout main;


        public ViewHolder(View base) {

            portfolio_exp_spname = (CustomTextView) base.findViewById(R.id.portfolio_exp_spname);
            portfolio_exp_cmpny_name = (CustomTextView) base.findViewById(R.id.portfolio_exp_cmpny_name);
            portfolio_exp_year_of_exp = (CustomTextView) base.findViewById(R.id.portfolio_exp_year_of_exp);
            portfolio_exp_since = (CustomTextView) base.findViewById(R.id.portfolio_exp_since);
            portfolio_exp_exp_detail = (CustomTextView) base.findViewById(R.id.portfolio_exp_exp_detail);
            delete_exp = (ImageView) base.findViewById(R.id.delete_exp);
            edit_exp = (ImageView) base.findViewById(R.id.edit_exp);
            main = (LinearLayout) base.findViewById(R.id.main);

        }
    }

    public class DeleteExperince implements View.OnClickListener {
        int pos;

        public DeleteExperince(int pos) {
            this.pos = pos;
        }

        @Override
        public void onClick(View view) {

            try {
                DeleteAlert(pos);
            } catch (Exception e) {

            }
        }
    }

    public class EditExperience implements View.OnClickListener {

        int pos;

        public EditExperience(int pos) {
            this.pos = pos;
        }

        @Override
        public void onClick(View v) {
            Bundle b = new Bundle();
            try {
                Intent i = new Intent(mContext, Add_Exeprience_Activity.class);
                Utility.setSharedPreference(mContext,Constants.ADDEXPERIENCEKEY,"EE");
                b.putString("experience_id", mlist.get(pos).getString("experience_id"));
                i.putExtras(b);
                mContext.startActivity(i);
            } catch (JSONException ed) {
                ed.printStackTrace();
            }
        }
    }
}
