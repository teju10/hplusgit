package com.h_plus.HP_Service_Provider.adapter;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.BaseAdapter;

import com.h_plus.HP_Service_Provider.fragment.ShowMilestone_History_Activity;
import com.h_plus.R;
import com.h_plus.ServerInteraction.ResponseListener;
import com.h_plus.ServerInteraction.ResponseTask;
import com.h_plus.customwidget.CustomTextView;
import com.h_plus.usertabs.utility.Constant_Urls;
import com.h_plus.usertabs.utility.Constants;
import com.h_plus.usertabs.utility.Utility;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.LinkedList;

/**
 * Created by Infograins on 1/3/2017.
 */

public class Adapter_MilestoneHistory extends BaseAdapter {

    LinkedList<JSONObject> mlist;
    int res = 0;
    Context mContext;
    ResponseTask rt;
    Dialog dialog;

    public Adapter_MilestoneHistory(Context mContext, int res, LinkedList list) {
        this.mContext = mContext;
        this.res = res;
        this.mlist = list;
    }


    @Override
    public int getCount() {
        return mlist.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        final ViewHolder viewHolder;

        if (convertView == null) {
            LayoutInflater li = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = li.inflate(res, null);
            viewHolder = new ViewHolder(convertView);

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        JSONObject j = mlist.get(position);
        try {

            if (Utility.getSharedPreferences(mContext, Constants.USER_TYPE).equals("2")) {
                if (j.getString("status").equals("requested")) {
                    viewHolder.milestonehistory_status.setText("Pending Milestone Request");
                    viewHolder.milestone_requestaccept.setVisibility(View.GONE);
                    viewHolder.milestone_requestreject.setVisibility(View.GONE);
                } else if (j.getString("status").equals("accepted")) {
                    viewHolder.milestone_requestaccept.setVisibility(View.VISIBLE);
                    viewHolder.milestone_requestaccept.setText("Request for Released");
                    viewHolder.milestonehistory_status.setText("Request for Released");
                    viewHolder.milestone_requestreject.setVisibility(View.GONE);
                } else if (j.getString("status").equals("released_request")) {
                    viewHolder.milestonehistory_status.setText("Waiting for released request");
                } else if (j.getString("status").equals("released")) {
                    viewHolder.milestonehistory_status.setText("Payment Done");
                    viewHolder.milestone_requestaccept.setVisibility(View.GONE);
                    viewHolder.milestone_requestreject.setVisibility(View.GONE);
                }
                viewHolder.milestonehistory_amount.setText("$ " + j.getString("requested_amount"));

            } else if (Utility.getSharedPreferences(mContext, Constants.USER_TYPE).equals("1")) {
                if (j.getString("status").equals("requested")) {
                    viewHolder.milestonehistory_status.setText("Requested Milestone");
                    viewHolder.milestone_requestaccept.setVisibility(View.VISIBLE);
                    viewHolder.milestone_requestreject.setVisibility(View.VISIBLE);

                } else if (j.getString("status").equals("accepted")) {
                    viewHolder.milestone_requestaccept.setVisibility(View.GONE);
                    viewHolder.milestone_requestreject.setVisibility(View.GONE);
                    viewHolder.milestonehistory_status.setText("Waiting for released request");
                } else if (j.getString("status").equals("released_request")) {
                    viewHolder.milestonehistory_status.setText("Request for released milestone");
                    viewHolder.milestone_requestaccept.setVisibility(View.VISIBLE);
                    viewHolder.milestone_requestreject.setVisibility(View.VISIBLE);

                } else if (j.getString("status").equals("released")) {
                    viewHolder.milestonehistory_status.setText("Payment Done");
                    viewHolder.milestone_requestaccept.setVisibility(View.GONE);
                    viewHolder.milestone_requestreject.setVisibility(View.GONE);
                }
                viewHolder.milestonehistory_amount.setText("$ " + j.getString("requested_amount"));
            }

        } catch (JSONException je) {
            je.printStackTrace();
        }
        viewHolder.milestone_requestaccept.setOnClickListener(new AcceptMilestoneRequest(position));
        viewHolder.milestone_requestreject.setOnClickListener(new RejectMilestoneRequest(position));

        return convertView;
    }

    public void AcceptMilestoneRequestTask(String action, final int pos, String milestone_id) {
        //Constants.ACCEPT_REJECT_MILESTONEREQUEST
        try {
            JSONObject jo = new JSONObject();
            jo.put(Constants.ACTION, action);
            jo.put("request_id", milestone_id);
            jo.put("status", "1");

            Utility.ShowLoading(mContext, mContext.getResources().getString(R.string.please_wait));
            rt = new ResponseTask(Constant_Urls.SERVER_URL, jo);
            rt.setListener(new ResponseListener() {
                @Override
                public void onGetPickSuccess(String result) {
                    Utility.HideDialog();
                    try {
                        JSONObject j = new JSONObject(result);
                        if (j.getString("success").equals("1")) {
                            Utility.showToast(mContext, j.getString("msg"));

                            ((ShowMilestone_History_Activity) mContext).removeaccptitem(pos);
                            dialog.dismiss();
                        } else if (j.getString("msg").equals("sorry wallet amount in sufficent")) {
                            dialog.dismiss();
                            Utility.showPromptDlgForSuccess(mContext, "Alert", "Sorry!Wallet amount is insufficient");
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                }

            });
            rt.execute();
        } catch (JSONException j) {
            j.printStackTrace();
        }
    }

    public void RejectMilestoneRequestTask(String action, final int pos, String milestone_id) {
        try {
            JSONObject jo = new JSONObject();
            jo.put(Constants.ACTION, action);
            jo.put("request_id", milestone_id);
            jo.put("status", "2");

            Utility.ShowLoading(mContext, mContext.getResources().getString(R.string.please_wait));
            rt = new ResponseTask(Constant_Urls.SERVER_URL, jo);
            rt.setListener(new ResponseListener() {
                @Override
                public void onGetPickSuccess(String result) {
                    Utility.HideDialog();
                    try {
                        JSONObject j = new JSONObject(result);
                        if (j.getString("success").equals("1")) {
                            Utility.showToast(mContext, j.getString("msg"));
                            // ((ShowMilestone_History_Activity) mContext).removeitem(pos);
                        } else {
                            Utility.showToast(mContext, j.getString("msg"));
                        }


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                }

            });
            rt.execute();
        } catch (JSONException j) {
            j.printStackTrace();
        }
    }

    public void AcceptRejectMilestoneDialog(final int pos) {

        dialog = new Dialog(mContext);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);
        dialog.setContentView(R.layout.dialog_select_reject_milestone_request);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        dialog.findViewById(R.id.request_milestone_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    if (mlist.get(pos).getString("status").equals("requested")) {
                        AcceptMilestoneRequestTask(Constants.ACCEPT_REJECT_MILESTONEREQUEST, pos, mlist.get(pos).getString("milestone_id"));
//                        ((CustomTextView) dialog.findViewById(R.id.milestone_warning_text)).setText("");

                    } else if (mlist.get(pos).getString("status").equals("accepted")) {
                        AcceptMilestoneRequestTask(Constants.SP_RELEASEDMILESTONE_REQUEST, pos, mlist.get(pos).getString("milestone_id"));
                    } else if (mlist.get(pos).getString("status").equals("released_request")) {
                        System.out.println("KY HORELLLLAAAAAAAAAAA DADA--------------");
                        AcceptMilestoneRequestTask(Constants.HIRE_RELEASEDMILESTONE_AMOUNT, pos, mlist.get(pos).getString("milestone_id"));
                    }
                } catch (JSONException ex) {
                    ex.printStackTrace();
                }
            }
        });

        dialog.findViewById(R.id.request_milestone_cancel_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.cancel();
            }
        });
        dialog.findViewById(R.id.cancel_send_milestone_dialog).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.cancel();
            }
        });
        dialog.show();
    }

    public class AcceptMilestoneRequest implements View.OnClickListener {

        int pos;

        public AcceptMilestoneRequest(int pos) {
            this.pos = pos;
        }

        @Override
        public void onClick(View v) {
            AcceptRejectMilestoneDialog(pos);
        }
    }

    public class RejectMilestoneRequest implements View.OnClickListener {

        int pos;

        public RejectMilestoneRequest(int pos) {
            this.pos = pos;
        }

        @Override
        public void onClick(View v) {
            try {
                if (mlist.get(pos).getString("status").equals("requested")) {
                    RejectMilestoneRequestTask(Constants.ACCEPT_REJECT_MILESTONEREQUEST, pos, mlist.get(pos).getString("milestone_id"));
                } else if (mlist.get(pos).getString("status").equals("accepted")) {
                    RejectMilestoneRequestTask(Constants.HIRE_RELEASEDMILESTONE_AMOUNT, pos, mlist.get(pos).getString("milestone_id"));

                }
            } catch (JSONException ex) {
                ex.printStackTrace();
            }
        }
    }

    public class ViewHolder {

        CustomTextView milestonehistory_status, milestonehistory_amount, milestone_requestaccept, milestone_requestreject;

        public ViewHolder(View b) {
            milestonehistory_status = (CustomTextView) b.findViewById(R.id.milestonehistory_status);
            milestonehistory_amount = (CustomTextView) b.findViewById(R.id.milestonehistory_amount);
            milestone_requestaccept = (CustomTextView) b.findViewById(R.id.milestone_requestaccept);
            milestone_requestreject = (CustomTextView) b.findViewById(R.id.milestone_requestreject);
        }
    }
}
