package com.h_plus.fragment_digg;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.ListView;

import com.h_plus.R;
import com.h_plus.ServerInteraction.ResponseListener;
import com.h_plus.ServerInteraction.ResponseTask;
import com.h_plus.useractivity.HomeLocation;
import com.h_plus.H_Booking.TripIT.adapter.MyHomeAddedPost_Adapter;
import com.h_plus.usertabs.utility.Constant_Urls;
import com.h_plus.usertabs.utility.Constants;
import com.h_plus.usertabs.utility.Utility;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Infograins on 13-Dec-16.
 */

public class MyHomePosted extends AppCompatActivity {
    public static final String TAG = "MyHomePosted";
    Context mContext;
    ResponseTask responseTask;
    ListView list_data;
    ArrayList<JSONObject> jObjectArrayList;
    MyHomeAddedPost_Adapter mhpadapter;
    ArrayList<String> listimg = new ArrayList<>();


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        setContentView(R.layout.listview);
        super.onCreate(savedInstanceState);
        mContext = this;
        bind();
        init();
    }

    
    private void bind() {
        jObjectArrayList = new ArrayList<>();
        list_data = (ListView) findViewById(R.id.listView);
        mhpadapter = new MyHomeAddedPost_Adapter(mContext, R.layout.my_home_list_item, jObjectArrayList, MyHomePosted.this);
        list_data.setAdapter(mhpadapter);
    }

    private void init() {
        Get_MyhomePostdata();
    }

    private void Get_MyhomePostdata() {
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put(Constants.ACTION, Constants.LISTALLMYDIGS);
            jsonObject.put("user_id", Utility.getSharedPreferences(mContext, Constants.USERID));
            //jsonObject.put("user_id","99");
            Utility.ShowLoading(mContext, mContext.getResources().getString(R.string.please_wait));
            responseTask = new ResponseTask(Constant_Urls.SERVER_URL, jsonObject);
            responseTask.setListener(new ResponseListener() {

                @Override
                public void onGetPickSuccess(String result) {
                    Utility.HideDialog();
                    if (result == null) {
                        Utility.ShowToastMessage(mContext, "sever not responding");
                    } else {
                        try {
                            JSONObject jsonObject = new JSONObject(result);
                            if (jsonObject.getString(Constants.RESULT_KEY).equals("1")) {
                                JSONArray jsonArray = jsonObject.getJSONArray("object");

                                Log.e(TAG, "result ===============> " + jsonArray.toString());
                                for (int i = 0; i < jsonArray.length(); i++) {
                                    jObjectArrayList.add(jsonArray.getJSONObject(i));
                                }
                            } else {
                                Utility.showCroutonWarn(jsonObject.getString(Constants.SERVER_MSG), MyHomePosted.this);
                            }

                            mhpadapter.notifyDataSetChanged();

                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
        responseTask.execute();
    }

    public void DeleteSelectedAdd(final int sel_pos) {
        try {
            String id = jObjectArrayList.get(sel_pos).getString("id");
            JSONObject jsonObject = new JSONObject();
            jsonObject.put(Constants.ACTION, Constants.DELETEDIGBYID);
            jsonObject.put("user_id", Utility.getSharedPreferences(mContext, Constants.USERID));
            // jsonObject.put("user_id","99");
            jsonObject.put("id", id);
            Utility.ShowLoading(mContext, getResources().getString(R.string.please_wait));
            responseTask = new ResponseTask(Constant_Urls.SERVER_URL, jsonObject);
            responseTask.setListener(new ResponseListener() {
                @Override
                public void onGetPickSuccess(String result) {
                    Utility.HideDialog();
                    if (result == null) {
                        Utility.showCroutonWarn(getResources().getString(R.string.server_fail),  MyHomePosted.this);
                    } else {
                        try {
                            JSONObject jobj = new JSONObject(result);
                            if (jobj.getString(Constants.RESULT_KEY).equals("1")) {
                                jObjectArrayList.remove(sel_pos);
                                mhpadapter.notifyDataSetChanged();
                                Utility.showToast(mContext, "Adds Deleted Successfully");
                            } else {
                                Utility.showCroutonWarn(jobj.getString(Constants.SERVER_MSG),  MyHomePosted.this);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
        responseTask.execute();
    }

    public void DetailEditSelectedAdd(int sel_pos) {
        try {
            String id = jObjectArrayList.get(sel_pos).getString("id");

            GetDetailEdite(id);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void GetDetailEdite(final String id) {
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put(Constants.ACTION, Constants.DIGDETAILBYID);
            // JSONArray jArry=jsonObject.getJSONArray("images");
           /* for (int i=0;i<jArry.length();i++){
                listimg.add(jArry.getString(i));
            }*/
            //jsonObject.put("user_id",Utility.getSharedPreferences(mContext,Constants.USERID));
            jsonObject.put("id", id);
            Utility.ShowLoading(mContext, mContext.getResources().getString(R.string.please_wait));
            responseTask = new ResponseTask(Constant_Urls.SERVER_URL, jsonObject);
            responseTask.setListener(new ResponseListener() {
                @Override
                public void onGetPickSuccess(String result) {
                    Utility.HideDialog();
                    if (result == null) {
                        Utility.showCroutonWarn(getResources().getString(R.string.server_fail),  MyHomePosted.this);
                    } else {
                        try {

                            JSONObject jobj = new JSONObject(result);
                            if (jobj.getString(Constants.RESULT_KEY).equals("1")) {
                                JSONObject jasobj = jobj.getJSONObject("object");
                                // prop_img=jarr.getString("ownership");
                                // Glide.with(mContext).load(jarr.getString("image_url")).into((circleImageView));

                                Bundle extraa = new Bundle();
                                extraa.putString(Constants.ID, id);
                                extraa.putString(Constants.MODE, "edit_mode");
                                extraa.putString(Constants.OBJECT, jasobj.toString());

                                Intent in = new Intent(mContext, HomeLocation.class);
                                in.putExtras(extraa);
                                System.out.println("GET value" + extraa);
                                startActivity(in);
                              finish();

                            } else {
                                Utility.showCroutonWarn(jobj.getString(Constants.SERVER_MSG),  MyHomePosted.this);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            });
            responseTask.execute();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
