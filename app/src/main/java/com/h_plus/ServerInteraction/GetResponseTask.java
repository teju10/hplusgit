package com.h_plus.ServerInteraction;

import android.content.Context;
import android.os.AsyncTask;
import android.support.v4.app.Fragment;
import android.util.Log;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;


/**
 * Created by Nilesh Kansal on 22-Jul-16.
 */
public class GetResponseTask extends AsyncTask<String, String, String> {

    String TAG = "";
    Fragment fragment;
    Context appContext;
    //Boolean mProgress = true;
    String result = "", serverURL;
    ResponseListener mListener;

    public GetResponseTask(String serverURL1, Context mContext1, String TAG1) {
        appContext = mContext1;
        serverURL = serverURL1;
        TAG = TAG1;
    }

    public GetResponseTask(String serverURL2, Context mContext2, Fragment fragment2, String TAG2) {
        appContext = mContext2;
        fragment = fragment2;
        serverURL = serverURL2;
        TAG = TAG2;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    //    Utility.ShowLoading(appContext, appContext.getResources().getString(R.string.please_wait));
    }

    @Override
    protected String doInBackground(String... params) {
        String result = getDataFromUrl(serverURL);
        return result;
    }

    @Override
    protected void onPostExecute(String result) {
        super.onPostExecute(result);
        Log.e(TAG, "result ===== " + result);
        try {
         //   Utility.HideDialog();
            returnResult(result);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void returnResult(String result) {

       /* if (TAG.equals("FR")) {
            mListener = (ResponseListener) fragment;
        } else {
            mListener = (ResponseListener) appContext;
        }*/
        if (mListener != null) {
            mListener.onGetPickSuccess(result);
        }
    }

    public static String getDataFromUrl(String url) {
        String result = null;
        try {
            URL myurl=new URL(url);
            HttpURLConnection urlConnection = (HttpURLConnection) myurl
                    .openConnection();
            urlConnection.setRequestMethod("GET");
            urlConnection.setDoInput(true);
            urlConnection.connect();
            InputStream is=urlConnection.getInputStream();
            if (is != null) {
                StringBuilder sb = new StringBuilder();
                String line;
                try {
                    BufferedReader reader = new BufferedReader(
                            new InputStreamReader(is));
                    while ((line = reader.readLine()) != null) {
                        sb.append(line);
                    }
                    reader.close();
                } finally {
                    is.close();
                }
                result = sb.toString();
            }
        }catch (Exception e){
            result=null;
        }
        return result;
    }
    public void setListener(ResponseListener listener) {
        mListener = listener;
    }
}