package com.h_plus.ServerInteraction;

import android.os.AsyncTask;
import android.util.Log;

import com.h_plus.usertabs.utility.Constant_Urls;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.net.UnknownHostException;
import java.util.Iterator;

import javax.net.ssl.HttpsURLConnection;

import static android.R.attr.action;


/**
 * Created by Nilesh Kansal on 12-Jul-16.
 */

public class ResponseTask extends AsyncTask<String, String, String> {

    //Boolean mProgress = true;
    String result = "", serverURL,method = "";
    JSONObject param;
    ResponseListener mListener;

    public ResponseTask(String serverURL1, JSONObject param1) {
        serverURL = serverURL1;
        param = param1;
    }

    public static String postParamsAndfindJSON(String url, JSONObject params) {
        try {
            Log.e("RESULT ",url+getParams(params));
            URL myurl = new URL(url);
            HttpURLConnection conn = (HttpURLConnection) myurl.openConnection();
            conn.setReadTimeout(10000 /* milliseconds */);
            conn.setConnectTimeout(10000 /* milliseconds */);
            conn.setRequestMethod("POST");
            conn.setDoInput(true);
            conn.setDoOutput(true);
            OutputStream os = conn.getOutputStream();
            BufferedWriter writer = new BufferedWriter(
                    new OutputStreamWriter(os, "UTF-8"));
            writer.write(getParams(params));
            writer.flush();
            writer.close();
            os.close();
            int responseCode = conn.getResponseCode();
            if (responseCode == HttpsURLConnection.HTTP_OK) {
                BufferedReader in = new BufferedReader(
                        new InputStreamReader(
                                conn.getInputStream()));
                StringBuffer sb = new StringBuffer("");
                String line = "";
                while ((line = in.readLine()) != null) {
                    sb.append(line);
                    break;
                }
                in.close();
                return sb.toString();
            } else {
                return null;
            }
        } catch (Exception e) {
            return null;
        }
    }

    public static String getParams(JSONObject parameter) throws Exception {
        StringBuilder result = new StringBuilder();
        boolean first = true;
        Iterator<String> itr = parameter.keys();
        while (itr.hasNext()) {
            String key = itr.next();
            Object value = parameter.get(key);
            if (first)
                first = false;
            else
                result.append("&");

            result.append(URLEncoder.encode(key, "UTF-8"));
            result.append("=");
            result.append(URLEncoder.encode(value.toString(), "UTF-8"));

        }
        return result.toString();
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected String doInBackground(String... params) {
        if (method.equalsIgnoreCase("GET")) {
            result = FindJSONFromUrl(Constant_Urls.SERVER_URL+ action);
        } else {
             result = postParamsAndfindJSON(serverURL, param);
        }


        return result;
    }

    @Override
    protected void onPostExecute(String result) {
        System.out.println("result"+result);
        try {
            returnResult(result);
        } catch (Exception e) {
        }
    }

    public void setListener(ResponseListener listener) {
        mListener = listener;
    }

    private void returnResult(String result) {
        if (mListener != null) {
            mListener.onGetPickSuccess(result);
        }
    }

    public String FindJSONFromUrl(String targetURL) {
        StringBuffer response = new StringBuffer();

        System.out.println("URL comes in json parser class is ========= " + targetURL);
        try {
            URL myURL = new URL(targetURL);
            URLConnection urlConnection;
            HttpURLConnection httpsURLConnection;
            //   SSLSocketFactory sslSocketFactory;
            BufferedReader in;
            String inputLine;
            if (targetURL.contains("http")) {
                urlConnection = myURL.openConnection();
                httpsURLConnection = (HttpsURLConnection) urlConnection;
                /*sslSocketFactory = createSslSocketFactory();
                httpsURLConnection.setSSLSocketFactory(sslSocketFactory);
                httpsURLConnection.setHostnameVerifier(new HostnameVerifier() {
                    @Override
                    public boolean verify(String s, SSLSession sslSession) {
                        return true;
                    }
                });
*/
                httpsURLConnection.setReadTimeout(10000); /* milliseconds */
                httpsURLConnection.setConnectTimeout(10000); /* milliseconds */
                httpsURLConnection.setRequestMethod("GET");
                httpsURLConnection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
                httpsURLConnection.setUseCaches(false);
                httpsURLConnection.setDoInput(true);
                httpsURLConnection.setDoOutput(true);
                OutputStream os = httpsURLConnection.getOutputStream();
                BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(os, "UTF-8"));
                writer.write(getPostDataString(param));
                writer.flush();
                writer.close();
                os.close();
                httpsURLConnection.connect();
                int responseStatus = httpsURLConnection.getResponseCode();
                System.out.println("status in json parser class ........" + responseStatus);
                if (responseStatus == HttpURLConnection.HTTP_OK) {
                    in = new BufferedReader(new InputStreamReader(httpsURLConnection.getInputStream()));
                } else {
                    in = new BufferedReader(new InputStreamReader(httpsURLConnection.getErrorStream()));
                }
            } else {
                HttpURLConnection httpURLConnection = (HttpURLConnection) (myURL).openConnection();
                httpURLConnection.setReadTimeout(10000); // milliseconds
                httpURLConnection.setConnectTimeout(10000); // milliseconds
                httpURLConnection.setRequestMethod("GET");
                httpURLConnection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
                httpURLConnection.setUseCaches(false);
                httpURLConnection.setDoInput(true);
                httpURLConnection.setDoOutput(true);
                OutputStream os = httpURLConnection.getOutputStream();
                BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(os, "UTF-8"));
                writer.write(getPostDataString(param));
                writer.flush();
                writer.close();
                os.close();
                httpURLConnection.connect();
                int responseStatus = httpURLConnection.getResponseCode();
                System.out.println("content encoding in json parser class ........" + httpURLConnection.getContentEncoding());
                System.out.println("status in json parser class ........" + responseStatus);
                if (responseStatus == HttpURLConnection.HTTP_OK) {
                    in = new BufferedReader(new InputStreamReader(httpURLConnection.getInputStream()));
                } else {
                    in = new BufferedReader(new InputStreamReader(httpURLConnection.getErrorStream()));
                }
            }

            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();
            System.out.println("result in json parser class ........" + response.toString());
            return response.toString();
        } catch (SocketTimeoutException e) {
            System.out.println("SocketTimeoutException in jsonparser class ........");
            e.printStackTrace();
            return "SocketTimeoutException";
        } catch (UnknownHostException e) {
            System.out.println("UnknownHostException in jsonparser class ........");
            e.printStackTrace();
            return null;
        }/* catch (SSLException e) {
            System.out.println("SSLException in jsonparser class ........");
            e.printStackTrace();
            return null;
        }*/ catch (Exception e) {
            System.out.println("exception in jsonparser class ........");
            e.printStackTrace();
            return null;
        }
    }

    public String getPostDataString(JSONObject params) throws Exception {
        StringBuilder result = new StringBuilder();
        boolean first = true;
        Iterator<String> itr = params.keys();
        while (itr.hasNext()) {
            String key = itr.next();
            Object value = params.get(key);

            if (first)
                first = false;
            else
                result.append("&");

            result.append(URLEncoder.encode(key, "UTF-8"));
            result.append("=");
            result.append(URLEncoder.encode(value.toString(), "UTF-8"));

        }
        return result.toString();
    }


}