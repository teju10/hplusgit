package com.h_plus.userfragment;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.h_plus.R;
import com.h_plus.ServerInteraction.ResponseTask;
import com.h_plus.useradapter.UserList_Adapter;

/**
 * Created by Infograins on 11/21/2016.
 */

public class Fragment_Home extends Fragment implements View.OnClickListener {

    Context mContext;
    View rooView;
    ResponseTask rt;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rooView = inflater.inflate(R.layout.fragment_home, container, false);
        mContext = getActivity();
        ((ListView) rooView.findViewById(R.id.user_list)).setAdapter(new UserList_Adapter(mContext));
        Init();
        return rooView;
    }

    public void Init() {

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

           /* case R.id.home_share_status:
                Utility.showToast(mContext, "comming soon");
                break;
            case R.id.home_edit_profile:
                startActivity(new Intent(mContext, EditProfile_Activity.class));
                break;*/
        }
    }


}
