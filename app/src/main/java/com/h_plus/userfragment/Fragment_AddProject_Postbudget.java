package com.h_plus.userfragment;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.h_plus.R;
import com.h_plus.ServerInteraction.ResponseTask;
import com.h_plus.RegistrationSection.MainActivity;
import com.h_plus.usertabs.utility.Constants;
import com.h_plus.customwidget.CustomButton;

/**
 * Created by Tejas on 11/21/2016.
 */

public class Fragment_AddProject_Postbudget extends Fragment {
    Context mContext;
    View rootView;
    ResponseTask rt;
    CustomButton home_next2;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = getActivity();
        //share = mContext.getSharedPreferences("pref", Context.MODE_PRIVATE);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
         rootView = inflater.inflate(R.layout.fragment_addproject_postbudget, container, false);
        home_next2 = (CustomButton) rootView.findViewById(R.id.home_next2);
        Init();
        return rootView;
    }

    public void Init() {

        home_next2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((MainActivity)mContext).callSelectedFragment(Constants.FRAGMENT_HOME_PROJECTSUMMARY,"Post a Project");
            }
        });

    }
}
