package com.h_plus.userfragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.h_plus.R;

/**
 * Created by Muzammil on 7/15/2016.
 */
public class PriceTab extends Fragment {
    View rootView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.discover_tab_layout, container, false);
        return rootView;
    }
}
