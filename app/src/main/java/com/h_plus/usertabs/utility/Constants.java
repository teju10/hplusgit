package com.h_plus.usertabs.utility;

import android.os.Environment;

public class Constants {
    public static final String SDCARD_ROOT = Environment.getExternalStorageDirectory().toString();
    public static final String ACTION = "action";
    public static final String FNAME = "first_name";
    public static final String LNAME = "last_name";
    public static final String PASS = "password";
    public static final String PHNO = "phone_number";
    public static final String GCMID = "gcmid";
    public static final String DEVICETYPE = "device_type";
    public static final String ANDROID = "Android";
    public static final String ADDRESS = "shipping_address";
    public static final String EMAIL = "email";
    public static final String IMG = "image_url";
    public static final String USERNAME = "username";
    public static final String PRO_IMG = "";
    public static final String O_PROVIDER = "outhprovider";
    public static final String USERID = "userid";
    public static final String TO_USERID = "to_userid";
    public static final String SPID = "sp_userid";
    public static final String VERIFICATION_CODE = "verification_code";
    public static final String SERVER_MSG = "msg";
    public static final String PROFILE_IMAGE = "profile_image";
    public static final String PAYPAL_VERIFICATION_CHECKED = "paypal_verify_checked";
    public static final String USER_BIO = "user_bio";
    public static final String USER_TYPE = "user_type";
    public static final String PAYPAL_PAYMENT_VERIFICATION = "user_paypal_payment_verification";
    public static final String PROJECTID = "project_id";
    public static final String PROJECT_TITLE = "project_title";
    public static final String EDITPROJECTBTNCLICK = "edt_projct_btn";
    public static final String EDITPROJECT = "203";
    public static final String UPDATE_PROJECTID = "Update_id";
    public static final String SENDREQUESR_FORAPPROAVAL = "sendRquestForApproval";
    public static final String FRAGMENT_FREELANCER_BIDING_LIST = "Fragment_Freelancer_Biding_List";
    public static final String Fragment_ProjectDetailandProposalListTab = "Fragment_ProjectDetailandProposalListTab";
    public static final String MyBidedProject = "MyBidedProject";
    public static final String Fragment_Freelancer_Biding_List = "Fragment_Freelancer_Biding_List";
    public static final String BACKPRESSEDNAME = "BackPressed";
    public static final String CHAT_DATA = "Chatdata";
    public static final String PROJECT_ID = "project_id";//rehendena imp he for bidlist
    public static final String PROFILETRUST_SCORE = "profile_trustscore";
    public static final String SOCIAL_LOGIN = "socailLogin";
    public static final String SP_LISYKEY = "splist_key";
    public static final String ADDEXPERIENCEKEY= "addexp_key";
    public static final String EDIT_EXPERIENCE= "editExperience";
    public static final String HIDE_EDITEXP_KEY= "edtexp_key";
    public static final String HIREPROVIDER= "hiresp_key";
    public static final String Hire_Home_MyProject_Fragment= "Hire_Home_MyProject_Fragment";
    public static final String RESULT_KEY= "success";


    /*Home URLs*/
    public static final String FRAGMENT_HOME = "Fragment_Home";
    public static final String FRAGMENT_HOME_POSTBUDGET = "Fragment_AddProject_Postbudget";
    public static final String FRAGMENT_POSTPROJECT= "Fragment_PostProject";
    public static final String FRAGMENT_HOME_PROJECTSUMMARY = "Fragment_Home_projectsummary";
    public static final String TABFRAGMENT = "Hire_Home_MyProject_Fragment";

    /* HIRE URLs*/
    public static final String SIGN_UP = "sign_up";
    public static final String POSTPROJECT = "post_project";
    public static final String LOGIN = "user_login";
    public static final String FORGETPASSWORD = "ForgetPassword";
    public static final String SEND_VERICATION_NUMBER = "send_verification_code";
    public static final String SEND_VERICATION_CODE = "check_phone_verification_code";
    public static final String GETUSERDETAILBYID = "getUserDetailByID";
    public static final String EDITPROFILE = "edit_profile";
    public static final String SPLASH_CHECK = "splash_check";
    public static final String CHANGEPW = "change_password";

    public static final String UPDATE_PROJECT = "edit_project";
    public static final String BIDLISTBYPROJECTID = "bidListByProjectID";
    public static final String HIRESIDE_LISTOFSERVICEPROVIDER = "listOfServiceprovider";
    public static final String MYPROJECT = "myProjectList";
    public static final String ShowMilestone_History_Fragment = "ShowMilestone_History_Activity";

    public static final String DELETEPROJECT = "deleteProject";

    public static final String CHATLIST = "ChatListBetweenUser";
    public static final String SENDCHATMESSAGE = "SendProjectChat";
    public static final String CHATBOXLIST = "chatList";
    public static final String CHECKCHATBOX_REDIRECT = "chat_box_redirect";
    public static final String LOGOUT = "LogoutByID";

    public static final String HIRE_SP_BIDINGREQUEST = "hire_me";



    /*SERVICE PROVIDER URLs*/
    public static final String SERVICEPROVIDER_MILESTONEHISTORY = "milestoneDetailServiceProvider";
    public static final String HIRE_MILESTONEHISTORY = "milestoneDetailsClientSide";
    public static final String LISTALLPROJECT = "listAllProject";
    public static final String SERVICEPROVIDER_BIDLIST = "bidListByUserId";
    public static final String SERVICEPROVIDER_CURRENTPROJECT = "CurrentProject";
    public static final String CREATEMILESTONE = "sendMilestone";
    public static final String GET_SP_MILESTONE_PROJECT_DETAIL = "projectDetailsByProjectId";
    public static final String ACCEPTREJECTAWARD_REQUEST = "acceptRejectAwardRequest";
    public static final String ACCEPT_REJECT_MILESTONEREQUEST = "acceptyarejectMilestone";
    public static final String SP_RELEASEDMILESTONE_REQUEST = "releaseRequestByServiceProvider";
    public static final String HIRE_RELEASEDMILESTONE_AMOUNT = "amountReleaseByClient";
    public static final String CATEGORYLIST = "categoryList";
    public static final String GETSELECTEDLIST = "listOfUserSkillById";
    public static final String ADDSKILLS = "addSkillByUserId";
    public static final String ADDPORTFOLIO = "addPortfolio";
    public static final String CERITIFICATELIST = "certificateListByid";
    public static final String ShowPortfolioatHireSideTask = "userAllDetailByUserId";
    public static final String ADDEXPERIENCE = "addExperience";
    public static final String SP_EXPERIENCELIST = "listExperienceByUserId";
    public static final String DELETE_EXPERIENCEBYSERVICEPROVIDER = "deleteExperienceByid";
    public static final String SHOWPORTFOLIO = "portfolioListByid";




    /*SOCIAL SIGNUPNAME*/
    public static final String SOCIAL_UNAME = "social_uname";
    public static final String SOCIAL_FIRSTNAME = "social_firstname";
    public static final String SOCIAL_LNAME = "social_lname";
    public static final String SOCIAL_EMAIL = "social_email";
    public static final String SOCIAL_GENDER = "social_gender";
    public static final String SOCIAL_PROFILEIMAGE = "social_profileimage";
    public static final String SOCIAL_ID = "social_id";
    public static final String Project_DetailBY_ID = "projectDetailByID";
    public static final String BIDTHEPROJECT = "bid_project";


    /*Home Digg*/
    public static final String PROPERTY_USE = "property_use";
    public static final String PROPERTY_TYPE = "property_type";
    public static final String PROPERTY_TITLE = "property_titles";
    public static final String PROPERTY_ADDRESS = "property_add";
    public static final String PROPERTY_CITY = "property_city";
    public static final String PROPERTY_STATE = "property_state";
    public static final String PROPERTY_COUNTYR = "property_country";
    public static final String PROPERTY_ZIP = "property_zip";
    public static final String PROPERTY_BUIL_AREA = "property_area";
    public static final String PROPERTY_HALL = "property_hall";
    public static final String PROPERTY_KITCHEN = "property_kitchen";
    public static final String PROPERTY_FLOORS = "property_floor";
    public static final String PROPERTY_BED = "property_bed";
    public static final String PROPERTY_BALCONIES = "property_balconies";
    public static final String PROPERTY_BATHROOM = "property_bathroom";
    public static final String PROPERTY_AVAIBILITY = "property_avaibility";
    public static final String PROPERTY_AGE_COUNT = "property_agecount";
    public static final String PROPERTY_AGE_YEAR = "property_ageyear";
    public static final String PROPERTY_TOTAL_PRICE = "property_totalprice";
    public static final String PROPERTY_DISCOUNT = "property_discount";
    public static final String PROPERTY_AFTER_DISCOUNT = "property_after_disc";
    public static final String PROPERTY_FROMDATE = "property_fromdate";
    public static final String PROPERTY_TODATE = "property_todate";
    public static final String PROPERTY_OWNERSHIP = "property_ownership";
    public static final String PROPERTY_FACING = "property_Facing";
    public static final String PROPERTY_TYPE_OF_FLOORING = "property_Type_of_Flooring";
    public static final String PROPERTY_FURNISHING = "property_Furnishing";
    public static final String PROPERTY_DESCRIPTION = "property_description";
    public static final String PROPERTY_FEATURES = "property_features";
    public static final String OBJECT = "object";
    public static final String MODE = "edit";
    public static final String ID = "id";
    public static final String MYHOMEPOSTED="MyHomePosted";

    public static final String APIKEY="AIzaSyAUg809ZTaMytKBo35CafN8NzwnhTIszZU";


    /*trip_it*/
    public static final String FRAGMENTTRIP_TABS = "Fragment_trip_tabs";
    /*trip_it  hotel booking*/
    public static final String DESTINATION = "destination";
    public static final String CHECKIN = "checkin";
    public static final String CHECKOUT = "checkout";
    public static final String ROOMS = "rooms";
    public static final String ADULT = "adult";
    public static final String CHILDRENS = "childrens";

    /*trip_it  flight booking*/
    public static final String FLYFROM = "fly-from";
    public static final String FLYTO = "fly-to";
    public static final String DEPARTURE_ON = "departure_on";
    public static final String RETURN_ON = "return_on";
    public static final String INFANTS = "infants";

    /*trip_it  car booking*/
    public static final String PICKUP = "pick_up";
    public static final String DROPOFF = "drop_off";
    public static final String PICKUP_DATE = "pick_up_date";
    public static final String DROPOFF_DATE = "drop_off_date";
    public static final String PICKUP_TIME = "pick_up_time";
    public static final String DROPOFF_TIME = "drop_off_time";

    public static final String GETCARDETAILBYID = "getCarDetailById";
    public static final String FLIGHTDETAILBYID = "flightDetailByid";
    public static final String MORDGAGE_SIGNUP = "mortagageSignup";
    public static final String CARSEARCHING = "carSearching";
    public static final String HOTELDETAILSBYID = "hotelDetailsByid";
    public static final String FLIGHTSEARCHING = "flightSearching";
    public static final String HOTELSEARCHING = "hotelSearching";
    public static final String LISTSALERENTVACATION = "ListSellRentVacation";
    public static final String SAVEDUNSAVEDHOMES = "SaveUnsaveHomes";

    /*Mordgage*/
    public static final String COMPANY_NAME = "company_name";
    public static final String COMPANY_ADD1 = "company_add1";
    public static final String COMPANY_ADD2 = "company_add2";
    public static final String CITY = "city";
    public static final String STATE = "state";
    public static final String ZIP = "zip";
    public static final String COMPANY_PHONE = "company_phone";
    public static final String COMPANY_FAX = "company_fax";
    public static final String COMPANY_SERVICE = "company_service";
    public static final String COMPANY_EMPLOYE_NO = "company_employe_no";

    public static final String DIGDETAILBYID = "DigDetailByID";
    public static final String DELETEDIGBYID = "deleteDigByID";
    public static final String LISTALLMYDIGS = "listAllMyDigs";
    public static final String UPDATEDIG = "update_dig";
    public static final String ADDDIGG = "add_dig";
    public static final String GETHOMEDETAILSBYDIGID = "getHomesDetailsByDigsId";
    public static final String ENQUIRYNOWBYUSER = "enquiryNowByUser";

    /*-------------------------Map Constants----------------------------*/
    public static final String MAP_NAME="name";
    public static final String MAP_LAT="latitude";
    public static final String MAP_LONG="longitude";
}
