package com.h_plus.usertabs.utility;

/**
 * Created by Muzammil & Nilesh on 04-Jan-16.
 */
public class Constant_Urls {

    public static final String LAT_LNG = "http://maps.google.com/maps/api/geocode/json?sensor=false&address=";
    public static final String DIRECTIONS_URL = "https://maps.googleapis.com/maps/api/directions/";
    //public static String SERVER_URL = "https://infograins.com/INFO01/homesplus/api/api.php?";
    //public static String SERVER_URL2s = "http://infograins.com.208-91-199-7.md-plesk-web2.webhostbox.net/INFO01/arkansalamat/api/api.php?";
    public static String SERVER_URL = "https://infograins.com/INFO01/info01_changes/api/api.php?";

    public static String GCM_SENDER_ID = "479967405674";
    public static String SERVER_KEY = "AIzaSyBsJ2KmkJ4q9Vsfh4e8lUjEFMuva2u2RsA";
    public static String DEMO_URL = "http://infograins.com.208-91-199-7.md-plesk-web2.webhostbox.net/INFO01/wa9al/api/api.php?action=";

    public static final class MyActions {
        public static final String MESSAGE_UPDATE = "message_update";
        public static final String SEND_MESSAGE = "send_message";
    }
}
