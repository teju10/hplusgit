package com.h_plus.usertabs;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.TextView;

import com.h_plus.R;
import com.h_plus.usertabs.utility.Utility;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * Created by Muzammil on 7/26/2016.
 */
public class ScheduledBookingTab extends Fragment implements View.OnClickListener {
    View rootView;
    Context mContext;
    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
    int mDay, mMonth, mYear;
    String  DATE,  from_date, to_date;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.scheduled_booking_tab, container, false);
        BINDING();
        mContext = getActivity();
        return rootView;
    }

    public void BINDING() {
        (rootView.findViewById(R.id.find_btn)).setOnClickListener(this);
        (rootView.findViewById(R.id.first_date)).setOnClickListener(this);
        (rootView.findViewById(R.id.last_date)).setOnClickListener(this);
    }

    /*================ For Comparision Date=====================*/
    public int CompareMyDate(String Date1, String Date2) {
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
        try {
            return sdf.parse(Date1).compareTo(sdf.parse(Date2));
        } catch (Exception e) {

        }
        return 0;
    }

    public int CompareMyDate2(String date) {
        Calendar ci = Calendar.getInstance();
        try {
            return sdf.parse(date).compareTo(sdf.parse(ci.get(Calendar.YEAR) + "-" + (ci.get(Calendar.MONTH) + 1) + "-" + ci.get(Calendar.DAY_OF_MONTH)));

        } catch (Exception e) {

        }
        return 0;
    }

    /*============================================>Set Date Picker<=============================================*/

    public void SetDatePicker(final String openFrom) {

        System.out.println("inside date picker");
        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View dialoglayout = inflater.inflate(R.layout.dialog_date_picker, null);
        final AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        builder.setView(dialoglayout);
        final DatePicker mDatePicker = (DatePicker) dialoglayout.findViewById(R.id.datePicker);
        mDatePicker.setDescendantFocusability(DatePicker.FOCUS_BLOCK_DESCENDANTS);
        builder.setCancelable(false).setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            public void onClick(final DialogInterface dialog, final int id) {
                mYear = mDatePicker.getYear();
                mMonth = mDatePicker.getMonth();
                mDay = mDatePicker.getDayOfMonth();
                mMonth = mMonth + 1;
                DecimalFormat formatter = new DecimalFormat("00");
                String aFormatted = formatter.format(mMonth);
                String aFormattedday = formatter.format(mDay);
                Log.d("mMonth ", "" + aFormatted);

                DATE = String.valueOf(mYear) + "-" + String.valueOf(aFormatted) + "-" + String.valueOf(aFormattedday);
                if (openFrom.equals("to")) {
                    to_date = DATE;
                    if (CompareMyDate(to_date, from_date) >= 0) {
                        ((TextView) rootView.findViewById(R.id.last_date)).setText(DATE);
                    } else {
                        Utility.showToast(mContext, "Your Date Is Not Greater Then From Date");
                    }
                } else {
                    from_date = DATE;
                    if (CompareMyDate2(from_date) >= 0) {
                        ((TextView) rootView.findViewById(R.id.first_date)).setText(DATE);
                    } else {
                        Utility.showToast(mContext, "Your Date Is Not Greater Then From Date");
                    }
                }
            }
        }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
                if (openFrom.equals("to")) {
                    to_date = "";
                    ((TextView) rootView.findViewById(R.id.last_date)).setText("");
                } else {
                    from_date = "";
                    ((TextView) rootView.findViewById(R.id.first_date)).setText("");
                }
            }
        });
        final AlertDialog alert = builder.create();
        alert.show();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.find_btn:
                if (((TextView) rootView.findViewById(R.id.first_date)).getText().toString().equals("")
                        && ((TextView) rootView.findViewById(R.id.last_date)).getText().toString().equals("")) {
                    Utility.Alert(mContext, "please select first & last  date");
                } else if (((TextView) rootView.findViewById(R.id.first_date)).getText().toString().equals("")) {
                    ((TextView) rootView.findViewById(R.id.first_date)).setError("First Date Missing");
                    ((TextView) rootView.findViewById(R.id.first_date)).requestFocus();
                } else if (((TextView) rootView.findViewById(R.id.last_date)).getText().toString().equals("")) {
                    ((TextView) rootView.findViewById(R.id.last_date)).setError("Last Date Missing");
                    ((TextView) rootView.findViewById(R.id.last_date)).requestFocus();
                } else {
                    /*new FindTask().execute(((TextView) rootView.findViewById(R.id.first_date)).getText().toString(),
                            ((TextView) rootView.findViewById(R.id.last_date)).getText().toString());*/
                }
                break;
            case R.id.first_date:
                SetDatePicker("from");
                break;
            case R.id.last_date:
                if (!from_date.equals("")) {
                    SetDatePicker("to");
                } else {
                    Utility.showToast(mContext, "Please Select To Date First");
                }
                break;
            default:
                break;
        }
    }

}
