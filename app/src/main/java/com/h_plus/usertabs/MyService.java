package com.h_plus.usertabs;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;

/**
 * Created by Tejus Dani on 08-Mar-16.
 */
public class MyService extends Service {
    // Binder given to clients
    public final IBinder binder = new LocalBinder();
    // Registered callbacks
    private ServiceCallbacks serviceCallbacks;


    // Class used for the client Binder.
    public class LocalBinder extends Binder {
       public MyService getService() {
            // Return this instance of MyService so clients can call public methods
            return MyService.this;
        }
    }
@Override
public int onStartCommand(Intent intent, int flags, int startId) {
    System.out.println("call Service");
    if (serviceCallbacks != null) {
        serviceCallbacks.doSomething();
    }
    return super.onStartCommand(intent, flags, startId);
}


    @Override
    public IBinder onBind(Intent intent) {
        return binder;
    }

    public void setCallbacks(ServiceCallbacks callbacks) {
        serviceCallbacks = callbacks;
    }
}
