package com.h_plus.usertabs;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.h_plus.R;
import com.h_plus.useradapter.RecommendedListAdapter;

/**
 * Created by Muzammil on 7/15/2016.
 */
public class RecommendedTab extends Fragment {
    View rootView;
    Context mContext;
    RecommendedListAdapter Adapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.recommended_list_item_tab_layout, container, false);
        mContext = getActivity();
        BINDING();
        return rootView;
    }

    public void BINDING() {

    }

}
