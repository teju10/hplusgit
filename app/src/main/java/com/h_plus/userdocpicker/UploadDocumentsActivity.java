package com.h_plus.userdocpicker;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.OrientationHelper;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.h_plus.R;
import com.h_plus.useractivity.MainActivityPermissionsDispatcher;
import com.h_plus.usertabs.utility.Constant_Urls;
import com.h_plus.usertabs.utility.MultipartUtility;
import com.h_plus.usertabs.utility.Utility;

import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;

import droidninja.filepicker.FilePickerBuilder;
import droidninja.filepicker.FilePickerConst;
import permissions.dispatcher.NeedsPermission;

/**
 * Created by Muzammil on 7/25/2016.
 */
public class UploadDocumentsActivity extends AppCompatActivity implements View.OnClickListener {
    Context mContext;
    int image_value = 0;
    String image_path1 = "", image_path2 = "", image_path3 = "", image_path4 = "", image_path5 = "", mCropImageUri = "";
    String encodedImage1 = "", encodedImage2 = "", encodedImage3 = "", encodedImage4 = "", encodedImage5 = "";
    Bitmap bitmap1, bitmap2, bitmap3, bitmap4, bitmap5;
    private int MAX_ATTACHMENT_COUNT = 5;
    private ArrayList<String> docPaths = new ArrayList<>();
    File textfile;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.upload_document);
        mContext = this;
          BINDING();
    }

    public void BINDING() {
        (findViewById(R.id.uploadfile_btn_layout)).setOnClickListener(this);
        (findViewById(R.id.upload_doc_one)).setOnClickListener(this);
        (findViewById(R.id.upload_doc_two)).setOnClickListener(this);
        (findViewById(R.id.upload_doc_three)).setOnClickListener(this);
        (findViewById(R.id.upload_doc_four)).setOnClickListener(this);
        (findViewById(R.id.upload_doc_five)).setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.uploadfile_btn_layout:
                OnFileDoc();
                break;
            case R.id.upload_doc_one:

            default:
                break;
        }
    }

    @NeedsPermission({Manifest.permission.WRITE_EXTERNAL_STORAGE})
    public void OnFileDoc() {
        int maxCount = MAX_ATTACHMENT_COUNT - docPaths.size();
        if ((docPaths.size()) == MAX_ATTACHMENT_COUNT)
            Toast.makeText(this, "Cannot select more than " + MAX_ATTACHMENT_COUNT + " items", Toast.LENGTH_SHORT).show();
        else
            FilePickerBuilder.getInstance().setMaxCount(maxCount)
                    .setSelectedFiles(docPaths)
                    .setActivityTheme(R.style.FilePickerTheme)
                    .pickDocument1(this);
    }

    public void onOpenFragmentClicked(View view) {
        Intent intent = new Intent(this, FragmentActivity.class);
        startActivity(intent);
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        //new functions
        switch (requestCode) {
            case FilePickerConst.REQUEST_CODE_DOC:
                if (resultCode == Activity.RESULT_OK && data != null) {
                    docPaths = new ArrayList<>();
                    docPaths.addAll(data.getStringArrayListExtra(FilePickerConst.KEY_SELECTED_DOCS));
                }
                for(int i = 0;i<docPaths.size();i++){
                    textfile =  new File(docPaths.get(i));
                }
                image_path1 = textfile.toString();

                System.out.println("KUBKUUUUUUU "+image_path1);
                break;
        }
        addThemToView(docPaths);
    }
    private void addThemToView(ArrayList<String> docPaths) {
        ArrayList<String> filePaths = new ArrayList<>();

        if(docPaths!=null)
            filePaths.addAll(docPaths);

        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.recyclerview);
        if(recyclerView!=null) {
            StaggeredGridLayoutManager layoutManager = new StaggeredGridLayoutManager(3, OrientationHelper.VERTICAL);
            layoutManager.setGapStrategy(StaggeredGridLayoutManager.GAP_HANDLING_MOVE_ITEMS_BETWEEN_SPANS);
            recyclerView.setLayoutManager(layoutManager);

            ImageAdapter imageAdapter = new ImageAdapter(this, filePaths);

            recyclerView.setAdapter(imageAdapter);
            recyclerView.setItemAnimator(new DefaultItemAnimator());
        }

        Toast.makeText(this, "Num of files selected: "+ filePaths.size(), Toast.LENGTH_SHORT).show();
    }

    //---------------->CODE FOR CROP<---------------------
    private Uri getCaptureImageOutputUri() {
        Uri outputFileUri = null;
        File getImage = getExternalCacheDir();
        if (getImage != null) {
            outputFileUri = Uri.fromFile(new File(getImage.getPath(), "pickImageResult.jpeg"));
        }
        return outputFileUri;
    }

   /*

    public Uri getPickImageResultUri(Intent data) {
        boolean isCamera = true;
        if (data != null && data.getData() != null) {
            String action = data.getAction();
            isCamera = action != null && action.equals(MediaStore.ACTION_IMAGE_CAPTURE);
        }
        return isCamera ? getCaptureImageOutputUri() : data.getData();
    }

    public boolean isUriRequiresPermissions(Uri uri) {
        try {
            ContentResolver resolver = getContentResolver();
            InputStream stream = resolver.openInputStream(uri);
            stream.close();
            return false;
        } catch (FileNotFoundException e) {
            if (e.getCause() instanceof ErrnoException) {
                return true;
            }
        } catch (Exception e) {
        }
        return false;
    }

    private void beginCrop(Uri source) {
        Uri destination = Uri.fromFile(new File(getCacheDir(), "cropped"));
        Crop.of(source, destination).asSquare().start(this);
    }


    private void handleCrop(int resultCode, Intent result) {
        if (resultCode == RESULT_OK) {
            File f = new File(Crop.getOutput(result).getPath());
            Bitmap roteted = Utility.loadBitmap(Crop.getOutput(result).toString());
            roteted = Utility.rotateImage(roteted, f);
            if (image_value == 1) {
                image_path1 = Utility.SaveImage(roteted, String.valueOf(System.currentTimeMillis()), Constants.SDCARD_ROOT);
                Log.w("imagepath1", "imagepath1 ===>" + image_path1);
                encodedImage1 = Utility.encodeTobase64(roteted);
                //System.out.println(encodedImage1);
                Log.e("image path1", "image path1 ======= " + f);
                ((ImageView) findViewById(R.id.upload_doc_one)).setImageBitmap(roteted);
                bitmap1 = roteted;


            } else if (image_value == 2) {
                image_path2 = Utility.SaveImage(roteted, String.valueOf(System.currentTimeMillis()), Constants.SDCARD_ROOT);
                Log.w("imagepath2", "imagepath2" + image_path2);
                encodedImage2 = Utility.encodeTobase64(roteted);
                //System.out.println(encodedImage2);
                Log.e("image path2", "image path2 ======= " + f);
                ((ImageView) findViewById(R.id.upload_doc_two)).setImageBitmap(roteted);
                bitmap2 = roteted;

            } else if (image_value == 3) {
                image_path3 = Utility.SaveImage(roteted, String.valueOf(System.currentTimeMillis()), Constants.SDCARD_ROOT);
                Log.w("imagepath", "imagepath" + image_path3);
                encodedImage3 = Utility.encodeTobase64(roteted);
                //System.out.println(encodedImage3);
                Log.e("image path3", "image path3" + f);
                ((ImageView) findViewById(R.id.upload_doc_three)).setImageBitmap(roteted);
                bitmap3 = roteted;

            } else if (image_value == 4) {
                image_path4 = Utility.SaveImage(roteted, String.valueOf(System.currentTimeMillis()), Constants.SDCARD_ROOT);
                Log.w("imagepath4", "imagepath" + image_path4);
                encodedImage4 = Utility.encodeTobase64(roteted);
                //System.out.println(encodedImage4);
                Log.e("image path4", "image path4 ======= " + f);
                ((ImageView) findViewById(R.id.upload_doc_four)).setImageBitmap(roteted);
                bitmap4 = roteted;

            } else if (image_value == 5) {
                image_path5 = Utility.SaveImage(roteted, String.valueOf(System.currentTimeMillis()), Constants.SDCARD_ROOT);
                Log.w("imagepath5", "imagepath5" + image_path5);
                encodedImage5 = Utility.encodeTobase64(roteted);
                //System.out.println(encodedImage5);
                Log.e("image path5", "image path5 ======= " + f);
                ((ImageView) findViewById(R.id.upload_doc_five)).setImageBitmap(roteted);
                bitmap5 = roteted;

            }
        } else if (resultCode == Crop.RESULT_ERROR) {
            Toast.makeText(this, Crop.getError(result).getMessage(), Toast.LENGTH_SHORT).show();
        }
    }*/

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        if (mCropImageUri != null && grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
        } else {
            Utility.showToast(mContext, "Required permissions are not granted");
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        MainActivityPermissionsDispatcher.onRequestPermissionsResult(this, requestCode, grantResults);
    }

    /*===============================================>SignUp Task Multi-part<===============================================*/
    public class UploadDocumentsTask extends AsyncTask<String, String, String> {
        String result = "";
        ProgressDialog dialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog = new ProgressDialog(mContext);
            dialog.setCancelable(false);
            dialog.setMessage("please wait...");
            dialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            try {
                MultipartUtility mu;
                mu = new MultipartUtility(Constant_Urls.SERVER_URL + "", "UTF-8");
                //mu.addFormField("id", share.getString("userid", ""));
                if (image_path1 != null && !image_path1.equals("")) {
                    mu.addFilePart("", image_path1);
                    Log.e("selectedImagePath 1 =", "selectedImagePath 1 =" + image_path1);
                }
                if (image_path2 != null && !image_path2.equals("")) {
                    mu.addFilePart("", image_path2);
                    Log.e("selectedImagePath 2 =", "selectedImagePath 2 =" + image_path2);
                }
                if (image_path3 != null && !image_path3.equals("")) {
                    mu.addFilePart("", image_path3);
                    Log.e("selectedImagePath 3 =", "selectedImagePath 3 =" + image_path3);
                }
                if (image_path4 != null && !image_path4.equals("")) {
                    mu.addFilePart("", image_path4);
                    Log.e("selectedImagePath 4 =", "selectedImagePath 4 =" + image_path4);
                }
                if (image_path5 != null && !image_path5.equals("")) {
                    mu.addFilePart("", image_path5);
                    Log.e("selectedImagePath 5 =", "selectedImagePath 5 =" + image_path5);
                }
                result = mu.finish();
            } catch (Exception e) {
                e.printStackTrace();
            }
            return result;
        }

        @Override
        protected void onPostExecute(String result) {
            if (result == null) {
                dialog.dismiss();
                Utility.ShowToastMessage(mContext, "Server not responding");
            } else {
                try {
                    JSONObject json = new JSONObject(result);
                    if (json.getString("success").equalsIgnoreCase("1")) {
                        dialog.dismiss();
                        Utility.ShowToastMessage(mContext, "Documents uploaded Successfully");
                       /* Intent intent = new Intent(mContext, HomeActivity.class);
                        startActivity(intent);
                        finish();*/
                    } else {
                        dialog.dismiss();
                        Utility.ShowToastMessage(mContext, "Something went wrong Documents not uploaded Successfully");
                    }
                } catch (Exception e) {
                    Utility.ShowToastMessage(mContext, "Something went wrong");
                    dialog.dismiss();
                    e.printStackTrace();
                }
            }
            super.onPostExecute(result);
        }
    }
}
