package com.h_plus.RegistrationSection;

import android.app.Activity;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.SimpleAdapter;
import android.widget.Spinner;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.Profile;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.plus.Plus;
import com.google.android.gms.plus.model.people.Person;
import com.google.firebase.iid.FirebaseInstanceId;
import com.h_plus.R;
import com.h_plus.ServerInteraction.GetResponseTask;
import com.h_plus.ServerInteraction.PlaceJSONParser;
import com.h_plus.ServerInteraction.ResponseListener;
import com.h_plus.ServerInteraction.ResponseTask;
import com.h_plus.customwidget.CustomAutoCompleteTextView;
import com.h_plus.customwidget.CustomEditText;
import com.h_plus.customwidget.CustomTextView;
import com.h_plus.usertabs.utility.Constant_Urls;
import com.h_plus.usertabs.utility.Constants;
import com.h_plus.usertabs.utility.GPSTracker;
import com.h_plus.usertabs.utility.Utility;
import com.linkedin.platform.APIHelper;
import com.linkedin.platform.LISession;
import com.linkedin.platform.LISessionManager;
import com.linkedin.platform.errors.LIApiError;
import com.linkedin.platform.errors.LIAuthError;
import com.linkedin.platform.listeners.ApiListener;
import com.linkedin.platform.listeners.ApiResponse;
import com.linkedin.platform.listeners.AuthListener;
import com.linkedin.platform.utils.Scope;
import com.rengwuxian.materialedittext.MaterialEditText;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Muzammil on 7/13/2016.
 */
public class SignupActivity extends Activity implements View.OnClickListener,
        GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {
    //TODO linked
    public static final String host = "api.linkedin.com";
    public static final String topCardUrl = "https://" + host + "/v1/people/~:(id,email-address,formatted-name,phone-numbers,public-profile-url,picture-url,picture-urls::(original))";
    private static final int SIGN_IN_CODE = 0;
    private static final int PROFILE_PIC_SIZE = 200;
    Context mContext;
    Dialog sndnumberdialog;
    SharedPreferences share;
    SharedPreferences.Editor edit;
    String encodedImage = "", gcmid;
    ResponseTask responseTask;
    GPSTracker gpsTracker;
    //  double lati, longii;
    double latitude = 0.0, longitude = 0.0;
    //TODO For facebook Login
    CallbackManager callbackManager = CallbackManager.Factory.create();
    String facebook_id, f_name, m_name, l_name, gender, profile_image, full_name, email_id, WeightString = "", pickupaddress = "";
    AccessToken f_token;
    //ProgressDialog progress_dialog;
    GoogleApiClient google_api_client;
    GoogleApiAvailability google_api_availability;
    String TAG = SignupActivity.class.getSimpleName();
    com.linkedin.platform.AccessToken accessToken;
    LISessionManager sessionManager;
    MaterialEditText signup_first_name, signup_last_name, email_signup, password_signup, signup_cnfrm_pw;
    CustomAutoCompleteTextView address;
    List<HashMap<String, String>> weight_spinner = new ArrayList<>();
    ParserTaskforautotv parserTask;
    String[] country_list;
    String hire_work = "1";
    private Handler customHandler = new Handler();
    private BroadcastReceiver mIntentReceiver;
    private Uri mCropImageUri;
    // todo For Google + Login
    private ConnectionResult connection_result;
    private boolean is_intent_inprogress;
    private boolean is_signInBtn_clicked;
    private int request_code;
    private Runnable updateTimerThread = new Runnable() {

        public void run() {
            sndnumberdialog.findViewById(R.id.resend_number).setEnabled(true);
            customHandler.removeCallbacks(updateTimerThread);

        }

    };

    private static Scope buildScope() {
        return Scope.build(Scope.R_BASICPROFILE, Scope.W_SHARE, Scope.R_EMAILADDRESS);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup_screen);
        mContext = this;
        share = mContext.getSharedPreferences("pref", MODE_PRIVATE);
        FacebookSdk.sdkInitialize(this.getApplicationContext());
        buidNewGoogleApiClient();
//        lati = gpsTracker.getLatitude();
//        longii = gpsTracker.getLongitude();
        gcmid = FirebaseInstanceId.getInstance().getToken();
        gpsTracker = new GPSTracker(mContext);
        System.out.println("GCMID " + gcmid);
        sessionManager = LISessionManager.getInstance(mContext);
        Init();
        Find();
        gPlusSignOut();
        ButtonClicks();
        printKeyHash();
        fbLogin();
        AutocompleteAddress();
    }

    public void Find() {

        signup_first_name = (MaterialEditText) findViewById(R.id.signup_first_name);
        signup_last_name = (MaterialEditText) findViewById(R.id.signup_last_name);
        email_signup = (MaterialEditText) findViewById(R.id.email_signup);
        password_signup = (MaterialEditText) findViewById(R.id.password_signup);
        signup_cnfrm_pw = (MaterialEditText) findViewById(R.id.signup_cnfrm_pw);
        address = (CustomAutoCompleteTextView) findViewById(R.id.address);

    }

    public void ButtonClicks() {
        (findViewById(R.id.signup_btn)).setOnClickListener(this);
        (findViewById(R.id.fb_btn_on_login)).setOnClickListener(this);
        (findViewById(R.id.linkedin_btn_on_login)).setOnClickListener(this);
        (findViewById(R.id.google_btn_on_login)).setOnClickListener(this);
       /* (findViewById(R.id.hire)).setOnClickListener(this);
        (findViewById(R.id.work)).setOnClickListener(this);*/
        (findViewById(R.id.already_have_account)).setOnClickListener(this);
    }

    public void Init() {

        weight_spinner = Utility.readXMLofCountries(mContext);

        ((RadioGroup) findViewById(R.id.rb_group)).setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (checkedId == R.id.rb_hire) {
                    hire_work = "1";
                } else {
                    hire_work = "2";
                }
            }
        });

    }

    public void AutocompleteAddress() {

        /*FOR AUTOCOMPLETE METHOD*/

        address.setThreshold(1);
        address.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int i, int i1, int i2) {
                Log.e("onTextChanged", "CharSequence ===" + s.toString());

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                pickupaddress = "";

            }

            @Override
            public void afterTextChanged(Editable s) {
                PlacesTask placesTask = new PlacesTask();
                placesTask.execute(s.toString());
            }
        });
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent need = new Intent(mContext, LoginActivity.class);
        startActivity(need);
        overridePendingTransition(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
        finish();

    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.signup_btn:
                if (Utility.isConnectingToInternet(mContext)) {
                    if ((signup_first_name).getText().toString().equals("")) {
                        (signup_first_name).setError("please enter your name");
                        (signup_first_name).requestFocus();
                    } else if ((signup_last_name).getText().toString().equals("")) {
                        (signup_last_name).setError("please enter last name");
                        (signup_last_name).requestFocus();

                    } else if (!Utility.isValidEmail((email_signup).getText().toString())) {
                        (email_signup).setError("Enter email like abc@xyz.com");
                        (email_signup).requestFocus();

                    } else if ((password_signup).getText().toString().equals("")) {
                        (password_signup).setError("password cannot be blank");
                        (password_signup).requestFocus();
                    } else if ((password_signup).getText().toString().length() < 6) {
                        (password_signup).setError("Password length should be more than 6 character!");
                        (password_signup).requestFocus();
                    } else if (!(signup_cnfrm_pw).getText().toString().equals((password_signup).getText().toString())) {
                        (signup_cnfrm_pw).setError("password does not match");

                    } else if ((address).getText().toString().equals("")) {
                        (address).setError("enter your email address");

                    } else if ((signup_first_name).getText().toString().equals("") ||
                            (signup_last_name).getText().toString().equals("") ||
                            (email_signup).getText().toString().equals("") ||
                            (password_signup).getText().toString().equals("") ||
                            (address).getText().toString().equals("")) {
                        Utility.showToast(mContext, "Please star_fill all fields");
                    } else {
                        SignUpTask(((EditText) findViewById(R.id.signup_first_name)).getText().toString(),
                                ((EditText) findViewById(R.id.signup_last_name)).getText().toString(),
                                ((EditText) findViewById(R.id.email_signup)).getText().toString(),
                                ((EditText) findViewById(R.id.password_signup)).getText().toString(),
                                ((EditText) findViewById(R.id.address)).getText().toString());
                    }
                } else {
                    Utility.showToast(mContext, "Please Check Your Internet Connection");
                }
                break;

            case R.id.already_have_account:
                Intent j = new Intent(mContext, LoginActivity.class);
                startActivity(j);
                finish();
                overridePendingTransition(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
                break;
            case R.id.fb_btn_on_login:
                LoginManager.getInstance().logInWithReadPermissions(SignupActivity.this, Arrays.asList("public_profile", "email"));
                break;
            case R.id.linkedin_btn_on_login:
                //Utility.showToast(mContext,"coming soon");
                linkedinLogin();
                break;
            case R.id.google_btn_on_login:
                gPlusSignIn();
                break;
            default:
                break;
        }
    }

    //TODO Google Login

    //TODO For fb Login
    public void fbLogin() {
        LoginManager.getInstance().registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                f_token = loginResult.getAccessToken();
                //progress_dialog.show();
                Profile profile = Profile.getCurrentProfile();

                Utility.ShowLoading(mContext, "please wait");
                GraphRequest request = GraphRequest.newMeRequest(loginResult.getAccessToken(), new GraphRequest.GraphJSONObjectCallback() {
                    @Override
                    public void onCompleted(JSONObject object, GraphResponse response) {
                        Utility.HideDialog();
                        //Start new activity or use this info in your project.
                        try {
                            String imageurl = "" + "https://graph.facebook.com/" + object.getString("id") + "/picture?type=large";
                            if (!imageurl.equals("")) {
                                int lastindex = imageurl.length();
                            }

                            Utility.setSharedPreference(mContext, Constants.SOCIAL_FIRSTNAME, object.getString("first_name"));
                            Utility.setSharedPreference(mContext, Constants.SOCIAL_LNAME, object.getString("last_name"));
                            Utility.setSharedPreference(mContext, Constants.SOCIAL_EMAIL, object.getString("email"));
                            Utility.setSharedPreference(mContext, Constants.SOCIAL_PROFILEIMAGE, imageurl);
                            Utility.setSharedPreference(mContext, Constants.SOCIAL_GENDER, gender);

                            signup_first_name.setText(object.getString("first_name"));
                            signup_last_name.setText(object.getString("last_name"));
                            email_signup.setText(object.getString("email"));

                           /* Utility.showToast(mContext, "facebook Login Success");
                            startActivity(new Intent(mContext, MainActivity.class));*/
                            /*new SocialLoginTask().execute(object.getString("name"), object.getString("email"), imageurl,
                                    object.getString("id"), "facebook", "Android", String.valueOf(lat), String.valueOf(lang));*/
                        } catch (Exception e) {
                            // progress_dialog.dismiss();
                            Utility.HideDialog();
                            e.printStackTrace();
                        }
                    }
                });

                Bundle parameters = new Bundle();
                parameters.putString("fields", "id, name,first_name,last_name, email, gender,location, birthday");
                request.setAccessToken(f_token);
                request.setParameters(parameters);
                request.executeAsync();
            }

            @Override
            public void onCancel() {
                Utility.showToast(mContext, "Facebook login Canceled");
                Utility.HideDialog();
            }

            @Override
            public void onError(FacebookException error) {
                Utility.showToast(mContext, "Facebook login Error");
                Utility.HideDialog();
            }
        });
    }

    private void buidNewGoogleApiClient() {
        google_api_client = new GoogleApiClient.Builder(this).addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this).addApi(Plus.API, Plus.PlusOptions.builder().build())
                .addScope(Plus.SCOPE_PLUS_LOGIN).build();
    }

    protected void onStart() {
        super.onStart();
        google_api_client.connect();
    }

    protected void onResume() {
        super.onResume();
        if (google_api_client.isConnected()) {
            google_api_client.connect();
        }
        IntentFilter intentFilter = new IntentFilter("SmsMessage.intent.MAIN");
        mIntentReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                String msg = intent.getStringExtra("get_msg");
                System.out.println("mmmmsssssssssg       " + msg);
                msg = msg.replace("\n", "");
                String body = msg.substring(msg.lastIndexOf(":") + 1, msg.length());
                String pNumber = msg.substring(0, msg.lastIndexOf(":"));
                System.out.println("body      " + body);
                Pattern p = Pattern.compile("\\d+");
                Matcher m = p.matcher(body);
                String code = "";
                while (m.find()) {
                    code = m.group();
                    System.out.println(m.group());
                    break;
                }
                if (sndnumberdialog != null && sndnumberdialog.isShowing()) {
                    ((CustomEditText) sndnumberdialog.findViewById(R.id.verificate_code)).setText(code);
                    sndnumberdialog.findViewById(R.id.send_number).setEnabled(true);
//    customHandler.removeCallbacks(updateTimerThread);
                }
//Add it to the list or do whatever you wish to

            }
        };
        this.registerReceiver(mIntentReceiver, intentFilter);
    }

    @Override
    public void onPause() {
        super.onPause();
        this.unregisterReceiver(this.mIntentReceiver);
    }

    @Override
    public void onConnectionFailed(ConnectionResult result) {
        if (!result.hasResolution()) {
            try {
                google_api_availability.getErrorDialog(this, result.getErrorCode(), request_code).show();
            } catch (Exception e) {
                e.printStackTrace();
            }
            return;
        }

        if (!is_intent_inprogress) {
            connection_result = result;
            if (is_signInBtn_clicked) {
                resolveSignInError();
            }
        }
    }

    @Override
    public void onConnected(Bundle arg0) {
        is_signInBtn_clicked = false;
        // Get user's information and set it into the layout
        getProfileInfo();
        // Update the UI after signin
        changeUI(true);
    }

    @Override
    public void onConnectionSuspended(int arg0) {
        google_api_client.connect();
        changeUI(false);
    }

    /*
      Sign-in into the Google + account
     */
    private void gPlusSignIn() {
        if (!google_api_client.isConnecting()) {
            Log.e("user connected", "connected");
            is_signInBtn_clicked = true;
            // progress_dialog.show();
            Utility.ShowLoading(mContext, mContext.getResources().getString(R.string.please_wait));
            resolveSignInError();
        }
        if (google_api_client.isConnected()) {
            gPlusRevokeAccess();
        }
    }

    /*
      Method to resolve any signin errors
     */
    private void resolveSignInError() {
        try {
            if (!(connection_result == null)) {
                if (connection_result.hasResolution()) {
                    try {
                        is_intent_inprogress = true;
                        connection_result.startResolutionForResult(this, SIGN_IN_CODE);
                        Log.d("resolve error", "sign in error resolved");
                    } catch (IntentSender.SendIntentException e) {
                        e.printStackTrace();
                        Log.e("IntentSender", "IntentSender.SendIntentException == " + e);
                        is_intent_inprogress = false;
                        google_api_client.connect();
                    }
                }
            } else {
                //progress_dialog.dismiss();
                Utility.HideDialog();
                google_api_availability.getErrorDialog(this, connection_result.getErrorCode(), request_code).show();
            }
        } catch (Exception e) {
            // progress_dialog.dismiss();
            Utility.HideDialog();
            e.printStackTrace();
        }
    }

    private void gPlusSignOut() {
        if (google_api_client.isConnected()) {
            Plus.AccountApi.clearDefaultAccount(google_api_client);
            google_api_client.disconnect();
            google_api_client.connect();
            changeUI(false);
        }
    }

    /*
     Revoking access from Google+ account
     */
    private void gPlusRevokeAccess() {
        if (google_api_client.isConnected()) {
            Plus.AccountApi.clearDefaultAccount(google_api_client);
            Plus.AccountApi.revokeAccessAndDisconnect(google_api_client).setResultCallback(new ResultCallback<Status>() {
                @Override
                public void onResult(Status arg0) {
                    Log.d("MainActivity", "User access revoked!");
                    buidNewGoogleApiClient();
                    google_api_client.connect();
                    changeUI(false);
                }
            });
        }
    }

    /*
     get user's information name, email, profile pic,Date of birth,tag line and about me
     */
    private void getProfileInfo() {
        try {
            if (Plus.PeopleApi.getCurrentPerson(google_api_client) != null) {
                Person currentPerson = Plus.PeopleApi.getCurrentPerson(google_api_client);
                setPersonalInfo(currentPerson);
            } else {
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /*
     By default the profile pic url gives 50x50 px image.
     If you need a bigger image we have to change the query parameter value from 50 to the size you want
    */

    /*
     set the User information into the views defined in the layout
     */
    private void setPersonalInfo(Person currentPerson) {
        int persongender = 0;
        String personName = currentPerson.getDisplayName();
        String personPhotoUrl = currentPerson.getImage().getUrl();
        String email = Plus.AccountApi.getAccountName(google_api_client);
        String Name = currentPerson.getNickname();
        personPhotoUrl = personPhotoUrl.substring(0, personPhotoUrl.length() - 2) + PROFILE_PIC_SIZE;
        if (currentPerson.hasGender()) {
            // it's not guaranteed
            persongender = currentPerson.getGender();
        }
        Log.e(TAG, "Person info is" + personPhotoUrl + "\n" + "personname->" + personName + "\n"
                + "email==>" + email + "\n" + "id" + currentPerson.getId());
        System.out.println("Person info is-------------------------->>--------------" + personPhotoUrl + "\n" + Name);
        // progress_dialog.dismiss();
        Utility.HideDialog();
        String imageurl = personPhotoUrl;
        if (!imageurl.equals("")) {
            int lastindex = personPhotoUrl.length();
            imageurl = personPhotoUrl.substring(8, lastindex);
        }
        Utility.setSharedPreference(mContext, Constants.SOCIAL_FIRSTNAME, personName);
        Utility.setSharedPreference(mContext, Constants.SOCIAL_EMAIL, email);
        Utility.setSharedPreference(mContext, Constants.SOCIAL_PROFILEIMAGE, personPhotoUrl);
        Utility.setSharedPreference(mContext, Constants.SOCIAL_GENDER, String.valueOf(persongender));

        signup_first_name.setText(personName);
        email_signup.setText(email);

        // startActivity(new Intent(mContext, MainActivity.class));
       /* if (latitude == 0.0 || longitude == 0.0) {
            Utility.AlertNoGPS(mContext);
        } else {
            new SocialLoginTask().execute(personName, email, imageurl,
                    currentPerson.getId(), "google", Utility.getSharedPreferences(mContext, "GCMID"), "android",
                    String.valueOf(latitude), String.valueOf(longitude));
        }*/

    }

    private void setProfilePic(String profile_pic) {
        profile_pic = profile_pic.substring(0, profile_pic.length() - 2) + PROFILE_PIC_SIZE;
    }

    private void changeUI(boolean signedIn) {
    }
    //TODO Google Login Ends Here

    /*
     Perform background operation asynchronously, to load user profile picture with new dimensions from the modified url
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        sessionManager.onActivityResult(SignupActivity.this, requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
        // Check which request we're responding to Google plus
        if (requestCode == SIGN_IN_CODE) {
            request_code = requestCode;
            if (resultCode != RESULT_OK) {
                is_signInBtn_clicked = false;
                //progress_dialog.dismiss();
                Utility.HideDialog();
            }
            is_intent_inprogress = false;
            if (!google_api_client.isConnecting()) {
                google_api_client.connect();
            }
        }
    }

    //TODO Linked Sign Here
    public void linkedinLogin() {

        sessionManager.init(SignupActivity.this, buildScope(), new AuthListener() {
            @Override
            public void onAuthSuccess() {
                // Utility.ShowLoading(mContext, "please wait...");

                setUpdateState();
                accessToken = sessionManager.getSession().getAccessToken();
                Log.e(TAG, "access token ==== " + accessToken.toString());
            }

            @Override
            public void onAuthError(LIAuthError error) {
                setUpdateState();
                Log.e(TAG, "onAuthError ==== " + error.toString());
            }
        }, true);
    }

    public void setUpdateState() {

        LISession session = sessionManager.getSession();
        boolean accessTokenValid = session.isValid();
        Log.e(TAG, "accessTokenValid ==== " + accessTokenValid);
        if (accessTokenValid) {
            APIHelper apiHelper = APIHelper.getInstance(mContext);
            apiHelper.getRequest(SignupActivity.this, topCardUrl, new ApiListener() {
                @Override
                public void onApiSuccess(ApiResponse s) {
                    try {
                        Log.e(TAG, "ApiResponse ===== " + s.getResponseDataAsJson());
                        JSONObject object = s.getResponseDataAsJson();
                        System.out.println("LINKED_DATA======>>>>" + object.getString("id") + object.getString("formattedName")
                                + object.getString("emailAddress") + object.getString("publicProfileUrl"));
                        if (object.has("pictureUrl") && object.getString("pictureUrl") != null &&
                                !object.getString("pictureUrl").equals("")) {
                            //startActivity(new Intent(mContext,MainActivity.class));
                          /*  LinkedinLoginTask(object.getString("id"), object.getString("formattedName"),
                                    object.getString("emailAddress"), object.getString("pictureUrl"), "linkedin");
                        } else {
                            LinkedinLoginTask(object.getString("id"), object.getString("formattedName"),
                                    object.getString("emailAddress"), "", "linkedin");*/
                        }
                        Utility.setSharedPreference(mContext, Constants.SOCIAL_FIRSTNAME, object.getString("formattedName"));
                        Utility.setSharedPreference(mContext, Constants.SOCIAL_EMAIL, object.getString("emailAddress"));
                        //Utility.setSharedPreference(mContext,Constants.SOCIAL_PROFILEIMAGE, object.getString("pictureUrl"));

                        signup_first_name.setText(object.getString("formattedName"));
                        email_signup.setText(object.getString("emailAddress"));
                        Utility.showToast(mContext, "Success");

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onApiError(LIApiError error) {
                    Toast.makeText(mContext, "failed " + error.toString(), Toast.LENGTH_LONG).show();
                }
            });
        } else {
            Toast.makeText(mContext, "not a valid session.", Toast.LENGTH_LONG).show();

        }
    }

    //TODO for print KeyHash
    private void printKeyHash() {
        // Add code to print out the key hash
        try {
            PackageInfo info = getPackageManager().getPackageInfo("com.hplus", PackageManager.GET_SIGNATURES); //replace com.demo with your package name
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.w("DeveloperKeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
                MessageDigest mda = MessageDigest.getInstance("SHA-1");
                mda.update(signature.toByteArray());
                Log.w("releseKeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (PackageManager.NameNotFoundException e) {
            Log.d("KeyHash:", e.toString());
        } catch (NoSuchAlgorithmException e) {
            Log.d("KeyHash:", e.toString());
        }
    }

    //Send mobile number for verification
    public void SendNumberdialog() {

        Spinner select_number_code;
        sndnumberdialog = new Dialog(mContext);
        sndnumberdialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        sndnumberdialog.setCancelable(false);
        sndnumberdialog.setCanceledOnTouchOutside(false);
        sndnumberdialog.setContentView(R.layout.dialog_select_cntry);
        sndnumberdialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        select_number_code = (Spinner) sndnumberdialog.findViewById(R.id.select_number_code);


        int[] to = new int[]{R.id.c_code, R.id.c_name};
        String[] from = new String[]{"phoneCode", "name"};
        //Creating a SimpleAdapter for the Spinner
        SimpleAdapter adapter = new SimpleAdapter(mContext, weight_spinner, R.layout.text_single_add, from, to);
        select_number_code.setAdapter(adapter);
        select_number_code.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position > 0) {
                    WeightString = weight_spinner.get(position).get("phoneCode").toString();
                    ((CustomTextView) sndnumberdialog.findViewById(R.id.country_code)).setText("+" + WeightString);
                } else {
                    Utility.showToast(mContext, "please select country code");
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

                // value_spinner = "";
            }
        });

        sndnumberdialog.findViewById(R.id.send_number).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Utility.isConnectingToInternet(mContext)) {

                    if (((CustomTextView) sndnumberdialog.findViewById(R.id.country_code)).getText().toString().equals("")) {
                        Utility.showToast(mContext, "please select country code");
                    } else if (((CustomEditText) sndnumberdialog.findViewById(R.id.dialog_mobile)).getText().toString().equals("")) {
                        Utility.showToast(mContext, "please enter your mobile number");

                    } else {
                        SendMobileNumber(((CustomEditText) sndnumberdialog.findViewById(R.id.dialog_mobile)).getText().toString(),
                                ((CustomTextView) sndnumberdialog.findViewById(R.id.country_code)).getText().toString());
                    }
                } else {
                    Utility.showToast(mContext, "PLease check your internet connection");
                }
            }
        });

        sndnumberdialog.findViewById(R.id.cancel_dialog).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sndnumberdialog.cancel();
            }
        });

        sndnumberdialog.findViewById(R.id.snd_vf_code).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Utility.isConnectingToInternet(mContext)) {
                    if (((CustomEditText) sndnumberdialog.findViewById(R.id.verificate_code)).getText().toString().equals("")) {
                        Utility.Alert(mContext, "please check your message and  enter your verification code");
                    } else {
                        SendVerificationCode(((CustomEditText) sndnumberdialog.findViewById(R.id.verificate_code)).getText().toString());
                    }
                } else {
                    Utility.showToast(mContext, "PLease check your internet connection");
                }
            }
        });
        sndnumberdialog.show();
    }

    public void SignUpTask(String name, String lname, String email, String pw, String address) {
        /*https://infograins.com/INFO01/homesplus/api/api.php?action=sign_up&
        usertype=1&first_name=hameed&last_name=khan&email=hameed.infogrian@gmail.com&password=123456&address=Indore,
        %20Madhya%20Pradesh&city=indore&state=Madhya%20Pradesh&country=india&zipcode=452001*/
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put(Constants.ACTION, Constants.SIGN_UP);
            jsonObject.put("usertype", hire_work);
            jsonObject.put("first_name", name);
            jsonObject.put("last_name", lname);
            jsonObject.put("email", email);
            jsonObject.put("password", pw);
            jsonObject.put("address", address);
            jsonObject.put("device_type", "android");
            jsonObject.put("device_id", gcmid);
            jsonObject.put("latitude", latitude);
            jsonObject.put("longitude", longitude);
            Utility.ShowLoading(mContext, mContext.getResources().getString(R.string.please_wait));
            responseTask = new ResponseTask(Constant_Urls.SERVER_URL, jsonObject);
            responseTask.setListener(new ResponseListener() {
                @Override
                public void onGetPickSuccess(String result) {
                    Utility.HideDialog();
                    System.out.println("Server result ====> " + result);
                    if (result == null) {
                        Utility.ShowToastMessage(mContext, "server not responding");
                    } else {
                        try {
                            JSONObject jobj = new JSONObject(result);
                            if (jobj.getString(Constants.RESULT_KEY).equals("1")) {

                                Utility.setSharedPreference(mContext, Constants.USERID, jobj.getJSONObject("object").getString("user_id"));
                                // Utility.setSharedPreference(mContext, Constants.SPLASH_CHECK, "1");
                                Utility.setSharedPreference(mContext, Constants.USER_TYPE, hire_work);

                                ((EditText) findViewById(R.id.signup_first_name)).setText("");
                                ((EditText) findViewById(R.id.signup_last_name)).setText("");
                                ((EditText) findViewById(R.id.email_signup)).setText("");
                                ((EditText) findViewById(R.id.password_signup)).setText("");
                                ((EditText) findViewById(R.id.address)).setText("");
                                SendNumberdialog();
                            } else {
                                Utility.showCroutonWarn(jobj.getString(Constants.SERVER_MSG), SignupActivity.this);
                            }
                        } catch (JSONException e2) {
                            Utility.showCroutonWarn(getResources().getString(R.string.server_fail), SignupActivity.this);
                        }
                    }
                }
            });
        } catch (JSONException e) {
            e.printStackTrace();
        }
        responseTask.execute();
    }

    public void SendMobileNumber(String mobile, String country_code) {
           /*https://infograins.com/INFO01/homesplus/api/api.php?action=send_verification_code&
    user_id=117&contact_number=9300014089&country_code=91*/
        try {

            JSONObject jobj = new JSONObject();
            jobj.put(Constants.ACTION, Constants.SEND_VERICATION_NUMBER);
            jobj.put("user_id", Utility.getSharedPreferences(mContext, Constants.USERID));
            jobj.put("contact_number", mobile);
            jobj.put("country_code", country_code);
            Utility.ShowLoading(mContext, mContext.getResources().getString(R.string.please_wait));

            responseTask = new ResponseTask(Constant_Urls.SERVER_URL, jobj);
            responseTask.setListener(new ResponseListener() {
                @Override
                public void onGetPickSuccess(String result) {
                    Utility.HideDialog();
                    System.out.println("Server result ====> " + result);
                    if (result == null) {
                        Utility.showToast(mContext, "Process incomplete..please try again!");
                    } else {
                        try {
                            JSONObject jobj = new JSONObject(result);
                            if (jobj.getString(Constants.RESULT_KEY).equals("1")) {
                                Utility.ShowToastMessage(mContext, jobj.getString("msg"));
                                sndnumberdialog.findViewById(R.id.resend_number).setEnabled(false);
                                customHandler.postDelayed(updateTimerThread, 30000);
                            } else {
                                Utility.ShowToastMessage(mContext, jobj.getString("msg"));
                                Utility.showCroutonWarn(jobj.getString(Constants.SERVER_MSG), SignupActivity.this);
                            }
                        } catch (JSONException e2) {
                            Utility.showCroutonWarn(getResources().getString(R.string.server_fail), SignupActivity.this);
                        }
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
        responseTask.execute();
    }

    public void SendVerificationCode(String VCode) {
        /*https://infograins.com/INFO01/homesplus/api/api.php?action=check_phone_verification_code&
        user_id=117&verification_code=1202*/
        try {

            JSONObject jobj = new JSONObject();
            jobj.put(Constants.ACTION, Constants.SEND_VERICATION_CODE);
            jobj.put("user_id", Utility.getSharedPreferences(mContext, Constants.USERID));
            jobj.put("verification_code", VCode);
            Utility.ShowLoading(mContext, mContext.getResources().getString(R.string.please_wait));

            responseTask = new ResponseTask(Constant_Urls.SERVER_URL, jobj);
            responseTask.setListener(new ResponseListener() {
                @Override
                public void onGetPickSuccess(String result) {
                    Utility.HideDialog();
                    System.out.println("Server result ====> " + result);
                    if (result == null) {

                    } else {
                        try {
                            JSONObject jobj = new JSONObject(result);
                            if (jobj.getString(Constants.RESULT_KEY).equals("1")) {
                                Utility.showToast(mContext, jobj.getString("msg"));
                                Utility.showToast(mContext, "your mobile number is verified successfully");
                               /* Utility.setSharedPreference(mContext, Constants.SPLASH_CHECK, "1");*/
                                startActivity(new Intent(mContext, Social_MediaVerification_Activity.class));
                            } else {
                                Utility.ShowToastMessage(mContext, jobj.getString("msg"));
                                Utility.showCroutonWarn(jobj.getString(Constants.SERVER_MSG), SignupActivity.this);
                            }
                        } catch (JSONException e2) {
                            Utility.showCroutonWarn(getResources().getString(R.string.server_fail), SignupActivity.this);
                        }
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
        responseTask.execute();
    }

    private String downloadUrl(String strUrl) throws IOException {
        String data = "";
        InputStream iStream = null;
        HttpURLConnection urlConnection = null;
        try {
            URL url = new URL(strUrl);
            // Creating an http connection to communicate with url
            urlConnection = (HttpURLConnection) url.openConnection();
            // Connecting to url
            urlConnection.connect();
            // Reading data from url
            iStream = urlConnection.getInputStream();
            BufferedReader br = new BufferedReader(new InputStreamReader(iStream));
            StringBuffer sb = new StringBuffer();
            String line = "";
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }
            data = sb.toString();
            br.close();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            iStream.close();
            urlConnection.disconnect();
        }
        return data;
    }

    protected void getLatLng(final String address) {
        String input = "";
        try {
            input = "address=" + URLEncoder.encode(address, "utf-8");
        } catch (UnsupportedEncodingException e1) {
            e1.printStackTrace();
        }
        String uri = "http://maps.google.com/maps/api/geocode/json?"
                + input + "&sensor=false";
        System.out.println("url is " + uri);
        GetResponseTask getResponseTask = new GetResponseTask(uri, mContext, "GOOGLE");
        getResponseTask.setListener(new ResponseListener() {
            @Override
            public void onGetPickSuccess(String result) {
                double lat = 0.0, lng = 0.0;
                JSONObject jsonObject;
                try {
                    jsonObject = new JSONObject(result);
                    longitude = ((JSONArray) jsonObject.get("results")).getJSONObject(0)
                            .getJSONObject("geometry").getJSONObject("location")
                            .getDouble("lng");
                    latitude = ((JSONArray) jsonObject.get("results")).getJSONObject(0)
                            .getJSONObject("geometry").getJSONObject("location")
                            .getDouble("lat");

                   /* Utility.hideKeyboard(mContext);
                    Intent intent = new Intent();
                    intent.putExtra("LAT", lat);
                    intent.putExtra("ADD", address);
                    intent.putExtra("LONG", lng);
                    setResult(Activity.RESULT_OK, intent);
                    finish();
                    overridePendingTransition(android.R.anim.slide_in_left, android.R.anim.slide_out_right);*/
                } catch (JSONException e) {
                    e.printStackTrace();
                    //    Utility.ShowToastMessage(mContext, getString(R.string.add_not_found));
                }
            }
        });
        getResponseTask.execute();
    }

    private class PlacesTask extends AsyncTask<String, Void, String> {


        @Override
        protected String doInBackground(String... place) {
            String data = "";
            try {
                String key = "key=AIzaSyCNsqAYzpFsI7fyyZgZ05hEFunvWJDogIk";
                String input = "";
                try {
                    input = "input=" + URLEncoder.encode(place[0], "utf-8");
                } catch (UnsupportedEncodingException e1) {
                    e1.printStackTrace();
                }
                // https://maps.googleapis.com/maps/api/place/autocomplete/xml?input=Amoeba&types=establishment&
                // location=37.76999,-122.44696&radius=500&key=YOUR_API_KEY
                String location = "location=" + String.valueOf(latitude) + "," + String.valueOf(longitude);
                String radius = "radius=50000";
                String types = "types=geocode";
                String sensor = "sensor=true";
                String parameters = input + "&" + types + "&" + location + "&" + radius + "&" + sensor + "&" + key;
                String output = "json";
                String url = "https://maps.googleapis.com/maps/api/place/autocomplete/" + output + "?" + parameters;
                try {
                    Log.e("downloadUrl", "url ==== " + url);
                    data = downloadUrl(url);
                } catch (Exception e) {
                    Log.d("Background Task", e.toString());
                }
                return data;
            } catch (Exception e) {
                e.printStackTrace();
            }
            return data;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            parserTask = new ParserTaskforautotv();
            parserTask.execute(result);
        }
    }

    private class ParserTaskforautotv extends AsyncTask<String, Integer, List<HashMap<String, String>>> {
        JSONObject jObject;

        @Override
        protected List<HashMap<String, String>> doInBackground(String... jsonData) {
            List<HashMap<String, String>> places = null;
            PlaceJSONParser placeJsonParser = new PlaceJSONParser();
            try {
                jObject = new JSONObject(jsonData[0]);
                // Getting the parsed data as a List construct
                places = placeJsonParser.parse(jObject);
            } catch (Exception e) {
                Log.d("Exception", e.toString());
            }
            return places;
        }

        @Override
        protected void onPostExecute(List<HashMap<String, String>> result) {
            String[] from = new String[]{"description"};
            int[] to = new int[]{android.R.id.text1};
            // Creating a SimpleAdapter for the AutoCompleteTextView
            SimpleAdapter adapter = new SimpleAdapter(mContext, result, android.R.layout.simple_list_item_1, from, to);
            // Setting the adapter
            address.setAdapter(adapter);
            address.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    pickupaddress = ((HashMap<String, String>) parent.getItemAtPosition(position)).get("description");
                    System.out.println("Adddis" + pickupaddress);
                   // pickupaddress = parent.getAdapter().getItem(position).toString();
                    getLatLng(pickupaddress);
                }
            });

        }
    }
}
