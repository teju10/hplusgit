package com.h_plus.RegistrationSection;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.android.gms.common.api.GoogleApiClient;
import com.h_plus.Fragment_Profile;
import com.h_plus.HP_Client.ClientFragment.Fragment_ChatList;
import com.h_plus.HP_Client.ClientFragment.Fragment_Freelancer_Biding_List;
import com.h_plus.HP_Client.ClientFragment.Fragment_PostProject;
import com.h_plus.HP_Client.ClientFragment.Fragment_ProjectDetailandProposalListTab;
import com.h_plus.HP_Client.ClientFragment.HireServiceProviderHomeFragment;
import com.h_plus.HP_Client.ClientFragment.Hire_Home_MyProject_Fragment;
import com.h_plus.HP_Service_Provider.fragment.Fragment_Search_Project;
import com.h_plus.HP_Service_Provider.fragment.MyBidedProject;
import com.h_plus.HP_Service_Provider.fragment.MyProject_Fragment;
import com.h_plus.H_Booking.HomeMap_Activity;
import com.h_plus.H_Booking.fragment.Fragment_trip_tabs;
import com.h_plus.R;
import com.h_plus.ServerInteraction.ResponseListener;
import com.h_plus.ServerInteraction.ResponseTask;
import com.h_plus.customwidget.CircleImageView;
import com.h_plus.customwidget.CustomTextView;
import com.h_plus.useractivity.HomeLocation;
import com.h_plus.userdrawer.FragmentDrawer;
import com.h_plus.userfragment.Fragment_AddProject_Postbudget;
import com.h_plus.userfragment.Fragment_Home;
import com.h_plus.usertabs.utility.Constant_Urls;
import com.h_plus.usertabs.utility.Constants;
import com.h_plus.usertabs.utility.Utility;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;


/**
 * Created by Muzammil on 19-Feb-16.
 */
public class MainActivity extends AppCompatActivity implements FragmentDrawer.FragmentDrawerListener {

    private static String TAG = MainActivity.class.getSimpleName();
    Context mContext;
    DrawerLayout d_layout;
    SharedPreferences share;
    SharedPreferences.Editor edit;
    String User_Type;
    ResponseTask rt;
    Bundle b;
    private Toolbar mToolbar;
    private FragmentDrawer drawerFragment;
    private ArrayList<String> docPaths = new ArrayList<>();
    private ArrayList<String> photoPaths = new ArrayList<>();
    private CircleImageView hp_profile_image_drawer;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mContext = this;
        share = mContext.getSharedPreferences("pref", MODE_PRIVATE);
        edit = share.edit();
        edit.putString("login_true", "true");
        edit.commit();
        edit.apply();
        mToolbar = (Toolbar) findViewById(R.id.h_toolbar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        GoogleApiClient google_api_client;
        User_Type = Utility.getSharedPreferences(mContext, Constants.USER_TYPE);
        d_layout = (DrawerLayout) findViewById(R.id.dj_drawer_layout);
        drawerFragment = (FragmentDrawer) getSupportFragmentManager().findFragmentById(R.id.fragment_navigation_drawer);
        drawerFragment.setUp(R.id.fragment_navigation_drawer, d_layout, mToolbar);
        drawerFragment.setDrawerListener(this);
        hp_profile_image_drawer = (CircleImageView) d_layout.findViewById(R.id.hp_profile_image_drawer);

        displayView(getIntent().getIntExtra("FOR",1));
//        GetDrawerImage();
        setdata();
        try {
            b = new Bundle();
            b = getIntent().getExtras();
            System.out.println("BUNDLE OF BIDTYPE -----------------" + getIntent().getExtras() + b.getString("project_id"));
            String Notification = b.getString("type");
            if (Notification != null && !Notification.equals("")) {
                if (Notification.equals("bid")) {
//                    b.putString(Constants.PROJECT_ID, b.getString("project_id"));
                    Utility.setSharedPreference(mContext, "FragmentName", Constants.Fragment_Freelancer_Biding_List);
                    Utility.setSharedPreference(mContext, Constants.PROJECT_ID, b.getString("project_id"));
                    callSelectedFragment(Constants.Fragment_Freelancer_Biding_List, "Bid List");
                }
            } else if (Notification != null && !Notification.equals("")) {
                if (Notification.equals("hire")) {
                    Utility.setSharedPreference(mContext, "FragmentName", Constants.Fragment_Freelancer_Biding_List);
                    callSelectedFragment(Constants.Fragment_Freelancer_Biding_List, "Bid List");
                }
            }
        } catch (NullPointerException e) {
            e.printStackTrace();
        }}
 public void setdata(){
        if (Utility.getSharedPreferences(mContext, Constants.PROFILE_IMAGE).equals("")) {

        } else {
            Glide.with(mContext)
                    .load(Utility.getSharedPreferences(mContext, Constants.PROFILE_IMAGE))
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(hp_profile_image_drawer);
        }
        ((CustomTextView) d_layout.findViewById(R.id.hp_name_on_drawer)).setText(Utility.getSharedPreferences(mContext, Constants.USERNAME));
    }
    /*@Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);//Menu Resource, Menu
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_search:
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }*/



    @Override
    public void onDrawerItemSelected(View view, int position) {
        if (position == 7 && User_Type.equals("1")) {
            Intent in = new Intent(mContext, HomeLocation.class);
            Bundle extraa = new Bundle();
            extraa.putString(Constants.MODE, "add_mode");
            in.putExtras(extraa);
            startActivity(in);
        } else if (position == 6 && User_Type.equals("2")) {
            Intent in = new Intent(mContext, HomeLocation.class);
            Bundle extraa = new Bundle();
            extraa.putString(Constants.MODE, "add_mode");
            in.putExtras(extraa);
            startActivity(in);
        } else if (position == 8 && User_Type.equals("1")) {
            Intent in = new Intent(mContext, HomeMap_Activity.class);
            startActivity(in);
        } else if (position == 7 && User_Type.equals("2")) {
            Intent in = new Intent(mContext, HomeMap_Activity.class);
            startActivity(in);
        }
        /*if (position == 4) {
            startActivity(new Intent(mContext, HireServiceProviderHomeFragment.class));
        }*/ else {
            displayView(position);
        }
    }

    public void displayView(int position) {
        Fragment fragment = null;
        String title = getString(R.string.app_name);
        if (d_layout.isDrawerOpen((View) findViewById(R.id.fragment_navigation_drawer))) {
            d_layout.closeDrawer((View) findViewById(R.id.fragment_navigation_drawer));
        }
        if (User_Type.equals("1")) {
            switch (position) {
                case 0:
                    fragment = new Fragment_Profile();
                    title = getString(R.string.profile);
                    break;
                case 1:
                    fragment = new Fragment_Home();
                    title = getString(R.string.home);
                    break;
                case 2:
                    Utility.setSharedPreference(mContext, Constants.EDITPROJECTBTNCLICK, "91");
                    fragment = new Fragment_PostProject();
                    title = getString(R.string.post_project);
                    break;
                case 3:
                    Utility.setSharedPreference(mContext, Constants.SP_LISYKEY, "my_key");
                    fragment = new Hire_Home_MyProject_Fragment();
                    title = getString(R.string.my_projects);
                    break;
                case 4:
                    Utility.setSharedPreference(mContext, Constants.SP_LISYKEY, "sp_key");
                    fragment = new HireServiceProviderHomeFragment();
                    title = getString(R.string.hiring_service_provider);
                    //Utility.showToast(mContext, "coming soon");
                    break;
                case 5:
                    title = getString(R.string.transaction_hist);
                    Utility.showToast(mContext, "coming soon");
                    break;

                case 6:
                    title = getString(R.string.chat_box);
                    Utility.setSharedPreference(mContext, Constants.CHECKCHATBOX_REDIRECT, "2");
                    //Utility.showToast(mContext,"coming soon");
                    fragment = new Fragment_ChatList();
                    break;
                case 7:
                    title = getString(R.string.home_digg);
                    break;

                case 8:
                    title = getString(R.string.home_booking);
                    break;
                case 9:
                    title = getString(R.string.trip_it);
                    fragment = new Fragment_trip_tabs();
                    break;

                case 10:
                    title = getString(R.string.logout);
                    Logout();
                    break;
                default:
                    Utility.hideKeyboard(mContext);
                    break;

            }
        } else if (User_Type.equals("2")) {
            switch (position) {
                case 0:
                    fragment = new Fragment_Profile();
                    title = getString(R.string.profile);
                    break;
                case 1:
                    fragment = new Fragment_Home();
                    title = getString(R.string.home);
                    break;
                case 2:
                    fragment = new MyProject_Fragment();
                    title = getString(R.string.my_projects);
                    break;
                case 3:
                    fragment = new Fragment_Search_Project();
                    title = getString(R.string.search_project);
                    break;

                case 4:
                    title = getString(R.string.transaction_hist);
                    Utility.showToast(mContext, "coming soon");
                    break;

                case 5:
                    title = getString(R.string.chat_box);
                    Utility.setSharedPreference(mContext, Constants.CHECKCHATBOX_REDIRECT, "2");
                   // Utility.showToast(mContext,"coming soon");
                    fragment = new Fragment_ChatList();
                    break;


                case 6:
                    title = getString(R.string.home_digg);
                    break;

                case 7:
                    title = getString(R.string.home_booking);
                    Utility.showToast(mContext, "coming soon");
                    break;

                case 8:
                    title = getString(R.string.trip_it);
                    fragment = new Fragment_trip_tabs();
                    break;

                case 9:
                    title = getString(R.string.logout);
                    Logout();
                    break;
                default:
                    Utility.hideKeyboard(mContext);
                    break;
            }
        }
        if (fragment != null) {
            FragmentManager fragmentManager = getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.dj_container_body, fragment);
            fragmentTransaction.commit();
            // set the toolbar title
            // getSupportActionBar().setTitle(title);
            ((CustomTextView) mToolbar.findViewById(R.id.toolbar_title)).setText(title);
        }
    }


   /* @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        System.out.println("MainrequestCode "+requestCode);
        System.out.println("MainresultCode "+resultCode);
        switch (requestCode) {
            case FilePickerConst.REQUEST_CODE_PHOTO:
                if (resultCode == Activity.RESULT_OK && data != null) {
                    //   photoPaths = new ArrayList<>();
                    photoPaths.addAll(data.getStringArrayListExtra(FilePickerConst.KEY_SELECTED_PHOTOS));
                   *//* System.out.println("filePathsimg " + filePathsimgnewquestion);
                    //   filePathsimgnewquestion = data.getStringArrayListExtra(FilePickerConst.KEY_SELECTED_PHOTOS);
                    // filePathsnewquestion = new ArrayList<>();
                    maxcount = 5;
                    //filePathsnewquestion.addAll(filePathsimgnewquestion);
                    maxcount = maxcount - filePathsnewquestion.size();
                    System.out.println("filePaths " + photoPaths);*//*
                }
                break;

            case FilePickerConst.REQUEST_CODE_DOC:
                if (resultCode == Activity.RESULT_OK && data != null) {
                    docPaths = new ArrayList<>();
                    docPaths.addAll(data.getStringArrayListExtra(FilePickerConst.KEY_SELECTED_DOCS));
                   *//* System.out.println("filePathsimg " + filePathsimgnewquestion);
                    filePathsnewquestion = new ArrayList<>();
                    maxcount = 5;
                    filePathsnewquestion.addAll(filePathsimgnewquestion);
                    maxcount = maxcount - filePathsnewquestion.size();
                    System.out.println("filePaths " + filePathsnewquestion);*//*
                }
                break;
        }

       // addThemToView(photoPaths, docPaths);
    }*/


    private void changeUI(boolean signedIn) {
    }

  /*  public void GetDrawerImage() {
        CircleImageView hp_profile_image_drawer = (CircleImageView) findViewById(hp_profile_image_drawer);
        if (Utility.getSharedPreferences(mContext, Constants.PROFILE_IMAGE).equals("")) {

        } else {
            Glide.with(mContext)
                    .load(Utility.getSharedPreferences(mContext, Constants.PROFILE_IMAGE))
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(hp_profile_image_drawer);
        }
    }*/

    public void Logout() {
        if (Utility.isConnectingToInternet(mContext)) {
            LogoutTask();
        } else {
            Utility.showPromptDlgForSuccess(mContext, "", "please check your network connection");
        }

    }

    public void LogoutTask() {
        try {
            JSONObject jlogout = new JSONObject();
            jlogout.put(Constants.ACTION, Constants.LOGOUT);
            jlogout.put("user_id", Utility.getSharedPreferences(mContext, Constants.USERID));
            rt = new ResponseTask(Constant_Urls.SERVER_URL, jlogout);

            Utility.ShowLoading(mContext, getResources().getString(R.string.please_wait));

            rt = new ResponseTask(Constant_Urls.SERVER_URL, jlogout);
            rt.setListener(new ResponseListener() {
                @Override
                public void onGetPickSuccess(String result) {
                    Utility.HideDialog();
                    try {
                        JSONObject jobj = new JSONObject(result);
                        if (jobj.getString("success").equals("1")) {
                            Utility.clearsharedpreference(mContext);
                            startActivity(new Intent(mContext, LoginActivity.class));
                            finish();
                            Utility.ShowToastMessage(mContext, "Logout Successfully");
                        }
                    } catch (JSONException j) {
                        j.printStackTrace();
                    }
                }

            });
            rt.execute();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void callSelectedFragment(String FragmentName, String Title) {
        Fragment fragment = null;
        if (FragmentName.equals("Hire_Home_MyProject_Fragment")) {
            fragment = new Hire_Home_MyProject_Fragment();
        } else if (FragmentName.equals(Constants.FRAGMENT_HOME_POSTBUDGET)) {
            fragment = new Fragment_AddProject_Postbudget();
        } else if (FragmentName.equals(Constants.FRAGMENT_HOME_PROJECTSUMMARY)) {
            fragment = new Fragment_PostProject();
        } else if (FragmentName.equals(Constants.TABFRAGMENT)) {
            fragment = new Hire_Home_MyProject_Fragment();
        } else if (FragmentName.equals(Constants.Fragment_Freelancer_Biding_List)) {
            fragment = new Fragment_Freelancer_Biding_List();
        } else if (FragmentName.equals(Constants.Fragment_ProjectDetailandProposalListTab)) {
            fragment = new Fragment_ProjectDetailandProposalListTab();
        } else if (FragmentName.equals(Constants.MyBidedProject)) {
            fragment = new MyBidedProject();
        } else if (FragmentName.equals(Constants.FRAGMENT_POSTPROJECT)) {
            fragment = new Fragment_PostProject();
        }/* else if (FragmentName.equals(Constants.ProfileFragment)) {
            fragment = new ProfileFragment();
        } else if (FragmentName.equals(Constants.NewProfileFragment)) {
            fragment = new NewProfileFragment();
        } else if(FragmentName.equals(Constants.ReViewerNotificationFragment)){
            fragment = new ReViewerNotificationFragment();
        }*/
        if (fragment != null) {
            FragmentManager fragmentManager = getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.dj_container_body, fragment);
            fragmentTransaction.commit();
            // set the toolbar title
            getSupportActionBar().setTitle(Title);
        }
    }

    @Override
    public void onBackPressed() {

        if (drawerFragment.isVisible()) {
            d_layout.closeDrawers();
        } else if (Utility.getSharedPreferences(mContext, Constants.BACKPRESSEDNAME).equals(Constants.FRAGMENT_HOME)) {
            Utility.removepreference(mContext, Constants.BACKPRESSEDNAME);
            displayView(0);
        } else if (Utility.getSharedPreferences(mContext, Constants.BACKPRESSEDNAME).equals(Constants.MYPROJECT)) {
            Utility.removepreference(mContext, Constants.BACKPRESSEDNAME);
            displayView(0);

        } else if (Utility.getSharedPreferences(mContext, Constants.BACKPRESSEDNAME).equals(Constants.Hire_Home_MyProject_Fragment)) {
            Utility.removepreference(mContext, Constants.BACKPRESSEDNAME);
            displayView(3);

        } else {
            finish();
        }

    }

}