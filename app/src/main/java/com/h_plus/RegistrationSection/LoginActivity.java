package com.h_plus.RegistrationSection;

import android.app.Activity;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.SimpleAdapter;
import android.widget.Spinner;

import com.facebook.FacebookSdk;
import com.google.firebase.iid.FirebaseInstanceId;
import com.h_plus.H_Booking.TripIT.activity.MordgageSignupActivity;
import com.h_plus.H_Booking.TripIT.activity.MortgageDetail_Activity;
import com.h_plus.R;
import com.h_plus.ServerInteraction.ResponseListener;
import com.h_plus.ServerInteraction.ResponseTask;
import com.h_plus.customwidget.CustomEditText;
import com.h_plus.customwidget.CustomTextView;
import com.h_plus.useractivity.PaymentDeduct_Activity;
import com.h_plus.usertabs.utility.Constant_Urls;
import com.h_plus.usertabs.utility.Constants;
import com.h_plus.usertabs.utility.Utility;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


/**
 * Created by Tejs on 7/13/2016.
 */
public class LoginActivity extends Activity implements View.OnClickListener {
    Context mContext;
    SharedPreferences share;
    SharedPreferences.Editor edit;
    ResponseTask responseTask;
    String gcmid = "", WeightString = "";
    Dialog approvaldialog, sndnumberdialog;
    List<HashMap<String, String>> country_code_spinner = new ArrayList<>();
    private Handler customHandler = new Handler();
    private BroadcastReceiver mIntentReceiver;

    private Runnable updateTimerThread = new Runnable() {

        public void run() {
            sndnumberdialog.findViewById(R.id.resend_number).setEnabled(true);
            customHandler.removeCallbacks(updateTimerThread);

        }

    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        FacebookSdk.sdkInitialize(this.getApplicationContext());
        setContentView(R.layout.activity_login_screen);
        mContext = this;
        gcmid = FirebaseInstanceId.getInstance().getToken();
        country_code_spinner = Utility.readXMLofCountries(mContext);
        ButtonClicks();
    }
    protected void onResume() {
        super.onResume();

        IntentFilter intentFilter = new IntentFilter("SmsMessage.intent.MAIN");
        mIntentReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                String msg = intent.getStringExtra("get_msg");
                System.out.println("mmmmsssssssssg       " + msg);
                msg = msg.replace("\n", "");
                String body = msg.substring(msg.lastIndexOf(":") + 1, msg.length());
                String pNumber = msg.substring(0, msg.lastIndexOf(":"));
                System.out.println("body      " + body);
                Pattern p = Pattern.compile("\\d+");
                Matcher m = p.matcher(body);
                String code = "";
                while (m.find()) {
                    code = m.group();
                    System.out.println(m.group());
                    break;
                }
                if (sndnumberdialog != null && sndnumberdialog.isShowing()) {
                    ((CustomEditText) sndnumberdialog.findViewById(R.id.verificate_code)).setText(code);
                    sndnumberdialog.findViewById(R.id.send_number).setEnabled(true);
//    customHandler.removeCallbacks(updateTimerThread);
                }
//Add it to the list or do whatever you wish to

            }
        };
        this.registerReceiver(mIntentReceiver, intentFilter);
    }

    @Override
    public void onPause() {
        super.onPause();
        this.unregisterReceiver(this.mIntentReceiver);
    }

    public void ButtonClicks() {
        //(findViewById(R.id.fb_btn_on_login)).setOnClickListener(this);
        //(findViewById(R.id.google_btn_on_login)).setOnClickListener(this);
        (findViewById(R.id.login_btn)).setOnClickListener(this);
        (findViewById(R.id.forgot_password)).setOnClickListener(this);
        (findViewById(R.id.need_a_account)).setOnClickListener(this);
        (findViewById(R.id.mordgage_sign_up)).setOnClickListener(this);
        //  (findViewById(R.id.linkedin_btn_on_login)).setOnClickListener(this);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
      /*  Intent need = new Intent(mContext, LoginSignUpSelectionActivity.class);
        startActivity(need);*/
        overridePendingTransition(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
        finish();

    }


    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.login_btn:
                if (((EditText) findViewById(R.id.login_email)).getText().toString().equals("")) {
                    ((EditText) findViewById(R.id.login_email)).setError("Email missing");
                    ((EditText) findViewById(R.id.login_email)).requestFocus();
                } else if (((EditText) findViewById(R.id.login_password)).getText().toString().equals("")) {
                    ((EditText) findViewById(R.id.login_password)).setError("Password missing");
                    ((EditText) findViewById(R.id.login_password)).requestFocus();
                } else {
                    if (!Utility.isValidEmail(((EditText) findViewById(R.id.login_email)).getText().toString())) {
                        ((EditText) findViewById(R.id.login_email)).setError("Please Enter Valid Email");
                        ((EditText) findViewById(R.id.login_email)).requestFocus();
                    } else {
                        if (Utility.isConnectingToInternet(mContext)) {
                            LoginTask(((EditText) findViewById(R.id.login_email)).getText().toString(),
                                    ((EditText) findViewById(R.id.login_password)).getText().toString());
                        } else {
                            Utility.Alert(mContext, "please check your netwotk connection");
                        }
                    }
                }
                break;

            case R.id.forgot_password:
                Intent j = new Intent(mContext, ForgotPasswordActivity.class);
                startActivity(j);
                overridePendingTransition(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
                finish();

                break;

            case R.id.need_a_account:
                Intent need = new Intent(mContext, SignupActivity.class);
                startActivity(need);
                overridePendingTransition(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
                finish();

                break;
            case R.id.mordgage_sign_up:
                startActivity(new Intent(mContext, MordgageSignupActivity.class));
            default:
                break;
        }
    }

    public void ApprovalDialog() {
        approvaldialog = new Dialog(mContext);
        approvaldialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        approvaldialog.setCancelable(false);
        approvaldialog.setCanceledOnTouchOutside(false);
        approvaldialog.setContentView(R.layout.dialog_profile_approval);
        approvaldialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        // ((CustomTextView) approvaldialog.findViewById(R.id.toolbar_title)).setText("Profile Approval");

        approvaldialog.findViewById(R.id.snd_approval_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Utility.isConnectingToInternet(mContext)) {
                    SendProfileApproval(Utility.getSharedPreferences(mContext, Constants.USERID));

                } else {
                    Utility.showPromptDlgForSuccess(mContext, "Please check your network connection", "");
                }

            }
        });

        approvaldialog.findViewById(R.id.cancel_profile_approval_dialog).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                approvaldialog.cancel();
            }
        });
        approvaldialog.show();
    }

    public void SendProfileApproval(String uid) {
        try {
            JSONObject joo = new JSONObject();
            joo.put(Constants.ACTION, Constants.SENDREQUESR_FORAPPROAVAL);
            joo.put("user_id", uid);

            Utility.ShowLoading(mContext, getResources().getString(R.string.please_wait));
            responseTask = new ResponseTask(Constant_Urls.SERVER_URL, joo);
            responseTask.setListener(new ResponseListener() {
                @Override
                public void onGetPickSuccess(String result) {

                    Utility.HideDialog();
                    if (result == null) {
                        Utility.showToast(mContext, "Process incomplete..please try again!");
                    } else {
                        try {
                            JSONObject j = new JSONObject(result);
                            if (j.getString("success").equals("1")) {
                                Utility.showToast(mContext, "your approval is send to our team.Please wait our support will get back as soon as possible");
                                approvaldialog.dismiss();
                            } else {
                                approvaldialog.dismiss();
                                Utility.showPromptDlgForSuccess(mContext, j.getString("msg"), "");
                            }
                        } catch (JSONException j) {
                            j.printStackTrace();
                        }

                    }
                }
            });

        } catch (JSONException e) {
            e.printStackTrace();
        }
        responseTask.execute();
    }

    public void LoginTask(String email, String pw) {
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put(Constants.ACTION, Constants.LOGIN);
            jsonObject.put("email", email);
            jsonObject.put("password", pw);
            jsonObject.put("device_id", gcmid);
            jsonObject.put("device_type", "android");
            Utility.ShowLoading(mContext, mContext.getResources().getString(R.string.please_wait));
            responseTask = new ResponseTask(Constant_Urls.SERVER_URL, jsonObject);
            responseTask.setListener(new ResponseListener() {
                @Override
                public void onGetPickSuccess(String result) {
                    Utility.HideDialog();
                    System.out.println("Server result ====> " + result);
                    if (result == null) {
                        Utility.showToast(mContext, "Process incomplete..please try again!");
                    } else {
                        try {
                            JSONObject jobj = new JSONObject(result);
                            if (jobj.getString(Constants.RESULT_KEY).equals("1")) {
                                Utility.setSharedPreference(mContext, Constants.USERID, jobj.getJSONObject("object").getString("user_id"));
                                Utility.setSharedPreference(mContext, Constants.USER_TYPE, jobj.getJSONObject("object").getString("usertype"));

                                if (jobj.getJSONObject("object").getString("user_name").equals("") && jobj.getJSONObject("object").getString("profile_image_url").equals("")) {
                                    Utility.setSharedPreference(mContext, Constants.USERNAME, "");
                                    Utility.setSharedPreference(mContext, Constants.PROFILE_IMAGE, "");
                                } else {
                                    Utility.setSharedPreference(mContext, Constants.USERNAME, jobj.getJSONObject("object").getString("user_name"));
                                    Utility.setSharedPreference(mContext, Constants.PROFILE_IMAGE, jobj.getJSONObject("object").getString("profile_image_url"));
                                }

                                if (jobj.getJSONObject("object").getString("phone_verify").equals("1") && jobj.getJSONObject("object").getString("paypal_payment_verificaion").equals("1")
                                        && jobj.getJSONObject("object").getString("profile_verify").equals("1")) {

                                    Utility.setSharedPreference(mContext, Constants.USER_BIO, jobj.getJSONObject("object").getString("bio"));
                                    Utility.setSharedPreference(mContext, Constants.PAYPAL_VERIFICATION_CHECKED, jobj.getJSONObject("object")
                                            .getString("paypal_payment_verificaion"));

                                    Utility.showToast(mContext, "Login successfully done");
                                    Utility.setSharedPreference(mContext, Constants.SPLASH_CHECK, "1");
                                    overridePendingTransition(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
                                    startActivity(new Intent(mContext, MainActivity.class));
                                    finish();

                                } else if (jobj.getJSONObject("object").getString("phone_verify").equals("0")) {
                                    SendNumberdialog();
                                } else if (jobj.getJSONObject("object").getString("paypal_payment_verificaion").equals("0")) {
                                    overridePendingTransition(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
                                    startActivity(new Intent(mContext, PaymentDeduct_Activity.class));
                                    finish();

                                } else if (jobj.getJSONObject("object").getString("profile_verify").equals("1")) {

                                    /*if (jobj.getJSONObject("object").getString("user_name").equals("") && jobj.getJSONObject("object").getString("image_url").equals("")) {
                                        Utility.setSharedPreference(mContext, Constants.USERNAME, "");
                                        Utility.setSharedPreference(mContext, Constants.PROFILE_IMAGE, "");
                                    } else {
                                        Utility.setSharedPreference(mContext, Constants.USERNAME, jobj.getJSONObject("object").getString("username"));
                                        Utility.setSharedPreference(mContext, Constants.PROFILE_IMAGE, jobj.getJSONObject("object").getString("profile_image_url"));
                                    }*/
                                    Utility.setSharedPreference(mContext, Constants.USER_BIO, jobj.getJSONObject("object").getString("bio"));
                                    Utility.setSharedPreference(mContext, Constants.PAYPAL_VERIFICATION_CHECKED, jobj.getJSONObject("object")
                                            .getString("paypal_payment_verificaion"));

                                    Utility.setSharedPreference(mContext, Constants.SPLASH_CHECK, "1");
                                    Utility.showToast(mContext, "Login successfully done");
                                    startActivity(new Intent(mContext, MainActivity.class));
                                } else {
                                    ApprovalDialog();
                                }
                                if (jobj.getJSONObject("object").getString("usertype").equals("3")) {
                                    Utility.setSharedPreference(mContext, "SplashCheck", "2");
                                    Utility.showToast(mContext, "Login successfully done");
                                    Intent in = new Intent(mContext, MortgageDetail_Activity.class);
                                    startActivity(in);
                                }
                            } else {
                                Utility.showCroutonWarn(jobj.getString(Constants.SERVER_MSG), LoginActivity.this);
                            }
                        } catch (JSONException e2) {
                            Utility.showCroutonWarn(getResources().getString(R.string.server_fail), LoginActivity.this);
                        }
                    }
                }
            });
        } catch (JSONException e) {
            e.printStackTrace();
        }
        responseTask.execute();
    }

    public void SendNumberdialog() {

        Spinner select_number_code;
        sndnumberdialog = new Dialog(mContext);
        sndnumberdialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        sndnumberdialog.setCancelable(false);
        sndnumberdialog.setCanceledOnTouchOutside(false);
        sndnumberdialog.setContentView(R.layout.dialog_select_cntry);
        sndnumberdialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        select_number_code = (Spinner) sndnumberdialog.findViewById(R.id.select_number_code);

        int[] to = new int[]{R.id.c_code, R.id.c_name};
        String[] from = new String[]{"phoneCode", "name"};
        //Creating a SimpleAdapter for the Spinner
        SimpleAdapter adapter = new SimpleAdapter(mContext, country_code_spinner, R.layout.text_single_add, from, to);
        select_number_code.setAdapter(adapter);
        select_number_code.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position > 0) {
                    WeightString = country_code_spinner.get(position).get("phoneCode").toString();
                    ((CustomTextView) sndnumberdialog.findViewById(R.id.country_code)).setText("+" + WeightString);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

                // value_spinner = "";
            }
        });

        sndnumberdialog.findViewById(R.id.send_number).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Utility.isConnectingToInternet(mContext)) {
                    if (((CustomTextView) sndnumberdialog.findViewById(R.id.country_code)).getText().toString().equals("")) {
                        Utility.showToast(mContext, "please select country code");
                    } else if (((CustomEditText) sndnumberdialog.findViewById(R.id.dialog_mobile)).getText().toString().equals("")) {
                        Utility.showToast(mContext, "please enter your mobile number");

                    } else {
                        SendMobileNumber(((CustomEditText) sndnumberdialog.findViewById(R.id.dialog_mobile)).getText().toString(),
                                ((CustomTextView) sndnumberdialog.findViewById(R.id.country_code)).getText().toString());
                    }
                } else {
                    Utility.showToast(mContext, "PLease check your internet connection");
                }
            }
        });

        sndnumberdialog.findViewById(R.id.snd_vf_code).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Utility.isConnectingToInternet(mContext)) {
                    if (((CustomEditText) sndnumberdialog.findViewById(R.id.verificate_code)).getText().toString().equals("")) {
                        Utility.Alert(mContext, "please check your message and  enter your verification code");
                    } else {
                        SendVerificationCode(((CustomEditText) sndnumberdialog.findViewById(R.id.verificate_code)).getText().toString());
                    }
                } else {
                    Utility.showToast(mContext, "PLease check your internet connection");
                }
            }
        });

        sndnumberdialog.findViewById(R.id.cancel_dialog).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sndnumberdialog.cancel();
            }
        });

        sndnumberdialog.show();
    }

    public void SendMobileNumber(String mobile, String country_code) {
           /*https://infograins.com/INFO01/homesplus/api/api.php?action=send_verification_code&
    user_id=117&contact_number=9300014089&country_code=91*/
        try {

            JSONObject jobj = new JSONObject();
            jobj.put(Constants.ACTION, Constants.SEND_VERICATION_NUMBER);
            jobj.put("user_id", Utility.getSharedPreferences(mContext, Constants.USERID));
            jobj.put("contact_number", mobile);
            jobj.put("country_code", country_code);
            Utility.ShowLoading(mContext, mContext.getResources().getString(R.string.please_wait));

            responseTask = new ResponseTask(Constant_Urls.SERVER_URL, jobj);
            responseTask.setListener(new ResponseListener() {
                @Override
                public void onGetPickSuccess(String result) {
                    Utility.HideDialog();
                    System.out.println("Server result ====> " + result);
                    if (result == null) {

                    } else {
                        try {
                            JSONObject jobj = new JSONObject(result);
                            if (jobj.getString(Constants.RESULT_KEY).equals("1")) {
                                Utility.ShowToastMessage(mContext, jobj.getString("msg"));
                                Utility.showToast(mContext, jobj.getString("msg"));
                                sndnumberdialog.findViewById(R.id.resend_number).setEnabled(false);
                                customHandler.postDelayed(updateTimerThread, 30000);
                            } else {
                                Utility.ShowToastMessage(mContext, jobj.getString("msg"));
                                Utility.showCroutonWarn(jobj.getString(Constants.SERVER_MSG), LoginActivity.this);
                            }
                        } catch (JSONException e2) {
                            Utility.showCroutonWarn(getResources().getString(R.string.server_fail), LoginActivity.this);
                        }
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
        responseTask.execute();
    }

    public void SendVerificationCode(String VCode) {
        /*https://infograins.com/INFO01/homesplus/api/api.php?action=check_phone_verification_code&
        user_id=117&verification_code=1202*/
        try {

            JSONObject jobj = new JSONObject();
            jobj.put(Constants.ACTION, Constants.SEND_VERICATION_CODE);
            jobj.put("user_id", Utility.getSharedPreferences(mContext, Constants.USERID));
            jobj.put("verification_code", VCode);
            Utility.ShowLoading(mContext, mContext.getResources().getString(R.string.please_wait));

            responseTask = new ResponseTask(Constant_Urls.SERVER_URL, jobj);
            responseTask.setListener(new ResponseListener() {
                @Override
                public void onGetPickSuccess(String result) {
                    Utility.HideDialog();
                    System.out.println("Server result ====> " + result);
                    if (result == null) {

                    } else {
                        try {
                            JSONObject jobj = new JSONObject(result);
                            if (jobj.getString(Constants.RESULT_KEY).equals("1")) {
                                Utility.showToast(mContext,"your phone number is verified successfully");
                                //Utility.showToast(mContext, jobj.getString("msg"));
                               /* Utility.setSharedPreference(mContext, Constants.SPLASH_CHECK, "1");*/
                                startActivity(new Intent(mContext, Social_MediaVerification_Activity.class));
                            } else {
                                Utility.ShowToastMessage(mContext, jobj.getString("msg"));
                                Utility.showCroutonWarn(jobj.getString(Constants.SERVER_MSG), LoginActivity.this);
                            }
                        } catch (JSONException e2) {
                            Utility.showCroutonWarn(getResources().getString(R.string.server_fail), LoginActivity.this);
                        }
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
        responseTask.execute();
    }


}
