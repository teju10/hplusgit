package com.h_plus.RegistrationSection;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.h_plus.R;
import com.h_plus.ServerInteraction.ResponseListener;
import com.h_plus.ServerInteraction.ResponseTask;
import com.h_plus.customwidget.CircleImageView;
import com.h_plus.customwidget.CustomEditText;
import com.h_plus.customwidget.CustomTextView;
import com.h_plus.usertabs.utility.Constant_Urls;
import com.h_plus.usertabs.utility.Constants;
import com.h_plus.usertabs.utility.Utility;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Infograins on 11/30/2016.
 */

public class Change_PasswordActivity extends AppCompatActivity {

    Context mContext;
    ResponseTask responseTask;
    private Toolbar mToolbar;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_changepw);

        mContext = this;
        mToolbar = (Toolbar) findViewById(R.id.h_toolbar);
        setSupportActionBar(mToolbar);
        mToolbar.setBackgroundColor(getResources().getColor(android.R.color.transparent));
        ((CustomTextView) findViewById(R.id.toolbar_title)).setText("Change Password");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        Init();
    }

    public void Init() {

        Glide.with(mContext)
                .load(Utility.getSharedPreferences(mContext, Constants.PROFILE_IMAGE))
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .listener(new RequestListener<String, GlideDrawable>() {
                    @Override
                    public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                        (findViewById(R.id.cp_pb)).setVisibility(View.GONE);
                        return false;
                    }
                })
                .into((CircleImageView) findViewById(R.id.cp_profile_img));
        if (!Utility.getSharedPreferences(mContext, Constants.USERNAME).equals("")) {
            ((CustomTextView) findViewById(R.id.cnge_pw_uname)).
                    setText(Utility.getSharedPreferences(mContext, Constants.USERNAME));
        } else {

        }

       /* System.out.println("USERNAME "+Utility.getSharedPreferences(mContext, Constants.USERNAME));
        ((CustomTextView) findViewById(R.id.cnge_pw_uname)).
                setText(Utility.getSharedPreferences(mContext, Constants.USERNAME));*/

        findViewById(R.id.change_pw_done).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (((CustomEditText) findViewById(R.id.change_pw_oldpw)).getText().toString().equals("")) {
                    Utility.showToast(mContext, "Please enter your old password");

                } else if (((CustomEditText) findViewById(R.id.change_pw_new_pw)).getText().toString().equals("")) {
                    Utility.showToast(mContext, "Please enter your new password");
                } else if (((CustomEditText) findViewById(R.id.change_pw_new_pw)).getText().toString().length() < 6) {
                    Utility.showToast(mContext, "Password length should be more than 6 character!");
                } else if (((CustomEditText) findViewById(R.id.change_pw_cnfrm_pw)).getText().toString().equals("")) {
                    Utility.showToast(mContext, "Please enter your new password");
                } else if (!((CustomEditText) findViewById(R.id.change_pw_cnfrm_pw)).getText().toString().equals(((EditText) findViewById(R.id.change_pw_new_pw)).getText().toString())) {
                    Utility.showToast(mContext, "password does not match");
                } else {
                    ChangePWTask((Utility.getSharedPreferences(mContext, Constants.USERID)), ((CustomEditText) findViewById(R.id.change_pw_oldpw)).getText().toString(),
                            ((CustomEditText) findViewById(R.id.change_pw_new_pw)).getText().toString());
                }
            }
        });

    }

    public void ChangePWTask(String u_id, String oldpw, String newpw) {
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put(Constants.ACTION, Constants.CHANGEPW);
            jsonObject.put("user_id", u_id);
            jsonObject.put("old_password", oldpw);
            jsonObject.put("new_password", newpw);

            Utility.ShowLoading(mContext, mContext.getResources().getString(R.string.please_wait));
            responseTask = new ResponseTask(Constant_Urls.SERVER_URL, jsonObject);
            responseTask.setListener(new ResponseListener() {
                @Override
                public void onGetPickSuccess(String result) {
                    Utility.HideDialog();
                    System.out.println("Server result ====> " + result);
                    if (result == null) {

                    } else {
                        try {
                            JSONObject jobj = new JSONObject(result);
                            if (jobj.getString(Constants.RESULT_KEY).equals("1")) {
                                Utility.showToast(mContext, "Password Updated Succesfully");
                                ((EditText) findViewById(R.id.change_pw_oldpw)).setText("");
                                ((EditText) findViewById(R.id.change_pw_cnfrm_pw)).setText("");
                                finish();
                            } else {
                                Utility.showCroutonWarn(jobj.getString(Constants.SERVER_MSG), Change_PasswordActivity.this);
                            }
                        } catch (JSONException e2) {
                            Utility.showCroutonWarn(getResources().getString(R.string.server_fail), Change_PasswordActivity.this);
                        }
                    }
                }
            });
        } catch (JSONException e) {
            e.printStackTrace();
        }
        responseTask.execute();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
