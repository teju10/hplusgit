package com.h_plus.RegistrationSection;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import com.h_plus.R;
import com.h_plus.ServerInteraction.ResponseListener;
import com.h_plus.ServerInteraction.ResponseTask;
import com.h_plus.usertabs.utility.Constant_Urls;
import com.h_plus.usertabs.utility.Constants;
import com.h_plus.usertabs.utility.Utility;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Tejs on 7/13/2016.
 */
public class ForgotPasswordActivity extends Activity {
    Context mContext;
    ResponseTask rt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.forgot_password_screen);
        mContext = this;
        ButtonClicks();
    }

    public void ButtonClicks() {
        (findViewById(R.id.send_btn)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Utility.isConnectingToInternet(mContext)) {
                    if (((EditText) findViewById(R.id.email_forgot)).getText().toString().equals("")) {
                        Utility.showToast(mContext, "Email address is missing");
                    } else if (!Utility.isValidEmail(((EditText) findViewById(R.id.email_forgot)).getText().toString())) {
                        Utility.showToast(mContext, "Please Enter Valid Email");

                    } else {
                        ForgetPassword(((EditText) findViewById(R.id.email_forgot)).getText().toString());
                    }
                } else {
                    Utility.showToast(mContext, "please check your internet connection");
                }
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent need = new Intent(mContext, LoginActivity.class);
        startActivity(need);
        finish();
        overridePendingTransition(android.R.anim.slide_out_right, android.R.anim.slide_in_left);
    }

    // Todo forgot task


    public void ForgetPassword(String email) {

        try {

            JSONObject jobj = new JSONObject();
            jobj.put(Constants.ACTION, Constants.FORGETPASSWORD);
            jobj.put("email", email);
            Utility.ShowLoading(mContext, mContext.getResources().getString(R.string.please_wait));
            rt = new ResponseTask(Constant_Urls.SERVER_URL, jobj);
            rt.setListener(new ResponseListener() {
                @Override
                public void onGetPickSuccess(String result) {
                    Utility.HideDialog();
                    System.out.println("Server result ====> " + result);
                    if (result == null) {

                    } else {
                        try {
                            JSONObject jobj = new JSONObject(result);
                            if (jobj.getString(Constants.RESULT_KEY).equals("1")) {
                                Utility.ShowLoading(mContext, "your password is send to your email..please check your email");
                                startActivity(new Intent(mContext, LoginActivity.class));
                            } else {
                                Utility.showCroutonWarn(jobj.getString(Constants.SERVER_MSG), ForgotPasswordActivity.this);
                            }
                        } catch (JSONException e2) {
                            Utility.showCroutonWarn(getResources().getString(R.string.server_fail), ForgotPasswordActivity.this);
                        }
                    }
                }

            });
            rt.execute();
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }
}
