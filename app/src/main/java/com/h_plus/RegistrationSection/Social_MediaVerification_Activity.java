package com.h_plus.RegistrationSection;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;

import com.facebook.FacebookSdk;
import com.facebook.share.model.AppInviteContent;
import com.facebook.share.widget.AppInviteDialog;
import com.google.android.gms.appinvite.AppInviteInvitation;
import com.h_plus.R;
import com.h_plus.customwidget.CustomTextView;
import com.h_plus.useractivity.PaymentDeduct_Activity;
import com.h_plus.usertabs.utility.Utility;

/**
 * Created by Infograins on 11/30/2016.
 */

public class Social_MediaVerification_Activity extends AppCompatActivity implements View.OnClickListener {
    public static final int REQUEST_INVITE = 234;
    Context mContext;
    String TAG = Social_MediaVerification_Activity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_social_media_verification);
        mContext = this;

        setSupportActionBar((Toolbar) findViewById(R.id.h_toolbar));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        ((CustomTextView) findViewById(R.id.toolbar_title)).setText("Social Media Verification");

        FacebookSdk.sdkInitialize(this);
        ButtonClicks();
    }


    public void ButtonClicks() {
        findViewById(R.id.skip_countinue).setOnClickListener(this);
        findViewById(R.id.fb_invitation).setOnClickListener(this);
        findViewById(R.id.linkedin_invitation).setOnClickListener(this);
        findViewById(R.id.google_invitation).setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.skip_countinue:

                startActivity(new Intent(mContext, PaymentDeduct_Activity.class));
                //finish();
                overridePendingTransition(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
                break;

            case R.id.fb_invitation:
                FBAppinvite();
                // Utility.showToast(mContext, "coming soon");
                break;

            case R.id.linkedin_invitation:
                Utility.showToast(mContext, "coming soon");
                break;

            case R.id.google_invitation:
                GoogleAppinvite();
                //    Utility.showToast(mContext, "coming soon");
                break;
        }
    }

    private void FBAppinvite() {

        String appLinkUrl, previewImageUrl;

        // appLinkUrl = "https://play.google.com/store/apps/details?id=com.whoddi";
        appLinkUrl = "https://fb.me/1874876096060996";
        previewImageUrl = "https://infograins.com/INFO01/homesplus/img/logo.png";

        if (AppInviteDialog.canShow()) {
            AppInviteContent content = new AppInviteContent.Builder()
                    .setApplinkUrl(appLinkUrl)
                    .setPreviewImageUrl(previewImageUrl)
                    .build();
            AppInviteDialog.show(this, content);
        }
    }


    private void GoogleAppinvite() {
        try {

            Intent intent = new AppInviteInvitation.IntentBuilder(getString(R.string.invitation_title))
                    .setMessage(getString(R.string.invitation_message))
                    .setDeepLink(Uri.parse(getString(R.string.invitation_deep_link)))
                    .setCustomImage(Uri.parse(getString(R.string.invitation_custom_image)))
                    // .setCallToActionText(getString(R.string.invitation_cta))
                    .build();
            startActivityForResult(intent, REQUEST_INVITE);
        } catch (ActivityNotFoundException ac) {
           /* Intent sendIntent = new Intent();
            sendIntent.setAction(Intent.ACTION_SEND);
            sendIntent.putExtra(Intent.EXTRA_TEXT, getString(R.string.sharing_book_title, bookTitle));
            sendIntent.setType("text/plain");
            startActivity(sendIntent);*/
            ac.printStackTrace();
        }
    }
    /*@Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == INVITE_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                String[] ids = AppInviteInvitation.getInvitationIds(resultCode, data);
                Log.d(TAG, getString(R.string.sent_invitations_fmt, ids.length));
            } else {

                Log.d(TAG, "invite send failed or cancelled:" + requestCode + ",resultCode:" + resultCode );
            }
        }
    }*/

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.d(TAG, "onActivityResult: requestCode=" + requestCode + ", resultCode=" + resultCode);

        if (requestCode == REQUEST_INVITE) {
            if (resultCode == RESULT_OK) {
                // Get the invitation IDs of all sent messages
                String[] ids = AppInviteInvitation.getInvitationIds(resultCode, data);
                for (String id : ids) {
                    Log.d(TAG, "onActivityResult: sent invitation " + id);
                }
            } else {
                // Sending failed or it was canceled, show failure message to the user
                // ...
            }
        }
    }

    @Override
    public void onBackPressed() {
        startActivity(new Intent(mContext, LoginActivity.class));
        finish();
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
