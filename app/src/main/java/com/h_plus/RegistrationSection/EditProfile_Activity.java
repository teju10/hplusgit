package com.h_plus.RegistrationSection;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.DatePicker;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.firebase.iid.FirebaseInstanceId;
import com.h_plus.R;
import com.h_plus.ServerInteraction.ResponseListener;
import com.h_plus.ServerInteraction.ResponseTask;
import com.h_plus.customwidget.CircleImageView;
import com.h_plus.customwidget.CustomButton;
import com.h_plus.customwidget.CustomEditText;
import com.h_plus.customwidget.CustomTextView;
import com.h_plus.usertabs.utility.Constant_Urls;
import com.h_plus.usertabs.utility.Constants;
import com.h_plus.usertabs.utility.MultipartUtility;
import com.h_plus.usertabs.utility.Utility;
import com.soundcloud.android.crop.Crop;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.Calendar;

/**
 * Created by Infograins on 29-Nov-16.
 */

public class EditProfile_Activity extends AppCompatActivity {

    Context mContext;
    CustomTextView uname, spin_date, spin_month, spin_year;

    CustomEditText edit_profile_first_name, edit_profile_last_name, edit_profile_user_name, edit_profile_email,
            edit_profile_phone, status, edit_profile_address, edit_profile_city, edit_profile_country, edit_state,
            edit_profile_zip_code;
    LinearLayout date_layout;
    RadioGroup gender_group;
    RadioButton male, female;
    CustomButton profile_done, change_pass;
    CircleImageView imageButton, circleImageView;
    ResponseTask responseTask;
    String dobb;
    String selected_gender = "";
    String encodedImage = "", gcmid, PicturePath = "", mCropImageUri, path = null;
    String TAG = EditProfile_Activity.class.getSimpleName();
    private Toolbar mToolbar;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_edit_profile);
        mContext = this;
        mToolbar = (Toolbar) findViewById(R.id.h_toolbar);
        setSupportActionBar(mToolbar);
        mToolbar.setBackgroundColor(getResources().getColor(android.R.color.transparent));

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        ((CustomTextView) findViewById(R.id.toolbar_title)).setText("Edit Profile");
        gcmid = FirebaseInstanceId.getInstance().getToken();
        System.out.println("GCMID " + gcmid);

        Find();
        Init();

    }


    public void Find() {

        edit_profile_user_name = (CustomEditText) findViewById(R.id.edit_profile_user_name);
        edit_profile_first_name = (CustomEditText) findViewById(R.id.edit_profile_first_name);
        edit_profile_last_name = (CustomEditText) findViewById(R.id.edit_profile_last_name);
        spin_month = (CustomTextView) findViewById(R.id.edit_profile_month);
        spin_year = (CustomTextView) findViewById(R.id.edit_profile_year);
        status = (CustomEditText) findViewById(R.id.status);
        edit_profile_email = (CustomEditText) findViewById(R.id.edit_profile_email);
        edit_profile_address = (CustomEditText) findViewById(R.id.edit_profile_address);
        edit_profile_phone = (CustomEditText) findViewById(R.id.edit_profile_phone);
        edit_profile_city = (CustomEditText) findViewById(R.id.edit_profile_city);
        edit_state = (CustomEditText) findViewById(R.id.edit_state);
        edit_profile_country = (CustomEditText) findViewById(R.id.edit_profile_country);
        edit_profile_zip_code = (CustomEditText) findViewById(R.id.edit_profile_zip_code);
        gender_group = (RadioGroup) findViewById(R.id.rb_gender);
        male = (RadioButton) findViewById(R.id.rb_edit_profile_male);
        female = (RadioButton) findViewById(R.id.rb_edit_profile_female);
        profile_done = (CustomButton) findViewById(R.id.edit_profile_done);
        change_pass = (CustomButton) findViewById(R.id.edit_profile_change_pw);
        imageButton = (CircleImageView) findViewById(R.id.imageButton);
        circleImageView = (CircleImageView) findViewById(R.id.circleImageView);
        date_layout = (LinearLayout) findViewById(R.id.date_layout);
        spin_date = (CustomTextView) findViewById(R.id.edit_profile_date);

    }

    public void Init() {

        if (Utility.isConnectingToInternet(mContext)) {
            getprofile();
        } else {
            Utility.Alert(mContext, "please check your netwotk connection");
        }

        date_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                chosedate();
            }
        });
        imageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectImage();
            }
        });
        profile_done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                init();
            }
        });

        findViewById(R.id.edit_profile_change_pw).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(mContext, Change_PasswordActivity.class));
                finish();
                // Utility.showToast(mContext, "coming soon");
            }
        });

    }

    public void selectImage() {
        final CharSequence[] options = {"From Camera", "From Gallery", "Close"};
        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(mContext);
        builder.setTitle("Add your image");
        builder.setItems(options, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (options[item].equals("From Camera")) {
                    Uri outputFileUri = getCaptureImageOutputUri();
                    Intent intent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                    // File f = new File(Environment.getExternalStorageDirectory(), "temp.jpg");
                    intent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri);
                    startActivityForResult(intent, 1);
                } else if (options[item].equals("From Gallery")) {
                    Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    startActivityForResult(intent, 2);
                } else if (options[item].equals("Close")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == 1) {
                Uri imageUri = getPickImageResultUri(data);
                beginCrop(imageUri);
            } else if (requestCode == 2) {

                Uri selectedImage = data.getData();
                beginCrop(selectedImage);
            } else if (requestCode == Crop.REQUEST_CROP) {
                handleCrop(resultCode, data);
            }
        }
    }

    private Uri getCaptureImageOutputUri() {
        Uri outputFileUri = null;
        File getImage = getExternalCacheDir();
        if (getImage != null) {
            outputFileUri = Uri.fromFile(new File(getImage.getPath(), "cropped.jpeg"));
        }
        return outputFileUri;
    }

    public Uri getPickImageResultUri(Intent data) {
        boolean isCamera = true;
        if (data != null && data.getData() != null) {
            String action = data.getAction();
            isCamera = action != null && action.equals(MediaStore.ACTION_IMAGE_CAPTURE);
        }
        return isCamera ? getCaptureImageOutputUri() : data.getData();
    }

    private void beginCrop(Uri source) {
        Uri destination = Uri.fromFile(new File(getCacheDir(), System.currentTimeMillis() + "cropped.jpeg"));
        Crop.of(source, destination).asSquare().start(this);
    }

    private void handleCrop(int resultCode, Intent result) {
        if (resultCode == RESULT_OK) {
            File f = new File(Crop.getOutput(result).getPath());
            String croped_file = "" + Crop.getOutput(result).getPath();
            PicturePath = croped_file;
            BitmapFactory.Options bmOptions = new BitmapFactory.Options();
            Bitmap bitmap = BitmapFactory.decodeFile(f.getAbsolutePath(), bmOptions);
            bitmap = Bitmap.createScaledBitmap(bitmap, 300, 300, true);
            bitmap = Utility.rotateImage(bitmap, f);
            Log.e(TAG, "croped_file ===========> " + croped_file);
            PicturePath = croped_file;
            // PicturePath = Utility.encodeTobase64(bitmap);
            Glide.with(mContext).load(PicturePath).into(circleImageView);
        } else if (resultCode == Crop.RESULT_ERROR) {
            Toast.makeText(this, Crop.getError(result).getMessage(), Toast.LENGTH_SHORT).show();
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        if (mCropImageUri != null && grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

        } else {
            Toast.makeText(this, "Required permissions are not granted", Toast.LENGTH_LONG).show();
        }
    }

    private void chosedate() {

        int mYear;
        int mMonth;
        int mDay;
        final Calendar c = Calendar.getInstance();
        // c.add(Calendar.DATE, 2);
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DATE);
        // Launch Date Picker Dialog
        DatePickerDialog dpd = new DatePickerDialog(mContext,
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {

                        // Display Selected date in textbox
                        ((CustomTextView) findViewById(R.id.edit_profile_date)).setText(Integer.toString(dayOfMonth));
                        ((CustomTextView) findViewById(R.id.edit_profile_month)).setText(Integer.toString(monthOfYear + 1));
                        ((CustomTextView) findViewById(R.id.edit_profile_year)).setText(Integer.toString(year));

                    }
                }, mYear, mMonth, mDay);
        dpd.getDatePicker().setMaxDate(System.currentTimeMillis());
        dpd.show();

    }

    private void getprofile() {
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put(Constants.ACTION, Constants.GETUSERDETAILBYID);
            jsonObject.put("user_id", Utility.getSharedPreferences(mContext, Constants.USERID));
            Utility.ShowLoading(mContext, mContext.getResources().getString(R.string.please_wait));
            responseTask = new ResponseTask(Constant_Urls.SERVER_URL, jsonObject);
            responseTask.setListener(new ResponseListener() {
                @Override
                public void onGetPickSuccess(String result) {
                    Utility.HideDialog();
                    if (result == null) {
                        Utility.showCroutonWarn(getResources().getString(R.string.server_fail), EditProfile_Activity.this);
                    } else {
                        try {
                            JSONObject jobj = new JSONObject(result);
                            if (jobj.getString(Constants.RESULT_KEY).equals("1")) {
                                JSONObject jarray = jobj.getJSONObject("object");


                                ((CustomTextView) findViewById(R.id.username)).setText(jarray.getString("user_name"));
                                edit_profile_user_name.setText(jarray.getString("user_name"));
                                Utility.setSharedPreference(mContext, Constants.USERNAME, jarray.getString("user_name"));
                                edit_profile_first_name.setText(jarray.getString("first_name"));
                                edit_profile_last_name.setText(jarray.getString("last_name"));
                                edit_profile_user_name.setText(jarray.getString("user_name"));
                                edit_profile_email.setText(jarray.getString("email"));
                                edit_profile_address.setText(jarray.getString("address"));
                                edit_profile_phone.setText(jarray.getString("contact_number"));
                                edit_profile_city.setText(jarray.getString("city"));
                                status.setText(jarray.getString("bio"));
                                edit_profile_country.setText(jarray.getString("country"));
                                edit_profile_zip_code.setText(jarray.getString("zipcode"));
                                edit_state.setText(jarray.getString("state"));
                                dobb = jarray.getString("dob");
                                if (!dobb.equals("")) {
                                    String[] separated = dobb.split("-");
                                    spin_year.setText(separated[0].trim());
                                    spin_month.setText(separated[1].trim());
                                    spin_date.setText(separated[2].trim());
                                }
                                if (jarray.getString("gender").equals("male")) {
                                    male.setChecked(true);
                                    female.setChecked(false);
                                } else if (jarray.getString("gender").equals("female")) {
                                    male.setChecked(false);
                                    female.setChecked(true);
                                }
                                if (jarray.getString("image_url").equals("")) {
                                    Glide.with(mContext).load(getResources().getDrawable(R.drawable.user)).into(circleImageView);
                                } else {
                                    Glide.with(mContext).load(jarray.getString("image_url")).into((circleImageView));
                                }

                            } else {
                                Utility.showCroutonWarn(jobj.getString(Constants.SERVER_MSG), EditProfile_Activity.this);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            });
            responseTask.execute();
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    private void init() {

        if (Utility.isConnectingToInternet(mContext)) {
            if (edit_profile_first_name.getText().toString().equals("")) {
                Utility.showToast(mContext, "please enter first name");

            } else if (edit_profile_last_name.getText().toString().equals("")) {
                Utility.showToast(mContext, "please enter last name");

            } else if (edit_profile_user_name.getText().toString().equals("")) {
                Utility.showToast(mContext, "please enter user name");

            } else if (spin_date.getText().toString().equals("")) {
                Utility.showToast(mContext, "please select date");

            } else if (spin_month.getText().toString().equals("")) {
                Utility.showToast(mContext, "please select date");

            } else if (spin_year.getText().toString().equals("")) {
                Utility.showToast(mContext, "please select date");

            } else if (status.getText().toString().equals("")) {
                Utility.showToast(mContext, "please enter status");

            } else if (edit_profile_email.getText().toString().equals("")) {
                Utility.showToast(mContext, "please enter email");

            } else if (edit_profile_address.getText().toString().equals("")) {
                Utility.showToast(mContext, "please enter address");

            } else if (edit_profile_phone.getText().toString().equals("")) {
                Utility.showToast(mContext, "please enter phone number");

            } else if (edit_profile_city.getText().toString().equals("")) {
                Utility.showToast(mContext, "please enter city");

            } else if (edit_state.getText().toString().equals("")) {
                Utility.showToast(mContext, "please enter state");

            } else if (edit_profile_country.getText().toString().equals("")) {
                Utility.showToast(mContext, "please enter country");

            } else if (edit_profile_zip_code.getText().toString().equals("")) {
                Utility.showToast(mContext, "please enter Zip code");

            } else if (gender_group.getCheckedRadioButtonId() <= 0) {
                Utility.showToast(mContext, "please select gender");
            } else {
                female.setError(null);
                dobb = String.valueOf(new StringBuilder().append(spin_year.getText().toString()).append("-").append(spin_month.getText().toString()).append("-").append(spin_date.getText().toString()));
                if (male.isChecked()) {
                    selected_gender = "male";
                } else {
                    selected_gender = "female";
                }
                //selected_gender = String.valueOf(gender_group.getCheckedRadioButtonId());
                new ProfileDoneTask().execute(edit_profile_first_name.getText().toString(), edit_profile_last_name.getText().toString(), edit_profile_user_name.getText().toString(),
                        dobb,
                        status.getText().toString(), edit_profile_email.getText().toString(), edit_profile_address.getText().toString(), edit_profile_phone.getText().toString(),
                        edit_profile_city.getText().toString(), edit_state.getText().toString(), edit_profile_country.getText().toString(),
                        edit_profile_zip_code.getText().toString(), selected_gender);
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public class ProfileDoneTask extends AsyncTask<String, String, String> {
        /*https://infograins.com/INFO01/homesplus/api/api.php?action=edit_profile&user_id=143&
        profile_image=image.png&first_name=sandy&last_name=khan&user_name=test123&dob=1999-08-12&
        about_us=gdfgdf&email=hameed.fogrian@gmail.com&address=Indore,%20Madhya%20Pradesh&contact_number=892348923&city=indore&
        country=India&zipcode=345343&gender=male&device_id=43758734&device_type=hdfjk98435*/
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            Utility.ShowLoading(mContext, mContext.getResources().getString(R.string.please_wait));
        }

        @Override
        protected String doInBackground(String... params) {
            try {
                MultipartUtility multipart = new MultipartUtility(Constant_Urls.SERVER_URL, "UTF-8");
                multipart.addFormField(Constants.ACTION, Constants.EDITPROFILE);
                multipart.addFormField("user_id", Utility.getSharedPreferences(mContext, Constants.USERID));
                if (PicturePath.equals("")) {
                    multipart.addFormField("profile_image", "");
                } else {
                    multipart.addFilePart("profile_image", PicturePath);
                }
                multipart.addFormField("first_name", params[0]);
                multipart.addFormField("last_name", params[1]);
                multipart.addFormField("user_name", params[2]);
                multipart.addFormField("dob", params[3]);
                multipart.addFormField("about_us", params[4]);
                multipart.addFormField("email", params[5]);
                multipart.addFormField("address", params[6]);
                multipart.addFormField("contact_number", params[7]);
                multipart.addFormField("city", params[8]);
                multipart.addFormField("state", params[9]);
                multipart.addFormField("country", params[10]);
                multipart.addFormField("zipcode", params[11]);
                multipart.addFormField("gender", params[12]);
                multipart.addFormField("device_id", gcmid);
                multipart.addFormField("device_type", "android");

                return multipart.finish();

            } catch (Exception e) {
                e.printStackTrace();
                return "Server Not Responding !";
            }

        }

        @Override
        protected void onPostExecute(String result) {
            Utility.HideDialog();
            System.out.println("Server result ====> " + result);
            if (result == null) {
                Utility.ShowToastMessage(mContext, "Please check your Internet connection");
            } else {
                try {
                    JSONObject jobj = new JSONObject(result);
                    if (jobj.get(Constants.RESULT_KEY).equals("1")) {
                        Utility.showToast(mContext, "your profile updated successfully");

                        if (jobj.getJSONObject("object").getString("user_name").equals("") && jobj.getJSONObject("object").getString("image_url").equals("")) {
                            Utility.setSharedPreference(mContext, Constants.USERNAME, "");
                            Utility.setSharedPreference(mContext, Constants.PROFILE_IMAGE, "");
                        } else {
                            Utility.setSharedPreference(mContext, Constants.USERNAME, jobj.getJSONObject("object").getString("user_name"));
                            Utility.setSharedPreference(mContext, Constants.PROFILE_IMAGE, jobj.getJSONObject("object").getString("image_url"));
                            finish();
                            // ((MainActivity)mContext).callSelectedFragment(Constants.FRAGMENT_HOME,"Home");
                        }
                       // ((MainActivity) mContext).GetDrawerImage();
                        getprofile();
                    } else {
                        Utility.showCroutonWarn(jobj.getString(Constants.SERVER_MSG), EditProfile_Activity.this);
                    }
                } catch (JSONException e) {
                    Utility.showCroutonWarn(getResources().getString(R.string.server_fail), EditProfile_Activity.this);
                }
            }
            super.onPostExecute(result);
        }

    }
}
