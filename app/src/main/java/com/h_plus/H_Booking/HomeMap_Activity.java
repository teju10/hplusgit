package com.h_plus.H_Booking;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.BottomSheetBehavior;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.h_plus.H_Booking.TripIT.activity.SelectedHomeDetail;
import com.h_plus.H_Booking.TripIT.adapter.GooglePlacesAutocompleteAdapter;
import com.h_plus.H_Booking.TripIT.adapter.HomeBooking_Adapter;
import com.h_plus.R;
import com.h_plus.ServerInteraction.ResponseListener;
import com.h_plus.ServerInteraction.ResponseTask;
import com.h_plus.customwidget.CustomButton;
import com.h_plus.customwidget.CustomTextView;
import com.h_plus.usertabs.utility.Constant_Urls;
import com.h_plus.usertabs.utility.Constants;
import com.h_plus.usertabs.utility.GPSTracker;
import com.h_plus.usertabs.utility.Utility;

import org.florescu.android.rangeseekbar.RangeSeekBar;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;


public class HomeMap_Activity extends AppCompatActivity implements OnMapReadyCallback, View.OnClickListener, RangeSeekBar.OnRangeSeekBarChangeListener<Integer>, AdapterView.OnItemClickListener, RadioGroup.OnCheckedChangeListener, TextWatcher {
    public static final String TAG = "HomeMap_Activity";
    Context mContext;
    int complete_msg=0, positiong = 0,savedcheck=0;
    public static String LAT="lat",LONG="long";
    HomeBooking_Adapter re_adapter;
    CustomButton filter_btn, btn_filter_apply,btn_saved_homes;
    Spinner spin_prop_for, spin_hometyp, spin_sort_by;
    String   s_prop_for;
    RangeSeekBar pricebar;
    AutoCompleteTextView auto_location;
    DrawerLayout drawer;
    RadioGroup Rg_radiogrp;
    String str_location = "", min, max, radio_value = "", s_hometyp, s_sortby;
    RadioButton rbbtn_any, rbbtn_one, rbbtn_two, rbbtn_three, rbbtn_four, rbbtn_five, rbbtn_six, rbbtn_seven;
    ResponseTask responseTask;
    ViewPager V_pager;
    ImageView img_frwrd,img_backwrd;
    ArrayList<JSONObject> jObjectArrayList;
    ArrayList<String>lat =new ArrayList<>();
    ArrayList<String>longg =new ArrayList<>();
    TextView text_count;
    private GoogleMap mMap;
    private LinearLayout linearLayout;
    private BottomSheetBehavior bottomSheetBehavior;
     ArrayList<HashMap<String,String>> markerlist = new ArrayList<>();
    private Toolbar mToolbar;
    GPSTracker gpsTracker;
    Double lati,longii;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_map_);
        mContext = this;
        mToolbar = (Toolbar) findViewById(R.id.h_toolbar);
        setSupportActionBar(mToolbar);
        ((CustomTextView) findViewById(R.id.toolbar_title)).setText("Home Booking");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        gpsTracker = new GPSTracker(mContext);
        lati = gpsTracker.getLatitude();
        longii = gpsTracker.getLongitude();
        bind();
    }

    private void Addmarker() {
        //mMap.clear();
if(mMap!=null) {
    mMap.clear();
    for (int i = 0; i < markerlist.size(); i++) {
// create marker
        MarkerOptions marker = new MarkerOptions().position(new LatLng(Double.valueOf(markerlist.get(i).get(LAT)), Double.valueOf(markerlist.get(i).get(LONG)))).title("");
// adding marker
        mMap.addMarker(marker);
        if (i == (markerlist.size() - 1)) {
            mMap.moveCamera(CameraUpdateFactory.newLatLng(new LatLng(Double.valueOf(markerlist.get(i).get(LAT)), Double.valueOf(markerlist.get(i).get(LONG)))));
            mMap.animateCamera(CameraUpdateFactory.zoomTo(6.0f));

        }
    }
}
      /*  BitmapDescriptor bitmapMarker;
        bitmapMarker = BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED);*/
      /*  Log.i(TAG, "RED");
        LatLng sydney = new LatLng(latitude, longitude);
        System.out.println("latlongg====="+sydney);
        mMap.addMarker(new MarkerOptions().position(sydney));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));
       // mMap.moveCamera(CameraUpdateFactory.newLatLngBounds(bounds, 70));*/
    }

    private void bind() {
        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        linearLayout = (LinearLayout) findViewById(R.id.nav_view);
        jObjectArrayList = new ArrayList<>();
        bottomSheetBehavior = BottomSheetBehavior.from(findViewById(R.id.bottomSheetLayout));
        V_pager = (ViewPager) findViewById(R.id.viewpager);
        re_adapter = new HomeBooking_Adapter(jObjectArrayList, R.layout.filter_property_listing_item, mContext);
        V_pager.setAdapter(re_adapter);
        img_frwrd =(ImageView)findViewById(R.id.img_frwrd);
        img_frwrd.setOnClickListener(this);
        img_backwrd =(ImageView)findViewById(R.id.img_backwrd);
        img_backwrd.setOnClickListener(this);
        text_count=(TextView)findViewById(R.id.text_count);
        bottomSheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);
        Rg_radiogrp = (RadioGroup) linearLayout.findViewById(R.id.Rg_radiogrp);
        rbbtn_any = (RadioButton) linearLayout.findViewById(R.id.rbbtn_any);
        rbbtn_one = (RadioButton) linearLayout.findViewById(R.id.rbbtn_one);
        rbbtn_two = (RadioButton) linearLayout.findViewById(R.id.rbbtn_two);
        rbbtn_three = (RadioButton) linearLayout.findViewById(R.id.rbbtn_three);
        rbbtn_four = (RadioButton) linearLayout.findViewById(R.id.rbbtn_four);
        rbbtn_five = (RadioButton) linearLayout.findViewById(R.id.rbbtn_five);
        rbbtn_six = (RadioButton) linearLayout.findViewById(R.id.rbbtn_six);
        rbbtn_seven = (RadioButton) linearLayout.findViewById(R.id.rbbtn_seven);
        Rg_radiogrp.setOnCheckedChangeListener(this);
        filter_btn = (CustomButton) findViewById(R.id.filter_btn);
        auto_location = (AutoCompleteTextView) linearLayout.findViewById(R.id.auto_location);
        auto_location.setAdapter(new GooglePlacesAutocompleteAdapter(this, R.layout.list_item));
        auto_location.setOnItemClickListener(this);
        auto_location.addTextChangedListener(this);
        spin_prop_for = (Spinner) linearLayout.findViewById(R.id.spin_prop_for);
        spin_hometyp = (Spinner) linearLayout.findViewById(R.id.spin_hometyp);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(mContext, android.R.layout.simple_spinner_dropdown_item, getResources().getStringArray(R.array.property_usee));
        spin_prop_for.setAdapter(adapter);
        spin_sort_by = (Spinner) linearLayout.findViewById(R.id.spin_sort_by);
        ArrayAdapter<String> adapter_sort = new ArrayAdapter<String>(mContext, android.R.layout.simple_spinner_dropdown_item, getResources().getStringArray(R.array.sort_by));
        spin_sort_by.setAdapter(adapter_sort);
        ArrayAdapter<String> adapter1 = new ArrayAdapter<String>(mContext, android.R.layout.simple_spinner_dropdown_item, getResources().getStringArray(R.array.property_type));
        spin_hometyp.setAdapter(adapter1);
        pricebar = (RangeSeekBar) linearLayout.findViewById(R.id.pricebar);
        pricebar.setRangeValues(0, 10000);
        pricebar.setTextAboveThumbsColor(getResources().getColor(R.color.colorPrimary));
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        filter_btn.setOnClickListener(this);
        pricebar.setOnRangeSeekBarChangeListener(this);
        btn_filter_apply = (CustomButton) linearLayout.findViewById(R.id.btn_filter_apply);
        btn_filter_apply.setOnClickListener(this);
        btn_saved_homes=(CustomButton)linearLayout.findViewById(R.id.btn_saved_homes);
        btn_saved_homes.setOnClickListener(this);
        V_pager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                    positiong=position;
                text_count.setText((positiong+1)+"/"+complete_msg);

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.END)) {
            drawer.closeDrawer(GravityCompat.END);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        // Add a marker in Sydney and move the camera
        LatLng sydney = new LatLng(lati, longii);
        mMap.addMarker(new MarkerOptions().position(sydney).title("User current location"));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));
        mMap.getUiSettings().setZoomControlsEnabled(true);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.filter_btn:
                drawer.openDrawer(linearLayout);
                break;
            case R.id.img_frwrd:

                if (positiong < (complete_msg-1)) {
                    positiong = positiong + 1;
                    V_pager.setCurrentItem(positiong);
                    //text_count.setText((positiong+1)+"/"+complete_msg);
                }
                break;
            case R.id.img_backwrd:  System.out.println("clickckckkck");
                if (positiong > 0) {
                    positiong = positiong - 1;
                    V_pager.setCurrentItem(positiong);
                    //text_count.setText((positiong+1)+"/"+complete_msg);
                }
                break;

            case R.id.btn_filter_apply:
                if (Utility.isConnectingToInternet(mContext)) {
                    if (spin_prop_for.getSelectedItemPosition() == 0) {
                        Utility.showToast(mContext, "Select Property Use");
                    } else if (str_location.equals("")) {
                        //auto_location.setError("please enter valid Address");
                        Utility.showToast(mContext, "please enter valid Address");
                       // auto_location.requestFocus();
                    } else {
                        Get_HomeData();
                        drawer.closeDrawer(linearLayout);
                       // bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);

                    }
                } else {
                    Utility.showToast(mContext, "Please Check Your Internet Connection");
                }
               // Toast.makeText(mContext, "button clickedd   ==", Toast.LENGTH_SHORT).show();
                break;
            case R.id.btn_saved_homes:
                savedcheck=1;
                Get_HomeData();
                drawer.closeDrawer(linearLayout);
                break;
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menu) {
        switch (menu.getItemId()) {

            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(menu);
        }
    }

    private void Get_HomeData() {

        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put(Constants.ACTION, Constants.LISTSALERENTVACATION);
            jsonObject.put("user_id", Utility.getSharedPreferences(mContext, Constants.USERID));
            //jsonObject.put("user_id", "244");
            if (savedcheck==1){
                savedcheck=0;
                jsonObject.put("type","saved_homes");
            }else {
                // jsonObject.put("property_type",spin_prop_for.getSelectedItem().toString());
                jsonObject.put("min_price", min);
                jsonObject.put("max_price", max);
                jsonObject.put("beadroom", radio_value);
                jsonObject.put("country", str_location);
                if (spin_prop_for.getSelectedItemPosition() == 0) {
                    s_prop_for = "";
                } else {
                    s_prop_for= spin_prop_for.getSelectedItem().toString();
                    jsonObject.put("homes_type", s_prop_for);
                }
                if (spin_hometyp.getSelectedItemPosition() == 0) {
                    s_hometyp = "";
                } else {
                    s_hometyp = spin_hometyp.getSelectedItem().toString();
                    jsonObject.put("homes_type", s_hometyp);
                }
                    if (spin_sort_by.getSelectedItemPosition() == 0) {
                        s_sortby = "";
                    } else {
                        s_sortby = spin_sort_by.getSelectedItem().toString();
                        jsonObject.put("type", s_sortby);
                    }

            }



            Utility.ShowLoading(mContext, mContext.getResources().getString(R.string.please_wait));
            responseTask = new ResponseTask(Constant_Urls.SERVER_URL, jsonObject);
            responseTask.setListener(new ResponseListener() {
                @Override
                public void onGetPickSuccess(String result) {
                    Utility.HideDialog();
                    if (result == null) {
                        Utility.ShowToastMessage(mContext, "sever not responding");
                    } else {
                        try {
                            JSONObject jsonObject = new JSONObject(result);
                            mMap.clear();
                            if (jsonObject.getString(Constants.RESULT_KEY).equals("0")){
                                /*final View coordinatorLayoutView = findViewById(R.id.snackbarPosition);
                                Snackbar.make(coordinatorLayoutView,"No record Found", Snackbar.LENGTH_LONG)
                                        .show();*/
                                Toast.makeText(mContext,"No Record Found",Toast.LENGTH_LONG).show();

                            }
                            else if (jsonObject.getString(Constants.RESULT_KEY).equals("1")) {
                                    JSONArray jsonArray = jsonObject.getJSONArray("list");
                                    bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                                    Log.e(TAG, "result ===============> " + jsonArray.toString());
                                    jObjectArrayList.clear();
                                markerlist.clear();
                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        jObjectArrayList.add(jsonArray.getJSONObject(i));
                                         //ll= new LatLng(jsonArray.getJSONObject(i).getLong("lat"),jsonArray.getJSONObject(i).getLong("long"));
                                       HashMap<String,String>hm=new HashMap<String, String>();
                                        try {
                                            if(jsonArray.getJSONObject(i).getString(LAT).equals("")){
                                                hm.put(LAT,"0");
                                            }else {
                                                hm.put(LAT,jsonArray.getJSONObject(i).getString(LAT));
                                            }if(jsonArray.getJSONObject(i).getString(LONG).equals("")){
                                                hm.put(LONG,"0");

                                            }else {
                                                hm.put(LONG,jsonArray.getJSONObject(i).getString(LONG));
                                            }
                                            markerlist.add(hm);
                                            System.out.println("dataa====="+markerlist);
                                        }catch (NumberFormatException e){
                                            Utility.showToast(mContext,"no latlong found");
                                        }

                                    }
                                    complete_msg = jObjectArrayList.size();
                                     positiong=0;
                                    text_count.setText(positiong+1+"/"+complete_msg);
                                if (markerlist.size()>0){
                                    Addmarker();
                                }

                            } else {
                                Utility.showCroutonWarn(jsonObject.getString(Constants.SERVER_MSG), HomeMap_Activity.this);
                            }
                            re_adapter.notifyDataSetChanged();
                            //mhpadapter.notifyDataSetChanged();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            });
        } catch (JSONException e) {
            e.printStackTrace();
        }
        responseTask.execute();
    }


    @Override
    public void onRangeSeekBarValuesChanged(RangeSeekBar bar, Integer minValue, Integer maxValue) {

        bar.setNotifyWhileDragging(true);
        min = minValue.toString();
        max = maxValue.toString();
       // Toast.makeText(mContext, "min" +min + "max " + max, Toast.LENGTH_SHORT).show();
        System.out.println(min + "  " + max);
    }

    @Override
    public void onItemClick(AdapterView adapterView, View view, int position, long id) {
        str_location = (String) adapterView.getItemAtPosition(position);
        // System.out.println("final========"+str_location);
       // Toast.makeText(this, str_location, Toast.LENGTH_SHORT).show();

    }

    public void clearRadioChecked() {
        rbbtn_any.setTextColor(Color.BLACK);
        rbbtn_one.setTextColor(Color.BLACK);
        rbbtn_two.setTextColor(Color.BLACK);
        rbbtn_three.setTextColor(Color.BLACK);
        rbbtn_four.setTextColor(Color.BLACK);
        rbbtn_five.setTextColor(Color.BLACK);
        rbbtn_six.setTextColor(Color.BLACK);
        rbbtn_seven.setTextColor(Color.BLACK);
        rbbtn_any.setBackgroundColor(Color.WHITE);
        rbbtn_one.setBackgroundColor(Color.WHITE);
        rbbtn_two.setBackgroundColor(Color.WHITE);
        rbbtn_three.setBackgroundColor(Color.WHITE);
        rbbtn_four.setBackgroundColor(Color.WHITE);
        rbbtn_five.setBackgroundColor(Color.WHITE);
        rbbtn_six.setBackgroundColor(Color.WHITE);
        rbbtn_seven.setBackgroundColor(Color.WHITE);
    }

    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId) {
        clearRadioChecked();
        if (checkedId == R.id.rbbtn_any) {
            rbbtn_any.setTextColor(Color.WHITE);
            rbbtn_any.setBackgroundColor(Color.parseColor("#e98d43"));
            radio_value = "any";
        }
        if (checkedId == R.id.rbbtn_one) {
            rbbtn_one.setTextColor(Color.WHITE);
            rbbtn_one.setBackgroundColor(Color.parseColor("#e98d43"));
            radio_value = "1";
        }
        if (checkedId == R.id.rbbtn_two) {
            rbbtn_two.setTextColor(Color.WHITE);
            rbbtn_two.setBackgroundColor(Color.parseColor("#e98d43"));
            radio_value = "2";
        }
        if (checkedId == R.id.rbbtn_three) {
            rbbtn_three.setTextColor(Color.WHITE);
            rbbtn_three.setBackgroundColor(Color.parseColor("#e98d43"));
            radio_value = "3";
        }
        if (checkedId == R.id.rbbtn_four) {
            rbbtn_four.setTextColor(Color.WHITE);
            rbbtn_four.setBackgroundColor(Color.parseColor("#e98d43"));
            radio_value = "4";
        }
        if (checkedId == R.id.rbbtn_five) {
            rbbtn_five.setTextColor(Color.WHITE);
            rbbtn_five.setBackgroundColor(Color.parseColor("#e98d43"));
            radio_value = "5";
        }
        if (checkedId == R.id.rbbtn_six) {
            rbbtn_six.setTextColor(Color.WHITE);
            rbbtn_six.setBackgroundColor(Color.parseColor("#e98d43"));
            radio_value = "6";
        }
        if (checkedId == R.id.rbbtn_seven) {
            rbbtn_seven.setTextColor(Color.WHITE);
            rbbtn_seven.setBackgroundColor(Color.parseColor("#e98d43"));
            radio_value = "7";
        }

    }

    public void SelectedPropDetail(int selected_position) {
              try {
                  String sel_id = jObjectArrayList.get(selected_position).getString("id");
                  GetpropDetail(sel_id);
              }catch (Exception e){
                  e.printStackTrace();
              }
    }

    private void GetpropDetail(final String sel_id) {
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put(Constants.ACTION, Constants.GETHOMEDETAILSBYDIGID);
            jsonObject.put("user_id", Utility.getSharedPreferences(mContext, Constants.USERID));
            //jsonObject.put("user_id","244");
            jsonObject.put("digid", sel_id);
            Utility.ShowLoading(mContext, mContext.getResources().getString(R.string.please_wait));
            responseTask = new ResponseTask(Constant_Urls.SERVER_URL, jsonObject);
            responseTask.setListener(new ResponseListener() {
                @Override
                public void onGetPickSuccess(String result) {
                    Utility.HideDialog();
                    if (result == null) {
                        Utility.showCroutonWarn(getResources().getString(R.string.server_fail), HomeMap_Activity.this);
                    }
                    else {
                        try {
                            JSONObject jobj = new JSONObject(result);
                            if (jobj.getString(Constants.RESULT_KEY).equals("1")) {
                                JSONObject jasobj = jobj.getJSONObject("list");
                                Bundle extraa = new Bundle();
                                //extraa.putString(Constants.ID, sel_id);
                                extraa.putString(Constants.OBJECT, jasobj.toString());
                                Intent in = new Intent(mContext, SelectedHomeDetail.class);
                                in.putExtras(extraa);
                                System.out.println("GET value" + extraa);
                                startActivity(in);
                               // finish();

                            } else {
                                Utility.showCroutonWarn(jobj.getString(Constants.SERVER_MSG),HomeMap_Activity.this);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            });
        }catch (Exception e){
            e.printStackTrace();
        }
        responseTask.execute();
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        str_location="";
    }

    @Override
    public void afterTextChanged(Editable s) {

    }
}
