package com.h_plus.H_Booking.TripIT.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.ListView;

import com.h_plus.H_Booking.TripIT.adapter.CArLIstingAdapter;
import com.h_plus.R;
import com.h_plus.ServerInteraction.ResponseListener;
import com.h_plus.ServerInteraction.ResponseTask;
import com.h_plus.customwidget.CustomTextView;
import com.h_plus.usertabs.utility.Constant_Urls;
import com.h_plus.usertabs.utility.Constants;
import com.h_plus.usertabs.utility.Utility;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Infograins on 05-Jan-17.
 */
public class CarListing extends AppCompatActivity{
    ResponseTask responseTask;
    Context mContext;
    ArrayList<JSONObject> jObjectArrayList;
    ListView list_data;
    private Toolbar mToolbar;
    CArLIstingAdapter mhpadapter;
    String pick_up_loc,drop_off_loc,pick_up_date,drop_off_date,pick_up_time,drop_off_time;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.listview_with_toolbar);
        mContext=this;
        mToolbar = (Toolbar) findViewById(R.id.h_toolbar);
        setSupportActionBar(mToolbar);
        ((CustomTextView) findViewById(R.id.toolbar_title)).setText("Car List");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        bind();
        PreviousData();
        //init();
        GetCarList();
    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case android.R.id.home:
                onBackPressed();
              /*  Intent in=new Intent(mContext,MainActivity.class);
                startActivity(in);*//*
                finish();*/
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
    private void bind() {
        jObjectArrayList = new ArrayList<>();
        list_data = (ListView) findViewById(R.id.listView);
        mhpadapter = new CArLIstingAdapter(mContext, R.layout.car_listing_item, jObjectArrayList);
        list_data.setAdapter(mhpadapter);
    }
    private void PreviousData() {
        Bundle extraa=getIntent().getExtras();
        pick_up_loc=extraa.getString(Constants.PICKUP);
        drop_off_loc=extraa.getString(Constants.DROPOFF);
        pick_up_date=extraa.getString(Constants.PICKUP_DATE);
        drop_off_date=extraa.getString(Constants.DROPOFF_DATE);
        pick_up_time=extraa.getString(Constants.PICKUP_TIME);
        drop_off_time=extraa.getString(Constants.DROPOFF_TIME);
    }
    private void GetCarList() {
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put(Constants.ACTION, Constants.CARSEARCHING);
            jsonObject.put("picking_up",pick_up_loc);
            jsonObject.put("dropping_off",drop_off_loc);
            jsonObject.put("pick_up_date",pick_up_date);
            jsonObject.put("drop_of_date",drop_off_date);
            jsonObject.put("picking_up_time",pick_up_time);
            jsonObject.put("dropping_up_time",drop_off_time);

            Utility.ShowLoading(mContext, mContext.getResources().getString(R.string.please_wait));
            responseTask = new ResponseTask(Constant_Urls.SERVER_URL, jsonObject);
            responseTask.setListener(new ResponseListener() {
                @Override
                public void onGetPickSuccess(String result) {
                    Utility.HideDialog();
                    if (result == null) {
                        Utility.ShowToastMessage(mContext, "sever not responding");
                    } else{
                        try {
                            JSONObject jsonObject = new JSONObject(result);
                            if (jsonObject.getString(Constants.RESULT_KEY).equals("1")){
                                JSONArray jsonArray = jsonObject.getJSONArray("object");
                                for (int i = 0; i < jsonArray.length(); i++){
                                    jObjectArrayList.add(jsonArray.getJSONObject(i));
                                }
                                mhpadapter.notifyDataSetChanged();
                            }
                            else {
                                Utility.showToast(mContext,jsonObject.getString(Constants.SERVER_MSG));
                            }
                        }catch (Exception e){
                            e.printStackTrace();
                        }

                    }
                }
            });
        }catch (Exception e){
            e.printStackTrace();
        }
        responseTask.execute();
    }


    public void SHowSelectedCar(int selected_pos) {
        try {
            String sel_id=jObjectArrayList.get(selected_pos).getString("id");
            GetCarDetail(sel_id);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void GetCarDetail(String sel_id) {
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put(Constants.ACTION, Constants.GETCARDETAILBYID);
            //jsonObject.put("user_id", Utility.getSharedPreferences(mContext, Constants.USERID));
            //jsonObject.put("user_id","244");
            jsonObject.put("car_id", sel_id);
            Utility.ShowLoading(mContext, mContext.getResources().getString(R.string.please_wait));
            responseTask = new ResponseTask(Constant_Urls.SERVER_URL, jsonObject);
            responseTask.setListener(new ResponseListener() {
                @Override
                public void onGetPickSuccess(String result) {
                    Utility.HideDialog();
                    if (result == null) {
                        Utility.showCroutonWarn(getResources().getString(R.string.server_fail), CarListing.this);
                    }
                    else {
                        try {
                            JSONObject jobj = new JSONObject(result);
                            if (jobj.getString(Constants.RESULT_KEY).equals("1")) {
                                JSONObject jasobj = jobj.getJSONObject("object");
                                Bundle extraa = new Bundle();
                                extraa.putString(Constants.OBJECT, jasobj.toString());
                                Intent in = new Intent(mContext, SelectedCarDetail.class);
                                in.putExtras(extraa);
                                System.out.println("GET value" + extraa);
                                startActivity(in);
                                // finish();

                            } else {
                                Utility.showCroutonWarn(jobj.getString(Constants.SERVER_MSG),CarListing.this);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            });
        }catch (Exception e){
            e.printStackTrace();
        }
        responseTask.execute();
    }
}
