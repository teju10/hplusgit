package com.h_plus.H_Booking.TripIT.activity;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;

import com.h_plus.H_Booking.fragment.Fragment_FLightEmail;
import com.h_plus.H_Booking.fragment.Fragment_FlightPayment;
import com.h_plus.H_Booking.fragment.Fragment_FlightReview;
import com.h_plus.R;
import com.h_plus.customwidget.CustomTextView;


/**
 * Created by Infograins on 09-Jan-17.
 */
public class SelectedFlightDetail extends AppCompatActivity implements View.OnClickListener {
    Context mContext;
    private Toolbar mToolbar;
    CustomTextView btn_selection_review_tab,btn_email_tab,btn_payment_tab;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.selectedflightdetail);
        mContext = this;
        mToolbar = (Toolbar) findViewById(R.id.h_toolbar);
        setSupportActionBar(mToolbar);
        ((CustomTextView) findViewById(R.id.toolbar_title)).setText("Flight Booking");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        bind();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case android.R.id.home:
                onBackPressed();
              /*  Intent in=new Intent(mContext,MainActivity.class);
                startActivity(in);*//*
                finish();*/
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void bind() {
        btn_selection_review_tab=(CustomTextView)findViewById(R.id.btn_selection_review_tab);
        btn_email_tab=(CustomTextView)findViewById(R.id.btn_email_tab);
        btn_payment_tab=(CustomTextView)findViewById(R.id.btn_payment_tab);
        /*btn_selection_review_tab.setOnClickListener(this);
        btn_email_tab.setOnClickListener(this);
        btn_payment_tab.setOnClickListener(this);*/
        SetTabs(0);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_selection_review_tab:
                SetTabs(0);
                break;
            case R.id.btn_email_tab:
                SetTabs(1);
                break;
            case R.id.btn_payment_tab:
                SetTabs(2);
                break;
        }

    }
    public void SetTabs(int selected_tab){
        Fragment fragment = null;
        switch (selected_tab){
            case 0:
                Bundle extraa=getIntent().getExtras();
                btn_selection_review_tab.setBackgroundColor(Color.WHITE);
                btn_selection_review_tab.setTextColor(getResources().getColor(R.color.colorPrimary));
                btn_email_tab.setBackgroundColor(Color.BLACK);
                btn_email_tab.setTextColor(Color.WHITE);
                btn_payment_tab.setBackgroundColor(Color.BLACK);
                btn_payment_tab.setTextColor(Color.WHITE);
                fragment= new Fragment_FlightReview();
                fragment.setArguments(extraa);
                break;
            case 1:
                btn_email_tab.setBackgroundColor(Color.WHITE);
                btn_email_tab.setTextColor(getResources().getColor(R.color.colorPrimary));
                btn_selection_review_tab.setBackgroundColor(Color.BLACK);
                btn_selection_review_tab.setTextColor(Color.WHITE);
                btn_payment_tab.setBackgroundColor(Color.BLACK);
                btn_payment_tab.setTextColor(Color.WHITE);
                fragment=new Fragment_FLightEmail();
                break;
            case 2:
                btn_payment_tab.setBackgroundColor(Color.WHITE);
                btn_payment_tab.setTextColor(getResources().getColor(R.color.colorPrimary));
                btn_selection_review_tab.setBackgroundColor(Color.BLACK);
                btn_selection_review_tab.setTextColor(Color.WHITE);
                btn_email_tab.setBackgroundColor(Color.BLACK);
                btn_email_tab.setTextColor(Color.WHITE);
                fragment= new Fragment_FlightPayment();
                break;
        }
        if (fragment != null) {
            FragmentManager fragmentManager = getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.container, fragment);
            fragmentTransaction.commit();
            // set the toolbar title
            // getSupportActionBar().setTitle(title);
        }
    }
}
