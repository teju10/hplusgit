package com.h_plus.H_Booking.TripIT.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import com.h_plus.H_Booking.TripIT.activity.FlightListing;
import com.h_plus.R;
import com.h_plus.customwidget.CustomButton;
import com.h_plus.customwidget.CustomTextView;

import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Infograins on 04-Jan-17.
 */
public class FlightListing_Adapter extends ArrayAdapter<JSONObject>{
    Context mContext;
    LayoutInflater layoutInflater;
    int res;
    ArrayList<JSONObject> jsonObjects;
    public FlightListing_Adapter(Context mContext, int flight_listing_item, ArrayList<JSONObject> jObjectArrayList) {
        super(mContext,flight_listing_item,jObjectArrayList);
        this.mContext = mContext;
        this.res = flight_listing_item;
        this.jsonObjects = jObjectArrayList;
        layoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }
    public View getView(int position, View convertView, ViewGroup parent){
       ViewHolder viewHolder;
        if (convertView == null) {
            convertView = layoutInflater.inflate(res, null);
            viewHolder = new ViewHolder(convertView);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        try {
            // viewHolder.Show_Name.setText(jobject.getString("title"));
           /* Glide.with(mContext).load(jsonObjects.get(position).getString("image")).diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(viewHolder.hotel_img);*/
            viewHolder.flight_name.setText(jsonObjects.get(position).getString("flight_name"));
            viewHolder.flight_time.setText(jsonObjects.get(position).getString("dept").substring(0,5)+"-"+jsonObjects.get(position).getString("arrive").substring(0,5));
           // viewHolder.flight_total_via.setText(jsonObjects.get(position).getString("dept"));
            viewHolder.flight_total_via.setText(jsonObjects.get(position).getString("duration")+" | via"+jsonObjects.get(position).getString("cityCode"));
            viewHolder.flight_price.setText("$ "+jsonObjects.get(position).getString("price"));
            viewHolder.btn_book_flight.setOnClickListener(new MyClick(position));

        } catch (Exception e) {
            e.printStackTrace();
        }
        return convertView;
    }

    private class ViewHolder {
        CustomTextView flight_name,flight_time,flight_total_via,flight_price;
       // LinearLayout layout;
        CustomButton btn_book_flight;
        public ViewHolder(View convertView) {
         //hotel_img = (ImageView) convertView.findViewById(R.id.hotel_img);
            flight_name=(CustomTextView)convertView.findViewById(R.id.flight_name);
            flight_time=(CustomTextView)convertView.findViewById(R.id.flight_time);
            flight_total_via=(CustomTextView)convertView.findViewById(R.id.flight_total_via);
            flight_price=(CustomTextView)convertView.findViewById(R.id.flight_price);
            btn_book_flight=(CustomButton)convertView.findViewById(R.id.btn_book_flight);
            //layout=(LinearLayout)convertView.findViewById(R.id.layout);

        }
    }

    private class MyClick implements View.OnClickListener {
        int selected_pos;
        public MyClick(int position) {
            this.selected_pos=position;
        }

        @Override
        public void onClick(View v) {
            ((FlightListing)mContext).SelectedFlight(selected_pos);
        }
    }
}
