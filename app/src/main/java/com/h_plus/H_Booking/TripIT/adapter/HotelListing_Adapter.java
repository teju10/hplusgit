package com.h_plus.H_Booking.TripIT.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.h_plus.H_Booking.TripIT.activity.HotelListing;
import com.h_plus.R;
import com.h_plus.customwidget.CustomTextView;

import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Infograins on 03-Jan-17.
 */
public class HotelListing_Adapter extends ArrayAdapter<JSONObject> {
    Context mContext;
    LayoutInflater layoutInflater;
    int res;
    ArrayList<JSONObject> jsonObjects;

    public HotelListing_Adapter(Context mContext, int hotel_listing_item, ArrayList<JSONObject> jObjectArrayList) {
        super(mContext,hotel_listing_item,jObjectArrayList);
        this.mContext = mContext;
        this.res = hotel_listing_item;
        this.jsonObjects = jObjectArrayList;
        layoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }
    public View getView(int position, View convertView, ViewGroup parent){
        ViewHolder viewHolder;
        if (convertView == null) {
            convertView = layoutInflater.inflate(res, null);
            viewHolder = new ViewHolder(convertView);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        try {
            // viewHolder.Show_Name.setText(jobject.getString("title"));
            Glide.with(mContext).load(jsonObjects.get(position).getString("image")).diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(viewHolder.hotel_img);
            viewHolder.hotel_name.setText(jsonObjects.get(position).getString("name"));
            viewHolder.review.setText(jsonObjects.get(position).getString("review"));
            viewHolder.price.setText(jsonObjects.get(position).getString("price"));
           viewHolder.layout.setOnClickListener(new MyClick(position));
            try {
                Ratingg(viewHolder,Float.parseFloat(jsonObjects.get(position).getString("review_star")));
            } catch (NumberFormatException e) {
                Ratingg(viewHolder, (float) 0);
                e.printStackTrace();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return convertView;
    }

    private void Ratingg(ViewHolder viewHolder, float rat) {
        if (rat >= 0 && rat < 0.5) {
            viewHolder.star1.setImageResource(R.drawable.star_blank);
            viewHolder.star2.setImageResource(R.drawable.star_blank);
            viewHolder.star3.setImageResource(R.drawable.star_blank);
            viewHolder.star4.setImageResource(R.drawable.star_blank);
            viewHolder.star5.setImageResource(R.drawable.star_blank);
        } else if (rat >= 0.5 && rat < 1) {
            viewHolder.star1.setImageResource(R.drawable.star_half);
            viewHolder.star2.setImageResource(R.drawable.star_blank);
            viewHolder.star3.setImageResource(R.drawable.star_blank);
            viewHolder.star4.setImageResource(R.drawable.star_blank);
            viewHolder.star5.setImageResource(R.drawable.star_blank);
        } else if (rat >= 1 && rat < 1.5) {
            viewHolder.star1.setImageResource(R.drawable.orange_star);
            viewHolder.star2.setImageResource(R.drawable.star_blank);
            viewHolder.star3.setImageResource(R.drawable.star_blank);
            viewHolder.star4.setImageResource(R.drawable.star_blank);
            viewHolder.star5.setImageResource(R.drawable.star_blank);
        } else if (rat >= 1.5 && rat < 2) {
            viewHolder.star1.setImageResource(R.drawable.orange_star);
            viewHolder.star2.setImageResource(R.drawable.star_half);
            viewHolder.star3.setImageResource(R.drawable.star_blank);
            viewHolder.star4.setImageResource(R.drawable.star_blank);
            viewHolder.star5.setImageResource(R.drawable.star_blank);
        } else if (rat >= 2 && rat < 2.5) {
            viewHolder.star1.setImageResource(R.drawable.orange_star);
            viewHolder.star2.setImageResource(R.drawable.orange_star);
            viewHolder.star3.setImageResource(R.drawable.star_blank);
            viewHolder.star4.setImageResource(R.drawable.star_blank);
            viewHolder.star5.setImageResource(R.drawable.star_blank);
        } else if (rat >= 2.5 && rat < 3) {
            viewHolder.star1.setImageResource(R.drawable.orange_star);
            viewHolder.star2.setImageResource(R.drawable.orange_star);
            viewHolder.star3.setImageResource(R.drawable.star_half);
            viewHolder.star4.setImageResource(R.drawable.star_blank);
            viewHolder.star5.setImageResource(R.drawable.star_blank);
        } else if (rat >= 3 && rat < 3.5) {
            viewHolder.star1.setImageResource(R.drawable.orange_star);
            viewHolder.star2.setImageResource(R.drawable.orange_star);
            viewHolder.star3.setImageResource(R.drawable.orange_star);
            viewHolder.star4.setImageResource(R.drawable.star_blank);
            viewHolder.star5.setImageResource(R.drawable.star_blank);
        } else if (rat >= 3.5 && rat < 4) {
            viewHolder.star1.setImageResource(R.drawable.orange_star);
            viewHolder.star2.setImageResource(R.drawable.orange_star);
            viewHolder.star3.setImageResource(R.drawable.orange_star);
            viewHolder.star4.setImageResource(R.drawable.star_half);
            viewHolder.star5.setImageResource(R.drawable.star_blank);
        } else if (rat >= 4 && rat < 4.5) {
            viewHolder.star1.setImageResource(R.drawable.orange_star);
            viewHolder.star2.setImageResource(R.drawable.orange_star);
            viewHolder.star3.setImageResource(R.drawable.orange_star);
            viewHolder.star4.setImageResource(R.drawable.orange_star);
            viewHolder.star5.setImageResource(R.drawable.star_blank);
        } else if (rat >= 4.5 && rat < 5) {
            viewHolder.star1.setImageResource(R.drawable.orange_star);
            viewHolder.star2.setImageResource(R.drawable.orange_star);
            viewHolder.star3.setImageResource(R.drawable.orange_star);
            viewHolder.star4.setImageResource(R.drawable.orange_star);
            viewHolder.star5.setImageResource(R.drawable.star_half);
        } else if (rat == 5) {
            viewHolder.star1.setImageResource(R.drawable.orange_star);
            viewHolder.star2.setImageResource(R.drawable.orange_star);
            viewHolder.star3.setImageResource(R.drawable.orange_star);
            viewHolder.star4.setImageResource(R.drawable.orange_star);
            viewHolder.star5.setImageResource(R.drawable.orange_star);
        }
    }

    private class ViewHolder {
        ImageView hotel_img,star1, star2, star3, star4, star5;
        CustomTextView hotel_name,review,price;
        LinearLayout layout;
        public ViewHolder(View convertView) {
            hotel_img = (ImageView) convertView.findViewById(R.id.hotel_img);
            star1 = (ImageView) convertView.findViewById(R.id.star1);
            star2 = (ImageView) convertView.findViewById(R.id.star2);
            star3 = (ImageView) convertView.findViewById(R.id.star3);
            star4 = (ImageView) convertView.findViewById(R.id.star4);
            star5 = (ImageView) convertView.findViewById(R.id.star5);
            hotel_name=(CustomTextView)convertView.findViewById(R.id.hotel_name);
            review=(CustomTextView)convertView.findViewById(R.id.review);
            price=(CustomTextView)convertView.findViewById(R.id.price);
            layout=(LinearLayout)convertView.findViewById(R.id.layout);

        }
    }

    private class MyClick implements View.OnClickListener {
        int selected_position;
        public MyClick(int position) {
         this.selected_position=position;
        }

        @Override
        public void onClick(View v) {
            ((HotelListing)mContext).SelectedHotelData(selected_position);
        }
    }
}
