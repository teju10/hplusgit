package com.h_plus.H_Booking.TripIT.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.h_plus.H_Booking.TripIT.adapter.SelectedHomeImage_Adapter;
import com.h_plus.H_Booking.TripIT.adapter.SelectedhomeFeaturesAdapter;
import com.h_plus.R;
import com.h_plus.ServerInteraction.ResponseListener;
import com.h_plus.ServerInteraction.ResponseTask;
import com.h_plus.customwidget.CustomButton;
import com.h_plus.customwidget.CustomTextView;
import com.h_plus.usertabs.utility.Constant_Urls;
import com.h_plus.usertabs.utility.Constants;
import com.h_plus.usertabs.utility.Utility;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by Infograins on 27-Dec-16.
 */

public class SelectedHomeDetail extends AppCompatActivity implements View.OnClickListener, ViewPager.OnPageChangeListener {
    Context mContext;
    ViewPager V_pager;
    ImageView img_backwrd,img_frwrd,img_love;
    TextView text_count;
    CustomTextView ctv_address,ctv_price,ctv_beds,ctv_hom_desc;
    CustomButton btn_enquiry;
    ArrayList<String>listmultiimg=new ArrayList<>();
    SelectedHomeImage_Adapter adapter;
    int maxlimit,pos=0;
    String digid,status;
    private Toolbar mToolbar;
    ResponseTask responseTask;
    RecyclerView recycleView;
    List<String> list = new ArrayList<>();
    SelectedhomeFeaturesAdapter adapter1;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.selected_home_detail);
        mContext=this;
        mToolbar = (Toolbar) findViewById(R.id.h_toolbar);
        setSupportActionBar(mToolbar);
        ((CustomTextView) findViewById(R.id.toolbar_title)).setText("Home Booking");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        bind();
        SelectedHomeData();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    // for back button in toolbar
    public boolean onOptionsItemSelected(MenuItem menu) {
        switch (menu.getItemId()) {

            case android.R.id.home:
                onBackPressed();
              /*  Intent in=new Intent(mContext,MainActivity.class);
                startActivity(in);*//*
                finish();*/
                return true;
            default:
                return super.onOptionsItemSelected(menu);
        }
    }


    private void SelectedHomeData() {
        Bundle extraa=getIntent().getExtras();
        try {
            JSONObject j = new JSONObject(extraa.getString(Constants.OBJECT));
            System.out.println("json object is===="+j);
            ctv_address.setText(j.getString("address"));
            ctv_price.setText(j.getString("price"));
            ctv_beds.setText(j.getString("beadroom"));
            ctv_hom_desc.setText(j.getString("disc"));
            digid=j.getString("id");
            list.addAll(Arrays.asList(j.getString("facility").split(",")));
            adapter1.notifyDataSetChanged();
            status=j.getString("saved_status");
            if (status=="1"){
                img_love.setImageResource(R.drawable.heartfull);
            }else {
                img_love.setImageResource(R.drawable.hearttransparent);
            }
            if (j.has("image")){    //has is used to check wheather the webservice contain that key  or not
                if (!j.getString("image").equals("")){
                    JSONArray jArry = j.getJSONArray("image");
                    for (int i = 0; i < jArry.length(); i++) {
                        listmultiimg.add(jArry.getString(i));
                    }
                    img_backwrd.setVisibility(View.VISIBLE);
                    img_frwrd.setVisibility(View.VISIBLE);
                    text_count.setVisibility(View.VISIBLE);
                    maxlimit = listmultiimg.size();
                    text_count.setText(pos+1+"/"+maxlimit);
                    System.out.println("imagedata======"+listmultiimg);
                    adapter.notifyDataSetChanged();
                }
            }
            else {
                img_backwrd.setVisibility(View.INVISIBLE);
                img_frwrd.setVisibility(View.INVISIBLE);
                text_count.setVisibility(View.INVISIBLE);
                V_pager.setBackgroundResource(R.drawable.no_img_found);
            }

        }catch (Exception e){

            e.printStackTrace();
        }
    }

    private void bind() {
        V_pager=(ViewPager)findViewById(R.id.viewpager);
        recycleView=(RecyclerView)findViewById(R.id.recycleView);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        recycleView.setLayoutManager(layoutManager);
        adapter1 = new SelectedhomeFeaturesAdapter(list, R.layout.list_item, mContext);
        recycleView.setAdapter(adapter1);
        recycleView.setItemAnimator(new DefaultItemAnimator());
        adapter = new SelectedHomeImage_Adapter(listmultiimg, R.layout.selected_propimg_item, mContext);
        V_pager.setAdapter(adapter);
        img_backwrd=(ImageView)findViewById(R.id.img_backwrd);
        img_frwrd=(ImageView)findViewById(R.id.img_frwrd);
        img_love=(ImageView)findViewById(R.id.img_love);
        text_count=(TextView)findViewById(R.id.text_count);
        ctv_address=(CustomTextView)findViewById(R.id.ctv_address);
        ctv_price=(CustomTextView)findViewById(R.id.ctv_price);
        ctv_beds=(CustomTextView)findViewById(R.id.ctv_beds);
        ctv_hom_desc=(CustomTextView)findViewById(R.id.ctv_hom_desc);
        btn_enquiry=(CustomButton)findViewById(R.id.btn_enquiry);
        btn_enquiry.setOnClickListener(this);
        img_frwrd.setOnClickListener(this);
        img_backwrd.setOnClickListener(this);
        img_love.setOnClickListener(this);
        V_pager.addOnPageChangeListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.img_frwrd:
                if (pos < (maxlimit-1)) {
                    pos = pos + 1;
                    V_pager.setCurrentItem(pos);
                    //text_count.setText(pos+"/"+maxlimit);
                }
                break;
            case R.id.img_backwrd:
                if (pos > 0) {
                    pos = pos - 1;
                    V_pager.setCurrentItem(pos);
                   // text_count.setText(pos+"/"+maxlimit);
                }
                break;
            case R.id.btn_enquiry:
                Intent in=new Intent(mContext,SendEnquiryActivity.class);
                in.putExtra("digid",digid);
                startActivity(in);
                break;
            case R.id.img_love:
                GetFavroute();

                break;
        }

    }

    private void GetFavroute() {
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put(Constants.ACTION, Constants.SAVEDUNSAVEDHOMES);
            jsonObject.put("user_id", Utility.getSharedPreferences(mContext, Constants.USERID));
            //jsonObject.put("user_id","244");
           // jsonObject.put("saved_status",status);
            jsonObject.put("digid",digid);
            Utility.ShowLoading(mContext, mContext.getResources().getString(R.string.please_wait));
            responseTask = new ResponseTask(Constant_Urls.SERVER_URL, jsonObject);
            responseTask.setListener(new ResponseListener() {
                @Override
                public void onGetPickSuccess(String result) {
                    Utility.HideDialog();
                    if (result == null) {
                        Utility.ShowToastMessage(mContext, "sever not responding");
                    }
                    else {
                        try {
                            JSONObject jsonObject = new JSONObject(result);
                            if (jsonObject.getString(Constants.RESULT_KEY).equals("1")&jsonObject.getString("saved").equals("1")){
                                img_love.setImageResource(R.drawable.heartfull);
                                Utility.showToast(mContext,"Saved Successfully");
                            }
                            else {
                                img_love.setImageResource(R.drawable.hearttransparent);
                                Utility.showToast(mContext,"UnSaved Successfully");
                            }
                        }catch (Exception e){
                            e.printStackTrace();
                            Utility.showToast(mContext,"Exception occured");
                        }
                    }
                }
            });
        }catch (Exception e){
            e.printStackTrace();
        }
        responseTask.execute();
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
        pos=position;
        text_count.setText((pos+1)+"/"+maxlimit);
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }
}
