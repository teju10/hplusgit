package com.h_plus.H_Booking.TripIT.activity;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.h_plus.R;
import com.h_plus.customwidget.CustomButton;
import com.h_plus.customwidget.CustomTextView;
import com.h_plus.usertabs.utility.Constants;
import com.h_plus.usertabs.utility.Utility;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Infograins on 09-Jan-17.
 */
public class SelectedCarDetail extends AppCompatActivity implements View.OnClickListener {
    Context mContext;
    private Toolbar mToolbar;
    String car_id;
    ImageView img_car;
    CustomTextView ctv_car_name,ctv_pick_up,ctv_pickup_date,ctv_drop_off_date,ctv_features,ctv_car_price;
    CustomButton btn_book_now;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.selected_car_detail);
        mContext = this;
        mToolbar = (Toolbar) findViewById(R.id.h_toolbar);
        setSupportActionBar(mToolbar);
        ((CustomTextView) findViewById(R.id.toolbar_title)).setText("Car Booking");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        bind();
        SelectedCarData();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void bind() {
        img_car=(ImageView)findViewById(R.id.img_car);
        ctv_car_name=(CustomTextView) findViewById(R.id.ctv_car_name);
        ctv_pick_up=(CustomTextView) findViewById(R.id.ctv_pick_up);
        ctv_pickup_date=(CustomTextView) findViewById(R.id.ctv_pickup_date);
        ctv_drop_off_date=(CustomTextView) findViewById(R.id.ctv_drop_off_date);
        ctv_features=(CustomTextView) findViewById(R.id.ctv_features);
        ctv_car_price=(CustomTextView) findViewById(R.id.ctv_car_price);
        btn_book_now=(CustomButton) findViewById(R.id.btn_book_now);
        btn_book_now.setOnClickListener(this);
    }

    private void SelectedCarData() {
        Bundle extraa=getIntent().getExtras();
        try {
            JSONObject j = new JSONObject(extraa.getString(Constants.OBJECT));
            car_id=(j.getString("id"));
            ctv_car_name.setText(j.getString("name"));
            ctv_pick_up.setText(j.getString("pick_up"));
            ctv_pickup_date.setText(j.getString("pick_up_date"));
            ctv_drop_off_date.setText(j.getString("drop_off_date"));
            ctv_features.setText(j.getString("features"));
            ctv_car_price.setText("$"+j.getString("price"));
            Glide.with(mContext).load(j.getString("imageimage")).into(img_car);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onClick(View v) {
        Utility.showToast(mContext,"Coming Soon");
    }
}
