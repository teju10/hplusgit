package com.h_plus.H_Booking.TripIT.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.h_plus.R;

import java.util.List;

/**
 * Created by Infograins on 29-Dec-16.
 */
public class SelectedhomeFeaturesAdapter extends RecyclerView.Adapter {
    LayoutInflater layoutInflater;
    Context mContext;
    List<String> arrayList;
    int res;
    public SelectedhomeFeaturesAdapter(List<String> list, int list_item, Context mContext) {
        this.arrayList=list;
        this.res = list_item;
        this.mContext = mContext;
        layoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = layoutInflater.inflate(res, parent, false);
        return new MyViewHolder(v);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
    try {
        ((MyViewHolder)holder).list_auto.setText(arrayList.get(position));
    }
    catch (Exception e){
        e.printStackTrace();
    }
    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    private class MyViewHolder extends RecyclerView.ViewHolder {
        TextView list_auto;
        public MyViewHolder(View itemView) {
            super(itemView);
            list_auto=(TextView)itemView.findViewById(R.id.list_auto);
        }
    }
}
