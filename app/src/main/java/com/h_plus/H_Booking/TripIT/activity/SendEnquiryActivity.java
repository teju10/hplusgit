package com.h_plus.H_Booking.TripIT.activity;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;

import com.h_plus.R;
import com.h_plus.ServerInteraction.ResponseListener;
import com.h_plus.ServerInteraction.ResponseTask;
import com.h_plus.customwidget.CustomButton;
import com.h_plus.customwidget.CustomEditText;
import com.h_plus.customwidget.CustomTextView;
import com.h_plus.usertabs.utility.Constant_Urls;
import com.h_plus.usertabs.utility.Constants;
import com.h_plus.usertabs.utility.Utility;

import org.json.JSONObject;

/**
 * Created by Infograins on 27-Dec-16.
 */

public class SendEnquiryActivity extends AppCompatActivity implements View.OnClickListener {
    Context mContext;
    CustomEditText edit_name,edit_mail,edit_contact,edit_msg;
    CustomButton done_btn;
    String digid;
    ResponseTask responseTask;
    private Toolbar mToolbar;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.send_enquiry_layout);
        mContext=this;
        mToolbar = (Toolbar) findViewById(R.id.h_toolbar);
        setSupportActionBar(mToolbar);
        ((CustomTextView) findViewById(R.id.toolbar_title)).setText("Send Enquiry");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        bind();
        getdatafromintent();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menu) {
        switch (menu.getItemId()) {

            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(menu);
        }
    }

    private void getdatafromintent() {
        digid = getIntent().getExtras().getString("digid");
        //System.out.println("dig iddd======"+digid);
    }

    private void bind() {
        edit_name=(CustomEditText)findViewById(R.id.edit_name);
        edit_mail=(CustomEditText)findViewById(R.id.edit_mail);
        edit_contact=(CustomEditText)findViewById(R.id.edit_contact);
        edit_msg=(CustomEditText)findViewById(R.id.edit_msg);
        done_btn=(CustomButton)findViewById(R.id.done_btn);
        done_btn.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.done_btn:
                if (Utility.isConnectingToInternet(mContext)){
                    if (edit_name.getText().toString().equals("")){
                        edit_name.setError("please enter Name");
                        edit_name.requestFocus();
                    }
                    else if (edit_mail.getText().toString().equals("")){
                        edit_mail.setError("please enter email");
                        edit_mail.requestFocus();
                    }
                    else if (!edit_mail.getText().toString().contains("@")){
                        edit_mail.setError("please enter  valid email");
                        edit_mail.requestFocus();
                    } else if (edit_contact.getText().toString().equals("")){
                        edit_contact.setError("please enter Contact");
                        edit_contact.requestFocus();
                    }
                    else if (edit_contact.getText().toString().length()<10){
                        edit_contact.setError("please enter valid Contact Number");
                        edit_contact.requestFocus();
                    }
                     else if (edit_msg.getText().toString().equals("")){
                        edit_msg.setError("message cannot left blank");
                        edit_msg.requestFocus();
                    }
                    else {
                           GetEnquiryTask();
                    }
                }
                break;
        }
    }

    private void GetEnquiryTask() {
              try {
                  JSONObject jsonObject = new JSONObject();
                  jsonObject.put(Constants.ACTION, Constants.ENQUIRYNOWBYUSER);
                  jsonObject.put("user_id", Utility.getSharedPreferences(mContext, Constants.USERID));
                  //jsonObject.put("user_id","20");
                  jsonObject.put("digid",digid);
                  jsonObject.put("name",edit_name.getText().toString());
                  jsonObject.put("email",edit_mail.getText().toString());
                  jsonObject.put("phone",edit_contact.getText().toString());
                  jsonObject.put("message",edit_msg.getText().toString());
                  Utility.ShowLoading(mContext, mContext.getResources().getString(R.string.please_wait));
                  responseTask = new ResponseTask(Constant_Urls.SERVER_URL, jsonObject);
                  responseTask.setListener(new ResponseListener() {
                      @Override
                      public void onGetPickSuccess(String result) {
                          Utility.HideDialog();
                          if (result == null) {
                              Utility.showCroutonWarn(getResources().getString(R.string.server_fail), SendEnquiryActivity.this);
                          }
                          else {
                             try {
                                 JSONObject jobj = new JSONObject(result);
                                 if (jobj.getString(Constants.RESULT_KEY).equals("1")){
                                     Utility.showToast(mContext,"Your Request Send Successfully");
                                     finish();
                                 }
                                else {
                                     Utility.showToast(mContext,"Not Updated");
                                 }

                             }catch (Exception e){
                                 e.printStackTrace();
                             }
                          }
                      }
                  });
              }catch (Exception e){
                  e.printStackTrace();
              }
        responseTask.execute();
    }
}
