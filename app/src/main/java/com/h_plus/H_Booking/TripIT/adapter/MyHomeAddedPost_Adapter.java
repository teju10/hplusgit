package com.h_plus.H_Booking.TripIT.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.h_plus.R;
import com.h_plus.customwidget.CustomButton;
import com.h_plus.customwidget.CustomTextView;
import com.h_plus.fragment_digg.MyHomePosted;

import org.json.JSONObject;

import java.util.ArrayList;


/**
 * Created by Infograins on 13-Dec-16.
 */

public class MyHomeAddedPost_Adapter extends ArrayAdapter<JSONObject> {
    Context mContext;
    int res;
    LayoutInflater layoutInflater;
    ArrayList<JSONObject> jsonObjects;
    MyHomePosted mfragment;
    int [] back =new int[]{R.drawable.purple_patti,R.drawable.blue_patti,R.drawable.red_patti};
    public MyHomeAddedPost_Adapter(Context context, int resource, ArrayList<JSONObject>objects,MyHomePosted mfragment) {
        super(context, resource,objects);
        this.mContext = context;
        this.res = resource;
        this.jsonObjects = objects;
        layoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.mfragment=mfragment;
    }
    public View getView(int position, View convertView, ViewGroup parent){
           ViewHolder viewHolder;
        if (convertView == null) {
            convertView = layoutInflater.inflate(res, null);
            viewHolder = new ViewHolder(convertView);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        try {
            //show things here
            //
viewHolder.layout.setBackgroundDrawable(mContext.getResources().getDrawable(back[position%3]));
            viewHolder.title.setText(jsonObjects.get(position).getString("title"));
            viewHolder.prop_type.setText(jsonObjects.get(position).getString("property_type"));
            viewHolder.price.setText(jsonObjects.get(position).getString("price"));
            viewHolder.posting_date.setText(jsonObjects.get(position).getString("posting_date"));
           viewHolder.btn_detail_edit.setOnClickListener(new myclick(position));
            viewHolder.btn_delete.setOnClickListener(new myclick(position));
           // viewHolder.btn_edit_prop.setOnClickListener(new MyHomeAddedPost_Adapter.myclick(position));
            Glide.with(mContext).load(jsonObjects.get(position).getString("image")).diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(viewHolder.Img);
        }catch (Exception e){
            e.printStackTrace();
        }
        return convertView;
    }

    private class ViewHolder {
        ImageView Img;
        CustomTextView title,prop_type,price,posting_date;
        CustomButton btn_detail_edit,btn_delete;
        LinearLayout layout;
        public ViewHolder(View convertView) {
            Img=(ImageView)convertView.findViewById(R.id.prop_img);
            title =(CustomTextView)convertView.findViewById(R.id.ctv_title);
            prop_type=(CustomTextView)convertView.findViewById(R.id.ctv_prop_type);
            price=(CustomTextView)convertView.findViewById(R.id.ctv_price);
            posting_date=(CustomTextView)convertView.findViewById(R.id.ctv_posting_date);
            btn_detail_edit=(CustomButton) convertView.findViewById(R.id.btn_detail);
            btn_delete=(CustomButton) convertView.findViewById(R.id.btn_delete);
            //btn_edit_prop=(CustomButton) convertView.findViewById(R.id.btn_edit_prop);
            layout=(LinearLayout) convertView.findViewById(R.id.layout);
        }
    }

    private class myclick implements View.OnClickListener {
        int selected_position;
        public myclick(int sel_position) {
            this.selected_position=sel_position;
        }

        @Override
        public void onClick(View v) {
           switch (v.getId()){
               case R.id.btn_detail:
                   ((MyHomePosted)mContext).DetailEditSelectedAdd(selected_position);

                   break;
               case R.id.btn_delete:
                   try {
                     /*  mfragment.DeleteSelectedAdd(selected_position);*/
                       ((MyHomePosted)mContext).DeleteSelectedAdd(selected_position);
                   } catch (Exception e) {
                       e.printStackTrace();
                   }
                   break;

           }

        }
    }
}
