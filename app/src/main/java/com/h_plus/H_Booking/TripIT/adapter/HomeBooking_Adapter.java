package com.h_plus.H_Booking.TripIT.adapter;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.h_plus.H_Booking.HomeMap_Activity;
import com.h_plus.R;
import com.h_plus.customwidget.CustomTextView;

import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Infograins on 26-Dec-16.
 */
public class HomeBooking_Adapter extends PagerAdapter {
    LayoutInflater layoutInflater;
    Context mContext;
    ArrayList<JSONObject> arrayList;
    int res;

    public HomeBooking_Adapter(ArrayList<JSONObject> jObjectArrayList, int filter_property_listing_item, Context mContext) {
        this.arrayList=jObjectArrayList;
        this.res=filter_property_listing_item;
        this.mContext=mContext;

    }

    @Override
    public int getCount() {
        return arrayList.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        ImageView img_prop;
        CustomTextView text_propname, text_price, text_address;
        layoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View v = layoutInflater.inflate(res, container, false);
        try {
            text_propname = (CustomTextView) v.findViewById(R.id.text_propname);
            text_price = (CustomTextView) v.findViewById(R.id.text_price);
            text_address = (CustomTextView) v.findViewById(R.id.text_address);
            img_prop = (ImageView) v.findViewById(R.id.img_prop);
            if(arrayList.get(position).getString("image").equals("")){
                img_prop.setImageResource(R.drawable.no_img_found);
            }else {
                Glide.with(mContext).load(arrayList.get(position).getString("image")).into(img_prop);
            }
            text_propname.setText(arrayList.get(position).getString("title"));
            text_price.setText(arrayList.get(position).getString("price"));
            text_address.setText(arrayList.get(position).getString("address"));
            //((MyViewHolder) holder).cross.setOnClickListener(new MultiImageAdapter.Myclick(position));
            img_prop.setOnClickListener(new Myclick(position));
            container.addView(v);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return v;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
       container.removeView((View) object);
    }

    private class Myclick implements View.OnClickListener {
        int selected_position;
        public Myclick(int position) {
     this.selected_position=position;
        }

        @Override
        public void onClick(View v) {
            switch (v.getId()){
                case R.id.img_prop:
                    ((HomeMap_Activity)mContext).SelectedPropDetail(selected_position);
                    break;
            }
        }
    }
}
