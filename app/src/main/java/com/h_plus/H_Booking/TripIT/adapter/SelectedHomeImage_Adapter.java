package com.h_plus.H_Booking.TripIT.adapter;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.h_plus.R;

import java.util.ArrayList;

/**
 * Created by Infograins on 27-Dec-16.
 */
public class SelectedHomeImage_Adapter extends PagerAdapter {
    LayoutInflater layoutInflater;
    Context mContext;
    ArrayList<String> arrayList;
    int res;
    public SelectedHomeImage_Adapter(ArrayList<String> listmultiimg, int selected_propimg_item, Context mContext) {
        this.arrayList=listmultiimg;
        this.res=selected_propimg_item;
        this.mContext=mContext;
    }

    @Override
    public int getCount() {
        return arrayList.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view==object;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View)object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        ImageView img_prop;
        layoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View v = layoutInflater.inflate(res, container, false);
        try {
            img_prop = (ImageView) v.findViewById(R.id.img_prop);
            Glide.with(mContext).load(arrayList.get(position)).into(img_prop);
            container.addView(v);
        }catch (Exception e){
            e.printStackTrace();
        }
        return v;
    }
}
