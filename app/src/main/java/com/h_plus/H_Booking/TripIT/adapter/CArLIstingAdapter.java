package com.h_plus.H_Booking.TripIT.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.h_plus.H_Booking.TripIT.activity.CarListing;
import com.h_plus.R;
import com.h_plus.customwidget.CustomButton;
import com.h_plus.customwidget.CustomTextView;

import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Infograins on 05-Jan-17.
 */
public class CArLIstingAdapter extends ArrayAdapter<JSONObject> {
    Context mContext;
    LayoutInflater layoutInflater;
    int res;
    ArrayList<JSONObject> jsonObjects;
    public CArLIstingAdapter(Context mContext, int car_listing_item, ArrayList<JSONObject> jObjectArrayList) {
        super(mContext,car_listing_item,jObjectArrayList);
        this.mContext = mContext;
        this.res = car_listing_item;
        this.jsonObjects = jObjectArrayList;
        layoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }
    public View getView(int position, View convertView, ViewGroup parent){
        ViewHolder viewHolder;
        if (convertView == null) {
            convertView = layoutInflater.inflate(res, null);
            viewHolder = new ViewHolder(convertView);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        try {
            // viewHolder.Show_Name.setText(jobject.getString("title"));
            Glide.with(mContext).load(jsonObjects.get(position).getString("image")).diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(viewHolder.car_img);
            viewHolder.car_name.setText(jsonObjects.get(position).getString("name"));
            viewHolder.person.setText(jsonObjects.get(position).getString("adults"));
            viewHolder.luggage.setText(jsonObjects.get(position).getString("luggage"));
            viewHolder.AC.setText(jsonObjects.get(position).getString("ac"));
            viewHolder.car_price.setText(jsonObjects.get(position).getString("price"));
            viewHolder.btn_car_choose.setOnClickListener(new MyClick(position));

        } catch (Exception e) {
            e.printStackTrace();
        }
        return convertView;
    }
    private class ViewHolder {
        ImageView car_img;
        CustomTextView car_name,person,luggage,AC,car_price;
        CustomButton btn_car_choose;
       // LinearLayout layout;
        public ViewHolder(View convertView) {
            car_img = (ImageView) convertView.findViewById(R.id.car_img);
            car_name=(CustomTextView)convertView.findViewById(R.id.car_name);
            person=(CustomTextView)convertView.findViewById(R.id.person);
            luggage=(CustomTextView)convertView.findViewById(R.id.luggage);
            AC=(CustomTextView)convertView.findViewById(R.id.AC);
            car_price=(CustomTextView)convertView.findViewById(R.id.car_price);
            btn_car_choose=(CustomButton) convertView.findViewById(R.id.btn_car_choose);
           // layout=(LinearLayout)convertView.findViewById(R.id.layout);

        }
    }

    private class MyClick implements View.OnClickListener {
        int selected_pos;
        public MyClick(int position) {
            this.selected_pos=position;
        }

        @Override
        public void onClick(View v) {
            ((CarListing)mContext).SHowSelectedCar(selected_pos);
        }
    }
}
