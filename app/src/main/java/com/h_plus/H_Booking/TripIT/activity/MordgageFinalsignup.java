package com.h_plus.H_Booking.TripIT.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.CheckBox;

import com.h_plus.R;
import com.h_plus.RegistrationSection.LoginActivity;
import com.h_plus.ServerInteraction.ResponseListener;
import com.h_plus.ServerInteraction.ResponseTask;
import com.h_plus.customwidget.CustomTextView;
import com.h_plus.usertabs.utility.Constant_Urls;
import com.h_plus.usertabs.utility.Constants;
import com.h_plus.usertabs.utility.Utility;
import com.rengwuxian.materialedittext.MaterialEditText;

import org.json.JSONObject;

/**
 * Created by Infograins on 06-Jan-17.
 */
public class MordgageFinalsignup extends AppCompatActivity implements View.OnClickListener {
    MaterialEditText first_name,last_name,contact_no,email_add,password,con_password;
    CheckBox check_agree_terms;
    CustomTextView log_in;
    Context mContext;
    String c_name,c_add1,c_add2,c_city,c_state,c_zip,c_phone,c_fax,c_service,c_employee_no;
    ResponseTask responseTask;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.mordgage_final_signup);
        mContext=this;
        bind();
        PreviousData();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void PreviousData() {
        Bundle extraa=getIntent().getExtras();
        c_name=extraa.getString(Constants.COMPANY_NAME);
        c_add1=extraa.getString(Constants.COMPANY_ADD1);
        c_add2=extraa.getString(Constants.COMPANY_ADD2);
        c_city=extraa.getString(Constants.CITY);
        c_state=extraa.getString(Constants.STATE);
        c_zip=extraa.getString(Constants.ZIP);
        c_phone=extraa.getString(Constants.COMPANY_PHONE);
        c_fax=extraa.getString(Constants.COMPANY_FAX);
        c_service=extraa.getString(Constants.COMPANY_SERVICE);
        c_employee_no=extraa.getString(Constants.COMPANY_EMPLOYE_NO);

    }

    private void bind() {
        first_name=(MaterialEditText)findViewById(R.id.first_name);
        last_name=(MaterialEditText)findViewById(R.id.last_name);
        contact_no=(MaterialEditText)findViewById(R.id.contact_no);
        email_add=(MaterialEditText)findViewById(R.id.email_add);
        password=(MaterialEditText)findViewById(R.id.password);
        con_password=(MaterialEditText)findViewById(R.id.con_password);
        check_agree_terms=(CheckBox)findViewById(R.id.check_agree_terms);
        (findViewById(R.id.btn_continue)).setOnClickListener(this);
        log_in=(CustomTextView)findViewById(R.id.log_in);
        log_in.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.log_in:
                Intent in=new Intent(mContext,LoginActivity.class);
                startActivity(in);
                break;
            case R.id.btn_continue:
                if (Utility.isConnectingToInternet(mContext)){
                    if (first_name.getText().toString().equals("")){
                        first_name.setError("please fill first name");
                        first_name.requestFocus();
                    } else if (last_name.getText().toString().equals("")){
                        last_name.setError("please fill last name");
                        last_name.requestFocus();
                    }else if (contact_no.getText().toString().equals("")){
                        contact_no.setError("please fill contact number");
                        contact_no.requestFocus();
                    } else if (email_add.getText().toString().equals("")){
                        email_add.setError("please fill email Address");
                        email_add.requestFocus();
                    } else if (password.getText().toString().equals("")){
                        password.setError("please fill password");
                        password.requestFocus();
                    } else if (con_password.getText().toString().equals("")){
                        con_password.setError("please fill confirm password");
                        con_password.requestFocus();
                    }else if (!check_agree_terms.isChecked()){
                        Utility.showToast(mContext,"please checked the terms & conditions");
                    }else if (!password.getText().toString().equals(con_password.getText().toString())){
                        con_password.setError("please fill confirm password");
                        con_password.getText().clear();
                        con_password.requestFocus();
                    }
                    else {
                        MordgageSignup();
                    }
                }
                break;
        }

    }

    private void MordgageSignup() {
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put(Constants.ACTION, Constants.MORDGAGE_SIGNUP);
            jsonObject.put("usertype",3);
            jsonObject.put("cname",c_name);
            jsonObject.put("add1",c_add1);
            jsonObject.put("add2",c_add2);
            jsonObject.put("city",c_city);
            jsonObject.put("state",c_state);
            jsonObject.put("zip",c_zip);
            jsonObject.put("cphone",c_phone);
            jsonObject.put("fax_no",c_fax);
            jsonObject.put("no_state",c_service);
            jsonObject.put("no_employee",c_employee_no);
            jsonObject.put("first_name",first_name.getText().toString());
            jsonObject.put("last_name",last_name.getText().toString());
            jsonObject.put("phone",contact_no.getText().toString());
            jsonObject.put("email",email_add.getText().toString());
            jsonObject.put("password",password.getText().toString());


            Utility.ShowLoading(mContext, mContext.getResources().getString(R.string.please_wait));
            responseTask = new ResponseTask(Constant_Urls.SERVER_URL, jsonObject);
            responseTask.setListener(new ResponseListener() {
                @Override
                public void onGetPickSuccess(String result) {
                    Utility.HideDialog();
                    if (result == null) {
                        Utility.ShowToastMessage(mContext, "sever not responding");
                    } else{
                        try {
                            JSONObject jsonObject = new JSONObject(result);
                            if (jsonObject.getString(Constants.RESULT_KEY).equals("1")){
                               Utility.showToast(mContext,"User Register Successfully");
                                Intent in= new Intent(mContext,LoginActivity.class);
                                Utility.setSharedPreference(mContext, Constants.SPLASH_CHECK, "2");
                                startActivity(in);
                            }else if (jsonObject.getString(Constants.RESULT_KEY).equals("0")){
                               Utility.showToast(mContext,jsonObject.getString(Constants.RESULT_KEY));
                            }
                        }catch (Exception e){
                            e.printStackTrace();
                        }

                    }
                }
            });
        }catch (Exception e){
            e.printStackTrace();
        }
        responseTask.execute();
    }
}
