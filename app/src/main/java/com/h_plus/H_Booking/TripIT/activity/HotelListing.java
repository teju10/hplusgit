package com.h_plus.H_Booking.TripIT.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.ListView;

import com.h_plus.H_Booking.TripIT.adapter.HotelListing_Adapter;
import com.h_plus.R;
import com.h_plus.ServerInteraction.ResponseListener;
import com.h_plus.ServerInteraction.ResponseTask;
import com.h_plus.customwidget.CustomTextView;
import com.h_plus.usertabs.utility.Constant_Urls;
import com.h_plus.usertabs.utility.Constants;
import com.h_plus.usertabs.utility.Utility;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Infograins on 02-Jan-17.
 */
public class HotelListing extends AppCompatActivity {
    ResponseTask responseTask;
    Context mContext;
    ArrayList<JSONObject> jObjectArrayList;
    String str_location,check_in,check_out,rooms,adult,children;
    ListView list_data;
    HotelListing_Adapter mhpadapter;
    private Toolbar mToolbar;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.listview_with_toolbar);
        mContext=this;
        mToolbar = (Toolbar) findViewById(R.id.h_toolbar);
        setSupportActionBar(mToolbar);
        ((CustomTextView) findViewById(R.id.toolbar_title)).setText("Hotel List");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        bind();
        PreviousData();
        //init();
        GetHotelList();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case android.R.id.home:
                onBackPressed();
              /*  Intent in=new Intent(mContext,MainActivity.class);
                startActivity(in);*//*
                finish();*/
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void PreviousData() {
        Bundle extraa=getIntent().getExtras();
        str_location=extraa.getString(Constants.DESTINATION);
        check_in=extraa.getString(Constants.CHECKIN);
        check_out=extraa.getString(Constants.CHECKOUT);
        rooms=extraa.getString(Constants.ROOMS);
        adult=extraa.getString(Constants.ADULT);
        children=extraa.getString(Constants.CHILDRENS);
    }

    private void bind() {
        jObjectArrayList = new ArrayList<>();
        list_data = (ListView) findViewById(R.id.listView);
        mhpadapter = new HotelListing_Adapter(mContext, R.layout.hotel_listing_item, jObjectArrayList);
        list_data.setAdapter(mhpadapter);
    }

    private void GetHotelList() {
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put(Constants.ACTION, Constants.HOTELSEARCHING);
            jsonObject.put("country",str_location);
            jsonObject.put("check_in_time",check_in);
            jsonObject.put("check_out_time",check_out);
            jsonObject.put("room",rooms);
            jsonObject.put("adult",adult);
            jsonObject.put("children",children);

            Utility.ShowLoading(mContext, mContext.getResources().getString(R.string.please_wait));
            responseTask = new ResponseTask(Constant_Urls.SERVER_URL, jsonObject);
            responseTask.setListener(new ResponseListener() {
                @Override
                public void onGetPickSuccess(String result) {
                    Utility.HideDialog();
                    if (result == null) {
                        Utility.ShowToastMessage(mContext, "sever not responding");
                    } else{
                        try {
                            JSONObject jsonObject = new JSONObject(result);
                            if (jsonObject.getString(Constants.RESULT_KEY).equals("1")){
                                JSONArray jsonArray = jsonObject.getJSONArray("object");
                                for (int i = 0; i < jsonArray.length(); i++){
                                    jObjectArrayList.add(jsonArray.getJSONObject(i));
                                }
                                mhpadapter.notifyDataSetChanged();
                            }
                            else {
                                Utility.showToast(mContext,jsonObject.getString(Constants.SERVER_MSG));
                            }
                        }catch (Exception e){
                            e.printStackTrace();
                        }

                    }
                }
            });
        }catch (Exception e){
            e.printStackTrace();
        }
        responseTask.execute();
    }

    public void SelectedHotelData(int sel_pos) {
        try {
            String id = jObjectArrayList.get(sel_pos).getString("id");
            GetHotelDetailByID(id);
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    private void GetHotelDetailByID(final String hotel_id) {
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put(Constants.ACTION, Constants.HOTELDETAILSBYID);
            //jsonObject.put("user_id",Utility.getSharedPreferences(mContext,Constants.USERID));
            jsonObject.put("hotel_id", hotel_id);
            Utility.ShowLoading(mContext, mContext.getResources().getString(R.string.please_wait));
            responseTask = new ResponseTask(Constant_Urls.SERVER_URL, jsonObject);
            responseTask.setListener(new ResponseListener() {
                @Override
                public void onGetPickSuccess(String result) {
                    Utility.HideDialog();
                    if (result == null) {
                        Utility.showCroutonWarn(getResources().getString(R.string.server_fail),  HotelListing.this);
                    } else {
                        try {

                            JSONObject jobj = new JSONObject(result);
                            if (jobj.getString(Constants.RESULT_KEY).equals("1")) {
                                JSONObject jasobj = jobj.getJSONObject("object");
                                Bundle extraa = new Bundle();
                                extraa.putString(Constants.ID, hotel_id);
                                extraa.putString(Constants.OBJECT, jasobj.toString());
                                System.out.println("dataaaa====="+jasobj);
                                Intent in = new Intent(mContext, SelectedHotelDetail.class);
                                in.putExtras(extraa);
                                System.out.println("GET value" + extraa);
                                startActivity(in);
                                //finish();

                            } else {
                                Utility.showCroutonWarn(jobj.getString(Constants.SERVER_MSG),  HotelListing.this);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            });
            responseTask.execute();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
