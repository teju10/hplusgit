package com.h_plus.H_Booking.TripIT.activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.h_plus.H_Booking.Model.Datamodel;
import com.h_plus.H_Booking.TripIT.adapter.MortgageLeasAdapter;
import com.h_plus.R;
import com.h_plus.RegistrationSection.LoginActivity;
import com.h_plus.customwidget.CustomTextView;
import com.h_plus.usertabs.utility.Utility;

import java.util.ArrayList;
import java.util.List;

import static com.h_plus.R.id.btn_fresh_automated;

/**
 * Created by Infograins on 06-Jan-17.
 */

public class MortgageDetail_Activity extends AppCompatActivity implements View.OnClickListener {
RecyclerView recyclevieww;
    List<Datamodel> list = new ArrayList<>();
    MortgageLeasAdapter adapter1;
   String pos_adptr="1";
    SharedPreferences share;
    SharedPreferences.Editor edit;
    private Toolbar mToolbar;
    Context mContext;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.mortgage_detail_layout);
        mContext = this;
        mToolbar = (Toolbar) findViewById(R.id.h_toolbar);
        setSupportActionBar(mToolbar);
        ((CustomTextView) findViewById(R.id.toolbar_title)).setText("Mortgage Detail");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        share = mContext.getSharedPreferences("pref", MODE_PRIVATE);
        edit = share.edit();
        edit.putString("login_true", "true");
        edit.commit();
        edit.apply();
        bind();
        init();
    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
       finish();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);//Menu Resource, Menu
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_logout:
                Utility.clearsharedpreference(mContext);
                startActivity(new Intent(mContext, LoginActivity.class));
                finish();
                return true;
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    private void bind() {
        recyclevieww=(RecyclerView)findViewById(R.id.recyclevieww);
        (findViewById(btn_fresh_automated)).setOnClickListener(this);
        (findViewById(R.id.btn_lot_lead)).setOnClickListener(this);
        (findViewById(R.id.btn_abt_our_lead)).setOnClickListener(this);
        (findViewById(R.id.btn_sample_lead)).setOnClickListener(this);
        (findViewById(R.id.btn_checkout)).setOnClickListener(this);
        (findViewById(btn_fresh_automated)).setBackgroundColor(getResources().getColor(R.color.colorPrimary));
    }
    private void init() {
        LinearLayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        recyclevieww.setLayoutManager(layoutManager);
        adapter1 = new MortgageLeasAdapter(list, R.layout.mortgage_lead_item, mContext);
        recyclevieww.setAdapter(adapter1);
        recyclevieww.setItemAnimator(new DefaultItemAnimator());
        prepareMovieData();

    }

    private void prepareMovieData() {
        list.clear();
        if ( pos_adptr.equals("1")){
            list.add(new Datamodel("NON-EXCLUSIVE LEADS","Filters include loan type, credit rating, property type and state."));
            list.add(new Datamodel("CUSTOM FILTERS (additional criteria)","For each of the items listed above under additional criteria. To use this feature of Homes Plus Leads, log into your Homes Plus Leads account under settings and choose Get More Specific."));
            adapter1.notifyDataSetChanged();
        }
        else if ( pos_adptr.equals("2")){
            list.add(new Datamodel("1 Business Day Old","$13.00"));
            list.add(new Datamodel("2 Business Days to 3 Days Old","$12.00"));
            list.add(new Datamodel("4 Business Days to 5 Days Old","$11.00"));
            list.add(new Datamodel("6 Business Days to 7 Days Old","$10.00"));
            list.add(new Datamodel("Business Days Old","$9.00"));
            list.add(new Datamodel("Business Days to 4 Weeks Old","$8.00") );
            list.add(new Datamodel("4 Weeks to 5 Weeks Old","$7.00"));
            list.add(new Datamodel("5 Weeks to 6 Weeks Old","$6.00"));
            adapter1.notifyDataSetChanged();
        }


    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case btn_fresh_automated:
                pos_adptr="1";
                (findViewById(btn_fresh_automated)).setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                (findViewById(R.id.btn_lot_lead)).setBackgroundColor(getResources().getColor(R.color.transparent_black));
                prepareMovieData();
                break;
            case R.id.btn_lot_lead:
                pos_adptr="2";
                (findViewById(btn_fresh_automated)).setBackgroundColor(getResources().getColor(R.color.transparent_black));
                (findViewById(R.id.btn_lot_lead)).setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                prepareMovieData();
                break;
            case R.id.btn_abt_our_lead:
                break;
            case R.id.btn_sample_lead:
                break;
            case R.id.btn_checkout:
                Utility.showToast(mContext,"Comming Soon");
                break;
        }

    }
}
