package com.h_plus.H_Booking.TripIT.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.ListView;

import com.h_plus.H_Booking.TripIT.adapter.FlightListing_Adapter;
import com.h_plus.R;
import com.h_plus.ServerInteraction.ResponseListener;
import com.h_plus.ServerInteraction.ResponseTask;
import com.h_plus.customwidget.CustomTextView;
import com.h_plus.usertabs.utility.Constant_Urls;
import com.h_plus.usertabs.utility.Constants;
import com.h_plus.usertabs.utility.Utility;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Infograins on 03-Jan-17.
 */

public class FlightListing extends AppCompatActivity {
    ResponseTask responseTask;
    Context mContext;
    ArrayList<JSONObject> jObjectArrayList;
    String flight_from,flight_to,departure_on,return_on,adult,children,infant;
    ListView listv;
    FlightListing_Adapter mhpadapter;
    private Toolbar mToolbar;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.listview_with_toolbar);
        mContext=this;
        mToolbar = (Toolbar) findViewById(R.id.h_toolbar);
        setSupportActionBar(mToolbar);
        ((CustomTextView) findViewById(R.id.toolbar_title)).setText("Flight List");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        bind();
        PreviousData();
        //init();
        GetFlightList();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
     finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case android.R.id.home:
                onBackPressed();
              /*  Intent in=new Intent(mContext,MainActivity.class);
                startActivity(in);*//*
                finish();*/
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void PreviousData() {
        Bundle extraa=getIntent().getExtras();
        flight_from=extraa.getString(Constants.FLYFROM);
        flight_to=extraa.getString(Constants.FLYTO);
        departure_on=extraa.getString(Constants.DEPARTURE_ON);
        return_on=extraa.getString(Constants.RETURN_ON);
        adult=extraa.getString(Constants.ADULT);
        children=extraa.getString(Constants.CHILDRENS);
        infant=extraa.getString(Constants.INFANTS);
    }

    private void bind() {
        jObjectArrayList = new ArrayList<>();
        listv =(ListView)findViewById(R.id.listView);
        mhpadapter = new FlightListing_Adapter(mContext, R.layout.flight_listing_item, jObjectArrayList);
        listv.setAdapter(mhpadapter);
    }

    private void GetFlightList() {
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put(Constants.ACTION, Constants.FLIGHTSEARCHING);
            jsonObject.put("round_trip/oneway",1);
            jsonObject.put("flying_from",flight_from);
            jsonObject.put("flying_to",flight_to);
            jsonObject.put("depart_on",departure_on);
            jsonObject.put("return_on",return_on);
            jsonObject.put("adult",adult);
            jsonObject.put("children",children);
            jsonObject.put("infants",infant);


            Utility.ShowLoading(mContext, mContext.getResources().getString(R.string.please_wait));
            responseTask = new ResponseTask(Constant_Urls.SERVER_URL, jsonObject);
            responseTask.setListener(new ResponseListener() {
                @Override
                public void onGetPickSuccess(String result) {
                    Utility.HideDialog();
                    if (result == null) {
                        Utility.ShowToastMessage(mContext, "sever not responding");
                    } else{
                        try {
                            JSONObject jsonObject = new JSONObject(result);
                            if (jsonObject.getString(Constants.RESULT_KEY).equals("1")){
                                JSONArray jsonArray = jsonObject.getJSONArray("object");
                                for (int i = 0; i < jsonArray.length(); i++){
                                    jObjectArrayList.add(jsonArray.getJSONObject(i));
                                }
                            }
                        }catch (Exception e){
                            e.printStackTrace();
                        }
                        mhpadapter.notifyDataSetChanged();

                    }
                }
            });
        }catch (Exception e){
            e.printStackTrace();
        }
        responseTask.execute();
    }

    public void SelectedFlight(int selected_pos) {
        try {
            String sel_id=jObjectArrayList.get(selected_pos).getString("id");
            GetFlightDetail(sel_id);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void GetFlightDetail(String sel_id) {
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put(Constants.ACTION, Constants.FLIGHTDETAILBYID);
            //jsonObject.put("user_id", Utility.getSharedPreferences(mContext, Constants.USERID));
            jsonObject.put("flight_id", sel_id);
            Utility.ShowLoading(mContext, mContext.getResources().getString(R.string.please_wait));
            responseTask = new ResponseTask(Constant_Urls.SERVER_URL, jsonObject);
            responseTask.setListener(new ResponseListener() {
                @Override
                public void onGetPickSuccess(String result) {
                    Utility.HideDialog();
                    if (result == null) {
                        Utility.showCroutonWarn(getResources().getString(R.string.server_fail), FlightListing.this);
                    }
                    else {
                        try {
                            JSONObject jobj = new JSONObject(result);
                            if (jobj.getString(Constants.RESULT_KEY).equals("1")) {
                                JSONObject jasobj = jobj.getJSONObject("object");
                                Bundle extraa = new Bundle();
                                //extraa.putString(Constants.ID, sel_id);
                                extraa.putString(Constants.OBJECT, jasobj.toString());
                                Intent in = new Intent(mContext, SelectedFlightDetail.class);
                                in.putExtras(extraa);
                                System.out.println("GET value" + extraa);
                                startActivity(in);
                                // finish();

                            } else {
                                Utility.showCroutonWarn(jobj.getString(Constants.SERVER_MSG),FlightListing.this);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            });
        }catch (Exception e){
            e.printStackTrace();
        }
        responseTask.execute();
    }
}
