package com.h_plus.H_Booking.TripIT.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;

import com.h_plus.R;
import com.h_plus.RegistrationSection.LoginActivity;
import com.h_plus.customwidget.CustomTextView;
import com.h_plus.usertabs.utility.Constants;
import com.h_plus.usertabs.utility.Utility;
import com.rengwuxian.materialedittext.MaterialEditText;

/**
 * Created by Infograins on 02-Jan-17.
 */

public class MordgageSignupActivity extends AppCompatActivity implements View.OnClickListener {
    Context mContext;
 MaterialEditText company_name,company_add1,company_add2,city,state,zip,company_phone,company_fax,company_service,
         company_employe_no;
    private Toolbar mToolbar;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.mordgage_signup);
        mContext=this;
        mToolbar = (Toolbar) findViewById(R.id.h_toolbar);
        setSupportActionBar(mToolbar);
        ((CustomTextView) findViewById(R.id.toolbar_title)).setText("Car List");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        bind();
    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent in = new Intent(mContext, LoginActivity.class);
        startActivity(in);
        overridePendingTransition(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
        finish();

    }
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void bind() {
        company_name=(MaterialEditText)findViewById(R.id.company_name);
        company_add1=(MaterialEditText)findViewById(R.id.company_add1);
        company_add2=(MaterialEditText)findViewById(R.id.company_add2);
        city=(MaterialEditText)findViewById(R.id.city);
        state=(MaterialEditText)findViewById(R.id.state);
        zip=(MaterialEditText)findViewById(R.id.zip);
        company_phone=(MaterialEditText)findViewById(R.id.company_phone);
        company_fax=(MaterialEditText)findViewById(R.id.company_fax);
        company_service=(MaterialEditText)findViewById(R.id.company_service);
        company_employe_no=(MaterialEditText)findViewById(R.id.company_employe_no);
        (findViewById(R.id.btn_nxt)).setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if (Utility.isConnectingToInternet(mContext)){
       if (company_name.getText().toString().equals("")){
           company_name.setError("Please fill company name");
           company_name.requestFocus();
       }
       else if (company_add1.getText().toString().equals("")){
           company_add1.setError("Please fill Company Address");
           company_add1.requestFocus();
       } else if (company_add2.getText().toString().equals("")){
           company_add2.setError("Please fill Company Address");
           company_add2.requestFocus();
       } else if (city.getText().toString().equals("")){
           city.setError("Please fill City name");
           city.requestFocus();
       } else if (state.getText().toString().equals("")){
           state.setError("Please fill state");
           state.requestFocus();
       } else if (zip.getText().toString().equals("")){
           zip.setError("Please fill Zipcode");
           zip.requestFocus();
       } else if (company_phone.getText().toString().equals("")){
           company_phone.setError("Please fill Company phone number");
           company_phone.requestFocus();
       }else if (company_fax.getText().toString().equals("")){
           company_fax.setError("Please fill fax number");
           company_fax.requestFocus();
       }else if (company_service.getText().toString().equals("")){
           company_service.setError("Please fill service");
           company_service.requestFocus();
       }else if (company_employe_no.getText().toString().equals("")){
           company_employe_no.setError("Please fill number of employee");
           company_employe_no.requestFocus();
       }
       else {
           Intent in=new Intent(mContext,MordgageFinalsignup.class);
           Bundle databundle = new Bundle();
           databundle.putString(Constants.COMPANY_NAME,company_name.getText().toString());
           databundle.putString(Constants.COMPANY_ADD1,company_add1.getText().toString());
           databundle.putString(Constants.COMPANY_ADD2,company_add2.getText().toString());
           databundle.putString(Constants.CITY,city.getText().toString());
           databundle.putString(Constants.STATE,state.getText().toString());
           databundle.putString(Constants.ZIP,zip.getText().toString());
           databundle.putString(Constants.COMPANY_PHONE,company_phone.getText().toString());
           databundle.putString(Constants.COMPANY_FAX,company_fax.getText().toString());
           databundle.putString(Constants.COMPANY_SERVICE,company_service.getText().toString());
           databundle.putString(Constants.COMPANY_EMPLOYE_NO,company_employe_no.getText().toString());
           in.putExtras(databundle);
           System.out.println("parameter========"+databundle);
           startActivity(in);
       }
        }else {
            Utility.Alert(mContext, "please check your netwotk connection");
        }
    }
}
