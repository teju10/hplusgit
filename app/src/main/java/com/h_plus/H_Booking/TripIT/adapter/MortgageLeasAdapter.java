package com.h_plus.H_Booking.TripIT.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.h_plus.H_Booking.Model.Datamodel;
import com.h_plus.R;
import com.h_plus.customwidget.CustomTextView;

import java.util.List;

/**
 * Created by Infograins on 07-Jan-17.
 */
public class MortgageLeasAdapter extends RecyclerView.Adapter {
    LayoutInflater layoutInflater;
    Context mContext;
    List<Datamodel> arrayList;
    int res;
    public MortgageLeasAdapter(List<Datamodel> list, int mortgage_lead_item, Context mContext) {
        this.arrayList=list;
        this.res = mortgage_lead_item;
        this.mContext = mContext;
        layoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = layoutInflater.inflate(res, parent, false);
        return new MyViewHolder(v);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        try {
            ((MyViewHolder)holder).title.setText(arrayList.get(position).getTitle());
            ((MyViewHolder)holder).data_content.setText(arrayList.get(position).getGenre());
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    private class MyViewHolder extends RecyclerView.ViewHolder {
        CustomTextView title,data_content;
        public MyViewHolder(View itemView) {
            super(itemView);
            title=(CustomTextView)itemView.findViewById(R.id.title);
            data_content=(CustomTextView)itemView.findViewById(R.id._data_content);
        }
    }
}
