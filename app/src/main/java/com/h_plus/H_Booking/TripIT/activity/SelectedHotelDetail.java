package com.h_plus.H_Booking.TripIT.activity;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.h_plus.R;
import com.h_plus.customwidget.CustomButton;
import com.h_plus.customwidget.CustomTextView;
import com.h_plus.usertabs.utility.Constants;
import com.h_plus.usertabs.utility.Utility;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Infograins on 09-Jan-17.
 */
public class SelectedHotelDetail extends AppCompatActivity implements View.OnClickListener {
    Context mContext;
    ViewPager viewpager;
    ImageView img_hotel,img_frwrd;
   /* TextView text_count;
    RecyclerView recycleview;*/
    CustomTextView ctv_hotel_name,ctv_hotel_price,ctv_option,ctv_expedia,ctv_rate_per_night;
    CustomButton btn_reserve;
    private Toolbar mToolbar;
    String Hotel_id;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //in future use selected_hotel_detail layout if needed
        setContentView(R.layout.selected_hotel);
        mContext=this;
        mToolbar = (Toolbar) findViewById(R.id.h_toolbar);
        setSupportActionBar(mToolbar);
        ((CustomTextView) findViewById(R.id.toolbar_title)).setText("Hotel Booking");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        bind();
        SelectedHotelData();
    }
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    private void bind() {
        img_hotel=(ImageView)findViewById(R.id.img_hotel);
        ctv_hotel_name=(CustomTextView)findViewById(R.id.ctv_hotel_name);
        ctv_hotel_price=(CustomTextView)findViewById(R.id.ctv_hotel_price);
        ctv_option=(CustomTextView)findViewById(R.id.ctv_option);
        ctv_expedia=(CustomTextView)findViewById(R.id.ctv_expedia);
        ctv_rate_per_night=(CustomTextView)findViewById(R.id.ctv_rate_per_night);
        btn_reserve=(CustomButton) findViewById(R.id.btn_reserve);
        btn_reserve.setOnClickListener(this);

    }

    private void SelectedHotelData() {
        Bundle extraa=getIntent().getExtras();
        try {
            JSONObject j = new JSONObject(extraa.getString(Constants.OBJECT));
            ctv_hotel_name.setText(j.getString("name"));
            ctv_hotel_price.setText(j.getString("price"));
            ctv_option.setText(j.getString("options"));
            ctv_expedia.setText(j.getString("expedia"));
            ctv_rate_per_night.setText(j.getString("rate_per_night"));
            Hotel_id=j.getString("id");
            Glide.with(mContext).load(j.getString("image")).into(img_hotel);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onClick(View v) {
        Utility.showToast(mContext,"Coming Soon");
    }
}
