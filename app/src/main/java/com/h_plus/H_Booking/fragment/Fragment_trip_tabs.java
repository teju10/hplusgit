package com.h_plus.H_Booking.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.h_plus.R;

/*import com.hplus.R;
import com.hplus.usertabs.RentTab;
import com.hplus.usertabs.SaleTab;
import com.hplus.usertabs.VacationTab;*/


/**
 * Created by Infograins on 03-Dec-16.
 */

public class Fragment_trip_tabs extends Fragment {
    public static final String TAG="Fragment_trip_tabs";
    Context mContext;
    View rootView;
    Toolbar toolbar;
   public static TabLayout tablayout;
    public static ViewPager viewpager;
    public static int items=3;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater,  ViewGroup container, Bundle savedInstanceState) {
       rootView=inflater.inflate(R.layout.tab_layout,container,false);
        mContext=getActivity();
       bind();

        return rootView;
    }
    private void bind() {
        tablayout=(TabLayout)rootView.findViewById(R.id.tabs);
        viewpager=(ViewPager)rootView.findViewById(R.id.viewpager);
        viewpager.setAdapter(new MyAdapter(getChildFragmentManager()));
        tablayout.post(new Runnable() {
            @Override
            public void run() {
                tablayout.setupWithViewPager(viewpager);
            }
        });
        tablayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewpager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }


    private class MyAdapter extends FragmentPagerAdapter {


        public MyAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            switch (position){
                case 0:
                    return new SearchHotel();
                case 1:
                    return new Fragmnet_SearchFlight();
                case 2:
                    return new Fragment_SearchCar();

            }
            return null;
        }

        @Override
        public int getCount() {
            return items;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position){
                case 0:
                    return "Hotel";
                case 1:
                    return "Flight";
                case 2:
                    return "Car";
            }
            return null;
        }
    }
}
