package com.h_plus.H_Booking.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;

import com.h_plus.H_Booking.TripIT.activity.SelectedFlightDetail;
import com.h_plus.R;
import com.h_plus.customwidget.CustomButton;
import com.h_plus.customwidget.CustomTextView;
import com.h_plus.usertabs.utility.Constants;
import com.h_plus.usertabs.utility.Utility;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Infograins on 09-Jan-17.
 */

public class Fragment_FlightReview extends Fragment implements View.OnClickListener {
    View rootView;
    Context mContext;
    CustomTextView ctv_airline,flight_no,ctv_leaves,ctv_arrives,ctv_duration,ctv_class;
    CustomButton btn_continue,btn_price;
    CheckBox check_terms;
    String flight_id;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView=inflater.inflate(R.layout.selected_flight_review,container,false);
        mContext=getActivity();
        bind();
        GetFlightDetails();
        return rootView;
    }

    private void GetFlightDetails() {
        Bundle extraa=this.getArguments();
        try {
           JSONObject j = new JSONObject(extraa.getString(Constants.OBJECT));
            flight_id=(j.getString("id"));
            ctv_airline .setText(j.getString("flight_name"));
            flight_no.setText(j.getString("flight_no"));
            ctv_leaves.setText(j.getString("dept").substring(0,5));
            ctv_arrives.setText(j.getString("arrive").substring(0,5));
            ctv_duration.setText(j.getString("duration"));
            ctv_class.setText(j.getString("class"));
            btn_price.setText("$"+j.getString("price"));
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    private void bind() {
        ctv_airline=(CustomTextView)rootView.findViewById(R.id.ctv_airline);
        flight_no=(CustomTextView)rootView.findViewById(R.id.flight_no);
        ctv_leaves=(CustomTextView)rootView.findViewById(R.id.ctv_leaves);
        ctv_arrives=(CustomTextView)rootView.findViewById(R.id.ctv_arrives);
        ctv_duration=(CustomTextView)rootView.findViewById(R.id.ctv_duration);
        ctv_class=(CustomTextView)rootView.findViewById(R.id.ctv_class);
        check_terms=(CheckBox)rootView.findViewById(R.id.check_terms);
        btn_continue=(CustomButton)rootView.findViewById(R.id.btn_continue);
        btn_price=(CustomButton)rootView.findViewById(R.id.btn_price);
        btn_continue.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if (Utility.isConnectingToInternet(mContext)){
            if (!check_terms.isChecked()){
                Utility.showToast(mContext,"please checked the terms & conditions");
            }
            else {
                ((SelectedFlightDetail)mContext).SetTabs(1);
            }
        }else {
            Utility.showToast(mContext, "Please Check Your Internet Connection");
        }
    }
}
