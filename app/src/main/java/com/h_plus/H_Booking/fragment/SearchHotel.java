package com.h_plus.H_Booking.fragment;


import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Spinner;

import com.borax12.materialdaterangepicker.date.DatePickerDialog;
import com.h_plus.H_Booking.TripIT.activity.HotelListing;
import com.h_plus.H_Booking.TripIT.adapter.GooglePlacesAutocompleteAdapter;
import com.h_plus.R;
import com.h_plus.customwidget.CustomButton;
import com.h_plus.customwidget.CustomTextView;
import com.h_plus.usertabs.utility.Constants;
import com.h_plus.usertabs.utility.Utility;


import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by Infograins on 02-Jan-17.
 */

public class SearchHotel extends Fragment implements TextWatcher, View.OnClickListener, AdapterView.OnItemClickListener, DatePickerDialog.OnDateSetListener {
    AutoCompleteTextView auto_locatn;
    CustomTextView cet_check_in,cet_check_out;
    Spinner spin_room,spin_adult,spin_chindren;
    CustomButton btn_search_hotel;
    String str_location="",opendpd_for="";
    Context mContext;
    //ResponseTask responseTask;
   //ArrayList<JSONObject> jObjectArrayList;
    View rootView;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView=inflater.inflate(R.layout.hotel_booking_layout,container,false);
        mContext=getActivity();
        bind();
        init();
        return rootView;
    }

    private void bind() {
       // jObjectArrayList = new ArrayList<>();
        auto_locatn=(AutoCompleteTextView)rootView.findViewById(R.id.auto_locatn);
        cet_check_in=(CustomTextView)rootView.findViewById(R.id.cet_check_in);
        cet_check_out=(CustomTextView)rootView.findViewById(R.id.cet_check_out);
        spin_room=(Spinner)rootView.findViewById(R.id.spin_room);
        spin_adult=(Spinner)rootView.findViewById(R.id.spin_adult);
        spin_chindren=(Spinner)rootView.findViewById(R.id.spin_chindren);
        btn_search_hotel=(CustomButton)rootView.findViewById(R.id.btn_search_hotel);
        cet_check_out.setEnabled(false);
        cet_check_out.setClickable(false);

    }
    private void init() {
        auto_locatn.setAdapter(new GooglePlacesAutocompleteAdapter(getActivity(), R.layout.list_item));
        auto_locatn.setOnItemClickListener(this);
        auto_locatn.addTextChangedListener(this);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(mContext, android.R.layout.simple_spinner_dropdown_item, getResources().getStringArray(R.array.rooms));
        spin_room.setAdapter(adapter);
        ArrayAdapter<String> adapter1 = new ArrayAdapter<String>(mContext, android.R.layout.simple_spinner_dropdown_item, getResources().getStringArray(R.array.rooms));
        spin_adult.setAdapter(adapter1);
        ArrayAdapter<String> adapter2 = new ArrayAdapter<String>(mContext, android.R.layout.simple_spinner_dropdown_item, getResources().getStringArray(R.array.rooms));
        spin_chindren.setAdapter(adapter2);
        btn_search_hotel.setOnClickListener(this);
        cet_check_in.setOnClickListener(this);
        cet_check_out.setOnClickListener(this);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        str_location = (String) parent.getItemAtPosition(position);
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        str_location="";
    }

    @Override
    public void afterTextChanged(Editable s) {

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){

            case R.id.btn_search_hotel:
                if (Utility.isConnectingToInternet(mContext)){
                    if (str_location.equals("")){
                        Utility.showToast(mContext, "please enter valid Address");
                    }
                    else if (cet_check_in.getText().toString().equals("")){
                        Utility.showToast(mContext, "please enter check-in date");
                    }
                    else if (cet_check_out.getText().toString().equals("")){
                        Utility.showToast(mContext, "please enter check-out date");
                    }
                   else if (spin_room.getSelectedItemPosition()==0){
                   Utility.showToast(mContext,"Please select Number of Rooms");
                    }
                    else if (spin_adult.getSelectedItemPosition()==0){
                        Utility.showToast(mContext,"Please select Number of People");
                    }
                    else {
                        Intent in= new Intent(mContext,HotelListing.class);
                        Bundle databundle = new Bundle();
                        databundle.putString(Constants.DESTINATION,str_location);
                        databundle.putString(Constants.CHECKIN,cet_check_in.getText().toString());
                        databundle.putString(Constants.CHECKOUT,cet_check_out.getText().toString());
                        databundle.putString(Constants.ROOMS,spin_room.getSelectedItem().toString());
                        databundle.putString(Constants.ADULT,spin_adult.getSelectedItem().toString());
                        databundle.putString(Constants.CHILDRENS,spin_chindren.getSelectedItem().toString());
                        in.putExtras(databundle);
                        startActivity(in);
                       // GetHotelList();
                    }
                }
                else {
                    Utility.showToast(mContext,"Please Check your Internet connection");
                }
                break;
            case R.id.cet_check_in:
                opendpd_for = "start";
                choosefromdate();
                break;
            case R.id.cet_check_out:
                opendpd_for = "end";
                choosefromdate();
                break;
        }

    }

//    private void GetHotelList() {
//      try {
//          JSONObject jsonObject = new JSONObject();
//          jsonObject.put(Constants.ACTION, Constants.HOTELSEARCHING);
//          jsonObject.put("country",str_location);
//          jsonObject.put("check_in_time",cet_check_in.getText().toString());
//          jsonObject.put("check_out_time",cet_check_out.getText().toString());
//          jsonObject.put("room",spin_room.getSelectedItem().toString());
//          jsonObject.put("adult",spin_adult.getSelectedItem().toString());
//          jsonObject.put("children",spin_chindren.getSelectedItem().toString());
//
//          Utility.ShowLoading(mContext, mContext.getResources().getString(R.string.please_wait));
//          responseTask = new ResponseTask(Constant_Urls.SERVER_URL, jsonObject);
//          responseTask.setListener(new ResponseListener() {
//              @Override
//              public void onGetPickSuccess(String result) {
//                  Utility.HideDialog();
//                  if (result == null) {
//                      Utility.ShowToastMessage(mContext, "sever not responding");
//                  } else{
//                      try {
//                          JSONObject jsonObject = new JSONObject(result);
//                          if (jsonObject.getString(Constants.RESULT_KEY).equals("1")){
//                              JSONArray jsonArray = jsonObject.getJSONArray("object");
//                              for (int i = 0; i < jsonArray.length(); i++){
//                                  jObjectArrayList.add(jsonArray.getJSONObject(i));
//                              }
//                             /* System.out.println("arrayyy===="+jObjectArrayList);
//                              Bundle extraa = new Bundle();
//                             // extraa.putString(Constants.OBJECT,jObjectArrayList.toString());
//                             *//* Intent in= new Intent(mContext,HotelListing.class);
//                              in.putExtras(extraa);
//                              startActivity(in);*/
//                          }
//                      }catch (Exception e){
//                          e.printStackTrace();
//                      }
//
//                  }
//              }
//          });
//      }catch (Exception e){
//          e.printStackTrace();
//      }
//        responseTask.execute();
//    }

    private void choosefromdate() {
        // Launch Date Picker Dialog
        if (opendpd_for.equals("start")) {
            Calendar now = Calendar.getInstance();
           DatePickerDialog dpd = DatePickerDialog.newInstance(this,
                    now.get(Calendar.YEAR),
                    now.get(Calendar.MONTH),
                    now.get(Calendar.DAY_OF_MONTH)
            );
            dpd.setMinDate(now);
            dpd.setAccentColor(Color.parseColor("#e98d43"));
            dpd.show(getActivity().getFragmentManager(), "Datepickerdialog");

        } else {
            Calendar now = Calendar.getInstance();
           DatePickerDialog dpd = DatePickerDialog.newInstance(this,
                    now.get(Calendar.YEAR),
                    now.get(Calendar.MONTH),
                    now.get(Calendar.DAY_OF_MONTH)
            );
            String dateString = cet_check_in.getText().toString();
            SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
            Date convertedDate = new Date();
            try {
                convertedDate = dateFormat.parse(dateString);
                now.setTime(convertedDate);
            } catch (ParseException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            System.out.println(convertedDate.getTime());
            dpd.setMinDate(now);
            dpd.setAccentColor(Color.parseColor("#e98d43"));
            dpd.show(getActivity().getFragmentManager(), "Datepickerdialog");

        }
    }


    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth, int yearEnd, int monthOfYearEnd, int dayOfMonthEnd) {
        if (opendpd_for.equals("start")) {
            cet_check_in.setText(dayOfMonth + "-"
                    + (monthOfYear + 1) + "-" + year );
            cet_check_out.setEnabled(true);
            cet_check_out.setClickable(true);
        }
        else {
            cet_check_out.setText(dayOfMonth + "-"
                    + (monthOfYear + 1) + "-" + year);
        }
    }
}
