package com.h_plus.H_Booking.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.h_plus.H_Booking.TripIT.activity.SelectedFlightDetail;
import com.h_plus.R;
import com.h_plus.customwidget.CustomButton;
import com.h_plus.customwidget.CustomEditText;
import com.h_plus.usertabs.utility.Utility;

/**
 * Created by Infograins on 09-Jan-17.
 */

public class Fragment_FLightEmail extends Fragment implements View.OnClickListener {
    View rootView;
    Context mContext;
    CustomEditText cet_email;
    CustomButton btn_continue;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView=inflater.inflate(R.layout.email_address_flight,container,false);
        mContext=getActivity();
        bind();
        return rootView;
    }

    private void bind() {
        cet_email=(CustomEditText)rootView.findViewById(R.id.cet_email);
        btn_continue=(CustomButton)rootView.findViewById(R.id.btn_continue);
        btn_continue.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        if (Utility.isConnectingToInternet(mContext)){

       if (cet_email.getText().toString().equals("")){
           cet_email.setError("Please Fill Email");
           cet_email.requestFocus();
       }
       else if (!cet_email.getText().toString().contains("@")){
           cet_email.setError("Please Fill Correct Email");
           cet_email.requestFocus();
       }
        else {
           ((SelectedFlightDetail)mContext).SetTabs(2);
       }
        }
        else {
            Utility.showToast(mContext, "Please Check Your Internet Connection");
        }
    }
}
