package com.h_plus.H_Booking.fragment;


import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;

import com.borax12.materialdaterangepicker.date.DatePickerDialog;
import com.h_plus.H_Booking.TripIT.activity.FlightListing;
import com.h_plus.H_Booking.TripIT.adapter.GooglePlacesAutocompleteAdapter;
import com.h_plus.R;
import com.h_plus.customwidget.CustomButton;
import com.h_plus.customwidget.CustomTextView;

import com.h_plus.usertabs.utility.Constants;
import com.h_plus.usertabs.utility.Utility;


import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by Infograins on 04-Jan-17.
 */

public class Fragmnet_SearchFlight extends Fragment implements RadioGroup.OnCheckedChangeListener, View.OnClickListener, DatePickerDialog.OnDateSetListener {
    View rootView;
    Context mContext;
    RadioButton rb_one,rb_round;
    RadioGroup radio_grp;
    AutoCompleteTextView auto_from,auto_to;
    CustomTextView ctv_departure,ctv_return_on;
    LinearLayout hidelayout;
    Spinner spin_adult,spin_chindren,spin_infants;
    CustomButton btn_search_flight;
    String from="",too="",opendpd_for="";
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView=inflater.inflate(R.layout.flight_booking,container,false);
        mContext=getActivity();
        bind();
        init();
        return rootView;
    }

    private void bind() {
        radio_grp=(RadioGroup) rootView.findViewById(R.id.radio_grp);
        rb_one=(RadioButton)rootView.findViewById(R.id.rb_one);
        rb_round=(RadioButton)rootView.findViewById(R.id.rb_round);
        auto_from=(AutoCompleteTextView)rootView.findViewById(R.id.auto_from);
        auto_to=(AutoCompleteTextView)rootView.findViewById(R.id.auto_to);
        ctv_departure=(CustomTextView)rootView.findViewById(R.id.ctv_departure);
        ctv_return_on=(CustomTextView)rootView.findViewById(R.id.ctv_return_on);
        hidelayout=(LinearLayout)rootView.findViewById(R.id.hidelayout);
        spin_adult=(Spinner)rootView.findViewById(R.id.spin_adult);
        spin_chindren=(Spinner)rootView.findViewById(R.id.spin_chindren);
        spin_infants=(Spinner)rootView.findViewById(R.id.spin_infants);
        btn_search_flight=(CustomButton)rootView.findViewById(R.id.btn_search_flight);
        hidelayout.setVisibility(View.GONE);
        ctv_departure.setOnClickListener(this);
        ctv_return_on.setOnClickListener(this);
        btn_search_flight.setOnClickListener(this);
        ctv_return_on.setEnabled(false);
        ctv_return_on.setClickable(false);
    }

    private void init() {
        radio_grp.setOnCheckedChangeListener(this);
        auto_from.setAdapter(new GooglePlacesAutocompleteAdapter(getActivity(), R.layout.list_item));
        auto_from.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                from= (String) parent.getItemAtPosition(position);
            }
        });
        auto_from.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                from="";
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        auto_to.setAdapter(new GooglePlacesAutocompleteAdapter(getActivity(), R.layout.list_item));
        auto_to.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                too= (String) parent.getItemAtPosition(position);
            }
        });
        auto_to.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                too="";
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(mContext, android.R.layout.simple_spinner_dropdown_item, getResources().getStringArray(R.array.rooms));
        spin_adult.setAdapter(adapter);
        ArrayAdapter<String> adapter1 = new ArrayAdapter<String>(mContext, android.R.layout.simple_spinner_dropdown_item, getResources().getStringArray(R.array.rooms));
        spin_chindren.setAdapter(adapter1);
        ArrayAdapter<String> adapter2 = new ArrayAdapter<String>(mContext, android.R.layout.simple_spinner_dropdown_item, getResources().getStringArray(R.array.rooms));
        spin_infants.setAdapter(adapter2);
    }


    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId) {
        if (checkedId==R.id.rb_one){
            hidelayout.setVisibility(View.GONE);
            ctv_return_on.setText("");

        }
      else   if (checkedId==R.id.rb_round){
            hidelayout.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.ctv_departure:
                opendpd_for = "start";
                choosefromdate();
                break;
            case R.id.ctv_return_on:
                opendpd_for = "end";
                choosefromdate();
                break;
            case R.id.btn_search_flight:
                if (Utility.isConnectingToInternet(mContext)){
                    if (from.equals("")){
                        Utility.showToast(mContext, "please select origin city");
                    }
                    else if (too.equals("")){
                        Utility.showToast(mContext, "please select destination city");
                    }
                    else if (ctv_departure.getText().toString().equals("")){
                        Utility.showToast(mContext, "please select departure date");
                    }
                  /*  if (rb_round.isChecked()){
                         if (ctv_return_on.getText().toString().equals("")){
                            ctv_return_on.setError("Please select check-in date");
                            ctv_return_on.requestFocus();
                        }
                    }*/
                    else if (spin_adult.getSelectedItemPosition()==0){
                        Utility.showToast(mContext,"Please select Number of Adults");
                    }
                   else {
                        Intent in=new Intent(mContext, FlightListing.class);
                        Bundle databundle = new Bundle();
                        databundle.putString(Constants.FLYFROM,from);
                        databundle.putString(Constants.FLYTO,too);
                        databundle.putString(Constants.DEPARTURE_ON,ctv_departure.getText().toString());
                        databundle.putString(Constants.RETURN_ON,ctv_return_on.getText().toString());
                        databundle.putString(Constants.ADULT,spin_adult.getSelectedItem().toString());
                        databundle.putString(Constants.CHILDRENS,spin_chindren.getSelectedItem().toString());
                        databundle.putString(Constants.INFANTS,spin_infants.getSelectedItem().toString());
                        in.putExtras(databundle);
                        startActivity(in);
                    }


                }
                break;
        }
    }

  private void choosefromdate() {
      // Launch Date Picker Dialog
      if (opendpd_for.equals("start")) {
          Calendar now = Calendar.getInstance();
         DatePickerDialog dpd = DatePickerDialog.newInstance(this,
                  now.get(Calendar.YEAR),
                  now.get(Calendar.MONTH),
                  now.get(Calendar.DAY_OF_MONTH)
          );
          dpd.setMinDate(now);
          dpd.setAccentColor(Color.parseColor("#e98d43"));
          dpd.show(getActivity().getFragmentManager(), "Datepickerdialog");

      } else {
          Calendar now = Calendar.getInstance();
          DatePickerDialog dpd = DatePickerDialog.newInstance(this,
                  now.get(Calendar.YEAR),
                  now.get(Calendar.MONTH),
                  now.get(Calendar.DAY_OF_MONTH)
          );
          String dateString = ctv_departure.getText().toString();
          SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
          Date convertedDate = new Date();
          try {
              convertedDate = dateFormat.parse(dateString);
              now.setTime(convertedDate);
          } catch (ParseException e) {
              // TODO Auto-generated catch block
              e.printStackTrace();
          }
          System.out.println(convertedDate.getTime());
          dpd.setMinDate(now);
          dpd.setAccentColor(Color.parseColor("#e98d43"));
          dpd.show(getActivity().getFragmentManager(), "Datepickerdialog");

      }
  }

    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth, int yearEnd, int monthOfYearEnd, int dayOfMonthEnd) {
        if (opendpd_for.equals("start")) {
            ctv_departure.setText(dayOfMonth + "-"
                    + (monthOfYear + 1) + "-" + year );
            ctv_return_on.setEnabled(true);
            ctv_return_on.setClickable(true);
        }
        else {
            ctv_return_on.setText(dayOfMonth + "-"
                    + (monthOfYear + 1) + "-" + year);
        }
    }
}
/*
9522277794 manish*/
