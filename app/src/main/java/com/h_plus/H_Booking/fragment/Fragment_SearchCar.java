package com.h_plus.H_Booking.fragment;

import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.TimePicker;

import com.borax12.materialdaterangepicker.date.DatePickerDialog;
import com.h_plus.H_Booking.TripIT.activity.CarListing;
import com.h_plus.H_Booking.TripIT.adapter.GooglePlacesAutocompleteAdapter;
import com.h_plus.R;
import com.h_plus.customwidget.CustomButton;
import com.h_plus.customwidget.CustomTextView;

import com.h_plus.usertabs.utility.Constants;
import com.h_plus.usertabs.utility.Utility;


import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by Infograins on 04-Jan-17.
 */

public class Fragment_SearchCar extends Fragment implements View.OnClickListener ,DatePickerDialog.OnDateSetListener{
    Context mContext;
    View rootView;
    AutoCompleteTextView auto_pickup,auto_drop;
    CustomTextView ctv_pickup_date,ctv_drop_date,ctv_pick_up_time,ctv_drop_time;
    CustomButton btn_search_car;
    String pick_up="",drop_off="",opendpd_for,open_timepickr;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView=inflater.inflate(R.layout.search_car_layout,container,false);
        mContext=getActivity();
        bind();
        init();
        return rootView;
    }

    private void bind() {
        auto_pickup=(AutoCompleteTextView)rootView.findViewById(R.id.auto_pickup);
        auto_drop=(AutoCompleteTextView)rootView.findViewById(R.id.auto_drop);
        ctv_pickup_date=(CustomTextView)rootView.findViewById(R.id.ctv_pickup_date);
        ctv_drop_date=(CustomTextView)rootView.findViewById(R.id.ctv_drop_date);
        ctv_pick_up_time=(CustomTextView)rootView.findViewById(R.id.ctv_pick_up_time);
        ctv_drop_time=(CustomTextView)rootView.findViewById(R.id.ctv_drop_time);
        btn_search_car=(CustomButton)rootView.findViewById(R.id.btn_search_car);
        ctv_drop_date.setEnabled(false);
        ctv_drop_date.setClickable(false);
       /* auto_pickup.getText().clear();
        auto_drop.getText().clear();
        ctv_pickup_date.setText("");
        ctv_drop_date.setText("");
        ctv_pick_up_time.setText("");
        ctv_drop_time.setText("");*/
    }

    private void init() {
        ctv_pickup_date.setOnClickListener(this);
        ctv_drop_date.setOnClickListener(this);
        btn_search_car.setOnClickListener(this);
        ctv_pick_up_time.setOnClickListener(this);
        ctv_drop_time.setOnClickListener(this);
        auto_pickup.setAdapter(new GooglePlacesAutocompleteAdapter(getActivity(), R.layout.list_item));
        auto_pickup.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                pick_up= (String) parent.getItemAtPosition(position);
            }
        });
        auto_pickup.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                pick_up="";
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        auto_drop.setAdapter(new GooglePlacesAutocompleteAdapter(getActivity(), R.layout.list_item));
        auto_drop.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                drop_off= (String) parent.getItemAtPosition(position);
            }
        });
        auto_drop.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                drop_off="";
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.ctv_pickup_date:
                opendpd_for = "start";
                choosefromdate();
                break;
            case R.id.ctv_drop_date:
                opendpd_for = "end";
                choosefromdate();
                break;
            case R.id.ctv_pick_up_time:
                open_timepickr="start";
               ChooseTime();
                break;
            case R.id.ctv_drop_time:
                open_timepickr="end";
                ChooseTime();
                break;
            case R.id.btn_search_car:
                if (Utility.isConnectingToInternet(mContext)){
                    if (pick_up.equals("")){
                        Utility.showToast(mContext, "please enter pick-up Address");
                    }
                    else if (drop_off.equals("")){
                        Utility.showToast(mContext, "please enter drop-off Address");
                    }
                    else if (ctv_pickup_date.getText().toString().equals("")){
                        Utility.showToast(mContext, "please enter pick-up Date");
                    }else if (ctv_drop_date.getText().toString().equals("")){
                        Utility.showToast(mContext, "please enter drop-off Date");
                    }else if (ctv_pick_up_time.getText().toString().equals("")){
                        Utility.showToast(mContext, "please enter pick-up Time");
                    }else if (ctv_drop_time.getText().toString().equals("")){
                        Utility.showToast(mContext, "please enter drop-off Time");
                    }
                    else {
                        Intent in= new Intent(mContext,CarListing.class);
                        Bundle databundle = new Bundle();
                        databundle.putString(Constants.PICKUP,pick_up);
                        databundle.putString(Constants.DROPOFF,drop_off);
                        databundle.putString(Constants.PICKUP_DATE,ctv_pickup_date.getText().toString());
                        databundle.putString(Constants.DROPOFF_DATE,ctv_drop_date.getText().toString());
                        databundle.putString(Constants.PICKUP_TIME,ctv_pick_up_time.getText().toString());
                        databundle.putString(Constants.DROPOFF_TIME,ctv_drop_time.getText().toString());
                        in.putExtras(databundle);
                        startActivity(in);
                    }
                }
                else {
                    Utility.showToast(mContext,"Please Check your Internet connection");
                }

                break;
        }
    }

    private void ChooseTime() {
        Calendar mcurrentTime = Calendar.getInstance();
        int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
        int minute = mcurrentTime.get(Calendar.MINUTE);
        TimePickerDialog mTimePicker;
        if (open_timepickr.equals("start")){
            mTimePicker = new TimePickerDialog(getActivity(), new TimePickerDialog.OnTimeSetListener() {
                @Override
                public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                    ctv_pick_up_time.setText( selectedHour + ":" + selectedMinute);
                }
            }, hour, minute, true);//if true 24 hour time  //if false 12 hour time
            mTimePicker.setTitle("Select Time");
            mTimePicker.show();
        }
        else {
            mTimePicker = new TimePickerDialog(getActivity(), new TimePickerDialog.OnTimeSetListener() {
                @Override
                public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                    ctv_drop_time.setText( selectedHour + ":" + selectedMinute);
                }
            }, hour, minute, true);//if true 24 hour time  //if false 12 hour time
            mTimePicker.setTitle("Select Time");
            mTimePicker.show();
        }

      /*  *//*Method for change time format*//*
        public static String Time(String inputFormat,String outputFormat,String time) {
            SimpleDateFormat parseFormat = new SimpleDateFormat(inputFormat);
            SimpleDateFormat displayFormat = new SimpleDateFormat(outputFormat);
            String str = "";
            try {
                Date date = parseFormat.parse(time);
                str = displayFormat.format(date);
            } catch (ParseException e) {
// e.printStackTrace();
            }
            return str;
        }
        Utility.Time("hh:mm a", "HH:mm", hashMap.get(Constants.UserConstants.NAME));*/
    }


    private void choosefromdate() {
        // Launch Date Picker Dialog
        if (opendpd_for.equals("start")) {
            Calendar now = Calendar.getInstance();
            DatePickerDialog dpd = DatePickerDialog.newInstance(this,
                    now.get(Calendar.YEAR),
                    now.get(Calendar.MONTH),
                    now.get(Calendar.DAY_OF_MONTH)
            );
            dpd.setMinDate(now);
            dpd.setAccentColor(Color.parseColor("#e98d43"));
          dpd.show(getActivity().getFragmentManager(), "Datepickerdialog");

        } else {
            Calendar now = Calendar.getInstance();
            DatePickerDialog dpd = DatePickerDialog.newInstance(this,
                    now.get(Calendar.YEAR),
                    now.get(Calendar.MONTH),
                    now.get(Calendar.DAY_OF_MONTH)
            );
            String dateString = ctv_pickup_date.getText().toString();
            SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
            Date convertedDate = new Date();
            try {
                convertedDate = dateFormat.parse(dateString);
                now.setTime(convertedDate);
            } catch (ParseException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            System.out.println(convertedDate.getTime());
            dpd.setMinDate(now);
            dpd.setAccentColor(Color.parseColor("#e98d43"));
            dpd.show(getActivity().getFragmentManager(), "Datepickerdialog");

        }
    }

    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth, int yearEnd, int monthOfYearEnd, int dayOfMonthEnd) {
        if (opendpd_for.equals("start")) {
            ctv_pickup_date.setText(dayOfMonth + "-"
                    + (monthOfYear + 1) + "-" + year );
            ctv_drop_date.setEnabled(true);
            ctv_drop_date.setClickable(true);
        }
        else {
            ctv_drop_date.setText(dayOfMonth + "-"
                    + (monthOfYear + 1) + "-" + year);
        }
    }
}
