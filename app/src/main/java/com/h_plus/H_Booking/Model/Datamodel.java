package com.h_plus.H_Booking.Model;

/**
 * Created by Infograins on 07-Jan-17.
 */
public class Datamodel {
    private String title, genre;

    public Datamodel() {
    }

    public Datamodel(String title, String genre) {
        this.title = title;
        this.genre = genre;

    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String name) {
        this.title = name;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }
}
