package com.h_plus.userdrawer;

public class NavDrawerItem {
    int image;
    private boolean showNotify;
    private String title;

    public NavDrawerItem() {
    }

    public NavDrawerItem(boolean showNotify, String title, int img) {
        this.showNotify = showNotify;
        this.title = title;
        this.image = img;
    }

    public boolean isShowNotify() {
        return showNotify;
    }

    public void setShowNotify(boolean showNotify) {
        this.showNotify = showNotify;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int img) {
        this.image = img;
    }
}