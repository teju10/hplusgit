package com.h_plus;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.h_plus.usertabs.GS_Message;

import java.util.ArrayList;

/**
 * Created by Infograins on 12/22/2016.
 */

public class DBHelper extends SQLiteOpenHelper {

    public static final String DATABASE_NAME = "databaseHPlus";
    public static final String TABLE_NAME_USER = "user";
    public static final String TABLE_NAME_MESSAGE = "message";
    public static final String KEY_ID = "id";
    public static final String KEY_USER_ID = "user_id";
    public static final String KEY_PROJECTID = "project_id";
    public static final String KEY_NAME = "name";
    public static final String KEY_IMAGE = "image";
    public static final String KEY_SHORT_DESCRIPTION = "shortdescription";
    public static final String KEY_ONLINE_STATUS = "online_status";
    public static final String KEY_LAST_MESSAGE = "last_message";

    public static final String KEY_MSG_ID = "msg_id";
    public static final String KEY_MSG = "msg";
    public static final String KEY_MSG_TYPE = "msg_type";
    public static final String KEY_WORKER_ID = "friend_id";
    public static final String KEY_DELIVERY_DATE = "delivery_date";
    public static final String KEY_DELIVERY_TIME = "delivery_time";
    public static final String KEY_READ_STATUS = "read_status";
    public static final String KEY_STATUS = "status";
    public static final String CREATE_MESSAGE_TABLE = "CREATE TABLE IF NOT EXISTS "
            + TABLE_NAME_MESSAGE
            + " ( "
            + KEY_ID
            + " INTEGER PRIMARY KEY,"
            + KEY_USER_ID
            + " INTEGER NOT NULL,"
            + KEY_WORKER_ID
            + " INTEGER NOT NULL,"
            + KEY_MSG_ID
            + " INTEGER NOT NULL,"
            + KEY_MSG
            + " text , "
            + KEY_DELIVERY_DATE
            + " text ,"
            + KEY_DELIVERY_TIME
            + " text ,"
            + KEY_PROJECTID
            + " INTEGER ,"
            + KEY_READ_STATUS
            + " INTEGER ,"
            + KEY_STATUS
            + " INTEGER );";
    private static final String TAG = "Hplus_DBHelper";
    private static final int DATABASE_VERSION = 1;
    Context mContext;
    ArrayList<GS_Message> listMessage;
    ArrayList<GS_Message> listChatBetweenTwoUsersMessage;

    public DBHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        mContext = context;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        // Log.e(TAG, "TABLE_USER_TABLE ::" + CREATE_USER_TABLE);
        System.out.println("TABLE_MESSAGE_TABLE " + CREATE_MESSAGE_TABLE);
        Log.e(TAG, "TABLE_MESSAGE_TABLE :: " + CREATE_MESSAGE_TABLE);
        //  db.execSQL(CREATE_USER_TABLE);
        db.execSQL(CREATE_MESSAGE_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Log.w(TAG, "Upgrading database from version " + oldVersion + " to "
                + newVersion + ", which will destroy all old data");
        //  db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME_USER);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME_MESSAGE);
        onCreate(db);
    }

    public boolean insertMsgData(int _userid, int _friendid, int _msgid, String _msg,
                                 String _deliverydate, String _deliverytime, int _projectid, int _readstatus, int _status) {
        boolean createSuccessful = false;

        ContentValues values = new ContentValues();

        values.put(KEY_USER_ID, _userid);
        values.put(KEY_WORKER_ID, _friendid);
        values.put(KEY_MSG_ID, _msgid);
        values.put(KEY_MSG, _msg);
        values.put(KEY_DELIVERY_DATE, _deliverydate);
        values.put(KEY_DELIVERY_TIME, _deliverytime);
        values.put(KEY_PROJECTID, _projectid);
        values.put(KEY_READ_STATUS, _readstatus);
        values.put(KEY_STATUS, _status);

        SQLiteDatabase db = this.getWritableDatabase();

        createSuccessful = db.insert(TABLE_NAME_MESSAGE, null, values) > 0;
        db.close();
        return createSuccessful;
    }

    public ArrayList<GS_Message> getMessageData() {
        listMessage = new ArrayList<>();
        String selectQuery = "SELECT * FROM " + TABLE_NAME_MESSAGE;
        SQLiteDatabase db = this.getReadableDatabase();
        Log.e("select level Query", selectQuery);
        Cursor c = db.rawQuery(selectQuery, null);
        GS_Message message;
        while (c.moveToNext()) {
            message = new GS_Message();
            message.setId(c.getInt(0));
            message.setUserid(c.getInt(1));
            message.setWorker_id(c.getInt(2));
            message.setMsgid(c.getInt(3));
            message.setMsg(c.getString(4));
            message.setDate(c.getString(5));
            message.setTime(c.getString(6));
            message.setProjectid(c.getInt(7));
            message.setReadstatus(c.getInt(8));
            message.setStatus(c.getInt(9));
            listMessage.add(message);
            message = null;
        }
        c.close();
        c = null;
        db.close();
        return listMessage;
    }

    public ArrayList<GS_Message> getChatBetweenTwoUsersData(int _userid, int _friendid,int _projectid) {
        listChatBetweenTwoUsersMessage = new ArrayList<>();
      /*  String selectQuery = "SELECT * FROM " + TABLE_NAME_MESSAGE + " WHERE " + KEY_USER_ID + " = " + _userid +
                " AND " + KEY_WORKER_ID + " = " + _friendid + " AND " + KEY_PROJECTID + " = " + _projectid;
*/
        SQLiteDatabase db = this.getReadableDatabase();
//        Log.e("select level Query", selectQuery);
  //      Cursor c = db.rawQuery(selectQuery, null);
        Cursor c = db.query(TABLE_NAME_MESSAGE, new String[]{}, "(("
                        + KEY_USER_ID + "=" + _userid + " AND " + KEY_WORKER_ID
                        + "=" + _friendid + " AND " + KEY_PROJECTID + " = "+ _projectid + ")" + " OR " + "(" + KEY_USER_ID + "="
                        + _friendid + " AND " + KEY_WORKER_ID + "=" + _userid
                        + " AND " + KEY_PROJECTID + " = "+ _projectid + "))", null, null, null,
                null,null);
        GS_Message message;
        while (c.moveToNext()) {
            message = new GS_Message();

            message.setId(c.getInt(0));
            message.setUserid(c.getInt(1));
            message.setWorker_id(c.getInt(2));
            message.setMsgid(c.getInt(3));
            message.setMsg(c.getString(4));
            message.setDate(c.getString(5));
            message.setTime(c.getString(6));
            message.setProjectid(c.getInt(7));
            message.setReadstatus(c.getInt(8));
            message.setStatus(c.getInt(9));
            listChatBetweenTwoUsersMessage.add(message);
            message = null;
        }
        c.close();
        c = null;
        db.close();
        return listChatBetweenTwoUsersMessage;
    }

    public boolean isMsgPresent(int _msgid) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.query(TABLE_NAME_MESSAGE, new String[]{KEY_MSG_ID}, KEY_MSG_ID + " = " + _msgid,
                null, null, null, null);

        return cursor != null && cursor.moveToFirst();
    }

    public boolean insertUpdateMsg(int _userid, int _friendid, int _msgid,
                                   String _deliverydate, String _deliverytime, int _projectid,int _readstatus, int _status) {
        boolean createSuccessful = false;

        ContentValues values = new ContentValues();

        values.put(KEY_USER_ID, _userid);
        values.put(KEY_WORKER_ID, _friendid);
        values.put(KEY_MSG_ID, _msgid);
        values.put(KEY_DELIVERY_DATE, _deliverydate);
        values.put(KEY_DELIVERY_TIME, _deliverytime);
        values.put(KEY_PROJECTID, _projectid);
        values.put(KEY_READ_STATUS, _readstatus);
        values.put(KEY_STATUS, _status);
        SQLiteDatabase db = this.getWritableDatabase();

        if (isMsgPresent(_msgid)) {
            createSuccessful = db.update(TABLE_NAME_MESSAGE, values, KEY_MSG_ID + " = " + _msgid, null) > 0;
            Log.e("insertUpdateMsg ", "db.update === " + createSuccessful);
        } else {
            createSuccessful = db.insert(TABLE_NAME_MESSAGE, null, values) > 0;
            Log.e("insertUpdateMsg ", "db.insert === " + createSuccessful);
        }
        db.close();
        return createSuccessful;
    }

    public boolean insertUpdateMsgData(int _userid, int _friendid, int _msgid, String _msg,
                                       String _deliverydate, String _deliverytime, int _projectid, int _readstatus, int _status) {
        boolean createSuccessful = false;

        ContentValues values = new ContentValues();

        values.put(KEY_USER_ID, _userid);
        values.put(KEY_WORKER_ID, _friendid);
        values.put(KEY_MSG_ID, _msgid);
        values.put(KEY_MSG, _msg);
        values.put(KEY_DELIVERY_DATE, _deliverydate);
        values.put(KEY_DELIVERY_TIME, _deliverytime);
        values.put(KEY_PROJECTID, _projectid);
        values.put(KEY_READ_STATUS, _readstatus);
        values.put(KEY_STATUS, _status);
        SQLiteDatabase db = this.getWritableDatabase();

        if (isMsgPresent(_msgid)) {
            createSuccessful = db.update(TABLE_NAME_MESSAGE, values, KEY_MSG_ID + " = " + _msgid, null) > 0;
            Log.e("insertUpdateMsgData", "db.update === " + createSuccessful);
        } else {
            createSuccessful = db.insert(TABLE_NAME_MESSAGE, null, values) > 0;
            Log.e("insertUpdateMsgData", "db.insert === " + createSuccessful);
        }
        db.close();
        return createSuccessful;
    }

}
