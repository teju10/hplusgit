package com.h_plus.HP_Client;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.h_plus.R;
import com.h_plus.customwidget.CustomTextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Infograins on 1/19/2017.
 */

public class Hire_ViewPortfolioDetail_Activity extends AppCompatActivity {
    Context mContext;
    Bundle b;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.hire_viewportfoliodetail_ac);

        mContext = this;

        setSupportActionBar((Toolbar) findViewById(R.id.h_toolbar));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        ((CustomTextView) findViewById(R.id.toolbar_title)).setText("View Portfolio");

        INIT();

    }

    public void INIT() {
        b = new Bundle();
        b = getIntent().getExtras();
        String Portpolio_JObj = b.getString("port_jarray");
        System.out.println("Portpolio_JObj          " + Portpolio_JObj);
        try {
            JSONObject jo = new JSONObject(Portpolio_JObj);
            if (jo.getString("type").equals("image")) {
                Glide.with(mContext).load(jo.getString("file")).into(((ImageView) findViewById(R.id.port_screen)));
            } else {
                Glide.with(mContext).load(R.drawable.blank_user).into(((ImageView) findViewById(R.id.port_screen)));
            }
            ((CustomTextView) findViewById(R.id.showport_title)).setText(jo.getString("title"));
            ((CustomTextView) findViewById(R.id.showport_desc)).setText(jo.getString("desc"));

            JSONArray portfolio_jsonArray = jo.getJSONArray("skill");

            String Skills = "";
            for (int jj = 0; jj < portfolio_jsonArray.length(); jj++) {
                JSONObject port_json = portfolio_jsonArray.getJSONObject(jj);
                Skills = Skills + ", " + port_json.getString("name");
            }
            ((CustomTextView) findViewById(R.id.showport_skills)).setText(Skills.substring(1));

        } catch (JSONException j) {
            j.printStackTrace();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
