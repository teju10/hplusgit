package com.h_plus.HP_Client;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.h_plus.R;
import com.h_plus.customwidget.CircleProgress.CircleProgress;
import com.h_plus.customwidget.CustomTextView;

import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by Infograins on 12/30/2016.
 */

public class ActivityProfile_CircleProgress extends AppCompatActivity {
    Context mContext;
    Bundle b;
    private CircleProgress circleProgress;
    private Timer timer;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profilereview_progresscircle);
        mContext = this;

        setSupportActionBar((Toolbar) findViewById(R.id.h_toolbar));
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        ((CustomTextView) findViewById(R.id.toolbar_title)).setText("Trust Score");

        circleProgress = (CircleProgress) findViewById(R.id.circle_progress);


        b = new Bundle();
        b = getIntent().getExtras();
        System.out.println("CIRCLE TRUST SCORE " + b.getString("TrustScore"));
        int progressV = Integer.parseInt(b.getString("TrustScore"));
        circleProgress.setProgress(progressV);
        Init();
    }

    public void Init() {
        timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        boolean a = false;
                       /* if (a) {
                            ObjectAnimator anim = ObjectAnimator.ofInt(circleProgress, "progress", 0, 10);
                            anim.setInterpolator(new DecelerateInterpolator());
                            anim.setDuration(1000);
                            anim.start();
                        } else {
                            AnimatorSet set = (AnimatorSet) AnimatorInflater.loadAnimator(ActivityProfile_CircleProgress.this, R.animator.progress_anim);
                            set.setInterpolator(new DecelerateInterpolator());
                            set.setTarget(circleProgress);
                            set.start();
                        }*/
                    }
                });
            }
        }, 0, 2000);


    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        timer.cancel();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menu) {

        switch (menu.getItemId()) {

            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(menu);

        }
    }
}
