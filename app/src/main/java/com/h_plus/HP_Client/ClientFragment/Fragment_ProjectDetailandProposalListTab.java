package com.h_plus.HP_Client.ClientFragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.h_plus.R;
import com.h_plus.usertabs.utility.Constants;
import com.h_plus.usertabs.utility.Utility;

/**
 * Created by Infograins on 12/10/2016.
 */

public class Fragment_ProjectDetailandProposalListTab extends Fragment{
    Context mContext;
    public static TabLayout tabLayout;
    public static ViewPager viewPager;
    public static int int_items = 2;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        /**
         *Inflate tab_layout and setup Views.
         */
        mContext = getActivity();
        View x = inflater.inflate(R.layout.tab_layout, null);
        tabLayout = (TabLayout) x.findViewById(R.id.tabs);
        viewPager = (ViewPager) x.findViewById(R.id.viewpager);

        /**
         *Set an Adapter for the View Pager
         */
        viewPager.setAdapter(new MyAdapter(getChildFragmentManager()));

        /**
         * Now , this is a workaround ,
         * The setupWithViewPager dose't works without the runnable .
         * Maybe a Support Library Bug .
         */

        tabLayout.post(new Runnable() {
            @Override
            public void run() {
                tabLayout.setupWithViewPager(viewPager);
            }
        });

        Utility.setSharedPreference(mContext, Constants.BACKPRESSEDNAME,Constants.Hire_Home_MyProject_Fragment);
        return x;
    }

    public class MyAdapter extends FragmentPagerAdapter {
        public MyAdapter(FragmentManager fm) {
            super(fm);
        }

        /**
         * Return fragment with respect to Position .
         */

        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0:
                    return new Fragment_Client_ProjectDetail();
                case 1:
                    return new Fragment_Freelancer_Biding_List();

               /* case 3:
                    return new ScheduledBookingTab();
                case 4:
                    return new PostaProjectTab();*/
            }
            return null;
        }

        @Override
        public int getCount() {
            return int_items;
        }

        /**
         * This method returns the title of the tab according to the position.
         */

        @Override
        public CharSequence getPageTitle(int position) {

            switch (position) {
                case 0:
                    return "Project Detail";
                case 1:
                    return "Freelancers bidding";
                /*case 2:
                    return "Price";*/
                /*case 3:
                    return "Scheduled Booking";
                case 4:
                    return "Post a Project";*/
            }
            return null;
        }
    }


}
