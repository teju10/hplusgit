package com.h_plus.HP_Client.ClientFragment;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import com.h_plus.R;
import com.h_plus.ServerInteraction.ResponseListener;
import com.h_plus.ServerInteraction.ResponseTask;
import com.h_plus.customwidget.CustomTextView;
import com.h_plus.usertabs.utility.Constant_Urls;
import com.h_plus.usertabs.utility.Constants;
import com.h_plus.usertabs.utility.Utility;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by Infograins on 12/5/2016.
 */

public class Fragment_Client_ProjectDetail extends Fragment implements View.OnClickListener {
    View rootView;
    Context mContext;
    ResponseTask rt;
    RecyclerView hp_skillsrequired;
    ArrayList<String> skills = new ArrayList<>();
    HorizontalListAdapter horizontalListAdapter;
    Bundle b;
    // CustomButton bid_on_project_btn, free_lancer_bidlist;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_client_project_detail, container, false);
        mContext = getActivity();
/*
        bid_on_project_btn = (CustomButton) rootView.findViewById(bid_on_project_btn);
        free_lancer_bidlist = (CustomButton) rootView.findViewById(free_lancer_bidlist);*/

       /* bid_on_project_btn.setOnClickListener(this);
        free_lancer_bidlist.setOnClickListener(this);*/
        Init();

        return rootView;
    }


    public void Init() {

        LinearLayoutManager horizontalLayoutManagaer = new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false);
        hp_skillsrequired = (RecyclerView) rootView.findViewById(R.id.hp_skillsrequired);
        hp_skillsrequired.setLayoutManager(horizontalLayoutManagaer);
        horizontalListAdapter = new HorizontalListAdapter(mContext, R.layout.horizontal_skills_required_adapter, skills);
        hp_skillsrequired.setAdapter(horizontalListAdapter);
        if (Utility.isConnectingToInternet(mContext)) {
            GetProjectDesription(Utility.getSharedPreferences(mContext, Constants.PROJECTID));
        } else {
            Utility.Alert(mContext, "please check your network connection");
        }
    }

    public void GetProjectDesription(String project_id) {
        try {
            JSONObject jobj = new JSONObject();
            jobj.put(Constants.ACTION, Constants.Project_DetailBY_ID);
            jobj.put("project_id", project_id);

            Utility.ShowLoading(mContext, mContext.getResources().getString(R.string.please_wait));

            rt = new ResponseTask(Constant_Urls.SERVER_URL, jobj);
            rt.setListener(new ResponseListener() {
                @Override
                public void onGetPickSuccess(String result) {
                    Utility.HideDialog();

                    System.out.println("Server result ====> " + result);
                    if (result == null) {

                    } else {
                        try {
                            JSONObject jobj = new JSONObject(result);
                            if (jobj.getString(Constants.RESULT_KEY).equals("1")) {

                                Utility.setSharedPreference(mContext, Constants.PROJECT_TITLE, jobj.getJSONObject("object").getString("project_name"));
                                ((CustomTextView) rootView.findViewById(R.id.pd_title)).setText(jobj.getJSONObject("object").getString("project_name"));
                                ((CustomTextView) rootView.findViewById(R.id.hp_desc)).setText(jobj.getJSONObject("object").getString("description"));
                                ((CustomTextView) rootView.findViewById(R.id.pd_bid_count)).setText(jobj.getJSONObject("object").getString("bid_count"));
                                ((CustomTextView) rootView.findViewById(R.id.pd_avg_bid)).setText(jobj.getJSONObject("object").getString("average_bid"));
                                if (jobj.getJSONObject("object").getString("number_of_hours").equals("0")) {
                                    ((LinearLayout) rootView.findViewById(R.id.hours_of_work_layout)).setVisibility(View.GONE);
                                } else {
                                    ((CustomTextView) rootView.findViewById(R.id.pd_hours_ofwork)).setText(jobj.getJSONObject("object").getString("number_of_hours"));
                                }
                                ((CustomTextView) rootView.findViewById(R.id.pd_completion_time)).setText(jobj.getJSONObject("object").getString("project_duration") + " days");
                                ((CustomTextView) rootView.findViewById(R.id.currency_txt)).
                                        setText("Avg Bid" + "(" + jobj.getJSONObject("object").getString("currency") + ")");

                                ((CustomTextView) rootView.findViewById(R.id.projct_budget_type)).
                                        setText("Project Budget" + "(" + jobj.getJSONObject("object").getString("currency") + ")");

                                ((CustomTextView) rootView.findViewById(R.id.pd_project_budget)).setText((jobj.getJSONObject("object").getString("currency_symbol") +
                                        jobj.getJSONObject("object").getString("min_budget") + "-" +
                                        jobj.getJSONObject("object").getString("currency_symbol") +
                                        jobj.getJSONObject("object").getString("max_budget")));

                                ((CustomTextView) rootView.findViewById(R.id.pd_time_left)).setText((jobj.getJSONObject("object").getString("days_left")) + " days" + "," +
                                        (jobj.getJSONObject("object").getString("hours_left")) + " hours left");

                                ((CustomTextView) rootView.findViewById(R.id.pd_bid_count)).setText(jobj.getJSONObject("object").getString("bid_count"));
                                ((CustomTextView) rootView.findViewById(R.id.pd_client_name)).setText(jobj.getJSONObject("object").getString("client_name"));

                                Utility.setSharedPreference(mContext, Constants.PROJECT_ID, jobj.getJSONObject("object").getString("id"));
                                ((CustomTextView) rootView.findViewById(R.id.pd_project_id)).setText(jobj.getJSONObject("object").getString("id"));

                                skills.addAll(Arrays.asList(jobj.getJSONObject("object").getString("skill").split(",")));
                                horizontalListAdapter.notifyDataSetChanged();

                                if (jobj.getJSONObject("object").getString("payment_verification").equals("1")) {
                                    ((ImageView) rootView.findViewById(R.id.dollar_veri_img)).setVisibility(View.VISIBLE);
                                } else if (jobj.getJSONObject("object").getString("payment_verification").equals("0")) {
                                    ((ImageView) rootView.findViewById(R.id.dollar_veri_img)).
                                            setBackgroundDrawable(getResources().getDrawable(R.drawable.dollar_veri_black));
                                }

                                if (jobj.getJSONObject("object").getString("email_verification").equals("1")) {
                                    ((ImageView) rootView.findViewById(R.id.email_veri_img)).setVisibility(View.VISIBLE);
                                } else if (jobj.getJSONObject("object").getString("email_verification").equals("0")) {
                                    ((ImageView) rootView.findViewById(R.id.email_veri_img)).
                                            setBackgroundDrawable(getResources().getDrawable(R.drawable.email_veri_black));
                                }

                                if (jobj.getJSONObject("object").getString("user_verification").equals("1")) {
                                    ((ImageView) rootView.findViewById(R.id.user_veri_img)).setVisibility(View.VISIBLE);
                                } else if (jobj.getJSONObject("object").getString("user_verification").equals("0")) {
                                    ((ImageView) rootView.findViewById(R.id.user_veri_img)).
                                            setBackgroundDrawable(getResources().getDrawable(R.drawable.user_veri_black));
                                }

                                if (jobj.getJSONObject("object").getString("phone_verification").equals("1")) {
                                    ((ImageView) rootView.findViewById(R.id.call_veri_img)).setVisibility(View.VISIBLE);
                                } else if (jobj.getJSONObject("object").getString("phone_verification").equals("0")) {
                                    ((ImageView) rootView.findViewById(R.id.call_veri_img)).
                                            setBackgroundDrawable(getResources().getDrawable(R.drawable.phone_veri_black));
                                }

                            } else {
                                Utility.showCroutonWarn(jobj.getString(Constants.SERVER_MSG), getActivity());
                            }
                        } catch (JSONException e2) {
                            Utility.showCroutonWarn(getResources().getString(R.string.server_fail), getActivity());
                        }
                    }
                }
            });

        } catch (JSONException e) {
            e.printStackTrace();
        }
        rt.execute();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

           /* case R.id.bid_on_project_btn:
                startActivity(new Intent(mContext, DoBidingScreen_Activity.class));

                break;*/

        }
    }

    class HorizontalListAdapter extends RecyclerView.Adapter<HorizontalListAdapter.ViewHolder> {

        Context mContext;
        int res;
        ArrayList<String> skillsArrayList;

        public HorizontalListAdapter(Context context, int resourse, ArrayList<String> imageList) {
            this.mContext = context;
            this.res = resourse;
            this.skillsArrayList = imageList;
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View convertView = LayoutInflater.from(parent.getContext()).inflate(res, parent, false);
            return new ViewHolder(convertView);
        }

        @Override
        public void onBindViewHolder(final ViewHolder holder, int position) {

            holder.adapter_skill_txtview.setText(skillsArrayList.get(position));
        }


        @Override
        public int getItemCount() {
            return skillsArrayList.size();
        }

        class ViewHolder extends RecyclerView.ViewHolder {

            CustomTextView adapter_skill_txtview;
            ProgressBar progress_bar;

            public ViewHolder(View itemView) {
                super(itemView);
                adapter_skill_txtview = (CustomTextView) itemView.findViewById(R.id.adapter_skill_txtview);
            }
        }
    }
}
