package com.h_plus.HP_Client.ClientFragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.h_plus.R;
import com.h_plus.ServerInteraction.ResponseTask;

import org.json.JSONObject;

import java.util.LinkedList;


/**
 * Created by Infograins on 1/6/2017.
 */

public class HireServiceProviderHomeFragment extends Fragment {
    View rooView;
    Fragment fragment;
    ResponseTask rt;
    Context mContext;
    LinkedList<JSONObject> serviceprovider_linkedlist = new LinkedList<>();

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rooView = inflater.inflate(R.layout.fragment_hire_service_home, container, false);
        mContext = getActivity();
        changeFragment(0);
        return rooView;
    }


    public void changeFragment(int pos) {
        switch (pos) {
            case 0:
                fragment = new Fragment_HiringServiceProviderList(this);
                break;
            case 1:
                fragment = new Fragment_View_spdetail_Map(this);
                break;
        }
        if (fragment != null) {
            FragmentManager fragmentManager = getChildFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.container_body, fragment);
            fragmentTransaction.commit();
        }
    }
}
