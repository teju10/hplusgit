package com.h_plus.HP_Client.ClientFragment;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.h_plus.R;
import com.h_plus.ServerInteraction.ResponseListener;
import com.h_plus.ServerInteraction.ResponseTask;
import com.h_plus.customwidget.CustomTextView;
import com.h_plus.customwidget.LoadMoreListView;
import com.h_plus.useradapter.Adapter_MyWorkingProjectList;
import com.h_plus.usertabs.utility.Constant_Urls;
import com.h_plus.usertabs.utility.Constants;
import com.h_plus.usertabs.utility.Utility;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.LinkedList;

/**
 * Created by Muzammil on 7/15/2016.
 */
public class Hire_MyWorkingProject extends Fragment {

    View rootView;
    Context mContext;
    ResponseTask rt;
    Adapter_MyWorkingProjectList adapter_mwproject;
    LinkedList<JSONObject> arraylist_mwproject = new LinkedList<>();
    int index = 0;
    LoadMoreListView mwproject_list;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.bid_workingproject_list, container, false);

        mContext = getActivity();
        Find();
        Init();
        return rootView;
    }

    private void Find() {
        mwproject_list = (LoadMoreListView) rootView.findViewById(R.id.mwproject_list);
    }

    private void Init() {
        adapter_mwproject = new Adapter_MyWorkingProjectList(mContext, R.layout.sp_adapter_awardedproject_fargment, arraylist_mwproject);
        mwproject_list.setAdapter(adapter_mwproject);
        GetMyWorkingProjectList(index);

        mwproject_list.setOnLoadMoreListener(new LoadMoreListView.OnLoadMoreListener() {
            @Override
            public void onLoadMore() {
                index = index + 1;
                GetMyWorkingProjectList(index);
            }
        });
    }

    public void GetMyWorkingProjectList(int page_index) {
        /*https://infograins.com/INFO01/homesplus/api/api.php?action=CurrentProject&user_id=261*/
        try {
            final JSONObject jo = new JSONObject();
            jo.put(Constants.ACTION, Constants.SERVICEPROVIDER_CURRENTPROJECT);
            jo.put("user_id", Utility.getSharedPreferences(mContext, Constants.USERID));
            jo.put("page", page_index);

            if (index == 0) {
                //  Utility.ShowLoading(mContext, getResources().getString(R.string.please_wait));
            }
            rt = new ResponseTask(Constant_Urls.SERVER_URL, jo);
            rt.setListener(new ResponseListener() {
                @Override
                public void onGetPickSuccess(String result) {
                    if (index == 0) {
                        //  Utility.HideDialog();
                    }
                    if (result == null) {
                        Utility.ShowToastMessage(mContext, "sever not responding");
                    } else {
                        try {
                            JSONObject jsonObject = new JSONObject(result);
                            if (jsonObject.getString("success").equals("1")) {
                                JSONArray jsonArray = jsonObject.getJSONArray("object");
                                Log.e("MYPROJECT", "result ===============> " + jsonArray.toString());
                                for (int i = 0; i < jsonArray.length(); i++) {
                                    arraylist_mwproject.add(jsonArray.getJSONObject(i));
                                }
                                adapter_mwproject.notifyDataSetChanged();
                                if (index == 0) {
                                } else {
                                    mwproject_list.onLoadMoreComplete();/*record not found !*/
                                }
                            } else {
                                if ((index == 0) && jsonObject.getString("msg").equals("record not found !")) {
                                    mwproject_list.setVisibility(View.GONE);
                                    ((CustomTextView) rootView.findViewById(R.id.else_txt)).setVisibility(View.VISIBLE);
//                                    Utility.ShowToastMessage(mContext, "You have no more working project available");
                                } else {
                                    // Utility.HideDialog();
                                    Utility.ShowToastMessage(mContext, "You have no more working project available");
                                    mwproject_list.onLoadMoreComplete();
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
            });
            rt.execute();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

}
