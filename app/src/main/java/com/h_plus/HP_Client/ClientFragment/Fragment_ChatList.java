package com.h_plus.HP_Client.ClientFragment;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;

import com.bumptech.glide.Glide;
import com.h_plus.HP_Service_Provider.adapter.AdapterSP_Bidlist;
import com.h_plus.R;
import com.h_plus.ServerInteraction.ResponseListener;
import com.h_plus.ServerInteraction.ResponseTask;
import com.h_plus.customwidget.CircleImageView;
import com.h_plus.customwidget.CustomButton;
import com.h_plus.customwidget.CustomTextView;
import com.h_plus.useractivity.ChattingScreen_Activity;
import com.h_plus.usertabs.utility.Constant_Urls;
import com.h_plus.usertabs.utility.Constants;
import com.h_plus.usertabs.utility.Utility;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.TreeSet;

/**
 * Created by Infograins on 12/24/2016.
 */

public class Fragment_ChatList extends Fragment {

    private static final int TYPE_ITEM = 0;
    private static final int TYPE_SEPARATOR = 1;
    Context appContext;
    ResponseTask rt;
    ChatBoxuserListAdapter listAdapter;
    View rootView;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        appContext = getActivity();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.chatbox_list_view, container, false);
        appContext = getActivity();
        Inti();
        return rootView;
    }

    private void Inti() {
        Getchatlist();
    }

    public void Getchatlist() {

        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put(Constants.ACTION, Constants.CHATBOXLIST);
            jsonObject.put(Constants.ACTION, "newchatList");
            //jsonObject.put("user_id", Utility.getSharedPreferences(appContext, Constants.USERID));
            jsonObject.put("user_id", Utility.getSharedPreferences(appContext,Constants.USERID));
            Utility.ShowLoading(appContext, appContext.getResources().getString(R.string.please_wait));
            rt = new ResponseTask(Constant_Urls.SERVER_URL, jsonObject);
            rt.setListener(new ResponseListener() {
                @Override
                public void onGetPickSuccess(String result) {
                    Utility.HideDialog();
                    System.out.println("server Result=====>" + result);
                    if (result == null) {
                        Utility.showCroutonWarn(getResources().getString(R.string.server_fail), getActivity());
                    } else {
                        try {
                            JSONObject jobj = new JSONObject(result);
                            if (jobj.getString(Constants.RESULT_KEY).equals("1")) {
                                listAdapter = new ChatBoxuserListAdapter(appContext);
                                HashMap<String, String> hm;
                                JSONArray jsonArray = jobj.getJSONArray("object");
                                for (int i = 0; i < jsonArray.length(); i++) {
                                    hm = new HashMap<String, String>();
                                    hm.put("project_id", jsonArray.getJSONObject(i).getString("project_id"));
                                    hm.put("project_name", jsonArray.getJSONObject(i).getString("project_name"));
                                    listAdapter.addSectionHeaderItem(hm);
                                    JSONArray jsonArray1 = jsonArray.getJSONObject(i).getJSONArray("client");
                                    for (int j = 0; j < jsonArray1.length(); j++) {
                                        hm = new HashMap<String, String>();
                                        hm.put("project_id", jsonArray1.getJSONObject(j).getString("project_id"));
                                        hm.put("other_user_id", jsonArray1.getJSONObject(j).getString("other_id"));
                                        hm.put("other_name", jsonArray1.getJSONObject(j).getString("other_name"));
                                        hm.put("image", jsonArray1.getJSONObject(j).getString("other_image"));
                                        listAdapter.addItem(hm);
                                    }
                                }

                                ((ListView) rootView.findViewById(R.id.chat_box_list)).setAdapter(listAdapter);
                            } else {
                                Utility.showCroutonWarn(jobj.getString(Constants.SERVER_MSG), getActivity());
                            }
                        } catch (JSONException e2) {
                            e2.printStackTrace();
                            Utility.showCroutonWarn(getResources().getString(R.string.server_fail), getActivity());
                        }
                    }
                }
            });
            rt.execute();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public class ChatBoxuserListAdapter extends BaseAdapter {

        Context mContext;
        ArrayList<HashMap<String, String>> list = new ArrayList<>();
        int[] img = {
                R.drawable.purple_patti,
                R.drawable.red_patti,
                R.drawable.blue_patti};
        private TreeSet<Integer> sectionHeader = new TreeSet<Integer>();

        public ChatBoxuserListAdapter(Context appContext) {
            this.mContext = appContext;
        }

        public void addItem(HashMap<String, String> item) {
            list.add(item);
            notifyDataSetChanged();
        }

        public void addSectionHeaderItem(HashMap<String, String> item) {
            list.add(item);
            sectionHeader.add(list.size() - 1);
            notifyDataSetChanged();
        }

        @Override
        public int getItemViewType(int position) {
            return sectionHeader.contains(position) ? TYPE_SEPARATOR : TYPE_ITEM;
        }

        @Override
        public int getViewTypeCount() {
            return 2;
        }

        @Override
        public int getCount() {
            return list.size();
        }

        @Override
        public Object getItem(int position) {
            return 0;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            final ViewHolder viewHolder;
            int rowType = getItemViewType(position);
            LayoutInflater li = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            if (convertView == null) {
                viewHolder = new ViewHolder();
                switch (rowType) {
                    case TYPE_SEPARATOR:
                        convertView = li.inflate(R.layout.hire_snippetchatlist_headeradapter, null);
                        viewHolder.chatlist_header = (CustomTextView) convertView.findViewById(R.id.chatlist_header);
                        viewHolder.chatlist_header.setText(list.get(position).get("project_name"));
                        break;
                    case TYPE_ITEM:
                        convertView = li.inflate(R.layout.adapter_chatbox_list_item, null);
                        viewHolder.chat_userdetail = (CustomTextView) convertView.findViewById(R.id.chat_userdetail);
                        viewHolder.chat_username = (CustomTextView) convertView.findViewById(R.id.chat_username);
                        viewHolder.chat_clientimg = (CircleImageView) convertView.findViewById(R.id.chat_clientimg);
                        viewHolder.chat_username.setText(list.get(position).get("other_name"));
                        Glide.with(mContext).load(list.get(position).get("image")).into(viewHolder.chat_clientimg);
                        convertView.setOnClickListener(new MyClick(position));
                        break;
                }
                viewHolder.chatbox_list_main = (LinearLayout) convertView.findViewById(R.id.chatbox_list_main);
                convertView.setTag(viewHolder);
            } else {
                viewHolder = (ViewHolder) convertView.getTag();
            }
            switch (rowType) {
                case TYPE_SEPARATOR:
                    viewHolder.chatlist_header.setText(list.get(position).get("project_name"));
                    break;
                case TYPE_ITEM:
                    viewHolder.chat_username.setText(list.get(position).get("other_name"));
                    break;
            }
            return convertView;
        }

        class MyClick implements View.OnClickListener {
            int position;

            public MyClick(int mposition) {
                position = mposition;
            }

            @Override
            public void onClick(View v) {
                try {
                    Bundle b=new Bundle();
                    Intent i = new Intent(mContext, ChattingScreen_Activity.class);
                    b.putString("project_id",list.get(position).get("project_id"));
                    b.putString("other_user_id",list.get(position).get("other_user_id"));
                    b.putString("image",list.get(position).get("image"));
                    b.putString("other_name",list.get(position).get("other_name"));
                   i.putExtras(b);
                    System.out.println("PHLE BUNDLE     "+b+list.get(position).get("project_id"));
                    startActivity(i);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }

        public class ViewHolder {

            CustomTextView chat_username, chat_userdetail, chatlist_header;
            com.h_plus.customwidget.CircleImageView chat_clientimg;
            LinearLayout chatbox_list_main;

        }
    }

}
