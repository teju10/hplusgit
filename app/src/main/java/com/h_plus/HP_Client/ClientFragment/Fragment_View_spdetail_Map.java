package com.h_plus.HP_Client.ClientFragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.h_plus.R;
import com.h_plus.usertabs.utility.Constants;

/**
 * Created by Infograins on 1/5/2017.
 */

public class Fragment_View_spdetail_Map extends Fragment implements OnMapReadyCallback {
    HireServiceProviderHomeFragment hireServiceProviderHomeFragment;
    View rooView;
    Context mContext;
    GoogleMap gMap;
    SupportMapFragment mapFragment;

    public Fragment_View_spdetail_Map(HireServiceProviderHomeFragment hireServiceProviderHomeFragment) {
        this.hireServiceProviderHomeFragment = hireServiceProviderHomeFragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        rooView = inflater.inflate(R.layout.hire_show_sp_map, container, false);
        mContext = getActivity();
        initilizeMap();
        return rooView;
    }

    private void initilizeMap() {
        mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.map_menu, menu);
        menu.getItem(1).setVisible(false);
        menu.getItem(0).setVisible(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_sp_list:
                hireServiceProviderHomeFragment.changeFragment(0);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        gMap = googleMap;
        googleMap.setMyLocationEnabled(true);
        googleMap.getUiSettings().setZoomControlsEnabled(true);
        googleMap.getUiSettings().setMyLocationButtonEnabled(true);
        googleMap.getUiSettings().setCompassEnabled(true);
        googleMap.getUiSettings().setRotateGesturesEnabled(true);
        googleMap.getUiSettings().setZoomGesturesEnabled(true);
        Add_Marker_On_Map();
    }

    public void Add_Marker_On_Map() {
        for (int i = 0; i < hireServiceProviderHomeFragment.serviceprovider_linkedlist.size(); i++) {
            try {
                // Drawing marker on the map
                LatLng point = new LatLng(Double.parseDouble(hireServiceProviderHomeFragment.serviceprovider_linkedlist.get(i).getString(Constants.MAP_LAT)), Double.parseDouble(hireServiceProviderHomeFragment.serviceprovider_linkedlist.get(i).getString(Constants.MAP_LONG)));
                MarkerOptions markerOptions = new MarkerOptions();
                markerOptions.position(point);
                markerOptions.title(hireServiceProviderHomeFragment.serviceprovider_linkedlist.get(i).getString(Constants.MAP_NAME));
//                markerOptions.snippet(hireServiceProviderHomeFragment.serviceprovider_linkedlist.get(i).getString(Constants.MapConstants.PICK_AT_TIME));
                gMap.addMarker(markerOptions);
                if (i == hireServiceProviderHomeFragment.serviceprovider_linkedlist.size() - 1) {
                    // Moving CameraPosition to last clicked position
                    gMap.moveCamera(CameraUpdateFactory.newLatLng(point));
                    // Setting the zoom level in the map on last position  is clicked
                    gMap.animateCamera(CameraUpdateFactory.zoomTo(13.0f));
//                    googleMap.setInfoWindowAdapter(new myInfoWindow());
//                    gMap.setOnInfoWindowClickListener(this);
                }
            } catch (Exception e) {
            }
        }

    }
}
