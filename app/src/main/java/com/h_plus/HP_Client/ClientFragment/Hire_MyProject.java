package com.h_plus.HP_Client.ClientFragment;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.h_plus.R;
import com.h_plus.ServerInteraction.ResponseListener;
import com.h_plus.ServerInteraction.ResponseTask;
import com.h_plus.useradapter.MyProjectAdapter;
import com.h_plus.usertabs.utility.Constant_Urls;
import com.h_plus.usertabs.utility.Constants;
import com.h_plus.usertabs.utility.Utility;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Tejs on 11/30/2016.
 */
public class Hire_MyProject extends Fragment {
    View rootView;
    ResponseTask rt;
    Context mContext;
    ArrayList<JSONObject> mypro_arraylist = new ArrayList<>();
    int index = 0;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.myproject_list_layout, container, false);
        mContext = getActivity();

        Utility.setSharedPreference(mContext,Constants.BACKPRESSEDNAME,Constants.MYPROJECT);
        GetMyProjectList();

        return rootView;
    }

    public void GetMyProjectList() {
        try {
            JSONObject jo = new JSONObject();
            jo.put(Constants.ACTION, Constants.MYPROJECT);
            jo.put("user_id", Utility.getSharedPreferences(mContext, Constants.USERID));

            Utility.ShowLoading(mContext, mContext.getResources().getString(R.string.please_wait));
            rt = new ResponseTask(Constant_Urls.SERVER_URL, jo);
            rt.setListener(new ResponseListener() {
                               @Override
                               public void onGetPickSuccess(String result) {
                                   Utility.HideDialog();
                                   if (result == null) {
                                       Utility.ShowToastMessage(mContext, "sever not responding");

                                   } else {
                                       try {
                                           JSONObject jsonObject = new JSONObject(result);
                                           if (jsonObject.getString("success").equals("1")) {
                                               JSONArray jsonArray = jsonObject.getJSONArray("object");
                                               mypro_arraylist = new ArrayList<>();
                                               Log.e("MYPROJECT", "result ===============> " + jsonArray.toString());
                                               for (int i = 0; i < jsonArray.length(); i++) {
                                                   mypro_arraylist.add(jsonArray.getJSONObject(i));
                                               }
                                               ((ListView) rootView.findViewById(R.id.myproject_list)).setAdapter
                                                       (new MyProjectAdapter(mContext, R.layout.hire_adapter_myproject, mypro_arraylist));
                                           } else if (jsonObject.getString("msg").equals("No Data Found !")) {

                                               Utility.showPromptDlgForSuccess(mContext, "You do not have any project!", "");
                                           }
                                       } catch (Exception e) {
                                           e.printStackTrace();
                                       }
                                   }
                               }
                           }
            );
        } catch (JSONException e) {
            e.printStackTrace();
        }
        rt.execute();
    }
}
