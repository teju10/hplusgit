package com.h_plus.HP_Client.ClientFragment;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.h_plus.R;
import com.h_plus.ServerInteraction.ResponseListener;
import com.h_plus.ServerInteraction.ResponseTask;
import com.h_plus.customwidget.LoadMoreListView;
import com.h_plus.useradapter.FreeLencerBidingAdapter;
import com.h_plus.usertabs.utility.Constant_Urls;
import com.h_plus.usertabs.utility.Constants;
import com.h_plus.usertabs.utility.Utility;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.LinkedList;

/**
 * Created by Infograins on 1/4/2017.
 */

public class Fragment_HiringServiceProviderList extends Fragment {
    HireServiceProviderHomeFragment hireServiceProviderHomeFragment;
    View rooView;
    Context mContext;
    ResponseTask rt;
    FreeLencerBidingAdapter adapter;
    LinkedList<JSONObject> serviceprovider_linkedlist = new LinkedList<>();
    int index = 0;
    LoadMoreListView search_service_provider_list;
    public Fragment_HiringServiceProviderList(HireServiceProviderHomeFragment hireServiceProviderHomeFragment){
       this.hireServiceProviderHomeFragment=hireServiceProviderHomeFragment;
    }
    @Override
    public void onCreate(Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        super.onCreate(savedInstanceState);
    }
    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.map_menu, menu);
        menu.getItem(0).setVisible(false);
        menu.getItem(1).setVisible(true);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.menu_map_icon:
                hireServiceProviderHomeFragment.changeFragment(1);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        rooView = inflater.inflate(R.layout.hire_search_serviceproviderlist, container, false);
        mContext = getActivity();

        search_service_provider_list = (LoadMoreListView) rooView.findViewById(R.id.search_service_provider_list);

        Init();

        return rooView;
    }

    public void Init() {
        adapter = new FreeLencerBidingAdapter(mContext, R.layout.hire_adapter_freelancer_bid_list, serviceprovider_linkedlist);
        search_service_provider_list.setAdapter(adapter);
        GetFreeLancerBid_List(index);
        search_service_provider_list.setOnLoadMoreListener(new LoadMoreListView.OnLoadMoreListener() {
            @Override
            public void onLoadMore() {
                index = index + 1;
                GetFreeLancerBid_List(index);
            }
        });
    }

    public void GetFreeLancerBid_List(int page) {
        try {
            JSONObject jo = new JSONObject();
            jo.put(Constants.ACTION, Constants.HIRESIDE_LISTOFSERVICEPROVIDER);
            jo.put("page", page);
            if(index==0) {
                Utility.ShowLoading(mContext, mContext.getResources().getString(R.string.please_wait));
            }
            rt = new ResponseTask(Constant_Urls.SERVER_URL, jo);
            rt.setListener(new ResponseListener() {
                               @Override
                               public void onGetPickSuccess(String result) {
                                   Utility.HideDialog();
                                   if (result == null) {
                                       Utility.ShowToastMessage(mContext, "sever not responding");
                                   } else {
                                       try {
                                           JSONObject jsonObject = new JSONObject(result);
                                           if (jsonObject.getString("success").equals("1")) {
                                               JSONArray jsonArray = jsonObject.getJSONArray("object");
                                               Log.e("MYPROJECT", "result ===============> " + jsonArray.toString());
                                               for (int i = 0; i < jsonArray.length(); i++) {
                                                   serviceprovider_linkedlist.add(jsonArray.getJSONObject(i));
                                               }
                                               adapter.notifyDataSetChanged();
                                               if (index == 0) {
                                               } else {
                                                   search_service_provider_list.onLoadMoreComplete();/*record not found !*/
                                               }
                                               hireServiceProviderHomeFragment.serviceprovider_linkedlist.clear();
                                               hireServiceProviderHomeFragment.serviceprovider_linkedlist.addAll(serviceprovider_linkedlist);

                                           } else {
                                               if (index == 0) {
                                                   Utility.ShowToastMessage(mContext, "You have no more Service provider are available");

                                               } else {
                                                   Utility.ShowToastMessage(mContext, "You have no more Service provider are available");

                                               }
                                           }

                                       } catch (JSONException e) {
                                           e.printStackTrace();
                                       }
                                   }
                               }
                           }
            );
        } catch (JSONException e) {
            e.printStackTrace();
        }
        rt.execute();
    }
}
