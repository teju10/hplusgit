package com.h_plus.HP_Client.ClientFragment;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.OrientationHelper;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.SimpleAdapter;
import android.widget.Spinner;

import com.h_plus.R;
import com.h_plus.RegistrationSection.MainActivity;
import com.h_plus.ServerInteraction.ResponseListener;
import com.h_plus.ServerInteraction.ResponseTask;
import com.h_plus.customwidget.CustomButton;

import com.h_plus.customwidget.CustomCheckBox;
import com.h_plus.customwidget.CustomEditText;
import com.h_plus.customwidget.CustomRadioButton;
import com.h_plus.customwidget.CustomTextView;
import com.h_plus.useractivity.MainActivityPermissionsDispatcher;
import com.h_plus.useractivity.PostSponsored_Activity;
import com.h_plus.userdocpicker.ImageAdapter;
import com.h_plus.usertabs.utility.Constant_Urls;
import com.h_plus.usertabs.utility.Constants;
import com.h_plus.usertabs.utility.MultipartUtility;
import com.h_plus.usertabs.utility.Utility;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;

import droidninja.filepicker.FilePickerBuilder;
import droidninja.filepicker.FilePickerConst;
import permissions.dispatcher.NeedsPermission;

/**
 * Created by Tejs on 11/21/2016.
 */

public class Fragment_PostProject extends Fragment implements View.OnClickListener {
    // public ArrayList<String> filePathsnewquestion;
    public ArrayList<String> filePaths = new ArrayList<>();
    public int maxcount = 5;
    Context mContext;
    View rooView;
    ResponseTask rt;
    ArrayList<HashMap<String, String>> currency_spinner, min_budgetspinner, max_budgetspinner, tow_arraylist;
    String[] currency_array, min_str_array, max_str_array, select_time_str_array;
    String currency_str, min_budget_str, max_budget_str, type_of_work, Price_Type = "fixed", i_am_not_sure = "0";
    Spinner currency/*, min_price, max_price*/, select_week_or_month;
    String ServiceProviderId = "0";
    private int MAX_ATTACHMENT_COUNT = 5;
    private ArrayList<String> docPaths = new ArrayList<>();
    private ArrayList<String> photoPaths = new ArrayList<>();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rooView = inflater.inflate(R.layout.hire_fragment_postproject, container, false);
        mContext = getActivity();
        BINDING();
        Init();
        return rooView;
    }

    public void BINDING() {

        (rooView.findViewById(R.id.post_project)).setOnClickListener(this);
        (rooView.findViewById(R.id.select_file)).setOnClickListener(this);
        (rooView.findViewById(R.id.select_image)).setOnClickListener(this);

        currency = (Spinner) rooView.findViewById(R.id.currency);
     /*   min_price = (Spinner) rooView.findViewById(R.id.min_price);
        max_price = (Spinner) rooView.findViewById(max_price);*/
        select_week_or_month = (Spinner) rooView.findViewById(R.id.select_week_or_month);

        currency_array = mContext.getResources().getStringArray(R.array.country_dollar);
        min_str_array = mContext.getResources().getStringArray(R.array.price);
        max_str_array = mContext.getResources().getStringArray(R.array.price);
        select_time_str_array = mContext.getResources().getStringArray(R.array.types_of_work);

    }

    public void Init() {

        if (!Utility.getSharedPreferences(mContext, Constants.SPID).equals("") &&
                !Utility.getSharedPreferences(mContext, Constants.SPID).equals(null)) {
            ServiceProviderId = Utility.getSharedPreferences(mContext, Constants.SPID);
            Log.e("*******POSTPROJECTFRAGMENT "+"Sericeproviderid----------********",ServiceProviderId);
        }
        else{
            ServiceProviderId = "0";
        }

        if (Utility.getSharedPreferences(mContext, Constants.EDITPROJECTBTNCLICK).equals("91")) {

        } else if (Utility.getSharedPreferences(mContext, Constants.EDITPROJECTBTNCLICK).equals("92")) {
            GetProjectDetail(Utility.getSharedPreferences(mContext, Constants.UPDATE_PROJECTID));
            ((CustomButton) rooView.findViewById(R.id.post_project)).setText("Update This Project");
        }

        getcurrency_spinner();
        tow_spinner();
        // get_min_price_spinner();
        //get_max_price_spinner();

        ((RadioGroup) rooView.findViewById(R.id.select_price_group_rb)).setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (checkedId == R.id.set_hourlyrate) {
                    Price_Type = "hourly";
                    ((CustomTextView) rooView.findViewById(R.id.gone_tv)).setVisibility(View.VISIBLE);
                    ((LinearLayout) rooView.findViewById(R.id.hourly_rate_layout)).setVisibility(View.VISIBLE);

                } else if (checkedId == R.id.set_fixedprice) {
                    Price_Type = "fixed";
                    ((CustomTextView) rooView.findViewById(R.id.gone_tv)).setVisibility(View.GONE);
                    ((LinearLayout) rooView.findViewById(R.id.hourly_rate_layout)).setVisibility(View.GONE);
                }
                System.out.println("PRICE_TYPE " + Price_Type);
            }
        });

        ((CustomCheckBox) rooView.findViewById(R.id.cb_notsure)).setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    i_am_not_sure = "1";
                } else {
                    i_am_not_sure = "0";
                }
                System.out.println("IAMNOTSURE " + i_am_not_sure);
            }
        });

        (rooView.findViewById(R.id.post_project)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String actionString = "", projectid = "";
                if (((CustomButton) rooView.findViewById(R.id.post_project)).getText().toString().equals("Update This Project")) {
                    actionString = Constants.UPDATE_PROJECT;
                    projectid = Utility.getSharedPreferences(mContext, Constants.UPDATE_PROJECTID);
                } else {
                    actionString = Constants.POSTPROJECT;
                    projectid = "";
                }
                if (((CustomEditText) rooView.findViewById(R.id.p_type)).getText().toString().equals("")) {
                    // ((CustomEditText) rooView.findViewById(R.id.p_type)).setHintTextColor(getResources().getColor(R.color.red));
                    Utility.showToast(mContext, "Project type is missing");

                } else if (((CustomEditText) rooView.findViewById(R.id.p_title)).getText().toString().equals("")) {
                    //  ((CustomEditText) rooView.findViewById(R.id.p_title)).setHintTextColor(getResources().getColor(R.color.red));
                    Utility.showToast(mContext, "Project title is missing");

                } else if (((CustomEditText) rooView.findViewById(R.id.min_price)).getText().toString().equals("")) {
                    //  ((CustomEditText) rooView.findViewById(R.id.p_title)).setHintTextColor(getResources().getColor(R.color.red));
                    Utility.showToast(mContext, "please enter minimum budget of your project");

                } else if (((CustomEditText) rooView.findViewById(R.id.max_price)).getText().toString().equals("")) {
                    //  ((CustomEditText) rooView.findViewById(R.id.p_title)).setHintTextColor(getResources().getColor(R.color.red));
                    Utility.showToast(mContext, "please enter maximum budget of your project");

                } else if (((CustomEditText) rooView.findViewById(R.id.skills_name)).getText().toString().equals("")) {
                    //  ((CustomEditText) rooView.findViewById(R.id.p_title)).setHintTextColor(getResources().getColor(R.color.red));
                    Utility.showToast(mContext, "please enter skills of your project");

                } else if (((CustomEditText) rooView.findViewById(R.id.time_duration)).getText().toString().equals("")) {
                    //((CustomEditText) rooView.findViewById(R.id.time_duration)).setHintTextColor(getResources().getColor(R.color.red));
                    Utility.showToast(mContext, "please provide your time duration");

                } else if (((CustomEditText) rooView.findViewById(R.id.project_desc)).getText().toString().equals("")) {
                    //   ((CustomEditText) rooView.findViewById(R.id.project_desc)).setHintTextColor(getResources().getColor(R.color.red));
                    Utility.showToast(mContext, "provide your description");

                } else {
                    if (Price_Type.equals("fixed")) {
                        new PostProjectTask().execute(((CustomEditText) rooView.findViewById(R.id.p_type)).getText().toString(),
                                ((CustomEditText) rooView.findViewById(R.id.p_title)).getText().toString(),
                                ((CustomEditText) rooView.findViewById(R.id.skills_name)).getText().toString(),
                                ((CustomEditText) rooView.findViewById(R.id.project_desc)).getText().toString(), currency_str,
                                ((CustomEditText) rooView.findViewById(R.id.min_price)).getText().toString(),
                                ((CustomEditText) rooView.findViewById(R.id.max_price)).getText().toString(),
                                ((CustomEditText) rooView.findViewById(R.id.time_duration)).getText().toString(), "1", Price_Type,
                                "", "", "", actionString, projectid,ServiceProviderId);
                        System.out.println("HIRE_First "+ServiceProviderId);
                    } else if (Price_Type.equals("hourly")) {
                        new PostProjectTask().execute(((CustomEditText) rooView.findViewById(R.id.p_type)).getText().toString(),
                                ((CustomEditText) rooView.findViewById(R.id.p_title)).getText().toString(),
                                ((CustomEditText) rooView.findViewById(R.id.skills_name)).getText().toString(),
                                ((CustomEditText) rooView.findViewById(R.id.project_desc)).getText().toString(), currency_str,
                                ((CustomEditText) rooView.findViewById(R.id.min_price)).getText().toString(),
                                ((CustomEditText) rooView.findViewById(R.id.max_price)).getText().toString(),
                                ((CustomEditText) rooView.findViewById(R.id.time_duration)).getText().toString(), "1", Price_Type,
                                ((CustomEditText) rooView.findViewById(R.id.hours_of_work)).getText().toString(),
                                type_of_work,
                                i_am_not_sure, actionString, projectid,ServiceProviderId);
                    }
                }
            }
        });

    }

    public void getcurrency_spinner() {
        currency_spinner = new ArrayList<>();
        for (int i = 0; i < currency_array.length; i++) {
            HashMap<String, String> hm = new HashMap<>();
            hm.put("Type", currency_array[i]);
            currency_spinner.add(hm);
        }
        int[] to = new int[]{R.id.s_item};
        String[] from = new String[]{"Type"};
        // Creating a SimpleAdapter for the Spinner
        SimpleAdapter adapter = new SimpleAdapter(mContext, currency_spinner, R.layout.text_single_spinner, from, to);
        currency.setAdapter(adapter);
        currency.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                currency_str = currency_spinner.get(position).get("Type").toString();
                System.out.println("currency----->>>>>" + currency_str);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    public void tow_spinner() {
        tow_arraylist = new ArrayList<>();
        for (int i = 0; i < select_time_str_array.length; i++) {
            HashMap<String, String> hm = new HashMap<>();
            hm.put("Work_Type", select_time_str_array[i]);
            tow_arraylist.add(hm);
        }
        int[] to = new int[]{R.id.s_item};
        String[] from = new String[]{"Work_Type"};
        // Creating a SimpleAdapter for the Spinner
        SimpleAdapter adapter = new SimpleAdapter(mContext, tow_arraylist, R.layout.text_single_spinner, from, to);
        select_week_or_month.setAdapter(adapter);
        select_week_or_month.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                type_of_work = tow_arraylist.get(position).get("Work_Type").toString();
                System.out.println("WORK----->>>>>" + type_of_work);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.select_file:
                //Utility.showToast(mContext, "coming soon");
                 onPickDoc();
                break;
            case R.id.select_image:
                onPickPhoto();
                break;

        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        System.out.println("requestCode " + requestCode);
        System.out.println("resultCode " + resultCode);
        switch (requestCode) {
            case FilePickerConst.REQUEST_CODE_PHOTO:
                if (resultCode == Activity.RESULT_OK && data != null) {
                    photoPaths = new ArrayList<>();
                    photoPaths.addAll(data.getStringArrayListExtra(FilePickerConst.KEY_SELECTED_PHOTOS));
                    System.out.println("filePathsimg " + photoPaths);
                    maxcount = 5;
                    filePaths = photoPaths;
                    maxcount = maxcount - filePaths.size();
                }
                break;

            case FilePickerConst.REQUEST_CODE_DOC:
                if (resultCode == Activity.RESULT_OK && data != null) {
                    docPaths = new ArrayList<>();
                    docPaths.addAll(data.getStringArrayListExtra(FilePickerConst.KEY_SELECTED_DOCS));
                    maxcount = 5;
                    System.out.println("filePathsDoc " + docPaths);
                    filePaths = docPaths;
                    maxcount = maxcount - filePaths.size();

                }
                break;
        }
        addThemToView(photoPaths, docPaths);
    }

    private void addThemToView(ArrayList<String> imagePaths, ArrayList<String> docPaths) {
        ArrayList<String> filePaths = new ArrayList<>();
        if (imagePaths != null)
            filePaths.addAll(imagePaths);

        if (docPaths != null)
            filePaths.addAll(docPaths);

        RecyclerView recyclerView = (RecyclerView) rooView.findViewById(R.id.recyclerview);
        if (recyclerView != null) {
            StaggeredGridLayoutManager layoutManager = new StaggeredGridLayoutManager(3, OrientationHelper.VERTICAL);
            layoutManager.setGapStrategy(StaggeredGridLayoutManager.GAP_HANDLING_MOVE_ITEMS_BETWEEN_SPANS);
            recyclerView.setLayoutManager(layoutManager);

            ImageAdapter imageAdapter = new ImageAdapter(getActivity(), filePaths);

            recyclerView.setAdapter(imageAdapter);
            recyclerView.setItemAnimator(new DefaultItemAnimator());
        }

        //  Toast.makeText(getActivity(), "Num of files selected: " + filePaths.size(), Toast.LENGTH_SHORT).show();
    }

    @NeedsPermission({Manifest.permission.WRITE_EXTERNAL_STORAGE})
    public void onPickPhoto() {
        //int maxCount = MAX_ATTACHMENT_COUNT - docPaths.size();
        if ((docPaths.size() + photoPaths.size()) == MAX_ATTACHMENT_COUNT) {
            // Toast.makeText(getActivity(), "Cannot select more than " + MAX_ATTACHMENT_COUNT + " items", Toast.LENGTH_SHORT).show();
        } else {
            FilePickerBuilder.getInstance().setMaxCount(maxcount).setSelectedFiles(photoPaths)
                    .setActivityTheme(R.style.FilePickerTheme).
                    pickPhoto(Fragment_PostProject.this);
        }
    }

    @NeedsPermission({Manifest.permission.WRITE_EXTERNAL_STORAGE})
    public void onPickDoc() {
        // int maxCount = MAX_ATTACHMENT_COUNT - photoPaths.size();
        if ((docPaths.size() + photoPaths.size()) == MAX_ATTACHMENT_COUNT) {
            //  Toast.makeText(getActivity(), "Cannot select more than " + MAX_ATTACHMENT_COUNT + " items", Toast.LENGTH_SHORT).show();
        } else {
            FilePickerBuilder.getInstance().setMaxCount(maxcount).setSelectedFiles(docPaths)
                    .setActivityTheme(R.style.FilePickerTheme).
                    pickDocument(Fragment_PostProject.this);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        MainActivityPermissionsDispatcher.onRequestPermissionsResult1(this, requestCode, grantResults);
    }

    public void onOpenFragmentClicked(View view) {
        Intent intent = new Intent(getActivity(), FragmentActivity.class);
        startActivity(intent);
    }

    public void GetProjectDetail(String project_id) {
        try {
            JSONObject jo = new JSONObject();

            jo.put(Constants.ACTION, Constants.Project_DetailBY_ID);
            jo.put("project_id", project_id);

            Utility.ShowLoading(mContext, mContext.getResources().getString(R.string.please_wait));

            rt = new ResponseTask(Constant_Urls.SERVER_URL, jo);
            rt.setListener(new ResponseListener() {
                @Override
                public void onGetPickSuccess(String result) {
                    Utility.HideDialog();
                    try {
                        JSONObject jo = new JSONObject(result);
                        if (jo.getString("success").equals("1")) {
                            ((CustomEditText) rooView.findViewById(R.id.p_title)).setText((jo.getJSONObject("object").getString("project_name")));
                            ((CustomEditText) rooView.findViewById(R.id.project_desc)).setText(jo.getJSONObject("object").getString("description"));
                            ((CustomEditText) rooView.findViewById(R.id.time_duration)).setText(jo.getJSONObject("object").getString("project_duration"));
                            ((CustomEditText) rooView.findViewById(R.id.min_price)).setText(jo.getJSONObject("object").getString("min_budget"));
                            ((CustomEditText) rooView.findViewById(R.id.max_price)).setText(jo.getJSONObject("object").getString("max_budget"));
                            ((CustomEditText) rooView.findViewById(R.id.skills_name)).setText(jo.getJSONObject("object").getString("skill"));
                            ((CustomEditText) rooView.findViewById(R.id.p_type)).setText(jo.getJSONObject("object").getString("project_type"));
                            ((CustomEditText) rooView.findViewById(R.id.hours_of_work)).setText(jo.getJSONObject("object").getString("number_of_hours"));

                            if (jo.getJSONObject("object").getString("price_type").equals("fixed ")) {
                                ((CustomRadioButton) rooView.findViewById(R.id.set_fixedprice)).setChecked(true);
                            } else if (jo.getJSONObject("object").getString("price_type").equals("hourly")) {
                                ((CustomRadioButton) rooView.findViewById(R.id.set_hourlyrate)).setChecked(true);
                            }

                            if (jo.getJSONObject("object").getString("currency").equals("")) {

                            } else {
                                for (int i = 0; i < currency_array.length; i++) {
                                    if (jo.getJSONObject("object").getString("currency").equals(currency_array[i])) {
                                        currency.setSelection(i);
                                        break;
                                    }
                                }
                            }

                            if (jo.getJSONObject("object").getString("price_type").equals("")) {


                            } else {
                                for (int i = 0; i < select_time_str_array.length; i++) {
                                    if (jo.getJSONObject("object").getString("price_type").equals(select_time_str_array[i])) {
                                        select_week_or_month.setSelection(i);
                                        break;
                                    }
                                }
                            }

                        }

                    } catch (JSONException j) {
                        j.printStackTrace();
                    }

                }
            });
        } catch (JSONException j) {
            j.printStackTrace();
        }
        rt.execute();
    }

    public class PostProjectTask extends AsyncTask<String, String, String> {

        /*https://infograins.com/INFO01/homesplus/api/api.php?action=post_project&
        user_id=99&project_type=test123&project_name= Name New&skill=php,java&project_description=This is he project&currency=USD
        &min_budget=1000&max_budget=2000&project_duration=30&freelancer_type=1&price_type=hourly or fixed&hours=12&
        hours_work_in=week or month&is_sure=1*/

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            Utility.ShowLoading(mContext, mContext.getResources().getString(R.string.please_wait));
        }

        @Override
        protected String doInBackground(String... params) {
            System.out.println("file_Paths_DURING " + filePaths);
            try {
                MultipartUtility multipart = new MultipartUtility(Constant_Urls.SERVER_URL, "UTF-8");
                multipart.addFormField(Constants.ACTION, params[13]);
                multipart.addFormField("user_id", Utility.getSharedPreferences(mContext, Constants.USERID));
                multipart.addFormField("project_type", params[0]);
                multipart.addFormField("project_name", params[1]);
                multipart.addFormField("skill", params[2]);
                multipart.addFormField("project_description", params[3]);
                multipart.addFormField("currency", params[4]);
                multipart.addFormField("min_budget", params[5]);
                multipart.addFormField("max_budget", params[6]);
                multipart.addFormField("project_duration", params[7]);
                multipart.addFormField("freelancer_type", params[8]);
                multipart.addFormField("price_type", params[9]);
                multipart.addFormField("hours", params[10]);
                multipart.addFormField("hours_work_in", params[11]);
                multipart.addFormField("is_sure", params[12]);
                if (params[13].equals(Constants.UPDATE_PROJECT)) {
                    multipart.addFormField("id", params[14]);
                }
                multipart.addFormField("hire_to", params[15]);
                if ((filePaths.size() > 0)) {
                    for (int i = 0; i < filePaths.size(); i++) {
                        if (!filePaths.get(i).equals("") && filePaths.get(i) != null) {
                            multipart.addFilePart("document[]", new File(filePaths.get(i)));
                            System.out.println("sourceFile[i] " + new File(filePaths.get(i)));

                        }
                    }
                }

                return multipart.finish();

            } catch (Exception e) {
                e.printStackTrace();
                return "Server Not Responding !";
            }

        }

        @Override
        protected void onPostExecute(String result) {
            Utility.HideDialog();
            System.out.println("Server result ====> " + result);
            if (result == null) {
                Utility.ShowToastMessage(mContext, "Server not responding");
            } else {
                try {
                    JSONObject jobj = new JSONObject(result);
                    if (jobj.get(Constants.RESULT_KEY).equals("1")) {
                        Utility.showToast(mContext, "Your project is posted successfully");
                        Intent i = new Intent(mContext, PostSponsored_Activity.class);
                        Bundle b = new Bundle();
                        b.putString("SPONSOR_RESULT", jobj.get("object").toString());
                        i.putExtras(b);
                        startActivity(i);

                       // ((MainActivity) mContext).callSelectedFragment(Constants.TABFRAGMENT, "My Project");

                    } else {
                        Utility.showCroutonWarn(jobj.getString(Constants.SERVER_MSG), getActivity());
                    }
                } catch (JSONException e) {
                    Utility.showCroutonWarn(getResources().getString(R.string.server_fail), getActivity());
                }
            }
            super.onPostExecute(result);
        }

    }


}
