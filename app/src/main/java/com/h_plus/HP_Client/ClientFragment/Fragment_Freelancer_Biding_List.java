package com.h_plus.HP_Client.ClientFragment;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.h_plus.R;
import com.h_plus.ServerInteraction.ResponseListener;
import com.h_plus.ServerInteraction.ResponseTask;
import com.h_plus.useradapter.FreeLencerBidingAdapter;
import com.h_plus.usertabs.utility.Constant_Urls;
import com.h_plus.usertabs.utility.Constants;
import com.h_plus.usertabs.utility.Utility;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.LinkedList;


/**
 * Created by Infograins on 12/10/2016.
 */

public class Fragment_Freelancer_Biding_List extends Fragment {

    Context mContext;
    View rootView;
    ResponseTask rt;
    LinkedList<JSONObject> freelancer_arraylist = new LinkedList<>();
    FreeLencerBidingAdapter adapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_freelist_biding_list, container, false);
        mContext = getActivity();

        if (Utility.isConnectingToInternet(mContext)) {
            GetFreeLancerBid_List();
        } else {

            Utility.showPromptDlgForSuccess(mContext, "please check your network connection", "");
        }

        return rootView;
    }

    public void GetFreeLancerBid_List() {
        try {
            JSONObject jo = new JSONObject();
            jo.put(Constants.ACTION, Constants.BIDLISTBYPROJECTID);
            // jo.put("project_id", Utility.getSharedPreferences(mContext, Constants.PROJECTID));
            jo.put("project_id", Utility.getSharedPreferences(mContext, Constants.PROJECT_ID));

            // Utility.ShowLoading(mContext, mContext.getResources().getString(R.string.please_wait));
            rt = new ResponseTask(Constant_Urls.SERVER_URL, jo);
            rt.setListener(new ResponseListener() {
                               @Override
                               public void onGetPickSuccess(String result) {
                                   // Utility.HideDialog();
                                   if (result == null) {
                                       Utility.ShowToastMessage(mContext, "sever not responding");

                                   } else {
                                       try {
                                           JSONObject jsonObject = new JSONObject(result);
                                           if (jsonObject.getString("success").equals("1")) {
                                               JSONArray jsonArray = jsonObject.getJSONArray("object");
                                               freelancer_arraylist = new LinkedList<JSONObject>();
                                               Log.e("MYPROJECT", "result ===============> " + jsonArray.toString());
                                               for (int i = 0; i < jsonArray.length(); i++) {
                                                   freelancer_arraylist.add(jsonArray.getJSONObject(i));
                                               }
                                               adapter = new FreeLencerBidingAdapter(mContext, R.layout.hire_adapter_freelancer_bid_list, freelancer_arraylist);
                                               ((ListView) rootView.findViewById(R.id.freelancer_bid_list)).setAdapter(adapter);

                                           } else if( jsonObject.getString("msg").equals("Already sent request")){
                                                Utility.ShowToastMessage(mContext, "You already hire this worker");

                                           }
                                            if(jsonObject.getString("msg").equals("No records Found !"));{

                                           }
                                       } catch (JSONException e) {
                                           e.printStackTrace();
                                       }
                                   }
                               }
                           }
            );
        } catch (JSONException e) {
            e.printStackTrace();
        }
        rt.execute();
    }
}
