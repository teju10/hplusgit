package com.h_plus.HP_Client;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.dpizarro.autolabel.library.AutoLabelUI;
import com.dpizarro.autolabel.library.AutoLabelUISettings;
import com.h_plus.HP_Service_Provider.adapter.Adapter_ExperienceDetailView;
import com.h_plus.R;
import com.h_plus.RegistrationSection.MainActivity;
import com.h_plus.ServerInteraction.ResponseListener;
import com.h_plus.ServerInteraction.ResponseTask;
import com.h_plus.customwidget.CircleImageView;
import com.h_plus.customwidget.CustomButton;
import com.h_plus.customwidget.CustomTextView;
import com.h_plus.usertabs.utility.Constant_Urls;
import com.h_plus.usertabs.utility.Constants;
import com.h_plus.usertabs.utility.ExpandableHeightListView;
import com.h_plus.usertabs.utility.ExpandableList_Helper;
import com.h_plus.usertabs.utility.Utility;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;


/**
 * Created by Infograins on 1/11/2017.
 */

public class Hire_ShowSP_PortfolioActivity extends AppCompatActivity {

    Context mContext;
    RecyclerView hire_horizontal_certificatelist, hire_horizontal_portfoliolist;
    CertificateHorizontalListAdapter horizontalListAdapter;
    PortfolioHorizontalListAdapter portfolioHorizontalListAdapter;
    ResponseTask rt;
    CustomTextView hire_spname, hire_address, hire_joindate;
    ImageView hire_sp_email_veri, hire_sp_phone_veri, hire_sp_payment_veri, show_port_flag;
    AutoLabelUI Show_skillsAutoLabelUI;
    String ProjectId, FromUserid;
    Bundle b;
    ExpandableHeightListView experiencelist;
    Adapter_ExperienceDetailView adapter_viewexp;
    ArrayList<JSONObject> exp_arralst = new ArrayList<>();
    ArrayList<JSONObject> certified_arraylst = new ArrayList<>();
    ArrayList<JSONObject> horiportfolio_arraylst = new ArrayList<>();

    @Override

    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.hire_activity_showportfolio);
        mContext = this;

        setSupportActionBar((Toolbar) findViewById(R.id.h_toolbar));
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        ((CustomTextView) findViewById(R.id.toolbar_title)).setText("Profile");

        hire_horizontal_certificatelist = (RecyclerView) findViewById(R.id.hire_horizontal_certificatelist);
        hire_horizontal_portfoliolist = (RecyclerView) findViewById(R.id.hire_horizontal_portfoliolist);

        LinearLayoutManager horizontalLayoutManagaer1 = new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false);
        hire_horizontal_certificatelist.setLayoutManager(horizontalLayoutManagaer1);

        LinearLayoutManager horizontalLayoutManagaer = new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false);
        hire_horizontal_portfoliolist.setLayoutManager(horizontalLayoutManagaer);

        b = new Bundle();
        b = getIntent().getExtras();
        ProjectId = b.getString("PROJECTID");
        FromUserid = b.getString("FROM_USERID");
        Utility.setSharedPreference(mContext, Constants.SPID, FromUserid);
        Find();
        Init();
    }

    public void Find() {
        hire_spname = (CustomTextView) findViewById(R.id.hire_spname);
        hire_address = (CustomTextView) findViewById(R.id.hire_address);
        hire_joindate = (CustomTextView) findViewById(R.id.hire_joindate);
        hire_spname = (CustomTextView) findViewById(R.id.hire_spname);

        hire_sp_payment_veri = (ImageView) findViewById(R.id.hire_sp_payment_veri);
        hire_sp_phone_veri = (ImageView) findViewById(R.id.hire_sp_phone_veri);
        hire_sp_email_veri = (ImageView) findViewById(R.id.hire_sp_email_veri);
        show_port_flag = (ImageView) findViewById(R.id.show_port_flag);

        Show_skillsAutoLabelUI = (AutoLabelUI) findViewById(R.id.Show_skillsAutoLabelUI);
        AutoLabelUISettings autoLabelUISettings = new AutoLabelUISettings.Builder()
                .withIconCross(R.drawable.cross)
                .withBackgroundResource(R.drawable.curve_orange_bg)
                .withLabelsClickables(false)
                .withShowCross(false)
                .withTextColor(R.color.white)
                .withTextSize(R.dimen.label_title_size)
                .withLabelPadding(R.dimen._5sdp)
                .build();

        Show_skillsAutoLabelUI.setSettings(autoLabelUISettings);
        experiencelist = (ExpandableHeightListView) findViewById(R.id.experiencelist);
    }

    public void Init() {
        if (Utility.isConnectingToInternet(mContext)) {
            ShowPortfolioatHireSideTask();
        } else {
            Utility.showPromptDlgForSuccess(mContext, "", "please check your network connection");
        }

        findViewById(R.id.hire_sp_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(mContext, MainActivity.class).putExtra("FOR", 2));
                finish();
            }
        });

    }

    public void ShowPortfolioatHireSideTask() {

        try {
            final JSONObject jk = new JSONObject();
            jk.put(Constants.ACTION, Constants.ShowPortfolioatHireSideTask);
            jk.put("user_id", FromUserid);
            Utility.ShowLoading(mContext, getResources().getString(R.string.please_wait));

            rt = new ResponseTask(Constant_Urls.SERVER_URL, jk);
            rt.setListener(new ResponseListener() {
                @Override
                public void onGetPickSuccess(String result) {
                    Utility.HideDialog();
                    try {
                        JSONObject skills = new JSONObject(result);
                        if (result == null) {
                            Utility.showToast(mContext, "server not responding");
                        } else if (skills.getString("success").equals("1")) {

                            JSONObject userdetail = skills.getJSONObject("object");
                            if (userdetail.getString("is_awarded").equals("1")) {
                                ((CustomButton) findViewById(R.id.hire_sp_btn)).setVisibility(View.GONE);
                            }
                            hire_spname.setText(userdetail.getString("name"));
                            hire_address.setText(userdetail.getString("address"));
                            Glide.with(mContext).load(userdetail.getString("flag_url")).into(show_port_flag);
                            Glide.with(mContext).load(userdetail.getString("image")).into((CircleImageView) findViewById(R.id.hire_spimage));

                            String datesplit[] = userdetail.getString("joining_date").split(" ");
                            String date = datesplit[0];
                            hire_joindate.setText(date);
                            JSONArray portfolio_jsonArray = userdetail.getJSONArray("skill");

                            for (int i = 0; i < portfolio_jsonArray.length(); i++) {
                                JSONObject port_json = portfolio_jsonArray.getJSONObject(i);
                                Show_skillsAutoLabelUI.addLabel(port_json.getString("name"));
                            }
                            JSONArray experience_jsonArray = userdetail.getJSONArray("experience");

                            for (int i = 0; i < experience_jsonArray.length(); i++) {
                                JSONObject exp_json = experience_jsonArray.getJSONObject(i);
                                exp_arralst.add(exp_json);
                            }
                            adapter_viewexp = new Adapter_ExperienceDetailView(mContext, R.layout.sp_adapter_show_experience_list, exp_arralst);
                            experiencelist.setAdapter(adapter_viewexp);
                            ExpandableList_Helper.getListViewSize(experiencelist);

                            horiportfolio_arraylst = new ArrayList<JSONObject>();
                            JSONArray horiportfolio_jsonArray = userdetail.getJSONArray("portfolio");
                            for (int i = 0; i < horiportfolio_jsonArray.length(); i++) {
                                horiportfolio_arraylst.add(horiportfolio_jsonArray.getJSONObject(i));
                            }
                            portfolioHorizontalListAdapter = new PortfolioHorizontalListAdapter(mContext, R.layout.horizontalimagelist_adapter, horiportfolio_arraylst);
                            hire_horizontal_portfoliolist.setAdapter(portfolioHorizontalListAdapter);

                            JSONArray certificate_jsonArray = userdetail.getJSONArray("certificate");
                            for (int i = 0; i < certificate_jsonArray.length(); i++) {
                                certified_arraylst.add(certificate_jsonArray.getJSONObject(i));
                            }
                            horizontalListAdapter = new CertificateHorizontalListAdapter(mContext, R.layout.horizontalimagelist_adapter, certified_arraylst);
                            hire_horizontal_certificatelist.setAdapter(horizontalListAdapter);
                        } else {
                            Utility.showToast(mContext, skills.getString("msg"));
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            });
            rt.execute();
        } catch (JSONException j) {
            j.printStackTrace();
        }

    }

    public void HireServiceProviderRequest() {
       /* try {
            JSONObject jo = new JSONObject();
            jo.put(Constants.ACTION, Constants.HIRE_SP_BIDINGREQUEST);
            jo.put("from_user", Utility.getSharedPreferences(mContext, Constants.USERID));
            jo.put("project_id", ProjectId);
            jo.put("to_user", FromUserid);

            Utility.ShowLoading(mContext, mContext.getResources().getString(R.string.please_wait));
            rt = new ResponseTask(Constant_Urls.SERVER_URL, jo);
            rt.setListener(new ResponseListener() {
                @Override
                public void onGetPickSuccess(String result) {
                    Utility.HideDialog();
                    try {
                        JSONObject j = new JSONObject(result);
                        if (j.getString("msg").equals("1")) {
                            Utility.showToast(mContext, "Hire Successfully");
                        } else {
                            Utility.showToast(mContext, j.getString("msg"));
                        }


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }

            });
            rt.execute();
        } catch (JSONException j) {
            j.printStackTrace();
        }*/
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menu) {

        switch (menu.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(menu);

        }
    }

    class CertificateHorizontalListAdapter extends RecyclerView.Adapter<CertificateHorizontalListAdapter.ViewHolder> {
        Dialog imageview_dia;
        Context mContext;
        int res;
        ArrayList<JSONObject> imageArrayList;

        public CertificateHorizontalListAdapter(Context context, int resourse, ArrayList<JSONObject> imageList) {
            this.mContext = context;
            this.res = resourse;
            this.imageArrayList = imageList;
        }

        @Override
        public CertificateHorizontalListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View convertView = LayoutInflater.from(parent.getContext()).inflate(res, parent, false);
            return new CertificateHorizontalListAdapter.ViewHolder(convertView);
        }

        @Override
        public void onBindViewHolder(final CertificateHorizontalListAdapter.ViewHolder holder, int position) {
            try {
                JSONObject j = imageArrayList.get(position);
                holder.certified_name.setText(j.getString("title"));
                Glide.with(mContext)
                        .load(j.getString("file"))
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .listener(new RequestListener<String, GlideDrawable>() {
                            @Override
                            public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                                return false;
                            }

                            @Override
                            public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                                (holder.hori_progress_bar).setVisibility(View.GONE);
                                return false;
                            }
                        })
                        .into(holder.hori_certificate_image);
                if (Utility.getSharedPreferences(mContext, Constants.USER_TYPE).equals("1")) {
                    holder.cancel_img.setVisibility(View.GONE);
                }
            } catch (JSONException j) {
                j.printStackTrace();
            }
            holder.hori_certificate_image.setOnClickListener(new onclick(position));
        }


        @Override
        public int getItemCount() {
            return imageArrayList.size();
        }

        class onclick implements View.OnClickListener {
            int position;

            public onclick(int pos) {
                position = pos;
            }

            @Override
            public void onClick(View view) {
                try {


                } catch (Exception g) {
                    g.printStackTrace();
                }
            }
        }

        public void ImageView() {
            imageview_dia = new Dialog(mContext);
            imageview_dia.requestWindowFeature(Window.FEATURE_NO_TITLE);
            imageview_dia.setCancelable(false);
            imageview_dia.setCanceledOnTouchOutside(true);
            imageview_dia.setContentView(R.layout.hire_dialog_certificate_imgview);
            imageview_dia.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            WebView load_img_wv;
            load_img_wv = ((WebView) imageview_dia.findViewById(R.id.load_img_wv));
            WebSettings settings = load_img_wv.getSettings();
            settings.setBuiltInZoomControls(true);
            settings.setSupportZoom(true);
            load_img_wv.loadUrl("https://s-media-cache-ak0.pinimg.com/736x/ce/5f/53/ce5f53437e291c48705428721406985c.jpg");

            imageview_dia.show();
        }

        class ViewHolder extends RecyclerView.ViewHolder {

            ImageView hori_certificate_image, cancel_img;
            ProgressBar hori_progress_bar;
            TextView certified_name;

            public ViewHolder(View itemView) {
                super(itemView);
                hori_certificate_image = (ImageView) itemView.findViewById(R.id.product_img);
                cancel_img = (ImageView) itemView.findViewById(R.id.cancel_img);
                certified_name = (TextView) itemView.findViewById(R.id.certified_name);
                hori_progress_bar = (ProgressBar) itemView.findViewById(R.id.hori_progress_bar);
            }
        }
    }

    class PortfolioHorizontalListAdapter extends RecyclerView.Adapter<PortfolioHorizontalListAdapter.ViewHolder> {

        Context mContext;
        int res;
        ArrayList<JSONObject> hori_portimageArrayList;

        public PortfolioHorizontalListAdapter(Context context, int resourse, ArrayList<JSONObject> imageList) {
            this.mContext = context;
            this.res = resourse;
            this.hori_portimageArrayList = imageList;
        }

        @Override
        public PortfolioHorizontalListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View convertView = LayoutInflater.from(parent.getContext()).inflate(res, parent, false);
            return new PortfolioHorizontalListAdapter.ViewHolder(convertView);
        }

        @Override
        public void onBindViewHolder(final PortfolioHorizontalListAdapter.ViewHolder holder, int position) {
            try {
                JSONObject j = hori_portimageArrayList.get(position);
                Glide.with(mContext)
                        .load(j.getString("file"))
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .listener(new RequestListener<String, GlideDrawable>() {
                            @Override
                            public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                                return false;
                            }

                            @Override
                            public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                                (holder.hori_progress_bar).setVisibility(View.GONE);
                                return false;
                            }
                        })
                        .into(holder.hori_certificate_image);
                if (Utility.getSharedPreferences(mContext, Constants.USER_TYPE).equals("1")) {
                    holder.cancel_img.setVisibility(View.GONE);
                }

            } catch (JSONException j) {
                j.printStackTrace();
            }
            holder.hori_certificate_image.setOnClickListener(new onclick(position));
        }


        @Override
        public int getItemCount() {
            return hori_portimageArrayList.size();
        }


        class onclick implements View.OnClickListener {
            int position;

            public onclick(int pos) {
                position = pos;
            }

            @Override
            public void onClick(View view) {
                try {
                    Intent i = new Intent(mContext, Hire_ViewPortfolioDetail_Activity.class);
                    Bundle b = new Bundle();
                    b.putString("port_jarray", hori_portimageArrayList.get(position).toString());
                    i.putExtras(b);
                    startActivity(i);
                } catch (Exception g) {
                    g.printStackTrace();
                }
            }
        }

        class ViewHolder extends RecyclerView.ViewHolder {

            ImageView hori_certificate_image, cancel_img;
            ProgressBar hori_progress_bar;
            TextView certified_name;

            public ViewHolder(View itemView) {
                super(itemView);
                hori_certificate_image = (ImageView) itemView.findViewById(R.id.product_img);
                cancel_img = (ImageView) itemView.findViewById(R.id.cancel_img);
                certified_name = (TextView) itemView.findViewById(R.id.certified_name);
                hori_progress_bar = (ProgressBar) itemView.findViewById(R.id.hori_progress_bar);
            }
        }
    }
}
